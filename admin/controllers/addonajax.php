<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: Rainbow PHP Framework v1.0
 * @copyright © 2015 ProThemes.Biz
 *
 */

//AJAX ONLY 


if(isset($_GET['manageBlogPosts'])){
    
// DB table to use
$table = 'blog_content';

// Table's primary key
$primaryKey = 'id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
	array( 'db' => 'id',   'dt' => 0 ),
	array( 'db' => 'post_title', 'dt' => 1 ),
	array( 'db' => 'category',  'dt' => 2 ),
	array( 'db' => 'date',  'dt' => 3),
	array( 'db' => 'post_enable',   'dt' => 4 ),
	array( 'db' => 'pageview',   'dt' => 5 ),
	array( 'db' => 'post_url',   'dt' => 6 )
);

$columns2 = array(
	array( 'db' => 'post_title', 'dt' => 0 ),
	array( 'db' => 'category',  'dt' => 1 ),
	array( 'db' => 'date',  'dt' => 2),
	array( 'db' => 'pageview',   'dt' => 3 ),
	array( 'db' => 'post_enable',   'dt' => 4 ),
	array( 'db' => 'action',   'dt' => 5)
);


// SQL server connection information
$sql_details = array(
	'user' => $dbUser,
	'pass' => $dbPass,
	'db'   => $dbName,
	'host' => $dbHost
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

echo json_encode(
	SSPBLOG::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $columns2 )
);
 die();   
}

die();
?>