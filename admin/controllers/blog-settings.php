<?php

defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
* @author Balaji
* @name: A to Z SEO Tools
* @copyright © 2015 ProThemes.Biz
*
*/

$fullLayout = 1;
$p_title = "Blog Settings";

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
        $blog_status = escapeTrim($con, $_POST['blog_status']);
        $maximum_posts = escapeTrim($con, $_POST['maximum_posts']);
        $meta_des = escapeTrim($con, $_POST['meta_des']);
        $posted_by = escapeTrim($con, $_POST['posted_by']);
        $enable_comments = escapeTrim($con, $_POST['enable_comments']);
        $truncate = escapeTrim($con, $_POST['truncate']);
        $meta_tags = escapeTrim($con, $_POST['meta_tags']);
        $no_image = escapeTrim($con, $_POST['no_image']);
        $disqus_id = escapeTrim($con, $_POST['disqus_id']);

        $query = "UPDATE blog SET meta_des='$meta_des', meta_tags='$meta_tags', maximum_posts='$maximum_posts', truncate='$truncate', no_image='$no_image', posted_by='$posted_by', show_comment='$enable_comments', discuss_id='$disqus_id', blog_enable='$blog_status' WHERE id='1'";
    
        if (!mysqli_query($con, $query))
        {
            $msg = '<div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                    <b>Alert!</b> Something Went Wrong!
                    </div>';
        } else
        {
            $msg = '<div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                    <b>Alert!</b> Blog Settings saved successfully!
                    </div>
                    ';
        }
}

$sql = "SELECT * FROM blog where id='1'";
$result = mysqli_query($con, $sql);


    while ($row = mysqli_fetch_array($result))
    {
        $meta_des = $row['meta_des'];
        $meta_tags = $row['meta_tags'];
        $maximum_posts = $row['maximum_posts'];
        $truncate = $row['truncate'];
        $no_image = $row['no_image'];
        $posted_by = $row['posted_by'];
        $enable_comments = filter_var($row['show_comment'], FILTER_VALIDATE_BOOLEAN);
        $blog_status = filter_var($row['blog_enable'], FILTER_VALIDATE_BOOLEAN);
        $disqus_id = $row['discuss_id'];
    }
    
?>