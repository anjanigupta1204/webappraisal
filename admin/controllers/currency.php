<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright © 2016 ProThemes.Biz
 *
 */
 
$fullLayout = 1;
$p_title = 'Manage Currencies';
$newPage = false;
$headTitle = 'Currencies';
$curDataList = getCurrencyList();

if(isset($_GET['delete'])) {
    
    $currency_id = escapeTrim($con, $_GET['id']);
    
    $query = "DELETE FROM premium_currency WHERE id='$currency_id'";
    mysqli_query($con, $query);

    if (mysqli_errno($con)) {
        $msg = '<div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                        <b>Alert!</b> ' . mysqli_error($con) . '
                                    </div>';
    } else {
        $msg = '
        <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                        <b>Alert!</b> Deleted successfully
                                    </div>';
    }
}

if(isset($_GET['new-rule'])) {
    $newPage = true;
    $headTitle = 'New Currency Rule';
}

if(isset($_GET['edit'])) {
    $newPage = true;
    $headTitle = 'Edit Currency Rule';
    
    $currency_id = escapeTrim($con, $_REQUEST['id']);
    $query = mysqli_query($con, "SELECT * FROM premium_currency WHERE id='$currency_id'");
    extract(mysqli_fetch_array($query));
}

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    
    if(isset($_POST['currencyEnable'])){
        
        $default_currency = raino_trim($_POST['default_currency']);
        
        $query = "UPDATE order_settings SET currency_type='$default_currency' WHERE id='1'";
    
        if (!mysqli_query($con, $query)) {
            $msg = '<div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                    <b>Alert!</b> Something Went Wrong!
                    </div>';
        } else {
            $msg = '<div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                    <b>Alert!</b> Settings saved successfully!
                    </div>
                    ';
        }
        
    }
    
    if(isset($_POST['editCurrency'])){
        $currency_name = raino_trim($_POST['currency_name']);
        $currency_rate = raino_trim($_POST['currency_rate']);
        $currency_code = raino_trim($_POST['currency_code']);
        
        $query = "UPDATE premium_currency SET currency_name='$currency_name', currency_code='$currency_code', currency_rate='$currency_rate' WHERE id='$currency_id'";
    
        if (!mysqli_query($con, $query)) {
            $msg = '<div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                    <b>Alert!</b> Something Went Wrong!
                    </div>';
        } else {
            $msg = '<div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                    <b>Alert!</b> Currency Settings saved successfully!
                    </div>
                    ';
        }        
    }
    
    if(isset($_POST['newCurrency'])){
        
        $currency_name = raino_trim($_POST['currency_name']);
        $currency_rate = raino_trim($_POST['currency_rate']);
        $currency_code = raino_trim($_POST['currency_code']);
        
        $query = "INSERT INTO premium_currency (currency_name,currency_code,currency_rate) VALUES ('$currency_name', '$currency_code', '$currency_rate')";
                            
        if (!mysqli_query($con, $query)) {
            $msg = '<div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                    <b>Alert!</b> Something Went Wrong!
                    </div>';
        } else {
            $msg = '<div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                    <b>Alert!</b> New Currency Rule added successfully!
                    </div>
                    <meta http-equiv="refresh" content="1,URL=?route=currency">
                    ';
        }        
    }
}

$defaultCurrency = defaultCurrency($con);
$defaultCurrencyData = '';

$query =  "SELECT * FROM premium_currency";
$result = mysqli_query($con,$query);
        
while($row = mysqli_fetch_array($result)){

    $currencyName = $row["currency_name"];
    $currencyCode = $row["currency_code"];
    $currencyRate = $row["currency_rate"];
    $currencySymbol = getCurrencySymbol($currencyCode);
    $currencySymbol = $currencySymbol[0];
    
    $editLink = '<a class="btn btn-sm btn-primary" href="/admin/?route=currency&id='.$row["id"].'&edit">Edit</a>';
    $deleteLink = '<a class="btn btn-sm btn-danger" href="/admin/?route=currency&id='.$row["id"].'&delete">Delete</a>';
    
    $currencyList[] = array($currencyName,$currencyCode,$currencySymbol,$currencyRate,$editLink,$deleteLink);
    
    if($defaultCurrency == $currencyCode){
        $defaultCurrencyData .= '<option value="'.$currencyCode.'" selected="">'.$currencyName.' - '.$currencyCode.'</option>';
    }else{
        $defaultCurrencyData .= '<option value="'.$currencyCode.'">'.$currencyName.' - '.$currencyCode.'</option>';
    }
}
?>