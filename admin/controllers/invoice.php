<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright © 2016 ProThemes.Biz
 *
 */
 
$fullLayout = 1;
$p_title = 'Invoice';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $invoice_prefix = escapeTrim($con, $_POST['invoice_prefix']);
    $company_add = escapeTrim($con, $_POST['company_add']);
    $invoice_footer = escapeTrim($con, $_POST['invoice_footer']);
    $invoiceFilePath = $invoiceFileData = '';
     
    if($_FILES["invoiceUpload"]["name"] != ''){
     
        $target_dir = ROOT_DIR."uploads/";
        $target_filename = basename($_FILES["invoiceUpload"]["name"]);
        $uploadSs = 1;
        $check = getimagesize($_FILES["invoiceUpload"]["tmp_name"]);

        // Check it is a image
        if ($check !== false) {
            // Check if file already exists
            $target_filename = unqFile($target_dir,$target_filename);
            $target_file = $target_dir . $target_filename;
            $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
            // Check file size
            if ($_FILES["invoiceUpload"]["size"] > 500000) {
                $imgMsg =  '<div class="alert alert-danger alert-dismissable alert-premium">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
         <strong>Alert!</strong> Sorry, your file is too large.
         </div>';
                $uploadSs = 0;
            } else {
                // Allow certain file formats
                if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType !=
                    "jpeg" && $imageFileType != "gif") {
                    $imgMsg =  '<div class="alert alert-danger alert-dismissable alert-premium">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
         <strong>Alert!</strong> Sorry, only JPG, JPEG, PNG & GIF files are allowed.
         </div>';
                    $uploadSs = 0;
                }
            }
    
            // Check if $uploadSs is set to 0 by an error
            if (!$uploadSs == 0) {
                if (move_uploaded_file($_FILES["invoiceUpload"]["tmp_name"], $target_file)) {
                     //Uploaded
                     $invoiceFilePath = "/uploads/$target_filename";
                     $invoiceFileData = ", invoice_logo='$invoiceFilePath'";
                } else {
                    $imgMsg =  '<div class="alert alert-danger alert-dismissable alert-premium">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
         <strong>Alert!</strong> Sorry, there was an error uploading your file.
         </div>';
                }
            }
    
        } else {
            $imgMsg =  '<div class="alert alert-danger alert-dismissable alert-premium">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
         <strong>Alert!</strong> File is not an image.
         </div>';
        }
    }
    
    $query = "UPDATE order_settings SET invoice_prefix='$invoice_prefix', company_add='$company_add', invoice_footer='$invoice_footer'$invoiceFileData WHERE id='1'";

    if (!mysqli_query($con, $query)) {
        $msg = '<div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                <b>Alert!</b> Something Went Wrong!
                </div>';
    } else {
        $msg = '<div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                <b>Alert!</b> Invoice Settings saved successfully!
                </div>
                ';
    }
        if(isset($imgMsg))
            $msg = $imgMsg;
}

extract(orderSettings($con));

?>