<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright © 2015 ProThemes.Biz
 *
 */
 
$fullLayout = 1;
$p_title = "Manage Blog Posts";

if(isset($_GET['postSuccess'])){
            $msg = '<div class="alert alert-success alert-dismissable">
            <i class="fa fa-check"></i>
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
            <b>Alert!</b> New post added successfully!
            </div>
            ';
}

if(isset($_GET['editSuccess'])){
            $msg = '<div class="alert alert-success alert-dismissable">
            <i class="fa fa-check"></i>
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
            <b>Alert!</b> Edited post saved successfully!
            </div>
            ';   
}

if(isset($_GET['status'])){
    $post_status = escapeTrim($con, $_GET['status']);
    $post_id = escapeTrim($con, $_GET['id']);

    if($post_status == "2")
    $post_enable = "off";  
    else
    $post_enable = "on";  
    
    $query = "UPDATE blog_content SET post_enable='$post_enable' WHERE id='$post_id'";
    mysqli_query($con, $query);

    if (mysqli_errno($con))
    {
        $msg = '<div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                        <b>Alert!</b> ' . mysqli_error($con) . '
                                    </div>';
    } else
    {
        $msg = '
        <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                        <b>Alert!</b> Post settings saved successfully
                                    </div>';
    }
}


if (isset($_GET{'delete'}))
{
    $delete = raino_trim($_GET['delete']);
    $query = "DELETE FROM blog_content WHERE id=$delete";
    $result = mysqli_query($con, $query);

    if (mysqli_errno($con))
    {
        $msg = '<div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                        <b>Alert!</b> ' . mysqli_error($con) . '
                                    </div>';
    } else
    {
        $msg = '
        <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                        <b>Alert!</b> Post deleted from database successfully.
                                    </div>';
    }
}

?>