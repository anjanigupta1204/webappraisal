<?php

defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
* @author Balaji
* @name: A to Z SEO Tools
* @copyright © 2016 ProThemes.Biz
*
*/

$fullLayout = 1;
$p_title = "Create New Order";
$neworder = true;
$orderSuc = false;
        
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $username = raino_trim($_POST['username']);
    $planID  = raino_trim($_POST['planID']);
    $userID = getUserID($username,$con);
    $premiumUserID = getPremiumUserID($username,$con);
    $billType = raino_trim($_POST['billType']);
    $paymentVal = raino_trim($_POST['payType']);
    $order_status = raino_trim($_POST['orderStatus']);
    $pay_status = raino_trim($_POST['payStatus']);
    $totalAmount = raino_trim($_POST['total']);
    $amWithTax = raino_trim($_POST['amount']);
    $nowDate = date('m/d/Y h:i:sA'); 
    $ip = getUserIP();
    $recType = raino_trim($_POST['recType']);
    $currencyType = defaultCurrency($con);
    $plan_name = raino_trim($_POST['plan']);
    $taxDataStr = serialize(array());
    $recCon = serialize(array('0','0','0','user','0'));
    
    if ($paymentVal != null && $totalAmount != null && $amWithTax != null && $billType != null && $planID != null){
                            
    $ordrNum = getInvoicePrefix($con);
    //Empty Order Comments
    $orderComments = base64_encode(serialize(array()));
    //Create the Order
    $query = "INSERT INTO premium_orders (plan_id, user_id, premium_user_id, invoice_prefix, billing_type, payment_type, status, amount, date, ip_address, rec_type, username, amount_tax, currency_type, plan_name, payment_status, tax_data, rec_data, order_comments, order_log) VALUES ('$planID', '$userID', '$premiumUserID', '$ordrNum', '$billType', '$paymentVal', '$order_status', '$totalAmount', '$nowDate', '$ip', '$recType', '$username', '$amWithTax', '$currencyType', '$plan_name', '$pay_status', '$taxDataStr', '$recCon', '$orderComments', '$orderComments')"; //New Premium User
    if (mysqli_query($con, $query)) {
        $orderID = mysqli_insert_id($con);
        $orderSuc = true;
        $msg = '<div class="alert alert-success">
        <i class="fa fa-check"></i>
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
        <b>Alert!</b> Order created successfully!
        </div>
        ';
    }else{
        $msg = '<div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
        <b>Alert!</b> Something Went Wrong!
        </div>';
        $orderSuc = false;
    }
    }else{
        $msg = '<div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
        <b>Alert!</b> Something Went Wrong!
        </div>';
        $orderSuc = false; 
    }
}
$paymentGatewaysData = '';
$paymentGateways = getPaymentGateways($con);
foreach ($paymentGateways as $paymentGateway){
    $paymentGatewaysData .= '<option value="'.$paymentGateway[2].'">'.$paymentGateway[1].'</option>';
}
?>