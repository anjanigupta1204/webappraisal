<?php

defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
* @author Balaji
* @name: A to Z SEO Tools
* @copyright © 2015 ProThemes.Biz
*
*/

$fullLayout = 1;
$p_title = "New Blog Post";

$post_enable = true;
$allow_comment = true;

//Load Blog Settings
$sql = "SELECT * FROM blog where id='1'";
$result = mysqli_query($con, $sql);

while($row = mysqli_fetch_array($result)) {
    $posted_by = $row['posted_by'];
}

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    if (!isset($_POST['editPage']))
    {
        $post_title = escapeTrim($con, $_POST['post_title']);
        $post_url = escapeTrim($con, $_POST['post_url']);
        $meta_des = escapeTrim($con, $_POST['meta_des']);
        $featured_image = escapeTrim($con, $_POST['thumbnail']);
        $post_enable = escapeTrim($con, $_POST['post_enable']);
        $category = strtolower(escapeTrim($con, $_POST['category']));
        $meta_tags = escapeTrim($con, $_POST['meta_tags']);
        $posted_date = escapeTrim($con, $_POST['posted_date']);
        $post_content = escapeTrim($con, $_POST['post_content']);
        $posted_by = escapeTrim($con, $_POST['posted_by']);
        $allow_comment = escapeTrim($con, $_POST['allow_comment']);
        
        $query = "INSERT INTO blog_content (post_title,post_url,meta_des,featured_image,post_enable,category,meta_tags,date,post_content,posted_by,allow_comment,pageview) VALUES ('$post_title', '$post_url', '$meta_des', '$featured_image', '$post_enable', '$category', '$meta_tags', '$posted_date', '$post_content', '$posted_by', '$allow_comment', '0')";

        if (!mysqli_query($con, $query))
        {
            $msg = '<div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
            <b>Alert!</b> Something Went Wrong!
            </div>';
        } else
        {
        header('Location: /admin/?route=manage-blog&postSuccess',true,302);
        exit();
        }
        
    }else{
        $post_title = escapeTrim($con, $_POST['post_title']);
        $post_url = escapeTrim($con, $_POST['post_url']);
        $meta_des = escapeTrim($con, $_POST['meta_des']);
        $featured_image = escapeTrim($con, $_POST['thumbnail']);
        $post_enable = escapeTrim($con, $_POST['post_enable']);
        $category = strtolower(escapeTrim($con, $_POST['category']));
        $meta_tags = escapeTrim($con, $_POST['meta_tags']);
        $posted_date = escapeTrim($con, $_POST['posted_date']);
        $post_content = escapeTrim($con, $_POST['post_content']);
        $posted_by = escapeTrim($con, $_POST['posted_by']);
        $allow_comment = escapeTrim($con, $_POST['allow_comment']);
        $editID = escapeTrim($con, $_POST['editID']);
        
        $query = "UPDATE blog_content SET post_title='$post_title', post_url= '$post_url', meta_des= '$meta_des',featured_image='$featured_image', post_enable='$post_enable',category='$category',meta_tags='$meta_tags',date='$posted_date',post_content='$post_content',posted_by='$posted_by',allow_comment='$allow_comment' WHERE id='$editID'";
    
        if (!mysqli_query($con, $query))
        {
            $msg = '<div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
            <b>Alert!</b> Something Went Wrong!
            </div>';
        } else
        {
        header('Location: /admin/?route=manage-blog&editSuccess',true,302);
        exit();

        }
    }
}


if(isset($_GET['edit'])){

    $page_id = raino_trim($_GET['edit']);
    $sql = "SELECT * FROM blog_content where id='$page_id'";
    $result = mysqli_query($con, $sql);


    while ($row = mysqli_fetch_array($result))
    {
        $editID = $row['id'];
        $post_title = $row['post_title'];
        $post_url = $row['post_url'];
        $meta_des = $row['meta_des'];
        $meta_tags = $row['meta_tags'];
        $featured_image = $row['featured_image'];
        $post_enable = filter_var($row['post_enable'], FILTER_VALIDATE_BOOLEAN);
        $category = $row['category'];
        $posted_date = $row['date'];
        $post_content = $row['post_content'];
        $posted_by = $row['posted_by'];
        $allow_comment = filter_var($row['allow_comment'], FILTER_VALIDATE_BOOLEAN);
    } 
}
?>