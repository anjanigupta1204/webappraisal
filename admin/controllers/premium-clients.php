<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright © 2016 ProThemes.Biz
 *
 */
 
$fullLayout = 1;
$p_title = 'Premium Clients';
$headTitle = 'Premium Customer List';

if (isset($_GET['delete'])) {
    $user_id = raino_trim($_GET['delete']);
    $premium_id = raino_trim($_GET['id']);
    $query = "DELETE FROM users WHERE id=$user_id";
    
    $result = mysqli_query($con, $query);
    if (mysqli_errno($con))
    {
        $msg = '<div class="alert alert-danger alert-dismissable">
         <strong>Alert!</strong> ' . mysqli_error($con) . '
         </div>';
    } else {
        
        $query = "DELETE FROM premium_users WHERE id=$premium_id";
        $result = mysqli_query($con, $query);
        if (mysqli_errno($con))
        {
            $msg = '<div class="alert alert-danger alert-dismissable">
             <strong>Alert!</strong> ' . mysqli_error($con) . '
             </div>';
        }else{
            $msg = '<div class="alert alert-success alert-dismissable">
             <strong>Alert!</strong> User deleted from database successfully
             </div>';
        }
    }

}

if (isset($_GET['ban'])) {
    $ban_id = raino_trim($_GET['ban']);
    $query = "UPDATE users SET verified='2' WHERE id='$ban_id'";
    $result = mysqli_query($con, $query);
    if (mysqli_errno($con))
    {
        $msg = '<div class="alert alert-danger alert-dismissable">
     <strong>Alert!</strong> ' . mysqli_error($con) . '
     </div>';
    } else
    {
        $msg = '<div class="alert alert-success alert-dismissable">
         <strong>Alert!</strong> User banned successfully
         </div>';
    }

}

if (isset($_GET['unban'])) {
    $ban_id = raino_trim($_GET['unban']);
    $query = "UPDATE users SET verified='1' WHERE id='$ban_id'";
    $result = mysqli_query($con, $query);
    if (mysqli_errno($con))
    {
        $msg = '<div class="alert alert-danger alert-dismissable">
     <strong>Alert!</strong> ' . mysqli_error($con) . '
     </div>';
    } else
    {
        $msg = '<div class="alert alert-success alert-dismissable">
         <strong>Alert!</strong> User un-banned successfully
         </div>';
    }

}

if (isset($_GET['details'])) {
    $headTitle = 'Premium Customer Details';
    $detail_id = raino_trim($_GET['details']);
    $premium_id = raino_trim($_GET['id']);
    $query = "SELECT * FROM users WHERE id='$detail_id'";
    $result = mysqli_query($con, $query);
    while ($row = mysqli_fetch_array($result))
    {
        $user_oauth_uid = $row['oauth_uid'];
        $user_username = $row['username'];
        $user_email_id = $row['email_id'];
        $user_full_name = $row['full_name'];
        $user_platform = Trim($row['platform']);
        $user_verified = $row['verified'];
        $user_date = $row['date'];
        $user_ip = $row['ip'];
    }
    if ($user_oauth_uid == '0') {
        $user_oauth_uid = "None";
    }
    if ($user_verified == '0') {
        $user_verified = "Not verfied user";
    } elseif ($user_verified == '1') {
        $user_verified = "Verfied User / Active";
    } elseif ($user_verified == '2') {
        $user_verified = "Banned User";
    }
    $addInfo = true;
    extract(getPremiumUserInfo($user_username,$con));
}
?>