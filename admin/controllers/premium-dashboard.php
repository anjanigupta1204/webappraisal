<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright � 2016 ProThemes.Biz
 *
 */
 
$fullLayout = 1;
$p_title = "Dashboard";

$orderList = array();
$orderListCount = 0;
$todayData = false;
$premiumDash = true;
$todayDate = date('m/d/Y');
$todayOrderCount = $todaySalesCount = $totalOrders = $totalUsers = $totalActivePlans = $aWaitingActivation = 0;
$sale1 = $sale2 = $sale3 = $sale4 = $sale5 = $sale6 = $sale7 = 0;

$date_raw = date_create(Trim($todayDate));
$year = date_format($date_raw,"Y");
$month = date_format($date_raw,"n");
$day = date_format($date_raw,"j");
$hour = date_format($date_raw,"H");
$min = date_format($date_raw,"i"); 
$sec = date_format($date_raw,"s");  

$date1 = $todayDate;
$date2 = date('m/d/Y', mktime($hour,$min,$sec,$month,$day-'1',$year));
$date3 = date('m/d/Y', mktime($hour,$min,$sec,$month,$day-'2',$year));
$date4 = date('m/d/Y', mktime($hour,$min,$sec,$month,$day-'3',$year));
$date5 = date('m/d/Y', mktime($hour,$min,$sec,$month,$day-'4',$year));
$date6 = date('m/d/Y', mktime($hour,$min,$sec,$month,$day-'5',$year));
$date7 = date('m/d/Y', mktime($hour,$min,$sec,$month,$day-'6',$year));

$result = mysqli_query($con,"SELECT * FROM premium_orders ORDER BY id DESC");

while ($row = mysqli_fetch_array($result)) {
    if($orderListCount == 10 && $todayData)
            break;
    if($orderListCount < 10){
        if($row['payment_status'] != 'pending' && $row['payment_status'] != 'canceled'){
            $currencyType = $row['currency_type'];
            $currencySymbol = getCurrencySymbol($currencyType);    
            $currencySymbol = $currencySymbol[0];
            $row['amount_tax'] = con2money_format($row['amount_tax'],$currencyType);
            $orderList[] = array($row['id'],$row['username'],$row['plan_name'],$row['status'],date('F, j Y', strtotime($row['date'])),$row['amount_tax'],$row['payment_status'],$currencySymbol);
            $orderListCount++;
        }
    }
    
    if(!$todayData){
        if($row['payment_status'] == 'completed'){
            $dbDate = date('m/d/Y', strtotime($row['date']));
            if($todayDate == $dbDate){
                $todayOrderCount++;
                $todaySalesCount = $todaySalesCount + $row['amount_tax'];
            }else{
                $todayData = true;
            }
        }
    }
}

$result = mysqli_query($con,'SELECT COUNT(*) AS id FROM premium_orders');
$totalOrders = mysqli_fetch_array($result);
$totalOrders = $totalOrders['id'];

$result = mysqli_query($con,'SELECT COUNT(*) AS id FROM premium_users');
$totalUsers = mysqli_fetch_array($result);
$totalUsers = $totalUsers['id'];

$result = mysqli_query($con,"SELECT * FROM premium_plans");
while ($row = mysqli_fetch_array($result)) {
    $status = filter_var(Trim($row['status']), FILTER_VALIDATE_BOOLEAN);
    if($status){
        $totalActivePlans++;
    }
}

$result = mysqli_query($con,"SELECT * FROM premium_orders WHERE payment_status='completed' AND status='pending'");
while ($row = mysqli_fetch_array($result)) {
    $aWaitingActivation++;
}

$result = mysqli_query($con,"SELECT * FROM premium_orders ORDER BY id DESC");

while ($row = mysqli_fetch_array($result)) {
    $dbDate = date('m/d/Y', strtotime($row['date']));
    
    if($row['payment_status'] == 'completed'){
        if($date1 == $dbDate){
            $sale1 = $sale1 + $row['amount_tax'];
        }elseif($date2 == $dbDate){
            $sale2 = $sale2 + $row['amount_tax'];
        }elseif($date3 == $dbDate){
            $sale3 = $sale3 + $row['amount_tax'];
        }elseif($date4 == $dbDate){
            $sale4 = $sale4 + $row['amount_tax'];
        }elseif($date5 == $dbDate){
            $sale5 = $sale5 + $row['amount_tax'];
        }elseif($date6 == $dbDate){
            $sale6 = $sale6 + $row['amount_tax'];
        }elseif($date7 == $dbDate){
            $sale7 = $sale7 + $row['amount_tax'];
        }else{
            break;
        }
    }
}
$date1 = date('jS M', strtotime($date1));
$date2 = date('jS M', strtotime($date2));
$date3 = date('jS M', strtotime($date3));
$date4 = date('jS M', strtotime($date4));
$date5 = date('jS M', strtotime($date5));
$date6 = date('jS M', strtotime($date6));
$date7 = date('jS M', strtotime($date7));

$currencyType = defaultCurrency($con);
$currencySymbol = getCurrencySymbol($currencyType);
?>