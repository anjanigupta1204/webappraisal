<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: Rainbow PHP Framework v1.0
 * @copyright � 2015 ProThemes.Biz
 *
 */

//AJAX ONLY 


if(isset($_GET['managePlans'])){
    
// DB table to use
$table = 'premium_plans';

// Table's primary key
$primaryKey = 'id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
	array( 'db' => 'id',   'dt' => 0 ),
	array( 'db' => 'plan_name', 'dt' => 1 ),
	array( 'db' => 'payment_type',  'dt' => 2 ),
	array( 'db' => 'premium_tools',  'dt' => 3),
	array( 'db' => 'allow_pdf',   'dt' => 4 ),
	array( 'db' => 'status',   'dt' => 5 ),
	array( 'db' => 'url',   'dt' => 6 )
);

$columns2 = array(
	array( 'db' => 'title', 'dt' => 0 ),
	array( 'db' => 'payment_type',  'dt' => 1 ),
	array( 'db' => 'premium_tools',  'dt' => 2),
	array( 'db' => 'allow_pdf',   'dt' => 3 ),
	array( 'db' => 'status',   'dt' => 4),
	array( 'db' => 'action',   'dt' => 5)
);


// SQL server connection information
$sql_details = array(
	'user' => $dbUser,
	'pass' => $dbPass,
	'db'   => $dbName,
	'host' => $dbHost
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

echo json_encode(
	SSPPLAN::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $columns2 )
);
 die();   
}

if(isset($_GET['manageInvoices'])){
    
// DB table to use
$table = 'premium_orders';

// Table's primary key
$primaryKey = 'id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
	array( 'db' => 'id',   'dt' => 0 ),
	array( 'db' => 'payment_status', 'dt' => 1 ),
	array( 'db' => 'date',  'dt' => 2 ),
	array( 'db' => 'billing_type',  'dt' => 3),
	array( 'db' => 'currency_type',   'dt' => 4 ),
	array( 'db' => 'plan_name',   'dt' => 5 ),
	array( 'db' => 'amount_tax',   'dt' => 6 ),
	array( 'db' => 'invoice_prefix',   'dt' => 7 )
);

$columns2 = array(
	array( 'db' => 'invoice_prefix', 'dt' => 0 ),
	array( 'db' => 'invoice_date',  'dt' => 1 ),
	array( 'db' => 'due_date',  'dt' => 2),
	array( 'db' => 'plan_name',   'dt' => 3 ),
	array( 'db' => 'total',   'dt' => 4),
	array( 'db' => 'status',   'dt' => 5),
	array( 'db' => 'view',   'dt' => 6)
);


// SQL server connection information
$sql_details = array(
	'user' => $dbUser,
	'pass' => $dbPass,
	'db'   => $dbName,
	'host' => $dbHost
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

echo json_encode(
	SSPINVOICE::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $columns2 )
);
 die();   
}

if(isset($_GET['manageOrders'])){
    
// DB table to use
$table = 'premium_orders';

// Table's primary key
$primaryKey = 'id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
	array( 'db' => 'id',   'dt' => 0 ),
	array( 'db' => 'username', 'dt' => 1 ),
	array( 'db' => 'date',  'dt' => 2 ),
	array( 'db' => 'billing_type',  'dt' => 3),
	array( 'db' => 'status',   'dt' => 4 ),
	array( 'db' => 'payment_status',   'dt' => 5),
    array( 'db' => 'amount_tax',   'dt' => 6 ),
	array( 'db' => 'currency_type',   'dt' => 7 ),
	array( 'db' => 'plan_name',   'dt' => 8 ),
	array( 'db' => 'invoice_prefix',   'dt' => 9 )

);

$columns2 = array(
	array( 'db' => 'id', 'dt' => 0 ),
	array( 'db' => 'username',  'dt' => 1 ),
	array( 'db' => 'date',  'dt' => 2),
	array( 'db' => 'bill_type',   'dt' => 3 ),
	array( 'db' => 'status',   'dt' => 4),
	array( 'db' => 'payment_status',   'dt' => 5),
	array( 'db' => 'total',   'dt' => 6),
	array( 'db' => 'view',   'dt' => 7)
);


// SQL server connection information
$sql_details = array(
	'user' => $dbUser,
	'pass' => $dbPass,
	'db'   => $dbName,
	'host' => $dbHost
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

echo json_encode(
	SSPORDER::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $columns2 )
);
 die();   
}

if(isset($_GET['manageCustomers'])){
    
// DB table to use
$table = 'premium_users';

// Table's primary key
$primaryKey = 'id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
	array( 'db' => 'id',   'dt' => 0 ),
	array( 'db' => 'username', 'dt' => 1 ),
	array( 'db' => 'firstname',  'dt' => 2 ),
	array( 'db' => 'lastname',  'dt' => 3),
	array( 'db' => 'country',   'dt' => 4 ),
	array( 'db' => 'client_id',   'dt' => 5 )
);

$columns2 = array(
	array( 'db' => 'username',  'dt' => 0 ),
	array( 'db' => 'email',  'dt' => 1),
	array( 'db' => 'since',  'dt' => 2),
	array( 'db' => 'firstname',   'dt' => 3 ),
	array( 'db' => 'lastname',   'dt' => 4),
	array( 'db' => 'country',   'dt' => 5),
	array( 'db' => 'ban',   'dt' => 6),
	array( 'db' => 'actions',   'dt' => 7)
);


// SQL server connection information
$sql_details = array(
	'user' => $dbUser,
	'pass' => $dbPass,
	'db'   => $dbName,
	'host' => $dbHost
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

echo json_encode(
	SSPCUSTOMERS::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $columns2, $con )
);
 die();   
}


if(isset($_GET['manageDomains'])){
    
// DB table to use
$table = 'domains_data';

// Table's primary key
$primaryKey = 'id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
	array( 'db' => 'domain',   'dt' => 0 ),
	array( 'db' => 'completed', 'dt' => 1 ),
	array( 'db' => 'score',  'dt' => 2 ),
	array( 'db' => 'date',  'dt' => 3),
	array( 'db' => 'id',   'dt' => 4 ),
	array( 'db' => 'id',   'dt' => 5 )
);

$columns2 = array(
	array( 'db' => 'domain', 'dt' => 0 ),
	array( 'db' => 'cache', 'dt' => 1),
	array( 'db' => 'score', 'dt' => 2),
	array( 'db' => 'date', 'dt' => 3 ),
	array( 'db' => 'genpdf', 'dt' => 4),
	array( 'db' => 'delete', 'dt' => 5)
);

// SQL server connection information
$sql_details = array(
	'user' => $dbUser,
	'pass' => $dbPass,
	'db'   => $dbName,
	'host' => $dbHost
);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
echo json_encode(
	SSPDOMAIN::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $columns2, $con )
);
 die();   
}

if(isset($_GET['getCustomers'])){
    $data = array();
    $term = escapeTrim($con,$_GET['term']);
    $qstring = "SELECT username,id,email_id,date FROM users WHERE username LIKE '%".$term."%'";
    $result = mysqli_query($con,$qstring);
    
    while ($row = mysqli_fetch_array($result))
    {
//    		$row['value']=htmlentities(stripslashes($row['username']));
//    		$row['id']=(int)$row['id'];
//    		$row_set[] = array('value'=>$row['value'],'id'=>$row['id']);
            $data[] = $row['username'].'|'.$row['email_id'].'|'.$row['date'].'|'.$row['id'];
    }
    echo json_encode($data);
    
    die();
}

if(isset($_GET['getPlans'])){
    $data = array();
    $recData = '';
    $term = escapeTrim($con,$_GET['term']);
    $qstring = "SELECT plan_name,id,payment_type,one_time_fee,recurrent_fee FROM premium_plans WHERE plan_name LIKE '%".$term."%'";
    $result = mysqli_query($con,$qstring);
    
    while ($row = mysqli_fetch_array($result)) {
        if($row['payment_type'] == '1'){
            $recurringPlanAmount = unserialize($row['recurrent_fee']);
            $rec1 = $recurringPlanAmount[0][0];
            $rec2 = $recurringPlanAmount[1][0];
            $rec3 = $recurringPlanAmount[2][0];
            $rec4 = $recurringPlanAmount[3][0];
            
            if($recurringPlanAmount[0][1])
                $rec1Sel = 'rec1'; //Monthly
            else
                $rec1Sel = '0';
                
            if($recurringPlanAmount[1][1])
                $rec2Sel = 'rec2'; //Every 3 months
            else
                $rec2Sel = '0';
          
            if($recurringPlanAmount[2][1])
                $rec3Sel = 'rec3'; //Every 6 months
            else
                $rec3Sel = '0';
          
            if($recurringPlanAmount[3][1])
                $rec4Sel = 'rec4'; //Every year
            else
                $rec4Sel = '0';
        }   
        $recData =  '|'. $rec1Sel . '|'. $rec1 . '|'.$rec2Sel . '|'. $rec2. '|'.$rec3Sel . '|'. $rec3. '|'.$rec4Sel . '|'. $rec4;
        $data[] = $row['plan_name'].'|'.$row['id'].'|'.$row['payment_type'].'|'.$row['one_time_fee'].$recData;
    }
    echo json_encode($data);
    
    die();
}

die();
?>