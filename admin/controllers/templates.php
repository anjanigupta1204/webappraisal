<?php

defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
* @author Balaji
* @name: A to Z SEO Tools
* @copyright © 2015 ProThemes.Biz
*
*/

$fullLayout = 1;
$p_title = "Email Templates";
$mailTemplates = true;

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    if (isset($_POST['confirmation']))
    {
        $confirmationMail = base64_encode(raino_trim($_POST['confirmation']));
        $confirmationSub = base64_encode(raino_trim($_POST['confirmationSub']));
        
        $query = "UPDATE premium_mails SET subject='$confirmationSub', body='$confirmationMail' WHERE code='confirmation'"; 
        
        if (!mysqli_query($con, $query)) {
            $msg = '<div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                <b>Alert!</b> Something Went Wrong!
            </div>';
        } else {
            $msg = '<div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                <b>Alert!</b> Mail updated successfully
            </div>';
        }
    }
    
    if (isset($_POST['reminder']))
    {
        $reminderMail = base64_encode(raino_trim($_POST['reminder']));
        $reminderSub = base64_encode(raino_trim($_POST['reminderSub']));
        
        $query = "UPDATE premium_mails SET subject='$reminderSub', body='$reminderMail' WHERE code='reminder'"; 
        
        if (!mysqli_query($con, $query)) {
            $msg = '<div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                <b>Alert!</b> Something Went Wrong!
            </div>';
        } else {
            $msg = '<div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                <b>Alert!</b> Mail updated successfully
            </div>';
        }
    }
    
    if (isset($_POST['invoice']))
    {
        $invoiceMail = base64_encode(raino_trim($_POST['invoice']));
        $invoiceSub = base64_encode(raino_trim($_POST['invoiceSub']));
        
        $query = "UPDATE premium_mails SET subject='$invoiceSub', body='$invoiceMail' WHERE code='invoice'"; 
        
        if (!mysqli_query($con, $query)) {
            $msg = '<div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                <b>Alert!</b> Something Went Wrong!
            </div>';
        } else {
            $msg = '<div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                <b>Alert!</b> Mail updated successfully
            </div>';
        }
    }
}

$query =  "SELECT * FROM premium_mails";
$result = mysqli_query($con,$query);
        
while($row = mysqli_fetch_array($result)) {
    $code =  Trim($row['code']);
    
    if($code == 'confirmation'){
      $confirmationSub = html_entity_decode(base64_decode($row['subject']));
      $confirmationMail = html_entity_decode(base64_decode($row['body']));
    }
    
    if($code == 'reminder'){
      $reminderSub = html_entity_decode(base64_decode($row['subject']));
      $reminderMail = html_entity_decode(base64_decode($row['body']));
    }
    
    if($code == 'invoice'){
      $invoiceSub = html_entity_decode(base64_decode($row['subject']));
      $invoiceMail = html_entity_decode(base64_decode($row['body']));
    }

}


?>