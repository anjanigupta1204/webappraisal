<?php

defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
* @author Balaji
* @name: A to Z SEO Tools
* @copyright � 2016 ProThemes.Biz
*
*/

$fullLayout = 1;
$p_title = 'Website Reviewer';
$reviewerSys = true;
$page1 = 'active';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    
        $reviewer_list = array();
        $indexed = escapeTrim($con, $_POST['cahced']);
        $free_limit = escapeTrim($con, $_POST['limit']);
        $pdfCopyright = escapeTrim($con, $_POST['pdfCopyright']);
        
        $orderData = orderSettings($con);        
        $arrPDF = decSerBase($orderData['pdf_data']);
        $headerFilePath = $arrPDF[1];
        $footerFilePath = $arrPDF[2];
        
        foreach($_POST['reviewerList'] as $reviewer)
           $reviewer_list[] = escapeTrim($con,$reviewer); 
        $reviewer_listStr = serialize($reviewer_list);
        
          if($_FILES["headerUpload"]["name"] != ''){
         
            $target_dir = ROOT_DIR."uploads/";
            $target_filename = basename($_FILES["headerUpload"]["name"]);
            $uploadSs = 1;
            $check = getimagesize($_FILES["headerUpload"]["tmp_name"]);
            
            // Check it is a image
            if ($check !== false) {
                // Check if file already exists
                $target_filename = unqFile($target_dir,$target_filename);
                $target_file = $target_dir . $target_filename;
                $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
                // Check file size
                if ($_FILES["headerUpload"]["size"] > 500000) {
                    $msg =  '<div class="alert alert-danger alert-dismissable alert-premium">
             <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
             <strong>Alert!</strong> Sorry, your file is too large.
             </div>';
                    $uploadSs = 0;
                } else {
                    // Allow certain file formats
                    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType !=
                        "jpeg" && $imageFileType != "gif") {
                        $msg =  '<div class="alert alert-danger alert-dismissable alert-premium">
             <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
             <strong>Alert!</strong> Sorry, only JPG, JPEG, PNG & GIF files are allowed.
             </div>';
                        $uploadSs = 0;
                    }
                }
        
                // Check if $uploadSs is set to 0 by an error
                if (!$uploadSs == 0) {
                    if (move_uploaded_file($_FILES["headerUpload"]["tmp_name"], $target_file)) {
                         //Uploaded
                         $headerFilePath = "/uploads/$target_filename";
                    } else {
                        $msg =  '<div class="alert alert-danger alert-dismissable alert-premium">
             <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
             <strong>Alert!</strong> Sorry, there was an error uploading your file.
             </div>';
                    }
                }
        
            } else {
                $msg =  '<div class="alert alert-danger alert-dismissable alert-premium">
             <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
             <strong>Alert!</strong> File is not an image.
             </div>';
            }
        }
        
          if($_FILES["footerUpload"]["name"] != ''){
         
            $target_dir = ROOT_DIR."uploads/";
            $target_filename = basename($_FILES["footerUpload"]["name"]);
            $uploadSs = 1;
            $check = getimagesize($_FILES["footerUpload"]["tmp_name"]);
            
            // Check it is a image
            if ($check !== false) {
                // Check if file already exists
                $target_filename = unqFile($target_dir,$target_filename);
                $target_file = $target_dir . $target_filename;
                $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
                // Check file size
                if ($_FILES["footerUpload"]["size"] > 500000) {
                    $msg =  '<div class="alert alert-danger alert-dismissable alert-premium">
             <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
             <strong>Alert!</strong> Sorry, your file is too large.
             </div>';
                    $uploadSs = 0;
                } else {
                    // Allow certain file formats
                    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType !=
                        "jpeg" && $imageFileType != "gif") {
                        $msg =  '<div class="alert alert-danger alert-dismissable alert-premium">
             <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
             <strong>Alert!</strong> Sorry, only JPG, JPEG, PNG & GIF files are allowed.
             </div>';
                        $uploadSs = 0;
                    }
                }
        
                // Check if $uploadSs is set to 0 by an error
                if (!$uploadSs == 0) {
                    if (move_uploaded_file($_FILES["footerUpload"]["tmp_name"], $target_file)) {
                         //Uploaded
                         $footerFilePath = "/uploads/$target_filename";
                    } else {
                        $msg =  '<div class="alert alert-danger alert-dismissable alert-premium">
             <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
             <strong>Alert!</strong> Sorry, there was an error uploading your file.
             </div>';
                    }
                }
        
            } else {
                $msg =  '<div class="alert alert-danger alert-dismissable alert-premium">
             <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
             <strong>Alert!</strong> File is not an image.
             </div>';
            }
        }

        $pdfData = serBase(array($pdfCopyright,$headerFilePath,$footerFilePath));
        
        $query = "UPDATE order_settings SET indexed='$indexed', free_limit='$free_limit', reviewer_list='$reviewer_listStr', pdf_data='$pdfData' WHERE id='1'";
    
        if (!mysqli_query($con, $query)) {
            $msg = '<div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                    <b>Alert!</b> Something Went Wrong!
                    </div>';
        } else {
            $msg = '<div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                    <b>Alert!</b> Settings saved successfully!
                    </div>
                    ';
        }
}

if(isset($_GET['delete'])){
    $page1 = '';
    $page2 = 'active';
        
    $user_id = raino_trim($_GET['delete']);
    $query = "DELETE FROM domains_data WHERE id=$user_id";
    $result = mysqli_query($con, $query);
    if (mysqli_errno($con)) {
        $msg = '<div class="alert alert-danger alert-dismissable">
     <strong>Alert!</strong> ' . mysqli_error($con) . '
     </div>';
    } else {
        $msg = '<div class="alert alert-success alert-dismissable">
         <strong>Alert!</strong> Domain deleted from database successfully
         </div>';
    }
}

$orderData = orderSettings($con);
$orderData['reviewer_list'] = unserialize($orderData['reviewer_list']);
$enable_cahced = filter_var($orderData['indexed'], FILTER_VALIDATE_BOOLEAN);
$reviewerList = getReviewerList();

$arrPDF = decSerBase($orderData['pdf_data']);

$pdfCopyright = $arrPDF[0];
$headerLogo = $arrPDF[1];
$footerLogo = $arrPDF[2];

if($headerLogo == '')
    $headerLogo = '/theme/default/premium/img/logo.jpg';
    
if($footerLogo == '')
    $footerLogo = '/theme/default/premium/img/footer-logo.jpg';
?>