<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright © 2016 ProThemes.Biz
 *
 */

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
       
    if(isset($_POST['skrill'])){
        $mySettings = unserialize($arrData['settings']);
        $skrillFilePath = $mySettings[3];
        $pay_id = escapeTrim($con, $_POST['skrill']);
        $gateway = escapeTrim($con, $_POST['gateway']);
        $gateway_title = escapeTrim($con, $_POST['gateway_title']);
        $sort_order = escapeTrim($con, $_POST['sort_order']);
        $skrill_id = escapeTrim($con, $_POST['skrill_id']);
        $secretword = escapeTrim($con, $_POST['secretword']);
        $log = escapeTrim($con, $_POST['log']);
        $amount = escapeTrim($con, $_POST['amount']);
        $idMismatch = escapeTrim($con, $_POST['idMismatch']);
        
        if($_FILES["skrillUpload"]["name"] != ''){
         
            $target_dir = ROOT_DIR."uploads/";
            $target_filename = basename($_FILES["skrillUpload"]["name"]);
            $uploadSs = 1;
            $check = getimagesize($_FILES["skrillUpload"]["tmp_name"]);
            
            // Check it is a image
            if ($check !== false) {
                // Check if file already exists
                $target_filename = unqFile($target_dir,$target_filename);
                $target_file = $target_dir . $target_filename;
                $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
                // Check file size
                if ($_FILES["skrillUpload"]["size"] > 500000) {
                    $imgMsg =  '<div class="alert alert-danger alert-dismissable alert-premium">
             <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
             <strong>Alert!</strong> Sorry, your file is too large.
             </div>';
                    $uploadSs = 0;
                } else {
                    // Allow certain file formats
                    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType !=
                        "jpeg" && $imageFileType != "gif") {
                        $imgMsg =  '<div class="alert alert-danger alert-dismissable alert-premium">
             <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
             <strong>Alert!</strong> Sorry, only JPG, JPEG, PNG & GIF files are allowed.
             </div>';
                        $uploadSs = 0;
                    }
                }
        
                // Check if $uploadSs is set to 0 by an error
                if (!$uploadSs == 0) {
                    if (move_uploaded_file($_FILES["skrillUpload"]["tmp_name"], $target_file)) {
                         //Uploaded
                         $skrillFilePath = "/uploads/$target_filename";
                    } else {
                        $imgMsg =  '<div class="alert alert-danger alert-dismissable alert-premium">
             <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
             <strong>Alert!</strong> Sorry, there was an error uploading your file.
             </div>';
                    }
                }
        
            } else {
                $imgMsg =  '<div class="alert alert-danger alert-dismissable alert-premium">
             <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
             <strong>Alert!</strong> File is not an image.
             </div>';
            }
        }
        
        $mySettings = serialize(array($skrill_id,$secretword,$log,$skrillFilePath,$amount,$idMismatch));
        
        $query = "UPDATE payment_gateways SET gateway_title='$gateway_title', sort_order='$sort_order', settings='$mySettings', active='$gateway' WHERE id='$pay_id'";
    
        if (!mysqli_query($con, $query)) {
            $msg = '<div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                    <b>Alert!</b> Something Went Wrong!
                    </div>';
        } else {
            $msg = '<div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                    <b>Alert!</b> Settings saved successfully!
                    </div>
                    ';
        }
            if(isset($imgMsg))
                $msg = $imgMsg;
    }
}
extract(getPaymentGateway($con,$pay_id));
$settings = unserialize($settings);
$skrill_id = $settings[0];
$$secretword = $settings[1];
$log = filter_var($settings[2], FILTER_VALIDATE_BOOLEAN);
$skrillFilePath = $settings[3];
$amount = $settings[4];
$idMismatch = $settings[5];
?>