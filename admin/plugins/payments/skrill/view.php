<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright © 2016 ProThemes.Biz
 *
 */
?>
                <div class="row">
                    <div class="col-md-8">
                                           
                       <br /><div class="box-header with-border">
                        <h3 class="box-title"><?php echo $arrData['gateway_title']. ' Settings'; ?></h3>
                       </div><br />
                       
                        <div class="form-group">
                          <label for="skrill_id">Skrill ID</label>
                          <input type="text" placeholder="Enter your skrill id" value="<?php echo $skrill_id; ?>" name="skrill_id" class="form-control" />
                        </div>
                        
                        <div class="form-group">
                          <label for="secretword">Skrill Secret Word</label>
                          <input type="text" placeholder="Enter your skrill secretword" value="<?php echo $secretword; ?>" name="secretword" class="form-control" />
                        </div>
                        
                        <div class="form-group">
                          <label for="ipnUrl">Callback URL:</label>
                          <input type="text" disabled="" value="<?php echo 'http://'.$_SERVER['HTTP_HOST'].'/payments/skrill/verify'; ?>" name="ipnUrl" class="form-control" />
                        </div>
                        
                        <div class="form-group">
                        <label for="log">Log IPN callback results into database</label>
                        <select class="form-control" name="log">
    						<option <?php echo $log ? 'selected=""' : ''; ?> value="yes">Yes</option>
    						<option <?php echo $log ? '' : 'selected=""'; ?> value="no">No</option>
    					</select>
                        </div>
                        
                        <div class="form-group">
                            <label for="skrillLogoBox">Header Logo: <small>(For Skrill Payment Page)</small></label><br />
                            <img class="skrillLogoBox" id="skrillLogoBox" alt="Skrill Logo" src="<?php echo $skrillFilePath; ?>" />   
                            <br />
                            Recommended Size: (PNG 150x79px)
                            <input type="file" name="skrillUpload" id="skrillUpload" class="btn btn-default" />
                       </div>    
                       
                       <br /><div class="box-header with-border">
                        <h3 class="box-title">Fraud Prevention</h3>
                       </div><br />
                        
                       <div class="form-group">
                        <label for="amount">Detect amount mismatch</label>
                        <select class="form-control" name="amount">
    						<option <?php echo $amount ? 'selected=""' : ''; ?> value="yes">Yes</option>
    						<option <?php echo $amount ? '' : 'selected=""'; ?> value="no">No</option>
    					</select>
                        </div> 
                        
                        <div class="form-group">
                        <label for="idMismatch">Detect Skrill ID mismatch</label>
                        <select class="form-control" name="idMismatch">
    						<option <?php echo $idMismatch ? 'selected=""' : ''; ?> value="yes">Yes</option>
    						<option <?php echo $idMismatch ? '' : 'selected=""'; ?> value="no">No</option>
    					</select>
                        </div>
                                            

                    </div>
                </div>