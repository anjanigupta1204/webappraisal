<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright � 2015 ProThemes.Biz
 *
 */
?>
<link href="<?php echo $theme_path; ?>dist/css/pricing.css" rel="stylesheet" type="text/css" />
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo $p_title; ?>  
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="/admin/?route=add-plan"><i class="fa fa-dollar"></i> Admin</a></li>
            <li class="active"><?php echo $p_title; ?> </li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Plan Settings</h3>
                </div><!-- /.box-header -->
                <form action="#" method="POST" onsubmit="return finalFixedLink();">
                <div class="box-body">
                <?php
                if(isset($msg)){
                    echo $msg;
                }?>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group">
                      <label for="plan_name">Plan Name</label>
                      <input type="text" placeholder="Enter your plan name" value="<?php echo $plan_name; ?>" name="plan_name" class="form-control" />
                    </div>
                    <div class="form-group">
                      <label for="plan_url">Plan URL</label> <small id="linkBox"> (<?php echo 'http://' . $_SERVER['HTTP_HOST'] ."/premium/".$plan_url; ?>) </small>
                      <input type="text" placeholder="Enter your plan url" value="<?php echo $plan_url; ?>" name="plan_url" id="planUrlBox" class="form-control" />
                    </div>
                    
                    <div class="form-group">
                      <label for="plan_des">Plan Description</label>
                      <textarea placeholder="Description must be within 150 Characters" style="height: 108px;" rows="4" name="plan_des" class="form-control"><?php echo $plan_des; ?></textarea>
                    </div>
                   
                    <div class="form-group">
                    <label for="plan_act">Activation:</label>
                    <select class="form-control" name="plan_act">
						<option <?php echo $plan_act_yes; ?> value="yes">Automatic activation (After payment is received)</option>
						<option <?php echo $plan_act_no; ?> value="no">Manual activation</option>
					</select>
                    </div>
                    
                    <div class="form-group">
                    <label for="allow_pdf">Allow PDF Reports:</label>
                    <select class="form-control" name="allow_pdf">
						<option <?php echo $allow_pdf_yes; ?> value="yes">Yes</option>
						<option <?php echo $allow_pdf_no; ?> value="no">No</option>
					</select>
                    </div>
                    
                    <div class="form-group">
                    <label for="brand_pdf">Download Reports as Branded PDFs:</label>
                    <select class="form-control" name="brand_pdf">
					    <option <?php echo $brand_pdf_no; ?> value="no">No</option>
						<option <?php echo $brand_pdf_yes; ?> value="yes">Yes</option>
					</select>
                    </div>
                  
                    </div><!-- /.col-md-6 -->
                    
                    <div class="col-md-6">
                    
                    <div class="form-group">
                      <label for="plan_no">Sort Order</label>
                      <input type="text" placeholder="Enter sort order number" value="<?php echo $plan_no; ?>" name="plan_no" class="form-control" />
                    </div>
                    
                    <div class="form-group">
                      <label for="plan_title">Plan Title</label>
                      <input type="text" placeholder="Enter your meta title" value="<?php echo $plan_title; ?>" name="plan_title" class="form-control" />
                    </div>

                    <div class="form-group">
                    <label for="plan_stats">Status:</label>
                    <select class="form-control" name="plan_stats">
						<option <?php echo $plan_stats_yes; ?> value="yes">Enabled</option>
						<option <?php echo $plan_stats_no; ?> value="no">Disabled</option>
					</select>
                    </div>
                   
                    <div class="form-group">
                    <label for="plan_hide">Hidden:</label>
                    <select class="form-control" name="plan_hide">
					    <option <?php echo $plan_hide_no; ?> value="no">No</option>
						<option <?php echo $plan_hide_yes; ?> value="yes">Yes</option>
					</select>
                    </div>
                    
                    <div class="form-group">
                    <label for="featured">Featured:</label>
                    <select class="form-control" name="featured">
					    <option <?php echo $featured_no; ?> value="no">No</option>
						<option <?php echo $featured_yes; ?> value="yes">Yes</option>
					</select>
                    </div>
                   
                    <div class="form-group">
                    <label for="max_pdf">Limit PDF Reports per Day: (0=Unlimited)</label>
                    <input type="text" placeholder="Allowed PDF Reports per day" value="<?php echo $max_pdf; ?>" name="max_pdf" class="form-control" />
                    </div>
                    
                    <div class="form-group">
                    <label for="captcha_free">No Annoying Captcha:</label>
                    <select class="form-control" name="captcha_free">
						<option <?php echo $captcha_free_yes; ?> value="yes">Yes</option>
						<option <?php echo $captcha_free_no; ?> value="no">No</option>
					</select>
                    </div>
                    
                    </div>
                </div><!-- /.row -->
                 
                 <div class="row">
                   <div class="form-group" style="margin: 12px;">
                    <label>Premium Tools</label>
                    <select name="premium_tools[]" class="form-control select2" multiple="multiple" data-placeholder="Select a Premium Tools" style="width: 100%;">
                      <?php if(!isset($_GET['edit'])) {
                        foreach($premiumTools as $premiumTool)
                            echo '<option value="'.$premiumTool[0].'">'.$premiumTool[1].'</option>';
                        }else{
                            foreach($premiumTools as $premiumTool){
                                if (in_array($premiumTool[0], $data['premium_tools']))
                                    echo '<option selected="" value="'.$premiumTool[0].'">'.$premiumTool[1].'</option>';
                                else
                                    echo '<option value="'.$premiumTool[0].'">'.$premiumTool[1].'</option>';
                            }
                        }
                      ?>
                    </select>
                  </div><!-- /.form-group -->
                 
                  <div class="form-group" style="margin: 12px; margin-top: 23px;">
                  
                  <label>Payment Type</label>
                  <div class="form-group" style="margin: 12px;">
                    <label>
                      <input <?php echo $oneTimeSel; ?> type="radio" name="payment_type" value="0" class="minimal">
                      One time Payment
                    </label>
                    <label style="margin-left: 12px;">
                      <input <?php echo $recTimeSel; ?> type="radio" name="payment_type" value="1" class="minimal">
                      Recurring Payment
                    </label>
                  </div>
                  </div><!-- /.form-group -->
                  <div style="margin: 12px;">
                 
                  <table class="onepay table table-hover" style="display: none;">
                    <tbody>
                        <tr> <th> &nbsp; </th> <th>Lifetime period cost (USD)</th> </tr> 
                        <tr> 
                            <td>One time cost</td> 
                            <td><input type="text" name="onepay_cost" value="<?php echo $planAmount; ?>" class="form-control" /></td> 
                        </tr>
                    </tbody>
                  </table>
                  
                  <table class="recpay table table-hover" style="display: none;">
                    <tbody>
                        <tr> <th> &nbsp; </th> <th>Price for period (USD)</th> <th>Enable</th> </tr> 
                        <tr> 
                            <td>Monthly</td> 
                            <td><input name="recpay[]" type="text" value="<?php echo $rec1; ?>" class="form-control" /></td> 
                            <td><input name="recpayCheck[0]" type="checkbox" <?php echo $rec1Sel; ?> class="minimal" /></td> 
                        </tr>
                        <tr> 
                            <td>Every 3 months</td> 
                            <td><input name="recpay[]" type="text" value="<?php echo $rec2; ?>" class="form-control" /></td> 
                            <td><input name="recpayCheck[1]" type="checkbox" <?php echo $rec2Sel; ?> class="minimal" /></td> 
                        </tr>
                        <tr> 
                            <td>Every 6 months</td> 
                            <td><input name="recpay[]" type="text" value="<?php echo $rec3; ?>" class="form-control" /></td> 
                            <td><input name="recpayCheck[2]" type="checkbox" <?php echo $rec3Sel; ?> class="minimal" /></td> 
                        </tr>
                        <tr> 
                            <td>Every year</td> 
                            <td><input name="recpay[]" type="text" value="<?php echo $rec4; ?>" class="form-control" /></td> 
                            <td><input name="recpayCheck[3]" type="checkbox" <?php echo $rec4Sel; ?> class="minimal" /></td> 
                        </tr>
                    </tbody>
                  </table>
                  </div>
                  
                 <br />
                 
                 <div class="box-header with-border">
                  <h3 class="box-title">Pricing Box Settings</h3>
                 </div><!-- /.box-header -->
                 
                 <div class="box-body">
                  <div class="row">
                    <div class="col-md-6">
                    
                        <div class="form-group">
                          <label for="plan_sub">Plan Subtitle</label>
                          <input type="text" placeholder="Enter your plan subtitle.." value="<?php echo $plan_sub; ?>" name="plan_sub" class="form-control" />
                        </div>
                        
                        <div class="form-group">
                          <label for="cur_type">Plan Currency Symbol</label>
                            <select class="form-control" name="cur_type">
                            <?php
                            foreach($curDataList as $curData=>$curDataVal){
                            if(isset($_GET['edit'])){
                                if($curData == $cur_type){
                                    echo '<option selected value="'.$curData.'">'.$curData . ' - ' . $curDataVal[1].'</option>';
                                    $selCurType = $curDataVal[1];
                                }else
                                    echo '<option value="'.$curData.'">'.$curData . ' - ' . $curDataVal[1].'</option>';
                            }else
                                echo '<option value="'.$curData.'">'.$curData . ' - ' . $curDataVal[1].'</option>';
                            }
                            ?>
                            </select>
                        </div>
                        
                        <div class="form-group">
                          <label for="plan_price">Plan Price</label>
                          <input type="text" placeholder="Enter your plan price.." value="<?php echo $plan_price; ?>" name="plan_price" class="form-control" />
                        </div>
                        
                        <div class="form-group">
                          <label for="plan_type">Plan Subscription Type</label>
                          <input type="text" placeholder="Enter your plan subscription type.." value="<?php echo $plan_type; ?>" name="plan_type" class="form-control" />
                        </div>
                    
                        <div class="form-group">
                          <label for="plan_features">Plan Features (One/per Line)</label>
                          <textarea placeholder="Enter one features per line..." rows="8" name="plan_features" class="form-control"><?php echo $plan_features; ?></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                    
                    <div class="rp-format-2 rp-hex-hover">
						<div class="col-sm-6 col-xs-8">
							<div class="rp-item rp-item-1">                                
                                <!-- Featured -->
								<div class="rp-plan-top">
									<h3 class="rp-plan-name"><?php echo $plan_name; ?></h3>
									<p class="rp-plan-description"><?php echo $plan_sub; ?></p>
									<div class="rp-hex">
										<div class="rp-plan-cost">
											<span class="rp-currency"><?php echo $selCurType; ?></span>
											<span class="rp-cost"><?php echo $plan_price; ?></span>
											<span class="rp-duration"><?php echo $plan_type; ?></span>
										</div>
									</div>
								</div>
								<div class="rp-plan-details">
									<ul id="plan_data"> 
                                    <?php foreach($plan_featuresArr as $plan_feature){
                                        if(Trim($plan_feature) != '')
                                            echo '<li><p>'.html_entity_decode(Trim($plan_feature)).'</p></li>';
                                    } ?>                                      
									</ul>
								</div>
								<div class="rp-plan-bottom">
									<a href="#" class="btn rp-btn rp-pricing-btn">Subscribe</a>
								</div>
							</div>
						</div>
					</div>
                    
                    </div>
                  </div>
                  

                  </div>
                 </div>
                 <?php
                 if(isset($_GET['edit'])){
                    echo '<input type="hidden" value="'.$editID.'" name="edit" id="edit" />';
                 }?>
                <input type="submit" name="save" value="Save" class="btn btn-primary"/>
                <br />
                
                </div><!-- /.box-body -->
                </form>
              </div><!-- /.box -->
      
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->