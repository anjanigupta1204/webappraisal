<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright © 2015 ProThemes.Biz
 *
 */
?>
    <link rel="stylesheet" type="text/css" href="<?php echo $theme_path; ?>plugins/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
   	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script src="<?php echo $theme_path; ?>plugins/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo $p_title; ?>  
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="/admin/?route=blog-settings"><i class="fa fa-edit"></i> Admin</a></li>
            <li class="active"><?php echo $p_title; ?> </li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Blog Settings</h3>
                </div><!-- /.box-header -->
                <form action="#" method="POST">
                <div class="box-body">
                <?php
                if(isset($msg)){
                    echo $msg;
                }?>
                  <div class="row">
                    <div class="col-md-6">
                    
                    <div class="form-group">
                        <label>Blog Status</label>
                        <select name="blog_status" class="form-control"> 
                            <?php if ($blog_status)
                            {
                                echo '<option selected value="on">Enabled</option>';
                                echo '<option value="off">Disable</option>';
                            }
                            else
                            {
                                echo '<option value="on">Enable</option>';
                                echo '<option selected value="off">Disabled</option>';
                            } ?>

                        </select>             
                    </div>  
                    
                    <div class="form-group">
                      <label for="meta_des">Meta Description</label>
                      <textarea placeholder="Description must be within 150 Characters" rows="3" name="meta_des" class="form-control"><?php echo $meta_des; ?></textarea>
                   </div>
                                                     
                   <div class="form-group">
                        <label for="maximum_posts">Maximum number of post shown on per page</label>
                        <input type="text" placeholder="Enter your maximum posts per page" id="maximum_posts" name="maximum_posts" value="<?php echo $maximum_posts; ?>" class="form-control">
                   </div>
                   
                   <div class="form-group">
                        <label for="posted_by">Default Posted By</label>
                        <input type="text" placeholder="Enter who posted by as default" id="posted_by" name="posted_by" value="<?php echo $posted_by; ?>" class="form-control">
                   </div>
                                                     
                                                              
                    </div><!-- /.col-md-6 -->
                    
                    <div class="col-md-6">
                    
                   <div class="form-group">
                        <label>Enable Comments (Powered by Disqus)</label>
                        <select name="enable_comments" class="form-control"> 
                            <?php if ($enable_comments)
                            {
                                echo '<option selected value="on">Yes</option>';
                                echo '<option value="off">No</option>';
                            }
                            else
                            {
                                echo '<option value="on">Yes</option>';
                                echo '<option selected value="off">No</option>';
                            } ?>

                        </select>             
                    </div>  
                                        
                    <div class="form-group">
                      <label for="meta_tags">Meta Keywords (Separate with commas)</label>
                      <textarea placeholder="keywords1, keywords2, keywords3" rows="3" name="meta_tags" class="form-control"><?php echo $meta_tags; ?></textarea>
                   </div>
                   
                   <div class="form-group">
                        <label for="truncate">Content Truncate Length (For Blog Index Page)</label>
                        <input type="text" placeholder="Enter your truncate length" id="truncate" name="truncate" value="<?php echo $truncate; ?>" class="form-control">
                   </div>
                                      
                   <div class="form-group">
                   <label for="no_image">Default Featured Image</label>
                   <div class="input-group">
                        <input type="text" id="no_image" value="<?php echo $no_image; ?>" class="form-control" name="no_image" />                                    
                        <span class="input-group-addon">
                        <a class="fa fa-picture-o iframe-btn" href="/core/library/filemanager/dialog.php?type=1&field_id=no_image"></a>
                        </span>
                   </div>
                   </div>   

                    </div>
                </div><!-- /.row -->
                
                <div class="row">
                 
                 <div class="form-group" style="margin: 12px;">
                      <label for="disqus_id">Put your Disqus Universal Code - Comment System(<a href="https://disqus.com/admin/create/" target="_blank" title="Disqus Site">Create Disqus Comment</a>)</label>
                      <textarea name="disqus_id" class="form-control" rows="6"><?php echo $disqus_id; ?></textarea>
                   </div>
                 
                 </div>
                    
                <input type="submit" name="save" value="Save" class="btn btn-primary"/>
                <br /> <br />
                
                </div><!-- /.box-body -->
                </form>
              </div><!-- /.box -->
      
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      
    <script type="text/javascript">
     jQuery(document).ready(function ($) {
          $('.iframe-btn').fancybox({
            'width': '75%',
            'height': '90%',
            'autoScale': false,
            'transitionIn': 'none',
            'transitionOut': 'none',
            'type': 'iframe'
        });
    });
    </script>
