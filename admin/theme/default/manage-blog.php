<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright © 2015 ProThemes.Biz
 *
 */
?>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    
    <script type="text/javascript" language="javascript" src="<?php echo $theme_path; ?>dist/js/jquery.dataTables.js"></script>
    
    <script type="text/javascript" language="javascript" class="init">
    
    $(document).ready(function() {
    	$('#mySitesTable').dataTable( {
    		"processing": true,
            "aaSorting": [[0, 'desc']],
    		"serverSide": true,
    		"ajax": "/admin/?route=addonajax&manageBlogPosts"
    	} );
    } );
    
    </script>
    
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo $p_title; ?>  
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="/admin/?route=manage-blog"><i class="fa fa-edit"></i> Admin</a></li>
            <li class="active"><?php echo $p_title; ?> </li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Posts</h3>
                  
                  <div style="position:absolute; top:4px; right:15px;">
                  <a href="/admin/?route=new-post" class="btn btn-primary"><i class="fa fa-plus"></i> New Post</a>
                  </div>
                  
                </div><!-- /.box-header -->

                <div class="box-body">
                <?php
                if(isset($msg)){
                    echo $msg;
                }?>

                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="mySitesTable">
                	<thead>
                		<tr>
                              <th>Page Title</th>
                              <th>Category</th>
                              <th>Created date</th>
                              <th>Pageviews</th>
                              <th>Status</th>
                              <th>Actions</th>
                		</tr>
                	</thead>         
                    <tbody>                        
                    </tbody>
                </table>
                

                <br />
                
                </div><!-- /.box-body -->

              </div><!-- /.box -->
      
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
