<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright � 2015 ProThemes.Biz
 *
 */
?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo $p_title; ?>  
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="/admin/?route=manage-pages"><i class="fa fa-book"></i> Admin</a></li>
            <li class="active"><?php echo $p_title; ?> </li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
        
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">New Order</h3>                  
                </div><!-- /.box-header -->
                
   
                <div class="box-body">
                 <?php
                if(isset($msg)){
                    echo $msg;
                }?>
                
                <div class="row">
                    
                   <?php if($orderSuc) { ?>
                     
                    <div class="text-center">  
                    <h2>Order created successfully!</h2>
                    <h6><a href="/admin/?route=orders&view=<?php echo $orderID; ?>" class="btn btn-primary" target="_blank">View Order</a></h6>
                    </div> 
                    
                   <?php } else { ?>
                   <form action="#" method="POST">
                    <div class="col-md-6">
                                        
                    <div class="form-group">
                    <label for="amount">Customer:<small>(Username)</small></label>
                    <input placeholder="Type your customer username (autocomplete)" type="text" name="username" id="username" class="form-control" />
                    </div>
                
                    <div class="form-group">
                    <label for="emailID">Email ID:</label>
                    <input disabled="" type="text" name="emailID" id="emailID" class="form-control" />
                    </div>
                    
                    <div class="form-group">
                    <label for="plan">Plan Name:</label>
                    <input placeholder="Type your plan name (autocomplete)" type="text" name="plan" id="plan" class="form-control" />
                    </div>
                    
                    <div class="form-group">
                    <label for="billDis">Billing Type:</label>
                    <input disabled="" type="text" id="billDis" name="billDis" class="form-control" />
                    </div>
                    
                    <div class="form-group">
                    <label for="payType">Payment Method</label>
                    <select class="form-control" name="payType">
                        <?php echo $paymentGatewaysData; ?>
    				</select>
                    </div>
                    
                    <div class="form-group">
                    <label for="orderStatus">Order Status</label>
                    <select class="form-control" name="orderStatus">
                        <option value="completed">Completed</option>
                        <option value="rec_completed">Recurring Completed</option>
                        <option value="pending">Pending</option>
                        <option value="rec_pending">Recurring Pending</option>
                        <option value="canceled">Canceled</option>
    				</select>
                    </div>
                    
                    </div><!-- /.col-md-6 -->
                    
                    <div class="col-md-6">
                    
                    <div class="form-group">
                    <label for="userID">Customer ID:</label>
                    <input disabled="" type="text" name="userID" id="userID" class="form-control" />
                    </div>
                    
                    <div class="form-group">
                    <label for="since">Member Since:</label>
                    <input disabled="" type="text" name="since" id="since" class="form-control" />
                    </div>
                    
                    <div class="form-group" id="recTypeBox" style="display: none;">
                    <label for="recType">Recurring Type:</label>
                    <select class="form-control" id="recType" name="recType"></select>
                    </div>  
                    
                    <div class="form-group">
                    <label for="amount">Plan Amount:</label>
                    <input type="text" id="amount" name="amount" class="form-control" />
                    </div>
                    
                    <div class="form-group">
                    <label for="total">Total Amount:</label>
                    <input type="text" id="total" name="total" class="form-control" />
                    </div>
                    
                    <div class="form-group">
                    <label for="payStatus">Payment Status</label>
                    <select class="form-control" name="payStatus">
                        <option value="completed">Paid</option>
                        <option value="pending">Unpaid</option>
                        <option value="fraud">Fraud</option>
                        <option value="refunded">Refunded</option>
                        <option value="chargeback">Chargeback</option>
                        <option value="reversed">Reversed</option>
                        <option value="canceled">Canceled</option>
    				</select>
                    </div>
             
                    </div>
                    <br />
                     <input type="hidden" name="rec1Am" id="rec1Am" />
                     <input type="hidden" name="rec2Am" id="rec2Am" />
                     <input type="hidden" name="rec3Am" id="rec3Am" />
                     <input type="hidden" name="rec4Am" id="rec4Am" />
                     
                     <input type="hidden" name="billType" id="billType" />
                     <input type="hidden" name="planID" id="planID" />
                     <div class="col-md-12">    <br />
                    <input type="submit" name="save" value="Create Order" class="btn btn-primary"/>
                    <br /><br />
                    </div>
                     </form>
                     <?php } ?>
                </div><!-- /.row -->
                
                </div><!-- /.box-body -->
       
            </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

