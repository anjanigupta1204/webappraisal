<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright � 2015 ProThemes.Biz
 *
 */
?>   

    <link rel="stylesheet" type="text/css" href="<?php echo $theme_path; ?>plugins/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
   	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script src="<?php echo $theme_path; ?>plugins/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo $p_title; ?>  
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="/admin/?route=manage-blog"><i class="fa fa-edit"></i> Admin</a></li>
            <li class="active"><?php echo $p_title; ?> </li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">New Post</h3>                  
                </div><!-- /.box-header -->

                <form action="#" method="POST" onsubmit="return finalFixedLink();">
                <div class="box-body">
                 <?php
                if(isset($msg)){
                    echo $msg;
                }?>
                
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group">
                      <label for="post_title">Post Title</label>
                      <input type="text" placeholder="Enter your post title" value="<?php echo $post_title; ?>" name="post_title" class="form-control" />
                    </div>
                    <div class="form-group">
                      <label for="post_url">Post URL</label><small id="linkBox"> (<?php echo 'http://' . $_SERVER['HTTP_HOST'] ."/blog/"; ?>) </small>
                      <input type="text" id="pageUrlBox" placeholder="Enter your post url" value="<?php echo $post_url; ?>" name="post_url" class="form-control" />
                    </div>
                    
                    <div class="form-group">
                      <label for="meta_des">Meta Description</label>
                      <textarea placeholder="Description must be within 150 Characters" rows="3" name="meta_des" class="form-control"><?php echo $meta_des; ?></textarea>
                   </div>
                   
                   <div class="form-group">
                   <label for="thumbnail">Featured Image</label>
                   <div class="input-group">
                        <input type="text" id="thumbnail" value="<?php echo $featured_image; ?>" class="form-control" name="thumbnail" />                                    
                        <span class="input-group-addon">
                        <a class="fa fa-picture-o iframe-btn" href="/core/library/filemanager/dialog.php?type=1&field_id=thumbnail"></a>
                        </span>
                   </div>
                   </div>   
                              
                   <div class="form-group">
                        <label>Status</label>
                        <select name="post_enable" class="form-control"> 
                            <?php if ($post_enable)
                            {
                                echo '<option selected value="on">Published</option>';
                                echo '<option value="off">Unpublished</option>';
                            }
                            else
                            {
                                echo '<option value="on">Published</option>';
                                echo '<option selected value="off">Unpublished</option>';
                            } ?>

                        </select>             
                    </div>  
                                                              
                    </div><!-- /.col-md-6 -->
                    
                    <div class="col-md-6">
                    
                    <div class="form-group">
                      <label for="category">Category</label>
                      <input type="text" placeholder="Enter the category name" value="<?php echo $category; ?>" name="category" class="form-control" />
                    </div>
                    
                    <div class="form-group">
                      <label for="posted_date">Posted Date</label>

                      <?php if($posted_date == '') { ?>
                      <input type="text" placeholder="Enter your posted date" id="postedDate" name="posted_date" class="form-control" />
                      <?php } else { ?>
                      <input type="text" placeholder="Enter your posted date" value="<?php echo $posted_date; ?>" id="postedDate" name="posted_date" class="form-control" />
                      <?php } ?>
                    </div>
                    
                    <div class="form-group">
                      <label for="meta_tags">Meta Keywords (Separate with commas)</label>
                      <textarea placeholder="keywords1, keywords2, keywords3" rows="3" name="meta_tags" class="form-control"><?php echo $meta_tags; ?></textarea>
                   </div>
                   
                    <div class="form-group">
                      <label for="posted_by">Posted By</label>
                      <input type="text" placeholder="Enter the posted by" value="<?php echo $posted_by; ?>" name="posted_by" class="form-control" />
                    </div>
                    
                    <div class="form-group">
                        <label>Allow comment</label>
                        <select name="allow_comment" class="form-control"> 
                            <?php if ($allow_comment)
                            {
                                echo '<option selected value="on">Yes</option>';
                                echo '<option value="off">No</option>';
                            }
                            else
                            {
                                echo '<option value="on">Yes</option>';
                                echo '<option selected value="off">No</option>';
                            } ?>

                        </select>             
                    </div>  

                    </div>
                </div><!-- /.row -->
                
                <div class="row">
                 
                 <div class="form-group" style="margin: 12px;">
                      <label for="post_content">Page Content</label>
                      <textarea id="editor1" name="post_content" class="form-control"><?php echo $post_content; ?></textarea>
                   </div>
                 
                 </div>
                 <?php if(isset($_GET['edit'])){ ?>
                 <input type="hidden" name="editPage" value="1" />
                 <input type="hidden" name="editID" value="<?php echo $editID; ?>" />
                 <?php } else { ?>
                 <input type="hidden" name="newPage" value="1" />
                 <?php } ?>
                <input type="submit" name="save" value="Save" class="btn btn-primary"/>
                <br />
                
                </div><!-- /.box-body -->
            </form>

              </div><!-- /.box -->
      
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
    <script type="text/javascript">
     jQuery(document).ready(function ($) {
          $('.iframe-btn').fancybox({
            'width': '75%',
            'height': '90%',
            'autoScale': false,
            'transitionIn': 'none',
            'transitionOut': 'none',
            'type': 'iframe'
        });
    });
    </script>