<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright � 2016 ProThemes.Biz
 *
 */
?>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    
    <script type="text/javascript" language="javascript" src="<?php echo $theme_path; ?>dist/js/jquery.dataTables.js"></script>
    
    <script type="text/javascript" language="javascript" class="init">

        $(document).ready(function() {

            var table = $('#mySitesTable').dataTable({
        		"processing": true,
                "aaSorting": [[0, 'desc']],
        		"serverSide": true,
                "ajax": "/admin/?route=premium_ajax&manageOrders"
            });
            
            var Qcount = 0;
            $('#mySitesTable thead td').each(function (i)  {
                if(Qcount < 6){
               
                var title = $('#mySitesTable thead th').eq($(this).index()).text();
                if(Qcount == 0)
                    var serach = '<input class="form-control input-sm" type="text" style="width: 70px;" placeholder="Search ID" />';
                else if(Qcount == 2)
                    var serach = '<input id="dbDate" class="form-control input-sm" type="text" placeholder="Search ' + title + '" />';
                else if(Qcount == 3)
                    var serach = '<select name="tableBill" class="form-control input-sm"><option value="" selected=""> </option><option value="1">Recurring Billing</option><option value="0">One Time Fee</option></select>';
                else if(Qcount == 4)
                    var serach = '<select name="tableStats" class="form-control input-sm"><option value="" selected=""> </option><option value="completed">Completed</option><option value="pending">Pending</option><option value="canceled">Canceled</option></select>';
                else if(Qcount == 5)
                    var serach = '<select name="tablePayStats" class="form-control input-sm"><option value="" selected=""> </option><option value="completed">Paid</option><option value="pending">Unpaid</option><option value="fraud">Fraud</option><option value="refunded">Refunded</option><option value="chargeback">Chargeback</option><option value="reversed">Reversed</option><option value="canceled">Canceled</option></select>';
                else
                    var serach = '<input class="form-control input-sm" type="text" placeholder="Search ' + title + '" />';
                $(this).html('');
                $(serach).appendTo(this).keyup(function(){
                    table.fnFilter($(this).val(),i)
                });       
                }
                Qcount++;
            });
            
            $('#dbDate').daterangepicker({singleDatePicker: true, timePicker: false, format: 'MM/DD/YYYY'});
             
            $('#dbDate').on('change', function() {
            var selVal = jQuery('#dbDate').val();
            table.fnFilter(selVal,2);
            });
                
            $('select[name="tableBill"]').on('change', function() {
            var selVal = jQuery('select[name="tableBill"]').val();
            table.fnFilter(selVal,3);
            });
            
            $('select[name="tableStats"]').on('change', function() {
            var selVal = jQuery('select[name="tableStats"]').val();
            table.fnFilter(selVal,4);
            });
            
            $('select[name="tablePayStats"]').on('change', function() {
            var selVal = jQuery('select[name="tablePayStats"]').val();
            table.fnFilter(selVal,5);
            });
        });
    </script>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo $p_title; ?>  
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="/admin/?route=orders"><i class="fa fa-file-text-o"></i> Admin</a></li>
            <li class="active"><?php echo $p_title; ?> </li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
                
                <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#manage" data-toggle="tab"><?php echo $headTitle; ?> </a></li>
                  <li><a href="#settings" data-toggle="tab">Order Settings</a></li>
                </ul>
                
                <div class="tab-content">
                
                <div class="tab-pane active" id="manage" >
                
                <br />
                <?php
                if(isset($msg)){
                    echo $msg;
                } if(isset($_GET['view'])) {?>
                

                <ul class="nav nav-tabs">
                  <li class="active"><a data-toggle="tab" href="#orderDetails">Overview</a></li>
                  <li><a data-toggle="tab" href="#payDetails">Payment Details</a></li>
                  <li><a data-toggle="tab" href="#orderCom">Order Comments</a></li>
                  <li><a data-toggle="tab" href="#orderLog">Order Log</a></li>
                  <li><a target="_blank" href="/invoice/<?php echo $orderInfo['id']; ?>/view">View Invoice</a></li>
                  <li><a href="/admin/?route=orders">Go Back</a></li>
                </ul>

                <div class="tab-content" style="margin-bottom: 20px;">
                  <div id="orderDetails" class="tab-pane fade in active">
                    
                    <table style="margin-top: 20px;" class="table table-hover table-bordered">
                        <tbody>
                            <tr><th style="width: 20%;">Order ID:</th><td>#<?php echo $orderInfo['id']; ?></td></tr>
                            <tr><th style="width: 20%;">Invoice No:</th><td><?php echo $orderInfo['invoice_prefix']; ?></td></tr>
                            <tr><th style="width: 20%;">Customer(Username):</th><td><?php echo $orderInfo['username']; ?></td></tr>
                            <tr><th style="width: 20%;">E-Mail:</th><td><?php echo $userInfo['email_id']; ?></td></tr>
                            <tr><th style="width: 20%;">Telephone:</th><td><?php echo $premiumUserInfo['telephone']; ?></td></tr>
                            <tr><th style="width: 20%;">Plan Name:</th><td><?php echo $orderInfo['plan_name']; ?></td></tr>
                            <tr><th style="width: 20%;">Amount:</th><td><?php echo $currencySymbol[0].$orderInfo['amount']; ?></td></tr>
                            <tr><th style="width: 20%;">Total (Including Tax):</th><td><?php echo $currencySymbol[0].con2money_format($orderInfo['amount_tax'],$orderInfo['currency_type']); ?></td></tr>
                            <tr><th style="width: 20%;">Billing Type:</th><td><?php echo $billType; ?></td></tr>
                            <tr><th style="width: 20%;">Order Status:</th><td><?php echo ucfirst($orderInfo['status']); ?></td></tr>
                            <tr><th style="width: 20%;">Payment Status:</th><td><?php echo $viewPayStatus; ?></td></tr>
                            <tr><th style="width: 20%;">IP Address:</th><td><?php echo $orderInfo['ip_address']; ?></td></tr>
                            <tr><th style="width: 20%;">Date:</th><td><?php echo $orderInfo['date']; ?></td></tr>
                            <tr><th style="width: 20%;">Expiry Date:</th><td><?php echo $expDate; ?></td></tr>
                        </tbody>
                    </table>

                  </div>
                  <div id="payDetails" class="tab-pane fade">
                
                    <table style="margin-top: 20px;" class="table table-hover table-bordered">
                        <tbody>
                            <tr><th style="width: 20%;">Payment Method:</th><td><?php echo $paymentInfo['gateway_title']; ?></td></tr>
                            <tr><th style="width: 20%;">Transaction ID:</th><td><?php echo $orderInfo['transaction_id']; ?></td></tr>
                            <tr><th style="width: 20%;">First Name:</th><td><?php echo $premiumUserInfo['firstname']; ?></td></tr>
                            <tr><th style="width: 20%;">Last Name:</th><td><?php echo $premiumUserInfo['lastname']; ?></td></tr>
                            <tr><th style="width: 20%;">Company:</th><td><?php echo $premiumUserInfo['company']; ?></td></tr>
                            <tr><th style="width: 20%;">Address Line 1:</th><td><?php echo $premiumUserInfo['address1']; ?></td></tr>
                            <tr><th style="width: 20%;">Address Line 2:</th><td><?php echo $premiumUserInfo['address2']; ?></td></tr>
                            <tr><th style="width: 20%;">Postcode:</th><td><?php echo $premiumUserInfo['postcode']; ?></td></tr>
                            <tr><th style="width: 20%;">Region / State:</th><td><?php echo $premiumUserInfo['statestr']; ?></td></tr>
                            <tr><th style="width: 20%;">Region / State Code:</th><td><?php echo $premiumUserInfo['state']; ?></td></tr>
                            <tr><th style="width: 20%;">Country:</th><td><?php echo $userCountry; ?></td></tr>
                            <tr><th style="width: 20%;">E-Mail:</th><td><?php echo $userInfo['email_id']; ?></td></tr>
                            <tr><th style="width: 20%;">Telephone:</th><td><?php echo $premiumUserInfo['telephone']; ?></td></tr>
                        </tbody>
                    </table>
                    
                  </div>
                  <div id="orderCom" class="tab-pane fade">
                                  
                    <table style="margin-top: 20px;" class="table table-hover table-bordered">
                        <tbody>
                        <?php foreach($orderComments as $orderComment){
                         $data = explode('|:|',$orderComment);
                         echo '<tr><th style="width: 20%;">'.$data[0].'</th><td>'.nl2br(strEOL($data[1])).'</td></tr>';
                        }?>
                        </tbody>
                    </table>
                    
                    <form method="POST" action="#">
                    <div class="form-group">
                        <label>Notify Customer:</label>
                        <input style="margin: 10px;" type="checkbox" name="notify" />
                    </div>
                    
                    <div class="form-group">
                        <label>Comment:</label>
                        <textarea name="myComment" class="form-control" rows="6"></textarea>
                    </div>
                    
                    <input type="hidden" name="addComment" value="1" />
                    <input type="hidden" name="addCommentID" value="<?php echo $orderInfo['id']; ?>" />
                    <input type="hidden" name="addCommentDate" value="<?php echo $orderInfo['date']; ?>" />
                    <input type="hidden" name="addCommentMail" value="<?php echo $userInfo['email_id']; ?>" />
                    <input type="submit" value="Add Comment" class="btn btn-danger" />
                    
                    </form>
                    
                  </div>
                  <div id="orderLog" class="tab-pane fade">
                    
                    <table style="margin-top: 20px;" class="table table-hover table-bordered">
                        <tbody>
                        <?php $orderLogs = array_reverse($orderLogs); foreach($orderLogs as $orderLog){
                         $data = explode('|:|',$orderLog);
                         $isArrCheck = text2array(Trim(strEOL($data[1])));
                         if($isArrCheck !== false){
                             if(is_array($isArrCheck)){
                                $tagData = 'Array Data <ul>';
                                foreach($isArrCheck as $key=>$val){
                                    $tagData .= '<li>'.$key.' : '.$val.'</li>';
                                }
                                $tagData .= '<ul>';
                                $data[1] = $tagData;
                            }
                        }
                         echo '<tr><th style="width: 20%;">'.$data[0].'</th><td>'.$data[1].'</td></tr>';
                        }?>
                        </tbody>
                    </table>
                  </div>
                </div>
                
                <?php } elseif(isset($_GET['edit'])){ ?> 
                
                <form method="POST" action="#">
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="orderID">Order ID:</label>
                            <input type="text" disabled="" value="#<?php echo $orderInfo['id']; ?>" name="orderID" class="form-control" />
                        </div>
                        
                        <div class="form-group">
                            <label for="invoice_prefix">Invoice No:</label>
                            <input type="text" disabled="" value="<?php echo $orderInfo['invoice_prefix']; ?>" name="invoice_prefix" class="form-control" />
                        </div>
                        
                        <div class="form-group">
                        <label for="orderStatus">Order Status</label>
                        <select class="form-control" name="orderStatus">
                            <option <?php echo $orderStatus1; ?> value="completed">Completed</option>
                            <option <?php echo $orderStatus2; ?> value="rec_completed">Recurring Completed</option>
                            <option <?php echo $orderStatus3; ?> value="pending">Pending</option>
                            <option <?php echo $orderStatus4; ?> value="rec_pending">Recurring Pending</option>
                            <option <?php echo $orderStatus5; ?> value="canceled">Canceled</option>
        				</select>
                        </div>
                        
                        <div class="form-group">
                        <label for="payStatus">Payment Status</label>
                        <select class="form-control" name="payStatus">
                            <option <?php echo $payStatus1; ?> value="completed">Paid</option>
                            <option <?php echo $payStatus2; ?> value="pending">Unpaid</option>
                            <option <?php echo $payStatus3; ?> value="fraud">Fraud</option>
                            <option <?php echo $payStatus4; ?> value="refunded">Refunded</option>
                            <option <?php echo $payStatus5; ?> value="chargeback">Chargeback</option>
                            <option <?php echo $payStatus6; ?> value="reversed">Reversed</option>
                            <option <?php echo $payStatus7; ?> value="canceled">Canceled</option>
        				</select>
                        </div>
                        
                        <div class="form-group">
                        <label for="amount">Plan Amount:<small>(Without Tax)</small></label>
                        <input type="text" value="<?php echo $orderInfo['amount']; ?>" name="amount" class="form-control" />
                        </div>
                        
                    </div>
                    
                    <div class="col-md-6">
                    
                        <div class="form-group">
                            <label for="username">Customer(Username):</label>
                            <input type="text" disabled="" value="<?php echo $orderInfo['username']; ?>" name="username" class="form-control" />
                        </div>
                        
                        <div class="form-group">
                            <label for="email">E-Mail:</label>
                            <input type="text" disabled="" value="<?php echo $userInfo['email_id']; ?>" name="email" class="form-control" />
                        </div>
                        
                        <div class="form-group">
                        <label for="plan">Subscribed Plan</label>
                        <select class="form-control" name="plan">
                            <?php echo $planListData; ?>
        				</select>
                        </div>
                        
                        <div class="form-group">
                        <label for="billType">Billing Type</label>
                        <select class="form-control" name="billType">
                            <option <?php echo $oneBillType; ?> value="0">One Time Fee</option>
                            <option <?php echo $recBillType; ?> value="1">Recurring Billing</option>
        				</select>
                        </div>
                        
                        <div class="form-group">
                        <label for="total">Total:<small>(Including Tax)</small></label>
                        <input type="text" value="<?php echo $orderInfo['amount_tax']; ?>" name="total" class="form-control" />
                        </div>
                    
                    </div>
                </div>
   
   
                    <input type="hidden" value="1" name="editOrder" />
                    <br />
                    <input class="btn btn-primary" type="submit" value="Save Order Settings" name="submit" />
                    <br />                    <br />                    <br />
                </form>
                
                <?php }else { ?>
                
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="mySitesTable">
                	<thead>
                		<tr>
                          <th>Order ID</th>
                          <th>Customer</th>
                          <th>Order Date</th>
                          <th>Billing Type</th>
                          <th>Status</th>
                          <th>Payment Status</th>
                          <th>Total</th>
                          <th>Actions</th>
                		</tr>
                	</thead>  
                    <thead>
                  		<tr>
                          <td>Order ID</td>
                          <td>Customer</td>
                          <td>Order Date</td>
                          <td>Billing Type</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                		</tr>
                    </thead>       
                </table>
                <?php } ?>
                </div>
                
                <div class="tab-pane" id="settings" >
                <br />
                <?php
                if(isset($msg)){
                    echo $msg;
                } ?>
                <form action="#" method="POST">
                <div class="form-group">
                <label for="defaultStats">Default Order Status while processing:</label>
                <select disabled="" class="form-control" name="defaultStats">
					<option value="pending">Pending</option>
					<option value="canceled">Canceled</option>
					<option value="completed">Completed</option>
				</select>
                </div>
                
                <div class="form-group">
                  <label for="cur_type">Default Currency Symbol</label>
                    <select class="form-control" name="cur_type">
                        <?php echo $defaultCurrencyData; ?>
                    </select>
                </div>
                        
                <div class="form-group">
                <label for="checkout">Checkout Terms:</label>
                <select class="form-control" name="checkout">
					<option <?php echo $checkout ? 'selected=""' : ''; ?> value="yes">Yes</option>
					<option <?php echo $checkout ? '' : 'selected=""'; ?> value="no">No</option>
				</select>
                </div>
                
                <div class="form-group">
                    <label for="checkoutPage">Checkout Terms Page:</label>
                    <input type="text" placeholder="Enter your terms page" value="<?php echo $checkoutPage; ?>" name="checkoutPage" class="form-control" />
                </div>
                <input type="hidden" value="1" name="orderSettings" />
                <input type="submit" name="save" value="Save" class="btn btn-primary"/>
                <br />
                </form>
                <br />
                </div>
                
                </div></div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->