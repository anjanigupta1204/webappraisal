<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright © 2016 ProThemes.Biz
 *
 */
?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo $p_title; ?>  
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="/admin/?route=payment-gateway"><i class="fa fa-money"></i> Admin</a></li>
            <li class="active"><?php echo $p_title; ?> </li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo $boxTitle; ?></h3>
                  <?php if(isset($_GET['settings'])){?>
                  <div style="position:absolute; top:4px; right:15px;">
                  <a href="/admin/?route=payment-gateway" class="btn btn-primary"><i class="fa fa-arrow-circle-left"></i> Go Back</a>
                  </div>
                  <?php } ?>
                </div><!-- /.box-header -->
                <div class="box-body">
                <?php
                if(isset($msg)){
                    echo $msg;
                }if(!isset($_GET['settings'])){?>
                  <table id="paymentTable" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Sort Order</th>
                        <th>Gateway Name</th>
                        <th>Status</th>
                        <th>Settings</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach($paymentGatewayData as $paymentGateway){
                        $isActive = filter_var($paymentGateway[5], FILTER_VALIDATE_BOOLEAN);
                        
                        if($isActive)
                            $isActiveLink = '<a href="?route=payment-gateway&status=2&id='.$paymentGateway[2].'" class="label label-success">Active</a>';
                        else
                            $isActiveLink = '<a href="?route=payment-gateway&status=1&id='.$paymentGateway[2].'" class="label label-danger">Disabled</a>';
                
                        echo '<tr>
                        <td>'.$paymentGateway[0].'</td>
                        <td>'.$paymentGateway[1].'</td>
                        <td>'.$isActiveLink.'</td>
                        <td><a class="btn btn-primary btn-sm" href="?route=payment-gateway&id='.$paymentGateway[2].'&settings">Settings</a></td>
                      </tr>';
                    }
                    ?>

                    </tbody>
                  </table>
                  <?php } else { ?>
                  <form action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-8">
                        
                        <div class="form-group">
                        <label for="gateway">Enable the payment gateway </label>
                        <select class="form-control" name="gateway">
    						<option <?php echo $gateway ? 'selected=""' : ''; ?> value="yes">Activate</option>
    						<option <?php echo $gateway ? '' : 'selected=""'; ?> value="no">Deactivate</option>
    					</select>
                        </div>
                        
                        <div class="form-group">
                          <label for="gateway_title">Gateway Name</label>
                          <input type="text" placeholder="Enter your gateway name" value="<?php echo $gateway_title; ?>" name="gateway_title" class="form-control" />
                        </div>
                        
                        <div class="form-group">
                          <label for="sort_order">Sort Order</label>
                          <input type="text" placeholder="Enter your sort order" value="<?php echo $sort_order; ?>" name="sort_order" class="form-control" />
                        </div>
                    </div>
                </div>
                  <?php
                  $viewPath = ADMIN_PLG_DIR.'payments'.DIRECTORY_SEPARATOR.$arrData['gateway_name'].DIRECTORY_SEPARATOR.'view.php';
                  if(file_exists($viewPath))
                    require($viewPath); 
                  ?>
                  <br />
                  <input type="hidden" name="<?php echo $arrData['gateway_name']; ?>" value="<?php echo $arrData['id']; ?>" />
                  <input type="submit" name="save" value="Save Settings" class="btn btn-primary"/>
                   <br /> <br />
                  </form>
                  <?php } ?>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
      
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
    <script type="text/javascript">
      $(function () {
        $('#paymentTable').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>