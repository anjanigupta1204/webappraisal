<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright � 2016 ProThemes.Biz
 *
 */
?>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    
    <script type="text/javascript" language="javascript" src="<?php echo $theme_path; ?>dist/js/jquery.dataTables.js"></script>
    
    <script type="text/javascript" language="javascript" class="init">
    
    $(document).ready(function() {
    	$('#mySitesTable').dataTable( {
    		"processing": true,
    		"serverSide": true,
    		"ajax": "/admin/?route=premium_ajax&manageCustomers"
    	} );
    } );
    
    </script>
    
   
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo $p_title; ?>  
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="/admin/?route=premium-clients"><i class="fa fa-group"></i> Admin</a></li>
            <li class="active"><?php echo $p_title; ?> </li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"><?php echo $headTitle; ?></h3>
                </div><!-- /.box-header -->

                <div class="box-body">
                <?php
                if(isset($msg)){
                    echo $msg;
                }?>
                <br />
                
                <?php if(isset($_GET['details'])){ ?>
                <div class="widget widget-table action-table">
                    <div class="widget-header"> <i class="icon-th-list"></i>
                      <h3><?php echo $user_username.' Details'; ?></h3>
                    </div>
                    <!-- /widget-header -->
                    <div class="widget-content">
                      <table class="table table-striped table-bordered">
                        <tbody>
                          <tr>
                            <td style="width: 30%;">  Username </td>
                                        <td> <?php echo $user_username; ?> </td>   
                          </tr>
                                        <tr>
                            <td style="width: 200px;"> Email ID </td>
                            <td> <?php echo $user_email_id; ?> </td>
                          </tr>
                          <tr>
                            <td style="width: 200px;"> Platform </td>
                             <td> <?php echo $user_platform; ?> </td>
                          </tr>
                          
                          <tr>
                            <td style="width: 200px;"> Oauth ID </td>
                            <td> <?php echo $user_oauth_uid; ?> </td>
                          </tr>
                                    
                                        <tr>
                            <td style="width: 200px;"> Current Status </td>
                             <td> <?php echo $user_verified; ?> </td>
                          </tr>
                                        <tr>
                            <td style="width: 200px;"> User IP </td>
                             <td> <?php echo $user_ip; ?> </td>
                          </tr>
                          <tr>
                            <td style="width: 200px;"> User Since </td>
                             <td> <?php echo $user_date; ?> </td>
                          </tr>        
                        
                        </tbody>
                      </table>
                      
              <?php if($addInfo) { ?>
                <div class="clear" style="margin-bottom: 10px;"></div>

                    <div class="widget-header"> <i class="icon-th-list"></i>
                      <h3>Personal Information:</h3>
                    </div>

                      <table class="table table-striped table-bordered">
                    <tbody>
                        <tr>
                            <td style="width: 30%;">First Name</td>
                            <td><span><?php echo $firstname; ?></span></td>
                        </tr> 
                        <tr>
                            <td style="width: 200px;">Last Name</td>
                            <td><span><?php echo $lastname; ?></span></td>         
                        </tr> 
                        <?php if($company != '') { ?>
                        <tr>
                            <td style="width: 200px;">Company</td>
                            <td><span><?php echo $company; ?></span></td>         
                        </tr> 
                        <?php } ?>
                        <tr>
                            <td style="width: 200px;">Address Line 1</td>
                            <td><span><?php echo $address1; ?></span></td>         
                        </tr> 
                        <?php if($address2 != '') { ?>
                        <tr>
                            <td style="width: 200px;">Address Line 2</td>
                            <td><span><?php echo $address2; ?></span></td>         
                        </tr> 
                        <?php } ?>
                        <tr>
                            <td style="width: 200px;">City</td>
                            <td><span><?php echo $city; ?></span></td>         
                        </tr>
                        <tr>
                            <td style="width: 200px;">State</td>
                            <td><span><?php echo $statestr; ?></span></td>         
                        </tr> 
                        <tr>
                            <td style="width: 200px;">Country</td>
                            <td><span><?php echo country_code_to_country($country); ?></span></td>         
                        </tr>  
                        <tr>
                            <td style="width: 200px;">Post Code</td>
                            <td><span><?php echo $postcode; ?></span></td>         
                        </tr> 
                        <tr>
                            <td style="width: 200px;">Telephone</td>
                            <td><span><?php echo $telephone; ?></span></td>         
                        </tr> 
                    </tbody>
                    </table>
                <?php } ?>
                
                      <div>
                        <a href="../admin/?route=premium-clients" class="btn btn-primary">Go Back</a>
                      </div>
                    </div>
                    <!-- /widget-content --> 
                  </div>    
 

                <?php } else { ?>
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="mySitesTable">
                	<thead>
                		<tr>
                              <th>Username</th>
                              <th>Email ID</th>
                              <th>User Since</th>
                              <th>First Name</th>
                              <th>Last Name</th>
                              <th>Country</th>
                              <th>Ban User</th>
                              <th>Actions</th>
                		</tr>
                	</thead>         
                    <tbody>                        
                    </tbody>
                </table>
                <?php } ?>
                <br />
                
                </div><!-- /.box-body -->

              </div><!-- /.box -->
      
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
