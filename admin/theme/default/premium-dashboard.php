<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright � 2015 ProThemes.Biz
 *
 */
?>

<style>
.orderStats {
    font-weight: bold;
}
.infobox-container {
    font-size: 0;
    text-align: center;
}
.infobox {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-color: #fff;
    border-color: #d8d8d8 !important;
    border-image: none;
    border-radius: 0;
    border-style: dotted;
    border-width: 1px;
    box-shadow: none;
    color: #555;
    padding: 8px 3px 6px 9px;
    position: relative;
    text-align: left;
    margin-bottom: 14px;
    vertical-align: middle;
}
.infobox > .infobox-icon {
    display: inline-block;
    vertical-align: top;
    width: 44px;
}
.infobox > .infobox-icon > [class*="icon-"] {
    background-color: transparent;
    border: 0 none;
    border-radius: 100%;
    box-shadow: 1px 1px 0 rgba(0, 0, 0, 0.2);
    display: inline-block;
    height: 42px;
    margin: 0;
    padding: 1px 1px 0 2px;
    position: relative;
    text-align: center;
}
.infobox > .infobox-icon > [class*="icon-"]::before {
    background-color: rgba(255, 255, 255, 0.2);
    border-radius: 100%;
    color: rgba(255, 255, 255, 0.9);
    display: block;
    font-size: 24px;
    padding: 6px 0 7px;
    text-align: center;
    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.14);
    width: 40px;
}
.infobox .infobox-content {
    color: #555;
}
.infobox .infobox-content:first-child {
    font-weight: bold;
}
.infobox > .infobox-data {
    border: 0 none;
    display: inline-block;
    font-size: 13px;
    line-height: 21px;
    padding-left: 8px;
    position: relative;
    text-align: left;
    top: 0;
}
.infobox > .infobox-data > .infobox-data-number {
    display: block;
    font-size: 22px;
    margin: 2px 0 4px;
    position: relative;
    text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.15);
}
.infobox > .infobox-data > .infobox-text {
    display: block;
    font-size: 16px;
    margin: 2px 0 4px;
    position: relative;
    text-shadow: none;
}
.infobox.no-border {
    border: medium none !important;
}
.infobox-purple {
    border-color: #6f3cc4;
    color: #6f3cc4;
}
.infobox-purple > .infobox-icon > [class*="icon-"] {
    background-color: #6f3cc4;
}
.infobox-purple.infobox-dark {
    background-color: #6f3cc4;
    border-color: #6f3cc4;
}
.infobox-purple2 {
    border-color: #5f47b0;
    color: #5f47b0;
}
.infobox-purple2 > .infobox-icon > [class*="icon-"] {
    background-color: #5f47b0;
}
.infobox-purple2.infobox-dark {
    background-color: #5f47b0;
    border-color: #5f47b0;
}
.infobox-pink {
    border-color: #cb6fd7;
    color: #cb6fd7;
}
.infobox-pink > .infobox-icon > [class*="icon-"] {
    background-color: #cb6fd7;
}
.infobox-pink.infobox-dark {
    background-color: #cb6fd7;
    border-color: #cb6fd7;
}
.infobox-blue {
    border-color: #6fb3e0;
    color: #6fb3e0;
}
.infobox-blue > .infobox-icon > [class*="icon-"] {
    background-color: #6fb3e0;
}
.infobox-blue.infobox-dark {
    background-color: #6fb3e0;
    border-color: #6fb3e0;
}
.infobox-blue2 {
    border-color: #3983c2;
    color: #3983c2;
}
.infobox-blue2 > .infobox-icon > [class*="icon-"] {
    background-color: #3983c2;
}
.infobox-blue2.infobox-dark {
    background-color: #3983c2;
    border-color: #3983c2;
}
.infobox-blue3 {
    border-color: #1144eb;
    color: #1144eb;
}
.infobox-blue3 > .infobox-icon > [class*="icon-"] {
    background-color: #1144eb;
}
.infobox-blue3.infobox-dark {
    background-color: #1144eb;
    border-color: #1144eb;
}
.infobox-red {
    border-color: #d53f40;
    color: #d53f40;
}
.infobox-red > .infobox-icon > [class*="icon-"] {
    background-color: #d53f40;
}
.infobox-red.infobox-dark {
    background-color: #d53f40;
    border-color: #d53f40;
}
.infobox-brown {
    border-color: #c67a3e;
    color: #c67a3e;
}
.infobox-brown > .infobox-icon > [class*="icon-"] {
    background-color: #c67a3e;
}
.infobox-brown.infobox-dark {
    background-color: #c67a3e;
    border-color: #c67a3e;
}
.infobox-wood {
    border-color: #7b3f25;
    color: #7b3f25;
}
.infobox-wood > .infobox-icon > [class*="icon-"] {
    background-color: #7b3f25;
}
.infobox-wood.infobox-dark {
    background-color: #7b3f25;
    border-color: #7b3f25;
}
.infobox-light-brown {
    border-color: #cebea5;
    color: #cebea5;
}
.infobox-light-brown > .infobox-icon > [class*="icon-"] {
    background-color: #cebea5;
}
.infobox-light-brown.infobox-dark {
    background-color: #cebea5;
    border-color: #cebea5;
}
.infobox-orange {
    border-color: #e8b110;
    color: #e8b110;
}
.infobox-orange > .infobox-icon > [class*="icon-"] {
    background-color: #e8b110;
}
.infobox-orange.infobox-dark {
    background-color: #e8b110;
    border-color: #e8b110;
}
.infobox-orange2 {
    border-color: #f79263;
    color: #f79263;
}
.infobox-orange2 > .infobox-icon > [class*="icon-"] {
    background-color: #f79263;
}
.infobox-orange2.infobox-dark {
    background-color: #f79263;
    border-color: #f79263;
}
.infobox-green {
    border-color: #9abc32;
    color: #9abc32;
}
.infobox-green > .infobox-icon > [class*="icon-"] {
    background-color: #9abc32;
}
.infobox-green.infobox-dark {
    background-color: #9abc32;
    border-color: #9abc32;
}
.infobox-green2 {
    border-color: #0490a6;
    color: #0490a6;
}
.infobox-green2 > .infobox-icon > [class*="icon-"] {
    background-color: #0490a6;
}
.infobox-green2.infobox-dark {
    background-color: #0490a6;
    border-color: #0490a6;
}
.infobox-grey {
    border-color: #999;
    color: #999;
}
.infobox-grey > .infobox-icon > [class*="icon-"] {
    background-color: #999;
}
.infobox-grey.infobox-dark {
    background-color: #999;
    border-color: #999;
}
.infobox-black {
    border-color: #393939;
    color: #393939;
}
.infobox-black > .infobox-icon > [class*="icon-"] {
    background-color: #393939;
}
.infobox-black.infobox-dark {
    background-color: #393939;
    border-color: #393939;
}
.infobox-dark {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: transparent !important;
    border-image: none;
    border-style: none;
    border-width: 0;
    color: #fff;
    margin: 1px 1px 0 0;
    padding: 4px;
}
.infobox-dark > .infobox-icon > [class*="icon-"], .infobox-dark > .infobox-icon > [class*="icon-"]::before {
    background-color: transparent;
    border-radius: 0;
    box-shadow: none;
    font-size: 30px;
    text-shadow: none;
}
.infobox-dark > .infobox-icon > [class*="icon-"]::before {
    opacity: 1;
}
.infobox-dark .infobox-content {
    color: #fff;
}
</style>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo $p_title; ?> 
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li class="active"><?php echo $p_title; ?> </li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          
          <!-- Main row -->
          <div class="row">
          <!-- Left col -->
          <section class="col-lg-4 connectedSortable">
          
              <!-- Custom tabs (Charts with tabs)-->
              <div class="nav-tabs-custom">
                <!-- Tabs within a box -->
                <ul class="nav nav-tabs pull-right">
                  <li class="pull-left header"><i class="fa fa-pie-chart"></i>  Overview</li>
                </ul>
                <div class="tab-content no-padding">
                
                <div class="row" style="margin: 5px;">
                    <br />
                    <div class="col-md-6" style="margin-top:7px;margin-bottom:7px;">

                        <div class="infobox infobox-blue">
							<div class="infobox-icon">
								<i class="ion ion-bag icon-comments"></i>
							</div>

							<div class="infobox-data">
								<span class="infobox-data-number"><?php echo $todayOrderCount; ?></span>
								<div class="infobox-content">New Orders</div>
							</div>
						</div>
                        
                        <div class="infobox infobox-red">
							<div class="infobox-icon">
								<i class="fa fa-key icon-comments"></i>
							</div>

							<div class="infobox-data">
								<span class="infobox-data-number"><?php echo $aWaitingActivation; ?></span>
								<div class="infobox-content">Activation</div>
							</div>
						</div>
                        
                        <div class="infobox infobox-orange">
							<div class="infobox-icon">
								<i class="fa fa-shopping-cart icon-comments"></i>
							</div>

							<div class="infobox-data">
								<span class="infobox-data-number"><?php echo $totalOrders; ?></span>
								<div class="infobox-content">Total Orders</div>
							</div>
						</div>
                        
                    </div>
                    
                    <div class="col-md-6" style="margin-top:7px;margin-bottom:7px;">

                        <div class="infobox infobox-green">
							<div class="infobox-icon">
								<i class="fa fa-gift icon-comments"></i>
							</div>

							<div class="infobox-data">
								<span class="infobox-data-number"><?php echo $currencySymbol[0].$todaySalesCount; ?></span>
								<div class="infobox-content">Today Sales</div>
							</div>
						</div>
                        
                        <div class="infobox infobox-purple">
							<div class="infobox-icon">
								<i class="fa fa-tags icon-comments"></i>
							</div>

							<div class="infobox-data">
								<span class="infobox-data-number"><?php echo $totalActivePlans; ?></span>
								<div class="infobox-content">Active Plans</div>
							</div>
                        </div>
                        
                        <div class="infobox infobox-wood">
							<div class="infobox-icon">
								<i class="fa fa-users icon-comments"></i>
							</div>

							<div class="infobox-data">
								<span class="infobox-data-number"><?php echo $totalUsers; ?></span>
								<div class="infobox-content">Clients</div>
							</div>
						</div>

                    </div>                        
                    <br /> &nbsp; <br />
                </div>
      
                </div>
              </div><!-- /.nav-tabs-custom -->
              
          </section><!-- /.Left col -->
          
          <section class="col-lg-8 connectedSortable">

            <!-- Custom tabs (Charts with tabs)-->
              <div class="nav-tabs-custom">
                <!-- Tabs within a box -->
                <ul class="nav nav-tabs pull-right">
                  <li class="pull-left header"><i class="fa fa-line-chart"></i>  Sales Report</li>
                </ul>
                <div class="tab-content no-padding">
                
                  <!-- Morris chart - Sales -->
                  <div class="chart tab-pane active" id="sales-chart" style="position: relative; height: 300px;"></div>

                </div>
              </div><!-- /.nav-tabs-custom -->
              
                            
          </section>
          
          <section class="col-lg-12 connectedSortable">
            <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"> <i class="fa fa-tasks"></i> &nbsp; Latest Orders</h3>
                  <div class="box-tools pull-right">
                    <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-minus"></i></button>
                    <button data-widget="remove" class="btn btn-box-tool"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>Order ID</th>
                          <th>Customer</th>
                          <th>Plan Name</th>
                          <th>Order Status</th>
                          <th>Payment Status</th>
                          <th>Date Added</th>
                          <th>Total</th>
                        </tr>
                      </thead>
                      <tbody>
                      
                      <?php foreach($orderList as $order){ ?>
                        <tr>
                          <td><a href="/admin/?route=orders&view=<?php echo $order[0]; ?>"><?php echo $order[0]; ?></a></td>
                          <td><?php echo $order[1]; ?></td>
                          <td><?php echo $order[2]; ?></td>
                          <td><span class="label label-<?php echo colorCode($order[3]); ?>"><?php echo ucfirst(str_replace('_',' ',$order[3])); ?></span></td>
                          <td><span class="label label-<?php echo colorCode($order[6]); ?>"><?php echo ucfirst($order[6]); ?></span></td>
                          <td><?php echo $order[4]; ?></td>
                          <td><?php echo $order[7].$order[5]; ?></td>
                        </tr>
                      <?php } ?>
            
                      </tbody>
                    </table>
                  </div><!-- /.table-responsive -->
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                  <a class="btn btn-sm btn-info btn-flat pull-left" href="/admin/?route=new-order">Place New Order</a>
                  <a class="btn btn-sm btn-default btn-flat pull-right" href="/admin/?route=orders">View All Orders</a>
                </div><!-- /.box-footer -->
              </div>	

          </section>
          
          </div><!-- /.Main row -->
      
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->