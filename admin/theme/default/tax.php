<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright � 2015 ProThemes.Biz
 *
 */
?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo $p_title; ?>  
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="/admin/?route=tax"><i class="fa fa-star"></i> Admin</a></li>
            <li class="active"><?php echo $p_title; ?> </li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
        

            <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $headTitle; ?> </h3>
                            <div style="position:absolute; top:4px; right:15px;">
                              <a href="/admin/?route=tax&new-rule" class="btn btn-primary"><i class="fa fa-plus-circle"></i> New Tax Rule </a>
                            </div>
                    </div><!-- /.box-header -->
                    <!-- form start -->

                
                <form action="#" method="POST">
                    <div class="box-body" style="width: 60%;">
                <?php
                if(isset($msg)){
                    echo $msg.'<br>';
                } if($newPage) {?>
                
                    <div class="form-group">
                      <label for="tax_title">Tax Name</label>
                      <input type="text" placeholder="Enter your tax name" value="<?php echo $tax_title; ?>" name="tax_title" class="form-control" />
                    </div>
                    
                    <div class="form-group">
                      <label for="tax_rate">Tax Rate</label>
                      <input type="text" placeholder="Enter your tax rate" value="<?php echo $tax_rate; ?>" name="tax_rate" class="form-control" />
                    </div>
                    
                    <div class="form-group">
                    <label for="rate_fixed">Is fixed amount or percentage </label>
                    <select class="form-control" name="rate_fixed">
						<option <?php echo $rate_fixed ? 'selected=""' : ''; ?> value="yes">Fixed Amount</option>
						<option <?php echo $rate_fixed ? '' : 'selected=""'; ?> value="no">Percentage</option>
					</select>
                    </div>
                    
                    <div class="form-group">
                    <label for="country">Country</label>
                    <select class="form-control" name="country">
                        <?php foreach($countriesList as $countryCode => $countryName) {
						  echo '<option '. ($countryCode == $dbCountryCode ? 'selected=""' : '').'value="'.$countryCode.'">'.$countryName.'</option>';
                        } ?>
					</select>
                    </div>
                    
                    <div class="form-group">
                    <label for="tax_status">Activate the tax rule </label>
                    <select class="form-control" name="tax_status">
						<option <?php echo $status ? 'selected=""' : ''; ?> value="yes">Yes</option>
						<option <?php echo $status ? '' : 'selected=""'; ?> value="no">No</option>
					</select>
                    </div>
                    
                    <?php if(isset($_GET['new-rule'])) { ?>
                        <input type="hidden" value="1" name="newTax" />
                    <?php } else { ?>
                        <input type="hidden" value="1" name="editTax" />
                        <input type="hidden" value="<?php echo $tax_id; ?>" name="id" />
                    <?php } ?>
                
                <?php  } else { ?>
                        <div class="form-group">
                        <label for="tax_enable">Enable Tax system support </label>
                        <select class="form-control" name="tax_enable">
    						<option <?php echo $tax ? 'selected=""' : ''; ?> value="yes">Yes</option>
    						<option <?php echo $tax ? '' : 'selected=""'; ?> value="no">No</option>
    					</select>
                        </div>
                        <input type="hidden" value="1" name="taxEnable" />
                      <?php } ?>                  
                            <button type="submit" class="btn btn-primary">Save</button>
                         </div><!-- /.box-body -->
                    </form>
                </div>
           <?php  if(!$newPage) {     ?>
         <div class="box box-danger">
        <div class="box-header with-border">
            <!-- tools box -->

            <h3 class="box-title">
                Tax List
            </h3>
        </div>

        <div class="box-body">

        <table id="taxTable" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Tax Name</th>
            <th>Tax Rate</th>
            <th>Amount Type</th>
            <th>Country</th>
            <th>Status</th>
            <th>Edit</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
        <?php foreach($taxList as $tax){

            echo '<tr>
            <td>'.$tax[0].'</td>
            <td>'.$tax[1].'</td>
            <td>'.$tax[2].'</td>
            <td>'.$tax[3].'</td>
            <td>'.$tax[4].'</td>
            <td>'.$tax[5].'</td>
            <td>'.$tax[6].'</td>
          </tr>';
        }
        ?>

        </tbody>
      </table>


        </div><!-- /.box-body -->

        <div class="box-footer">

        </div>
    </div>
    
    <?php } ?>
      
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      <script type="text/javascript">
      $(function () {
        $('#taxTable').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
