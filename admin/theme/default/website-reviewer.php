<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright � 2015 ProThemes.Biz
 *
 */
?>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    
    <script type="text/javascript" language="javascript" src="<?php echo $theme_path; ?>dist/js/jquery.dataTables.js"></script>
    
    <script type="text/javascript" language="javascript" class="init">
    
    $(document).ready(function() {
    	$('#mySitesTable').dataTable( {
    		"processing": true,
    		"serverSide": true,
    		"ajax": "/admin/?route=premium_ajax&manageDomains"
    	} );
    } );
    
    </script>
    
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo $p_title; ?>  
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="/admin/?route=website-reviewer"><i class="fa fa-search"></i> Admin</a></li>
            <li class="active"><?php echo $p_title; ?> </li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                <li class="<?php echo $page1; ?>"><a href="#settings" data-toggle="tab">Settings</a></li>
                  <li class="<?php echo $page2; ?>"><a href="#manageDomains" data-toggle="tab">Cached Domains</a></li>
                </ul>
                <div class="tab-content">
                
                <div class="tab-pane <?php echo $page1; ?>" id="settings" >
                
                <form action="#" method="POST" enctype="multipart/form-data">
                <div class="box-body">
                <?php
                if(isset($msg)){
                    echo $msg;
                }?>
                    <div class="row" style="padding-left: 5px;">
                    <div class="col-md-8">
                    <br />
					<div class="form-group">
			            <div class="checkbox">
				           <label class="checkbox inline">
					          <input <?php if ($enable_cahced) echo 'checked="true"'; ?>
					          type="checkbox" name="cahced" /> Index cached domains into search engines
                           </label>
			            </div>
	  	           </div>
                   
                   <div class="form-group">
                        <label for="limit">How much domains allowed for Visitors(Freely)?</label>
                        <input type="text" placeholder="Enter your visitors limit" id="limit" name="limit" value="<?php echo $orderData['free_limit']; ?>" class="form-control" />
                   </div>
                
                    <div class="form-group">
                    <label>Restrict some stats from Visitors with 'Upgrade Subscription' button:</label>
                    <select name="reviewerList[]" class="form-control select2" multiple="multiple" data-placeholder="Which stats need premium access?" style="width: 100%;">
                      <?php 
                            foreach($reviewerList as $reviewer=>$reviewerName){
                                if (in_array($reviewer, $orderData['reviewer_list']))
                                    echo '<option selected="" value="'.$reviewer.'">'.$reviewerName.'</option>';
                                else
                                    echo '<option value="'.$reviewer.'">'.$reviewerName.'</option>';
                            }
                      ?>
                    </select>
                  </div><!-- /.form-group -->
                 </div>
                </div> 
                <br />
                <div class="box-header with-border">
                  <h3 class="box-title">PDF Report Settings</h3>
                </div><!-- /.box-header -->
                
                <div class="row" style="padding-left: 5px;">
                    <div class="col-md-8">
                    <br />
                       <div class="form-group">
                            <label for="pdfCopyright">PDF Copyright Text:</label>
                            <input type="text" placeholder="Enter your PDF Copyright Text" id="pdfCopyright" name="pdfCopyright" value="<?php echo $pdfCopyright; ?>" class="form-control" />
                       </div>
                       <br />
                       <div class="form-group">
                            <label for="headerLogoBox">PDF Header Logo:</label><br />
                            <img class="headerLogoBox" id="headerLogoBox" alt="Header Logo" src="<?php echo $headerLogo; ?>" />   
                            <br />
                            Recommended Size: (JPEG 150x75px)
                            <input type="file" name="headerUpload" id="headerUpload" class="btn btn-default" />
                       </div>
                       <br />
                       <div class="form-group">
                            <label for="footerLogoBox">PDF Footer Logo:</label><br />
                            <img class="footerLogoBox" id="footerLogoBox" alt="Footer Logo" src="<?php echo $footerLogo; ?>" />   
                            <br />
                            Recommended Size: (JPEG 100x50px)
                            <input type="file" name="footerUpload" id="footerUpload" class="btn btn-default" />
                       </div>                
                    </div>
                    </div> 
                
                <br /><input type="submit" name="save" value="Save Settings" class="btn btn-primary"/>
                <br /> <br />
                
                </div><!-- /.box-body -->
                </form>
              </div>
              
              <div class="tab-pane <?php echo $page2; ?>" id="manageDomains" >
                <br />
                <?php
                if(isset($msg)){
                    echo $msg;
                }?>
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="mySitesTable">
                	<thead>
                		<tr>
                              <th>Domain Name</th>
                              <th>Completely Cached</th>
                              <th>Website Score</th>
                              <th>Last Updated</th>
                              <th>Generate PDF</th>
                              <th>Delete</th>
                		</tr>
                	</thead>         
                    <tbody>                        
                    </tbody>
                </table>
                
                </div>
                
              </div>
              </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
