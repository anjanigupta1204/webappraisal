<?php

defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

if(!defined('SEO_ADDON_TOOLS')){
    $controller = "error";
    require(CON_DIR. $controller . '.php');
}

/*
 * @author Balaji
 * @name: Rainbow PHP Framework v1.0
 * @copyright � 2015 ProThemes.Biz
 *
 */



//HT03 - CSS Minifier
if($toolUid == 'HT03') {
        
    $controller = 'output'.D_S.'css-minifier';
   
    if($pointOut == '')
        $tab1 = 'active';
        
    elseif($pointOut == 'upload')
        $tab2 = 'active';
        
    if ($pointOut == 'output') {
    
        if ($_FILES['cssUpload']) {
            $comArr = array();
            $fileArr = conArrayFiles($_FILES['cssUpload']);
            foreach($fileArr as $cssFile){
                if($cssFile["name"] != ''){
                $targetDir = ROOT_DIR."uploads/exfiles/";
                $targetFilename = basename($cssFile["name"]);
                $baseFileName = $targetFilename;
                $uploadSs = 1;
                
                //Generate random file name
                $targetFile = $targetDir . rand(1, 9999999) . "_" . $targetFilename;
                
                //Check if file already exists
                if(file_exists($targetFile))
                    delFile($targetFile);
                    
                $checkFileType = pathinfo($targetFile, PATHINFO_EXTENSION);
                
                //Check file size
                if ($cssFile["size"] > 2000000) {
                    $comArr[] = array(0,'Sorry, your file is too large.',0,$baseFileName,0,false);
                    $uploadSs = 0;
                    
                } else {
                    //Allow certain file formats
                    if ($checkFileType != "css") {
                        $comArr[] = array(0,'Sorry, only CSS files are allowed.',0,$baseFileName,0,false);
                        $uploadSs = 0;
                    }
                }
                //Check if $uploadSs is set to 0 by an error
                if (!$uploadSs == 0) {
                    if (move_uploaded_file($cssFile["tmp_name"], $targetFile)) {
                        //Uploaded
                        $fileCSSData = getMyData($targetFile);
                        $fileCSSSize = strlen($fileCSSData);
                        $comCSSData = compressCSS($fileCSSData);
                        $comCSSSize = strlen($comCSSData);
                        $percentage = round((((int)$comCSSSize / (int)$fileCSSSize) * 100),1);
                        $comArr[] = array($fileCSSSize,$comCSSData,$comCSSSize,$baseFileName,$percentage,true);
                        delFile($targetFile);
                    } else {
                        $comArr[] = array(0,'Sorry, there was an error uploading your file.',0,$baseFileName,0,false);
                    }
                }
                }
            }
        }else{
            if (!isset($_POST['data']))
                die($lang['4']); //Malformed Request!
            if (!isset($error)) {
                $userCSSInput = raino_trim($_POST['data']);
                $fileCSSSize = strlen($userCSSInput);
                $comCss = compressCSS($userCSSInput);
                $comCSSSize = strlen($comCss);
                $percentage = round((((int)$comCSSSize / (int)$fileCSSSize) * 100),1);
            }
        }
    }
}


//HT01 - HTML Compressor
if($toolUid == 'HT01') {
        
    $controller = 'output'.D_S.'html-compressor';
   
    if($pointOut == '')
        $tab1 = 'active';
        
    elseif($pointOut == 'upload')
        $tab2 = 'active';
        
    if ($pointOut == 'output') {
    
        if ($_FILES['htmlUpload']) {
            $comArr = array();
            $fileArr = conArrayFiles($_FILES['htmlUpload']);
            foreach($fileArr as $htmlFile){
                if($htmlFile["name"] != ''){
                $targetDir = ROOT_DIR."uploads/exfiles/";
                $targetFilename = basename($htmlFile["name"]);
                $baseFileName = $targetFilename;
                $uploadSs = 1;
                
                //Generate random file name
                $targetFile = $targetDir . rand(1, 9999999) . "_" . $targetFilename;
                
                //Check if file already exists
                if(file_exists($targetFile))
                    unlink($targetFile);
                    
                $checkFileType = pathinfo($targetFile, PATHINFO_EXTENSION);
                
                //Check file size
                if ($htmlFile["size"] > 2000000) {
                    $comArr[] = array(0,'Sorry, your file is too large.',0,$baseFileName,0,false);
                    $uploadSs = 0;
                    
                } else {
                    //Allow certain file formats
                    if ($checkFileType != "html" && $checkFileType != "htm") {
                        $comArr[] = array(0,'Sorry, only HTML files are allowed.',0,$baseFileName,0,false);
                        $uploadSs = 0;
                    }
                }
                //Check if $uploadSs is set to 0 by an error
                if (!$uploadSs == 0) {
                    if (move_uploaded_file($htmlFile["tmp_name"], $targetFile)) {
                        //Uploaded
                        $fileHTMLData = file_get_contents($targetFile);
                        $fileHTMLSize = strlen($fileHTMLData);
                        $minifier = new minifier();
                        $comHTMLData = $minifier->minify($fileHTMLData);
                        $comHTMLSize = strlen($comHTMLData);
                        $percentage = round((((int)$comHTMLSize / (int)$fileHTMLSize) * 100),1);
                        $comArr[] = array($fileHTMLSize,$comHTMLData,$comHTMLSize,$baseFileName,$percentage,true);
                        unlink($targetFile);
                    } else {
                        $comArr[] = array(0,'Sorry, there was an error uploading your file.',0,$baseFileName,0,false);
                    }
                }
                }
            }
        }else{
            if (!isset($_POST['data']))
                die($lang['4']); //Malformed Request!
            if (!isset($error)) {
                $userHTMLInput = Trim($_POST['data']);
                $fileHTMLSize = strlen($userHTMLInput);
                $comHtml = compressHTML($userHTMLInput);
                $comHTMLSize = strlen($comHtml);
                $comHtml = str_replace(array('<textarea>','</textarea>','<TEXTAREA>','</TEXTAREA>'),array('<textarea&gt;','&lt;/textarea>','<TEXTAREA&gt;','&lt;/TEXTAREA>'),$comHtml);
                $percentage = round((((int)$comHTMLSize / (int)$fileHTMLSize) * 100),1);
            }
        }
    }
}


//HT04 - Image Optimizer
if($toolUid == 'HT04') {
        
    $controller = 'output'.D_S.'image-optimizer';
   
    if ($pointOut == 'output') {
    
        if ($_FILES['imageUpload']) {
            $comArr = array();
            $fileArr = conArrayFiles($_FILES['imageUpload']);
            foreach($fileArr as $imageFile){
                if($imageFile["name"] != ''){
                $targetDir = ROOT_DIR."uploads/exfiles/";
                $targetFilename = basename($imageFile["name"]);
                $baseFileName = $targetFilename;
                $uploadSs = 1;
                $checkRealImage = getimagesize($_FILES["imageUpload"]["tmp_name"]);
                
                // Check it is a image
                if ($checkRealImage !== false) {
                    
                //Generate random file name
                $targetFile = $targetDir . rand(1, 9999999) . "_" . $targetFilename;
                
                //Check if file already exists
                if(file_exists($targetFile))
                    delFile($targetFile);
                    
                $checkFileType = pathinfo($targetFile, PATHINFO_EXTENSION);
                
                //Check file size
                if ($imageFile["size"] > 2000000) {
                    $comArr[] = array(0,'Sorry, your file is too large.',0,$baseFileName,0,false);
                    $uploadSs = 0;
                    
                } else {
                    //Allow certain file formats
                    if ($checkFileType != "jpeg" && $checkFileType != "jpg" && $checkFileType != "png") {
                        $comArr[] = array(0,'Sorry, only image files are allowed.',0,$baseFileName,0,false);
                        $uploadSs = 0;
                    }
                }
                //Check if $uploadSs is set to 0 by an error
                if (!$uploadSs == 0) {
                    if (move_uploaded_file($imageFile["tmp_name"], $targetFile)) {
                        //Uploaded
                        $fileImageData = getMyData($targetFile);
                        $fileImageSize = strlen($fileImageData);
                        $imageInfoArr = compressImage($targetFile);
                        $comImageData = getMyData($targetFile);
                        $comImageSize = strlen($comImageData);
                        if($imageInfoArr[1] == 'image/jpeg' || $imageInfoArr[1] == 'image/png'){
                            $comImageData = 'data:'.$imageInfoArr[1].';base64,'.base64_encode($comImageData);
                            $percentage = round((((int)$comImageSize / (int)$fileImageSize) * 100),1);
                            $comArr[] = array($fileImageSize,$comImageData,$comImageSize,$baseFileName,$percentage,true);
                        }else{
                            $comArr[] = array(0,'Sorry, only image files are allowed.',0,$baseFileName,0,false);
                            $uploadSs = 0;
                        }
                        delFile($targetFile);
                    } else {
                        $comArr[] = array(0,'Sorry, there was an error uploading your file.',0,$baseFileName,0,false);
                    }
                }
                }else{
                    $comArr[] = array(0,'Sorry, only image files are allowed.',0,$baseFileName,0,false);
                    $uploadSs = 0;
                }
                }
            }
        }else{
            die($lang['4']); //Malformed Request!
        }
    }
}


//HT02 - JS Minifier
if($toolUid == 'HT02') {
        
    $controller = 'output'.D_S.'js-minifier';
   
    if($pointOut == '')
        $tab1 = 'active';
        
    elseif($pointOut == 'upload')
        $tab2 = 'active';
        
   if ($pointOut == 'output') {
    
        if ($_FILES['jsUpload']) {
            $comArr = array();
            $fileArr = conArrayFiles($_FILES['jsUpload']);
            foreach($fileArr as $jsFile){
                if($jsFile["name"] != ''){
                $targetDir = ROOT_DIR."uploads/exfiles/";
                $targetFilename = basename($jsFile["name"]);
                $baseFileName = $targetFilename;
                $uploadSs = 1;
                
                //Generate random file name
                $targetFile = $targetDir . rand(1, 9999999) . "_" . $targetFilename;
                
                //Check if file already exists
                if(file_exists($targetFile))
                    delFile($targetFile);
                    
                $checkFileType = pathinfo($targetFile, PATHINFO_EXTENSION);
                
                //Check file size
                if ($jsFile["size"] > 2000000) {
                    $comArr[] = array(0,'Sorry, your file is too large.',0,$baseFileName,0,false);
                    $uploadSs = 0;
                    
                } else {
                    //Allow certain file formats
                    if ($checkFileType != "js") {
                        $comArr[] = array(0,'Sorry, only JS files are allowed.',0,$baseFileName,0,false);
                        $uploadSs = 0;
                    }
                }
                //Check if $uploadSs is set to 0 by an error
                if (!$uploadSs == 0) {
                    if (move_uploaded_file($jsFile["tmp_name"], $targetFile)) {
                        //Uploaded
                        $fileJSData = getMyData($targetFile);
                        $fileJSSize = strlen($fileJSData);
                        $minifier = new minifier();
                        $comJSData = $minifier->minify($fileJSData);
                        $comJSSize = strlen($comJSData);
                        $percentage = round((((int)$comJSSize / (int)$fileJSSize) * 100),1);
                        $comArr[] = array($fileJSSize,$comJSData,$comJSSize,$baseFileName,$percentage,true);
                        delFile($targetFile);
                    } else {
                        $comArr[] = array(0,'Sorry, there was an error uploading your file.',0,$baseFileName,0,false);
                    }
                }
                }
            }
        }else{
            if (!isset($_POST['data']))
                die($lang['4']); //Malformed Request!
            if (!isset($error)) {
                $userJSInput = Trim($_POST['data']);
                $fileJSSize = strlen($userJSInput);
                $minifier = new minifier();
                $comJs = $minifier->minify($userJSInput);
                $comJSSize = strlen($comJs);
                $percentage = round((((int)$comJSSize / (int)$fileJSSize) * 100),1);
            }
        }
    }
}


//PR55 - Article Rewriter Pro
if($toolUid == 'PR55') {
        
    $controller = 'output'.D_S.'article_rewriter_pro';

    if ($pointOut == 'output') {
        if (!isset($_POST['data']))
        die($lang['4']); //Malformed Request!
        if (!isset($error)) {
        $userInput = stripslashes($_POST['data']);
        $userInputLang = raino_trim($_POST['lang']);
        $regUserInput = truncate($userInput,30,150);
        $spin=new spin_my_data;
        $spinned=$spin->spinMyData($userInput,$userInputLang);
        $spinned_data=$spin->randomSplit($spinned); 
        $spinned_data = ucfirst($spinned_data);
        $spinned_data= preg_replace("/([.!?]\s*\w)/e", "strtoupper('$1')", $spinned_data);
        $spinned_data = implode(PHP_EOL, array_map("ucfirst", explode(PHP_EOL, $spinned_data)));
        }
    }
}


//PR57 - Bulk Domain Availability Checker
elseif($toolUid == 'PR57') {
    
    $controller = 'output'.D_S.'domain_availability_checker';
    
    if ($pointOut == 'whois') {
        $route = escapeTrim($con,$_GET['route']); 
        $route = explode('/',$route);
        if(!isset($route[2]) || Trim($route[2]) == '' || Trim($route[3]) == '' || !isset($_SESSION['whoToken']))
            die($lang['4']);
        $url = raino_trim($route[2]);
        $whoToken = raino_trim($route[3]);
        
        if($_SESSION['whoToken'] != $whoToken)
            die($lang['4']);
        
        $url = parse_url(Trim('http://'.clean_with_www($url))); 
        $host =  str_replace('www.','',$url['host']);
        $myHost = ucfirst($host);
        $whois= new whois;
        $site = $whois->cleanUrl($host);
        $whois_data = $whois->whoislookup($site);
        $whoisData = $whois_data[0];
    }
    
    if ($pointOut == 'output') {
        if (!isset($_POST['data']))
        die($lang['4']); //Malformed Request!
        if (!isset($error)) {
        $userInput = raino_trim($_POST['data']);
        $regUserInput = truncate($userInput,30,150);
        $array = explode("\n", $userInput);
        $count = 0;
        $statusColor = $whoisLink = $domainAvailabilityStats = $statusMsg = '';
        $statusColors = $whoisLinks = array();
        
        //Server List Path
        $path = LIB_DIR.'domainAvailabilityservers.tdata';
            
        if (file_exists($path)) {
            $contents = file_get_contents($path);
            $serverList = json_decode($contents, true);
        }
        
        $whoToken = randomPassword();
        $_SESSION['whoToken'] = $whoToken;
           
        foreach ($array as $url) {
            $url = clean_with_www($url); $url = Trim("http://$url");
            if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
            if($count == 20)
                break;
            $count++;
            $my_url[] = Trim($url);
            $url = parse_url(Trim($url));
            $host =  str_replace('www.','',$url['host']);
            $myHost[] = ucfirst($host);
                       
            //Get the status of domain name
            $domainAvailabilityChecker = new domainAvailability($serverList);
            $domainAvailabilityStats = $domainAvailabilityChecker->isAvailable($host);
            
            //Response Code - Reason
            //2 - Domain is already taken!
            //3 - Domain is available
            //4 - No WHOIS entry was found for that TLD
            //5 - WHOIS Query failed

            if($domainAvailabilityStats == 2){
                $statusMsg = 'Domain is already taken!';
                $statusColor = '#e74c3c';
                $whoisLink = '<a href="'.$toolURL.'/whois/'.$host.'/'.$whoToken.'" target="_blank" rel="nofollow" title="View WHOIS">View WHOIS</a>';
            } elseif($domainAvailabilityStats == 3){
                $statusMsg = 'Domain is available';
                $statusColor = '#27ae60';
                $whoisLink = '-';
            } elseif($domainAvailabilityStats == 4){ 
                $statusMsg = 'Not Supported TLD';
                $statusColor = '#ec5e00';
                $whoisLink = '-';
            } else {
                $statusMsg = 'Query failed';
                $statusColor = '#ec5e00';
                $whoisLink = '-';
            }
            $whoisLinks[] = $whoisLink;
            $statusColors[] = $statusColor;
            $stats[] = $statusMsg;
            }
        }
        }
    }
    
}


//PR54 - Check GZIP compression
if($toolUid == 'PR54') {
        
    $controller = 'output'.D_S.'check_gzip_compression';

    if ($pointOut == 'output') {
        if (!isset($_POST['url']))
        die($lang['4']); //Malformed Request!
        if (!isset($error)) {
            $my_url = raino_trim($_POST['url']);
            $my_url = "http://".clean_with_www($my_url);
            if (filter_var($my_url, FILTER_VALIDATE_URL) === false) {
            $error = $lang['327'];
            }else {
            $regUserInput = $my_url;
            $my_host = parse_url($my_url);
            $my_host = $my_host['host'];
            $myHost = ucfirst(clean_url($my_host));
            $outData = compressionTest($my_host);
            $comSize = $outData[0];
            $unComSize = $outData[1];
            $isGzip = $outData[2];
            $gzdataSize = $outData[3];
            $header = $outData[4];
            $body = Trim($outData[5]);
            if($body == ""){
                $error = $lang['97'];
            }else{
            if($isGzip){
                $percentage = round(((((int)$unComSize - (int)$comSize) / (int)$unComSize) * 100),1);
            }else{
                $percentage = round(((((int)$unComSize - (int)$gzdataSize) / (int)$unComSize) * 100),1);
            }
            }
            }
        }
    }
}


//PR51 - Get HTTP Headers
if($toolUid == 'PR51') {
        
    $controller = 'output'.D_S.'get_http_headers';
    
    if ($pointOut == 'output') {
        if (!isset($_POST['url']))
        die($lang['4']); //Malformed Request!
        if (!isset($error)) {
            $my_url = raino_trim($_POST['url']);
            $my_url = "http://".clean_with_www($my_url);
            if (filter_var($my_url, FILTER_VALIDATE_URL) === false) {
            $error = $lang['327'];
            }else {
            $regUserInput = $my_url;
            $outData = getHeaders($my_url);
            if($outData=="")
            $error = $lang['327'];
            }
        }
    }
}


//GS01 - Grammar Checker
if($toolUid == 'GS01') {
        
    $controller = 'output'.D_S.'grammar_checker';
    
    if ($pointOut == 'output') {
            die($lang['4']); //Malformed Request!
    }
}


//PR52 - Mobile Friendly Test
if($toolUid == 'PR52') {
        
    $controller = 'output'.D_S.'mobile_friendly_test';
    
    if ($pointOut == 'output') {
    die($lang['4']); //Malformed Request!
    }
}


//PR52 - Mobile Friendly Test
if($toolUid == 'PR52') {
        
    $controller = 'output'.D_S.'mobile_friendly_test';
    
    if ($pointOut == 'output') {
    die($lang['4']); //Malformed Request!
    }
}


//PR53 - Social Stats Checker
if($toolUid == 'PR53') {
        
    $controller = 'output'.D_S.'social_stats_checker';
    
    if ($pointOut == 'output') {
        if (!isset($_POST['url']))
        die($lang['4']); //Malformed Request!
        if (!isset($error)) {
            $my_url = raino_trim($_POST['url']);
            $my_url = "http://".clean_with_www($my_url);
            if (filter_var($my_url, FILTER_VALIDATE_URL) === false) {
            $error = $lang['327'];
            }else {

            $regUserInput = $my_url;
            $my_url = parse_url($my_url);
            $host = $my_url['host'];
            $myHost = ucfirst($host);
            $social_count=new socialCount($host); 
            $tweets_count = Trim($social_count->getTweets());
            
            $facebookData = $social_count->getFb();
            $facebook_like = Trim($facebookData[1]);
            $facebook_share = Trim($facebookData[0]);
            $facebook_comment = Trim($facebookData[2]);
            
            $linkedin_count = Trim($social_count->getLinkedin());
            $gplus_count = Trim($social_count->getPlusones());
            $stumble_count = Trim($social_count->getStumble());
            $pinterest_count = Trim($social_count->getPinterest());
            }
        }
    }
}


//PR56 - Website Reviewer
elseif($toolUid == 'PR56') {
    
   $controller = 'reviewer';

    if ($pointOut == 'output') {
        if (!isset($_POST['url']))
            die($lang['4']); //Malformed Request!
        if (!isset($error)) {
            $my_url = raino_trim($_POST['url']);
            $my_url = 'http://'.clean_url($my_url);
            $my_url_parse = parse_url($my_url);
            $my_url_host = str_replace("www.","",$my_url_parse['host']);
            header('Location: '.$toolURL.'/'.$my_url_host);
            die();
        }
    }
    
    if ($pointOut != '') {            
        define('TEMP_DIR',APP_DIR.'temp'.D_S);
        $isOnline = '0';
        $my_url = raino_trim($pointOut);
        $pointOut = 'output';
        $my_url = 'http://'.clean_url($my_url);
        //Parse Host
        $my_url_parse = parse_url($my_url);
        $inputHost = $my_url_parse['scheme'] . "://" . $my_url_parse['host'];
        $my_url_host = str_replace("www.","",$my_url_parse['host']);
        $my_url_path = $my_url_parse['path'];
        $my_url_query = $my_url_parse['query']; 
        
        $hashCode = md5($my_url_host);
        $filename = TEMP_DIR.$hashCode.'.tdata';
        
        //Get Data of the URL
        $sourceData = getMyData($my_url);
        
        if($sourceData == ""){
           
            //Second try with Curl
            $sourceData = curlGET($my_url);
            
            if($sourceData == "")
                $error  = 'Input Site is not valid!';
        }
        
        if(!isset($error)){
            $isOnline = '1';
            putMyData($filename,$sourceData);
        }
    }
    
}


//ASX4 - Adsense Calculator
if($toolUid == 'ASX4') {
        
    $controller = 'output'.D_S.'adsense-calculator';
    
    //Formula - Daily Earnings
    // (Daily page impressions * CTR * CPR) / 100 = Daily Earnings
    
    //Formula - Daily Clicks
    // (Daily page impressions * CTR) / 100 = Daily Clicks
    
    if ($pointOut == 'output') {
        
        if(!isset($_POST['impressions']) || !isset($_POST['ctr']) || !isset($_POST['cpr'])){
            $error = $lang['4']; //Malformed Request!
        }else{
            $impressions = raino_trim($_POST['impressions']);
            $ctr = raino_trim($_POST['ctr']);
            $cpr = raino_trim($_POST['cpr']);
                    
            $dailyEarnings = (($impressions * $ctr * $cpr) / 100);
            $dailyClicks = (($impressions * $ctr) / 100);
            
            $monthlyEarnings = $dailyEarnings * 30;
            $monthlyClicks = $dailyClicks * 30;
            
            $yearlyEarnings = $monthlyEarnings * 12;
            $yearlyClicks = $monthlyClicks * 12;
        }
    }
}


//BA01 - Backlink Extractor
if($toolUid == 'BA01') {
        
    $controller = 'output'.D_S.'backlink-extractor';
    
    if ($pointOut == 'download') {
        $download = "";
        $download = $_POST['download'];
        if($download != ""){
            $darray = unserialize(base64_decode($download));	
            header('Content-type: application/vnd.ms-excel');
            header('Content-disposition: attachment; filename="backlinks.xls"');
            foreach ($darray as $value) {
            	$value = str_replace("|DELIMITER|","	",$value);
            	echo $value."\n";
            }
            die();	
        }
    }

    if ($pointOut == 'output') {
        if (!isset($_POST['url']))
        die($lang['4']); //Malformed Request!
        if (!isset($error)) {
            $my_url = raino_trim($_POST['url']);
            $domain = "http://".clean_with_www($my_url);
            if (filter_var($domain, FILTER_VALIDATE_URL) === false) {
            $error = $lang['327'];
            }else {
            $regUserInput = $domain;
           
            function get_domain_name($url){
            	$domain	=	parse_url($url);
            	return $domain['host'];
            }
            
            $max = 0;           
            $max	=	raino_trim($_POST['max']);
            $outData = null;
            $sarray = array();
            $sarray[] = "#|DELIMITER|Linking Domain|DELIMITER|Linking Page URL|DELIMITER|Linking Page Title";
            		if(substr($domain, 0, 7) == "http://")	$domain	=	substr_replace($domain, "", 0, 7);
            
            		// Checking empty values
            		if($domain == ""){
            			$error = 'Please enter domain!';
            			exit();
            		}else{
            			$url	=	"http://" . $domain;
            			$domain	=	str_replace("www.","",get_domain_name($url));
            			$url	=	"http://" . $domain;
            			
            	$class = "row2";
            	$count = "1";			
             	for($i=0;$i<=$max;$i=$i+50){
                     //Select a Random API Key  
                 	$bingAPIKey = $bingAPIKeys[array_rand($bingAPIKeys)];
                    $accountKey = $bingAPIKey;
                   
                    // Replace this value with your account key  
                    $ServiceRootURL =  'https://api.datamarket.azure.com/Bing/SearchWeb/';                    
                    $WebSearchURL = $ServiceRootURL . 'Web?$format=json&$skip='.$i.'&Query=';
                
                    $cred = sprintf('Authorization: Basic %s', 
                      base64_encode($accountKey . ":" . $accountKey) );
                
                    $context = stream_context_create(array(
                        'http' => array(
                            'header'  => $cred
                        )
                    ));
                
                    $request = $WebSearchURL . urlencode("'inbody: $domain'");
                    //$response = file_get_contents($request, 0, $context);
                    $process = curl_init($request);
                    curl_setopt($process, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                    curl_setopt($process, CURLOPT_USERPWD, "username:$bingAPIKey");
                    curl_setopt($process, CURLOPT_TIMEOUT, 30);
                    curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
                    curl_setopt($process, CURLOPT_SSL_VERIFYPEER, false);
                    $response = curl_exec($process);
                    $response;
                    curl_close($process);
                    
                        $jsonobj = json_decode($response);
                    
                        foreach($jsonobj->d->results as $value)
                        { 
                    	$class = ($class == "row2")? "row1":"row2";
                        			$lurl = $value->Url;
                    				$ltitle = trim($value->Title);
                    				$ldomain = str_replace("www.", "", get_domain_name($value->Url));
                    				$lpage = '<a href="'.$lurl.'" target="_blank">'.$ltitle.'</a>';				
                    				if($domain != $ldomain){									
                    					$outData.= '<tr class="'.$class.'"><td>'.$count.'</td><td>'.$ldomain.'</td><td>'.$lpage.'</td></tr>';
                    					$sarray[] = "$count|DELIMITER|$ldomain|DELIMITER|$lurl|DELIMITER|$ltitle";					
                    					$count++;
                    				}    	
                        }
                    
                        if(count($jsonobj->d->results) < 50) break;
                    	
                 	}			
              		$outData.= '<tr class="'.$class.'"><td colspan="3" align="center"><form action="'.$toolURL.'/download" method="POST"><input name="download" type="hidden" value="'.base64_encode(serialize($sarray)).'" size="30"><input class="btn btn-danger" type="submit" value="Export"></form></td></tr>';					
            		}
            }
      }
      }
    
}


//ASX3 - Bulk Facebook ID Finder
if($toolUid == 'ASX3') {
        
    $controller = 'output'.D_S.'bulk-facebook-id-finder';
    
    if ($pointOut == 'output') {
            die($lang['4']); //Malformed Request!
    }
}


//DO01 - Donate Button Generator
if($toolUid == 'DO01') {
        
    $controller = 'output'.D_S.'donate_button';
    
    if ($pointOut == 'output') {
        die($lang['4']); //Malformed Request!
    }
}


//NS03 - Keyword Suggestion Tool v2
if($toolUid == 'NS03') {
        
    $controller = 'output'.D_S.'suggestion-tool';

    if ($pointOut == 'output') {
        $allKeywords = array();
        $userKeyword = raino_trim($_POST['keyword']);
        
        if(isset($_POST['google'])){
            $curKeys = getGoogleSuggestion($userKeyword);
            $allKeywords = array_merge($allKeywords,$curKeys);
            foreach($curKeys as $curKey){
                $allKeywords = array_merge($allKeywords,getGoogleSuggestion($curKey));
            }
        }
        
        if(isset($_POST['bing'])){
            $curKeys = getBingSuggestion($userKeyword);
            $allKeywords = array_merge($allKeywords,$curKeys);
            foreach($curKeys as $curKey){
                $allKeywords = array_merge($allKeywords,getBingSuggestion($curKey));
            }
        }
        
        if(isset($_POST['yahoo'])){
            $curKeys = getYahooSuggestion($userKeyword);
            $allKeywords = array_merge($allKeywords,$curKeys);
            foreach($curKeys as $curKey){
                $allKeywords = array_merge($allKeywords,getYahooSuggestion($curKey));
            }
        }
        
        if(isset($_POST['youtube'])){
            $curKeys = getYoutubeSuggestion($userKeyword);
            $allKeywords = array_merge($allKeywords,$curKeys);
            foreach($curKeys as $curKey){
                $allKeywords = array_merge($allKeywords,getYoutubeSuggestion($curKey));
            }
        }
        
        if(isset($_POST['amazon'])){
            $curKeys = getAmazonSuggestion($userKeyword);
            $allKeywords = array_merge($allKeywords,$curKeys);
            foreach($curKeys as $curKey){
                $allKeywords = array_merge($allKeywords,getAmazonSuggestion($curKey));
            }
        }
        
        if(isset($_POST['wikipedia'])){
            $curKeys = getWikipediaSuggestion($userKeyword);
            $allKeywords = array_merge($allKeywords,$curKeys);
            foreach($curKeys as $curKey){
                $allKeywords = array_merge($allKeywords,getWikipediaSuggestion($curKey));
            }
        }
        //Get Only Unique 
        $allKeywords = array_unique($allKeywords);
    }
}


//SC01 - SoundCloud Downloader
if($toolUid == 'SC01') {
        
    $controller = 'output'.D_S.'soundcloud_downloader';
    
    if ($pointOut == 'output') {
    die($lang['4']); //Malformed Request!
    }
}


//CS02 - GEO IP Locator
if($toolUid == 'CS02') {
        
    $controller = 'output'.D_S.'geo_ip_locator';
    
    if ($pointOut == 'output') {
        if (!isset($_POST['ip']))
            die($lang['4']); //Malformed Request!
        if (!isset($error)) {
            $my_ip = raino_trim($_POST['ip']);
            if (!filter_var($my_ip, FILTER_VALIDATE_IP) === false) {
                //Valid IP address
                $ip_info = getMyGeoInfo($my_ip,$item_purchase_code);
                $ip = $my_ip;
                $city = $ip_info[0];
                $region = $ip_info[1];
                $country = $ip_info[2];
                $country_code = $ip_info[3];
                $isp = $ip_info[4];
                $latitude = $ip_info[5];
                $longitude = $ip_info[6];
            } else {
                //Not a valid IP address
                $error = 'Not a valid IP address!';
            }
        }
    }
}


//ASX1 - Article Scraper
if($toolUid == 'ASX1') {
        
    $controller = 'output'.D_S.'article-scraper';
    
    if ($pointOut == 'output') {
            die($lang['4']); //Malformed Request!
    }
}


//NS04 - Article Spinner Pro
if($toolUid == 'NS04') {
        
    $controller = 'output'.D_S.'article-spinner-pro';
    
    if ($pointOut == 'output') {
        die($lang['4']); //Malformed Request!
    }
}


//NS01 - Bulk URL Shortener
if($toolUid == 'NS02') {
        
    $controller = 'output'.D_S.'url_shortener';

    if ($pointOut == 'output') {
        die($lang['4']); //Malformed Request!
    }
}


//NS01 - Comma Separating Tool
if($toolUid == 'NS01') {
        
    $controller = 'output'.D_S.'comma-separating-tool';

    if ($pointOut == 'output') {
        die($lang['4']); //Malformed Request!
    }
}


//ASX2 - Link Tracker
if($toolUid == 'ASX2') {
        
    $controller = 'output'.D_S.'link-tracker';
    
    if ($pointOut == 'output') {
            die($lang['4']); //Malformed Request!
    }
}


//GS02 - Remove Duplicate Lines
if($toolUid == 'GS02') {
        
    $controller = 'output'.D_S.'remove_duplicate_lines';
    
    if ($pointOut == 'output') {
            die($lang['4']); //Malformed Request!
    }
}


//US03 - Reverse Image Search
if($toolUid == 'US03') {
        
    $controller = 'output'.D_S.'reverse-image';
    
    if ($pointOut == 'output') {
    define('TEMP_DIR',APP_DIR.'temp'.D_S);   
    //Output
    $yandex_file_path = $bing_file_path = $google_file_path = $file_path = '';
    if (isset($_POST['logoID'])) {
    //Upload Image
    $target_dir = TEMP_DIR;
    $target_filename = basename($_FILES["logoUpload"]["name"]);
    if($target_filename != ''){
    $target_file = $target_dir . $target_filename;
    $uploadSs = 1;
    $check = getimagesize($_FILES["logoUpload"]["tmp_name"]);
    // Check it is a image
    if ($check !== false)
    {
        // Check if file already exists
        if (file_exists($target_file))
        {
            $target_filename = rand(1, 9999999999999) . "_" . $target_filename;
            $target_file = $target_dir . $target_filename;
        }
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
        // Check file size
        if ($_FILES["logoUpload"]["size"] > 500000)
        {
            $msg =  '<div class="alert alert-danger">
     <button data-dismiss="alert" class="close" type="button">&times;</button>
     <strong>Alert!</strong> Sorry, your file is too large.
     </div>';
            $uploadSs = 0;
        } else
        {
            // Allow certain file formats
            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType !=
                "jpeg" && $imageFileType != "gif")
            {
                $msg =  '<div class="alert alert-danger">
     <button data-dismiss="alert" class="close" type="button">&times;</button>
     <strong>Alert!</strong> Sorry, only JPG, JPEG, PNG & GIF files are allowed.
     </div>';
                $uploadSs = 0;
            }
        }

        // Check if $uploadSs is set to 0 by an error
        if (!$uploadSs == 0)
        {

            if (move_uploaded_file($_FILES["logoUpload"]["tmp_name"], $target_file))
            {
          $file_path = 'https://'.$_SESSION['HTTP_HOST'].'/core/temp/'.$target_filename;
          $yandex_file_path = 'https://www.yandex.com/images/search?text='.$file_path.'&img_url='.$file_path.'&rpt=imageview';
          $bing_file_path = 'https://www.bing.com/images/searchbyimage?FORM=IRSBIQ&cbir=sbi&imgurl='.$file_path;
          $google_file_path = 'https://www.google.com/searchbyimage?image_url='.$file_path;
         
         
            } else
            {
                $msg =  '<div class="alert alert-danger">
     <button data-dismiss="alert" class="close" type="button">&times;</button>
     <strong>Alert!</strong> Sorry, there was an error uploading your file.
     </div>';
            }
        }

    } else
    {
        $msg =  '<div class="alert alert-danger">
     <button data-dismiss="alert" class="close" type="button">&times;</button>
     <strong>Alert!</strong> File is not an image.
     </div>';
    }
    
    }else{
        //URL Image
        $file_path = raino_trim($_POST['url']);
        $yandex_file_path = 'https://www.yandex.com/images/search?text='.$file_path.'&img_url='.$file_path.'&rpt=imageview';
        $bing_file_path = 'https://www.bing.com/images/searchbyimage?FORM=IRSBIQ&cbir=sbi&imgurl='.$file_path;
        $google_file_path = 'https://www.google.com/searchbyimage?image_url='.$file_path;
    }
    }else{
        die('Malformed Request!');
    }
    }
}


//CS01 - Wordpress Theme Detector
if($toolUid == 'CS01') {
        
    $controller = 'output'.D_S.'wordpress_theme_detector';
    
    if ($pointOut == 'output') {
        die($lang['4']); //Malformed Request!
    }
}
