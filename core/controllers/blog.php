<?php

defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
* @author Balaji
* @name: Rainbow PHP Framework v1.0
* @copyright © 2015 ProThemes.Biz
*
*/

// Default Page Title
$p_title = 'Blog';

function getBlogPosts($con, $offset, $per_page)
{
    $sql = "select * from blog_content WHERE post_enable='on' ORDER BY id *1 DESC LIMIT $offset, $per_page";
    $result = mysqli_query($con, $sql);
    return $result;
}

function getBlogPostsCat($con, $offset, $per_page, $categoryName)
{
    $sql = "select * from blog_content WHERE category='$categoryName' AND post_enable='on' ORDER BY id *1 DESC LIMIT $offset, $per_page";
    $result = mysqli_query($con, $sql);
    return $result;
}


//Load Blog Settings
$sql = "SELECT * FROM blog where id='1'";
$result = mysqli_query($con, $sql);

while($row = mysqli_fetch_array($result)) {
    $des = $row['meta_des'];
    $keyword = $row['meta_tags'];
    $per_page = (int)Trim($row['maximum_posts']);
    $post_length = (int)Trim($row['truncate']);
    $no_image_path = $row['no_image'];
    $default_posted_by = $row['posted_by'];
    $discuss_id = htmlspecialchars_decode($row['discuss_id']);
    $showComment = filter_var($row['show_comment'], FILTER_VALIDATE_BOOLEAN);
    $showRelated = filter_var($row['show_related'], FILTER_VALIDATE_BOOLEAN);
    $blogEnable = filter_var($row['blog_enable'], FILTER_VALIDATE_BOOLEAN);
}

if(!$blogEnable){
    header("Location: /");
    echo '<meta http-equiv="refresh" content="1;url=/">';
    die();
}
$blogPosts = false;   

if($pointOut == ""){
   //No Blog URL Found

   $blogPosts = true; 
   $sql = "SELECT * FROM blog_content WHERE post_enable='on'";
   
   $resVal = mysqli_query($con, $sql);
   $totalrecords = mysqli_num_rows($resVal);
   if (isset($_GET['page']))
   {
        $pagenumber = raino_trim($_GET['page']);
        $page = $pagenumber -1;
   }else{
        $pagenumber = '1';
        $page = 0;
   }
   $offset = $per_page * $page;
   
   $page_url = '/blog';
   $page_p_url = '/blog&page=[p]';
 
   $rsd = getBlogPosts($con,$offset,$per_page);
   
}elseif($pointOut == "category"){
    //Blog Category Found
    
    $blogPosts = true; 

    $routePath = escapeTrim($con,$_GET['route']); 
    $routePath = explode('/',$routePath);
    $categoryName = $routePath[2];
    
    if($categoryName == "" || $categoryName == null){
    header("Location: /blog");
    echo '<meta http-equiv="refresh" content="1;url=/blog">';
    die();
    }
    
    $sql = "SELECT * FROM blog_content where category='$categoryName' AND post_enable='on'"; 
    $resVal = mysqli_query($con, $sql);
    $totalrecords = mysqli_num_rows($resVal);
    if (isset($_GET['page']))
    {
        $pagenumber = raino_trim($_GET['page']);
        $page = $pagenumber -1;
    }else{
        $pagenumber = '1';
        $page = 0;
    }
    $offset = $per_page * $page;
   
    $page_url = '/blog/category/'.$categoryName;
    $page_p_url = '/blog/category/'.$categoryName.'&page=[p]';
   
    $categoryName = str_replace('-',' ',$categoryName);
    $p_title = ucwords($categoryName);
    
    $rsd = getBlogPostsCat($con,$offset,$per_page,$categoryName);
    
} else{
    //Blog URL Found
    
    $query = mysqli_query($con, "SELECT * FROM blog_content WHERE post_url='$pointOut'");
    if (mysqli_num_rows($query) > 0)
    {
        $data = mysqli_fetch_array($query);
        $blogPostID = $data['id'];
        $post_title = $data['post_title'];
        $posted_by = $data['posted_by'];
        $posted_date = $data['date'];
        $category = $data['category'];
        $featured_image = $data['featured_image'];
        $meta_des = $data['meta_des'];
        $meta_tags = $data['meta_tags'];
        $post_url = $data['post_url'];
        $blogpageview = (int)trim($data['pageview']);
        $post_allow_comment = filter_var($data['allow_comment'], FILTER_VALIDATE_BOOLEAN);
        $post_enable = filter_var($data['post_enable'], FILTER_VALIDATE_BOOLEAN);
        $post_content = htmlspecialchars_decode($data['post_content']);
        $controller = "blog_view";
        if(!$post_enable){
            require_once (CON_DIR . "error.php");
        }else{
        $blogpageview = $blogpageview+1;    
        $query = "UPDATE blog_content SET pageview='$blogpageview' WHERE id='$blogPostID'";
        mysqli_query($con, $query);
        }
    } else
    {
        require_once (CON_DIR . "error.php");
    }
    
    $posted_date_raw=date_create($posted_date);
    $post_month = date_format($posted_date_raw,"M");
    $post_day = date_format($posted_date_raw,"j");
    
    $p_title = $post_title;
    $des = $meta_des;
    $keyword = $meta_tags;
}

if($blogPosts){
    
$pg = new blogPagination();
$pg->pagenumber = $pagenumber;
$pg->pagesize = $per_page;
$pg->totalrecords = $totalrecords;
$pg->showfirst = true;
$pg->showlast = true;
$pg->paginationcss = "pagination-normal";
$pg->paginationstyle = 0;
$pg->defaultUrl = $page_url;
$pg->paginationUrl = $page_p_url;

}

?>