<?php header("HTTP/1.0 404 Not Found",true,404); ?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Website Appraisal - Seo Checker | OOPS!, Error 404! </title>
	<link rel="stylesheet" href="http://website-appraisal.com/core/controllers/css/bootstrap.min.css">
	<link rel="stylesheet" href="http://website-appraisal.com/core/controllers/css/font-awesome.min.css">
	<link rel="stylesheet" href="http://website-appraisal.com/core/controllers/css/style.css">
	<link rel="stylesheet" href="http://website-appraisal.com/core/controllers/css/simpletextrotator.css">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="http://website-appraisal.com/core/controllers/js/jquery.simple-text-rotator.min.js"></script>
	<script src="http://website-appraisal.com/core/controllers/js/main.js"></script>
</head>
<body>
	<div class="main">
		<div class="container page-text">
			<div class="row">
				<div class="col-sm-12">
					<h1 class="rotate">OOPS!, Error 404!</h1>
					<span>It seems that page you are looking for no longer exists.<br>
						Please Go to <a href="http://website-appraisal.com">homepage</a></span>
					<div class="search-form-container">
						<form action="#" method="POST">
							<input type="text" name="" placeholder="What are you looking for?" autofocus>
							<input type="submit" value="GO">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="menu-container clearfix">
							<ul class="menu">
								<!-- start menu links -->
								<!-- replace the # with menu links -->
								<li><a href="http://website-appraisal.com/page/about">About</a></li>
								<li><a href="http://website-appraisal.com/">Home</a></li>
								<li><a href="http://website-appraisal.com/Blog/">Blog</a></li>
								<li><a href="http://website-appraisal.com/contact">Contact</a></li>
								<!-- end menu links -->
							</ul>
						</div>
					</div>
					<div class="col-md-6">
						<div class="socila-container clearfix">
							<ul class="social pull-right">
								<!-- start social links -->
								<!-- replace the # with your own profile link address -->
								<li><a href="https://www.facebook.com/WebsiteAppraisal"><i class="fa fa-facebook"></i></a></li>
								<li><a href="https://twitter.com/appraisai"><i class="fa fa-twitter"></i></a></li>
								<li><a href="https://plus.google.com/+Websiteappraisal12"><i class="fa fa-google-plus"></i></a></li>
								<!-- end social links -->
							</ul>
						</div>
						<div class="copyright pull-right">Copyright &copy; 2016 <a href="http://website-appraisal.com">Website Appraisal</a> | All Right Reserved.</div>
					</div>
				</div>
			</div>
		</div>
</body>
</html>

<?php die(); ?>