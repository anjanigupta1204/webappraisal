<?php

defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools - PHP Script
 * @copyright © 2017 ProThemes.Biz
 *
 */

if(!$enable_reg){
    header("Location: ../");
    exit();  
}

if(!$enable_oauth){
    header("Location: ../");
    exit();  
}

//Redirect URL
$protocol = isset($_SERVER["HTTPS"]) ? 'https://' : 'http://';
$fb_redirect_url = $protocol.$_SERVER["HTTP_HOST"].'/?route=facebook';

// Oauth Facebook
define('FB_APP_ID', $fb_app_id);   // Enter your facebook application id
define('FB_APP_SECRET', $fb_app_secret);    // Enter your facebook application secret code
define('FB_REDIRECT_URI', $fb_redirect_url); // Enter your facebook application redirect url

//Facebook Oauth Library
require_once (LIB_DIR . 'facebook/autoload.php');

$fb = new Facebook\Facebook(array(
  'app_id' => FB_APP_ID,
  'app_secret' => FB_APP_SECRET
));

if(isset($_GET['login'])){
    
    // Get the FacebookRedirectLoginHelper
    $helper = $fb->getRedirectLoginHelper();
    
    $permissions = array('email'); // optional
    $loginUrl = $helper->getLoginUrl(FB_REDIRECT_URI, $permissions);
    header('Location: '. $loginUrl);
    exit();
    
}else{
    
    // Get the FacebookRedirectLoginHelper
    $helper = $fb->getRedirectLoginHelper();
    
    try {
        $accessToken = $helper->getAccessToken(FB_REDIRECT_URI);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
        // When Graph returns an error
        //echo 'Graph returned an error: ' . $e->getMessage();
        //exit;
         die($lang['261']);
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
        // When validation fails or other local issues
        //echo 'Facebook SDK returned an error: ' . $e->getMessage();
        //exit;
        die($lang['261']);
    }
    
    
    if (isset($accessToken)) {
        // Logged in
        
        try {
            // Returns a `Facebook\FacebookResponse` object
            $response = $fb->get('/me?fields=id,name,email,first_name,last_name', $accessToken);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            //echo 'Graph returned an error: ' . $e->getMessage();
            //exit;
            die($lang['261']);
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            //echo 'Facebook SDK returned an error: ' . $e->getMessage();
            //exit;
            die($lang['261']);
        }
        
        // Returns a `Facebook\GraphNodes\GraphUser` collection
        $user = $response->getGraphUser();
        
        if (!empty($user)){
            $client_name = $user['name'];
            $client_id = $user['id'];
            $client_email = $user['email'];
            $client_plat = 'Facebook';
            $query = mysqli_query($con, "SELECT * FROM users WHERE oauth_uid='$client_id'");
            if (mysqli_num_rows($query) > 0)
            {
                $query = "SELECT * FROM users WHERE oauth_uid='$client_id'";
                $result = mysqli_query($con, $query);
                while ($row = mysqli_fetch_array($result))
                {
                    $user_username = $row['username'];
                    $db_verified = $row['verified'];
                }
                if ($db_verified == "2")
                {
                    die($lang['260']);
                } else
                {

                    $_SESSION['token'] = Md5($db_id . $username);
                    $_SESSION['username'] = escapeTrim($con,$username);
                    $_SESSION['oauth_uid'] = escapeTrim($con,$client_id);
                    $_SESSION['pic'] = $client_pic;
                    $_SESSION['userToken'] = passwordHash($db_id . $username);
                    
                    $old_user = 1;
                    header("Location: ../");
                    exit();
                }
            } else
            {
                $new_user = 1;
                #user not present.
                $query = "SELECT @last_id := MAX(id) FROM users";
                $result = mysqli_query($con, $query);
                while ($row = mysqli_fetch_array($result))
                {
                    $last_id = $row['@last_id := MAX(id)'];
                }
                if ($last_id == "" || $last_id == null)
                {
                    $username = "User1";
                } else
                {
                    $last_id = $last_id + 1;
                    $username = "User$last_id";
                }
                $_SESSION['username'] = escapeTrim($con,$username);
                $_SESSION['oauth_uid'] = escapeTrim($con,$client_id);
                $_SESSION['token'] = Md5($db_id . $username);
                $_SESSION['userToken'] = passwordHash($db_id . $username);
                $query = "INSERT INTO users (oauth_uid,username,email_id,full_name,platform,password,verified,picture,date,ip) VALUES ('$client_id','$username','$client_email','$client_name','$client_plat','$password','1','$client_pic','$date','$ip')";
                mysqli_query($con, $query);
                header("Location: ../?route=oauth&new_user&successInt");
                exit();
            }

        }
    } elseif ($helper->getError()) {
        // There was an error (user probably rejected the request)
        //echo '<p>Error: ' . $helper->getError();
        //echo '<p>Code: ' . $helper->getErrorCode();
        //echo '<p>Reason: ' . $helper->getErrorReason();
        //echo '<p>Description: ' . $helper->getErrorDescription();
        //exit;
        die($lang['261']);
    }

}
die();
?>