<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: Rainbow PHP Framework v1.0
 * @copyright � 2015 ProThemes.Biz
 *
 */

//AJAX ONLY 

//POST REQUEST Handler
if ($_SERVER['REQUEST_METHOD'] =='POST') {
    
    if(isset($_POST['facebook'])){
        
        $sitelink = raino_trim($_POST['sitelink']);
        
        if($sitelink != ''){
            $sitelink = 'https://'.clean_with_www($sitelink);
            echo getMyFBID($sitelink);
            die();
        }
    }
    
}

die();