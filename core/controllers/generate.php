<?php

defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: Article Rewriter Pro - SEO Tool Addons
 * @copyright � 2015 ProThemes.Biz
 *
 */

if(!isset($_POST['format'])){
    $controller = "error";
    require(CON_DIR. $controller . '.php');
}

$format = raino_trim($_POST['format']);
$toolName = raino_trim($_POST['toolName']);
$inputData = raino_trim($_POST['inputTextArea']);
$data = stripslashes($_POST['textArea']);
$randomFile = rand(0111,999999);
$val = strlen("$toolName by ".$site_name); 

if ($format == "txt")
{
header('Content-type: text/plain');
header('Content-Disposition: attachment; filename="'.$randomFile.'.txt"');
echo "\r\n"."$toolName by ".$site_name."\r\n";
for($loop=0;$loop<=$val;$loop++)
echo "=";
echo "\r\n";
echo "\r\n"."Original Content:"."\r\n".$inputData."\r\n";
echo "\r\n"."Rewritten Content:"."\r\n".$data."\r\n";
echo "\r\n"."\r\n"."Visit for more SEO tools: "."\r\n".baseURL()."\r\n";
echo "\r\n".html_entity_decode($copyright)."\r\n";
die();
}
elseif($format == "html"){
$inputData = str_replace(array("\n","\r\n"),"<br />",$inputData);
$data = str_replace(array("\n","\r\n"),"<br />",$data);
header('Content-Type: text/html; charset=utf-8');
header('Content-Disposition: attachment; filename="'.$randomFile.'.html"');
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta charset="utf-8" />
	<title><?php echo "$toolName by ".$site_name; ?></title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" />
</head>

<body>
<div class="container">

        <!-- Introduction Row -->
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="page-header"><?php echo "$toolName by ".$site_name; ?>
                </h2>
                <div class="text-center">
                <a target="_blank" href="<?php echo baseURL(); ?>" class="btn btn-default">Visit for More SEO Tools</a>
                </div>
            </div>
        </div>

        <!-- Team Members Row -->
        <div class="row">
            <div class="col-lg-12 text-center">
                <h3 class="page-header"></h3>
            </div>
            <div class="col-lg-6 col-sm-6 text-center">
                <h3 style="color: #c0392b;">Original Content</h3>
                <p style="text-align: justify;"><?php echo $inputData; ?></p>
            </div>
            <div class="col-lg-6 text-center">
                <h3 style="color: #27ae60;">Rewritten Content</h3>
                <p style="text-align: justify;"><?php echo $data; ?></p>
            </div>
        </div>

        <hr />
        <br />
        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p><?php echo $copyright; ?></p>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </footer>

    </div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>
<?php
}else{
    echo "Unknown Format";
    die();
}

//Close It!
die();
?>