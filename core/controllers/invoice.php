<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright � 2016 ProThemes.Biz
 *
 */

$invoiceClass = $invoiceStats = $userAddress = '';

if(isset($_SESSION['userToken'])){
    //Get Username
    $username = strtolower($_SESSION['username']);
}else{
    if(!isset($_SESSION['adminToken'])){
        header('Location: /?route=account&login');
        die();
    }
}

//User Data
$invoiceID = raino_trim($pointOut);   

if($invoiceID == '')
    die($lang['97']);

//Order Info         
$orderInfo = getOrderInfo($invoiceID,$con);

if(isset($_SESSION['adminToken']))
    $username = $orderInfo['username'];

if($orderInfo['username'] != $username)
    die($lang['97']);


$userInfo = getPremiumUserInfo($username,$con); 
$actionCode = $activateMsg = $activateClass = '';
$refreshMyUrl = false;

$currencySymbol = getCurrencySymbol($orderInfo['currency_type']);
$amount = con2money_format($orderInfo['amount'],$orderInfo['currency_type']);
$totalAmount = con2money_format($orderInfo['amount_tax'],$orderInfo['currency_type']);
$taxArr = unserialize($orderInfo['tax_data']);
$taxAm = 0;
$taxArrData = array();

foreach($taxArr as $tax){
    if($tax[0])
        $taxAm = $tax[2];
    else
        $taxAm = $amount * ($tax[2]/100); 
$taxArrData[] = array($tax[1], con2money_format($taxAm,$orderInfo['currency_type'])); 
}

$date_raw = date_create(Trim($orderInfo['date']));
$subDate = date_format($date_raw,"jS F Y");

if($orderInfo['billing_type'] == '1'){
    $recCon = unserialize($orderInfo['rec_data']);
    if($recCon[3] == 'auto'){
        $invoiceDate = $dueDate = $subDate;
        $invoiceDate = date_create($invoiceDate);
        date_sub($invoiceDate, date_interval_create_from_date_string('5 days'));
        $invoiceDate = date_format($invoiceDate, 'jS F Y');
    }else{
        $invoiceDate = $dueDate = $subDate;
    }
}else{
    $invoiceDate = $dueDate = $subDate;
}

if($orderInfo['payment_status'] == 'pending'){
    $invoiceClass = 'pending';
    $invoiceStats = 'Due';
}elseif($orderInfo['payment_status'] == 'completed'){
    $invoiceClass = 'paid';
    $invoiceStats = 'Paid';
}elseif($orderInfo['payment_status'] == 'canceled'){
    $invoiceClass = 'cancel';
    $invoiceStats = '';
}elseif($orderInfo['payment_status'] == 'fraud'){
    $invoiceClass = 'cancel';
    $invoiceStats = 'Due';
}
if(isset($route[2])){
    $actionCode = raino_trim($route[2]);
    if($actionCode == 'activate'){
        if($orderInfo['payment_status'] == 'completed'){
            if($orderInfo['status'] == 'completed'){
                //Activate Subscription
                $activateMsg = '<b>Success!</b> Your order has been successfully processed.';
                $activateClass = 'success';
                $subArr = subscriptionCheck($username,$con);
                if($subArr[0]){
                    if($subArr[1]){
                        //Premium Active User
                        $dataPlan = getPlanInfo($subArr[4],$con);
                        if($dataPlan[0]){
                            //Plan Found
                            $_SESSION['premiumClient'] = 1;
                            $_SESSION['premiumToken'] = array($subArr[2],$subArr[3],$subArr[4],$dataPlan[1],$dataPlan[2],$dataPlan[3],$dataPlan[4],$dataPlan[5],$subArr[6],$dataPlan[6]);
                        }else{
                            //Plan Not Found!
                            $_SESSION['premiumClient'] = 1;
                            $_SESSION['premiumError'] = 'Subscribed "'.$subArr[5].'" Plan not found. Either removed or changed! <br> Please <a href="/contact">contact support</a> to solve the issue.<br>';
                        }
                    }else{
                        //Premium Non-Active User
                        $_SESSION['premiumError'] = 'Your subscription has expired! <br> Expired on '.$subArr[3].' <br> To reactivate your subscription <a href="/invoice/'.$subArr[4].'">renew now</a>.<br>';
                    }
                }
            }else{
                //Admin Manual Activation 
                $activateMsg = '<b>Alert!</b> Your Payment was successful but all orders are manually verified and approved by site administrator. <br> Please wait a few minutes and refresh this page!';
                $activateClass = 'info';
                $refreshMyUrl = true;
            }
        }elseif($orderInfo['payment_status'] == 'fraud'){
            //High Risk Order
            $activateMsg = '<b>Alert!</b> Your order marked as highly risk by automated system. If you think it is wrong, please contact support team to solve the issue!';
            $activateClass = 'danger';
        }elseif($orderInfo['payment_status'] == 'pending'){
            //Payment Pending / Waiting for IPN Callback
            $activateMsg = '<b>Alert!</b> Still waiting for payment confirmation from payment provider. Please wait a few minutes and refresh this page! <br>
            Incase it takes to long, please contact support team to solve the issue!';
            $activateClass = 'info';
            $refreshMyUrl = true;
        }
    } elseif($actionCode == 'view'){ 
        $controller = 'invoice-view';
    }
}

if(Trim($userInfo['company']) != '')
    $userAddress = $userInfo['company'].'<br>';

$userAddress .= $userInfo['firstname'] .' ' . $userInfo['lastname'].'<br>';
$userAddress .= $userInfo['address1'].'<br>';
if(Trim($userInfo['address2']) != '')
    $userAddress .= $userInfo['address2'].'<br>';
$userAddress .= $userInfo['city'].', '.$userInfo['statestr'].', '.$userInfo['postcode'].'<br>';
$userAddress .= country_code_to_country($userInfo['country']).'<br>';

extract(orderSettings($con));

$paymentGateways = getPaymentGateways($con);
array_multisort($paymentGateways, SORT_ASC);
$isSelVal = $paymentGatewayData = null;

foreach($paymentGateways as $paymentGateway){
    $isPaymentEnabled = filter_var($paymentGateway[5], FILTER_VALIDATE_BOOLEAN);
    if($isPaymentEnabled){
        $paymentType = Trim($orderInfo['payment_type']);
       
        if($paymentType == $paymentGateway[2])
            $isSelVal = 'selected=""';
        $paymentGatewayData.= '<option '.$isSelVal.' value="'.$paymentGateway[3].'">'.$paymentGateway[1].'</option>';
    }
}

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    
    if(isset($_POST['paynow'])){
        $gateway = escapeTrim($con, $_POST['gateway']);
        $eToken = randomPassword();
        $_SESSION['sLevel'] = 3;
        $_SESSION['eToken'] = $eToken;
        $_SESSION['orderID'] = $invoiceID;
        $_SESSION['planID'] = $orderInfo['plan_id'];
        header('Location: /payments/'.$gateway.'/process/'.$eToken);
        die();
    }
}
   
$p_title = 'Invoice';

if($controller == 'invoice-view'){
    require_once(THEME_DIR.$controller.'.php');
    die();
}
?>