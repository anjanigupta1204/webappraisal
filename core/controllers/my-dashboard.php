<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright � 2016 ProThemes.Biz
 *
 */
 
$p_title = 'My Dashboard';

if(!isset($_SESSION['premiumClient']))
    die($lang['97']);

//Get Username
$username = $_SESSION['username'];
$premiumUserInfo = getPremiumUserInfo($_SESSION['username'],$con);

$tools = array();
$result = mysqli_query($con, "SELECT * FROM seo_tools ORDER BY CAST(tool_no AS UNSIGNED) ASC");

while ($row = mysqli_fetch_array($result)) {
    $toolID = $row['id'];
    $premiumUserTools = unserialize($_SESSION['premiumToken'][7]);

    if (in_array($toolID, $premiumUserTools)) {               
        $tools[] = array($row['tool_name'],$row['tool_url'],$row['icon_name'],$row['tool_show'],$row['tool_no']);
    }
}
?>