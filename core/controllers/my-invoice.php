<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright � 2016 ProThemes.Biz
 *
 */
 
$p_title = 'My Invoices';

if(!isset($_SESSION['premiumClient']))
    die($lang['97']);

//Get Username
$username = $_SESSION['username'];
                
$invoices = array();

$result = mysqli_query($con,"SELECT * FROM premium_orders WHERE username='$username'");

while ($row = mysqli_fetch_array($result)) {
    if($row['payment_status'] == 'pending'){
        $paymentStatus = '<span class="label label-warning">Unpaid</span>';
    }elseif($row['payment_status'] == 'completed'){
        $paymentStatus = '<span class="label label-success">Paid</span>';
    }elseif($row['payment_status'] == 'canceled'){
        $paymentStatus = '<span class="label label-default">Cancelled</span>';
    }elseif($row['payment_status'] == 'fraud'){
        $paymentStatus = '<span class="label label-danger">Fraud</span>';
    }
    
    $date_raw = date_create(Trim($row['date']));
    $subDate = date_format($date_raw,"jS F Y");
    
    if($row['billing_type'] == '1'){
        $recCon = unserialize($row['rec_data']);
        if($recCon[3] == 'auto'){
            $invoiceDate = $dueDate = $subDate;
            $invoiceDate = date_create($invoiceDate);
            date_sub($invoiceDate, date_interval_create_from_date_string('5 days'));
            $invoiceDate = date_format($invoiceDate, 'jS F Y');
        }else{
            $invoiceDate = $dueDate = $subDate;
        }
    }else{
        $invoiceDate = $dueDate = $subDate;
    }
    
    $currencySymbol = getCurrencySymbol($row['currency_type']);
    $invoices[] = array($row['invoice_prefix'],$invoiceDate,$dueDate,$row['plan_name'],$currencySymbol[0].$row['amount_tax'],$paymentStatus,$row['id']);
}

?>