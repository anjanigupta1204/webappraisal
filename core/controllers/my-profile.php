<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright � 2016 ProThemes.Biz
 *
 */
 
$p_title = 'My Profile';

if(!isset($_SESSION['premiumClient']))
    die($lang['97']);

//Get Username
$username = $_SESSION['username'];
$addInfo = false;
$errorCheckData = true;

require_once (LIB_DIR . 'geoip.inc');
$gi = geoip_open(LIB_DIR.'GeoIP.dat', GEOIP_MEMORY_CACHE);

if ($_SERVER['REQUEST_METHOD'] == 'POST'){

       if(isset($_POST['password'])){
        
        $userPassData = getUserInfo($username,$con);
        $userOldPass = $userPassData['password'];
                    
        $new_pass = passwordHash(escapeTrim($con, $_POST['new_pass']));
        $retype_pass = passwordHash(escapeTrim($con, $_POST['retype_pass']));
        $old_pass = passwordHash(escapeTrim($con, $_POST['old_pass']));
        
        if($new_pass == $retype_pass){
            
        if($old_pass == $userOldPass){
            
        $query = "UPDATE users SET password='$new_pass' WHERE username='$username'";
        mysqli_query($con, $query);
    
        if (mysqli_errno($con))
        {
            $msg = '<div class="alert alert-danger alert-dismissable alert-premium">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                <b>Alert!</b> ' . mysqli_error($con) . '
                </div>';
        } else
        {
            $msg = '
            <div class="alert alert-success alert-dismissable alert-premium">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
            <b>Alert!</b> New Passwors updated successfully!
            </div>';
        }
        }else{
            $msg = '<div class="alert alert-danger alert-dismissable alert-premium">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
            <b>Alert!</b> Current password is wrong!
            </div>';
        }
        }else{
            $msg = '<div class="alert alert-danger alert-dismissable alert-premium">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
            <b>Alert!</b> New password & Retype password field can\'t matched!
            </div>';
        }
      }
      
      if(isset($_POST['user'])){
        
        $fullname = escapeTrim($con, $_POST['fullname']);
        $firstname = escapeTrim($con, $_POST['firstname']);
        $lastname = escapeTrim($con, $_POST['lastname']);
        $company = escapeTrim($con, $_POST['company']);
        $telephone = escapeTrim($con, $_POST['telephone']);
        $country = escapeTrim($con, $_POST['country']);
        $address1 = escapeTrim($con, $_POST['address1']);
        $address2 = escapeTrim($con, $_POST['address2']);
        $city = escapeTrim($con, $_POST['city']);
        $postcode = escapeTrim($con, $_POST['postcode']);
        $state = escapeTrim($con, $_POST['state']);
        $stateStr = escapeTrim($con, $_POST['statestr']);
        $userID = getUserID($username,$con);
        $nowDate = date('m/d/Y h:i:sA'); 
        
        if ($address1 != null && $city != null && $postcode != null && $state != null && $firstname != null && $telephone!=null && $country!= null){
            if($userID !== false){    
                $userCheckQuery = mysqli_query($con, "SELECT * FROM premium_users WHERE username='$username'");
                if(mysqli_num_rows($userCheckQuery) > 0)
                    $query = "UPDATE premium_users SET firstname='$firstname', lastname='$lastname', company='$company', telephone='$telephone',address1='$address1', address2='$address2', city='$city', state='$state', statestr='$stateStr', postcode='$postcode', country='$country' WHERE client_id='$userID'"; //Already Premium User 
                else
                    $query = "INSERT INTO premium_users (firstname,lastname,company,telephone,address1,address2,city,state,postcode,country,added_date,ip_address,client_id,username,statestr) VALUES ('$firstname', '$lastname', '$company', '$telephone', '$address1', '$address2', '$city', '$state', '$postcode', '$country', '$nowDate', '$ip', '$userID', '$username', '$stateStr')"; //New Premium User
                
                if (mysqli_query($con, $query)) {
                    $query = "UPDATE users SET full_name='$fullname' WHERE id='$userID'";
                    if (mysqli_query($con, $query)) {
                        $errorCheckData = false;
                        if($_FILES["logoUpload"]["name"] != ''){
                         
                            $target_dir = ROOT_DIR."uploads/users/";
                            $target_filename = basename($_FILES["logoUpload"]["name"]);
                            $uploadSs = 1;
                            $check = getimagesize($_FILES["logoUpload"]["tmp_name"]);
                            
                            // Check it is a image
                            if ($check !== false)
                            {
                                // Check if file already exists
                                $target_filename = unqFile($target_dir,$target_filename);
                                $target_file = $target_dir . $target_filename;
                                
                                $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
                                // Check file size
                                if ($_FILES["logoUpload"]["size"] > 500000)
                                {
                                    $msg =  '<div class="alert alert-danger alert-dismissable alert-premium">
                             <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                             <strong>Alert!</strong> Sorry, your file is too large.
                             </div>';
                                    $uploadSs = 0;
                                } else
                                {
                                    // Allow certain file formats
                                    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType !=
                                        "jpeg" && $imageFileType != "gif")
                                    {
                                        $msg =  '<div class="alert alert-danger alert-dismissable alert-premium">
                             <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                             <strong>Alert!</strong> Sorry, only JPG, JPEG, PNG & GIF files are allowed.
                             </div>';
                                        $uploadSs = 0;
                                    }
                                }
                        
                                // Check if $uploadSs is set to 0 by an error
                                if (!$uploadSs == 0)
                                {
                                    if (move_uploaded_file($_FILES["logoUpload"]["tmp_name"], $target_file))
                                    {
                                        //Uploaded
                                 $file_path = "uploads/users/$target_filename";
                                 $query = "UPDATE users SET picture='$file_path' WHERE id='$userID'"; 
                                 mysqli_query($con,$query); 
                                 
                                    } else
                                    {
                                        $msg =  '<div class="alert alert-danger alert-dismissable alert-premium">
                             <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                             <strong>Alert!</strong> Sorry, there was an error uploading your file.
                             </div>';
                                    }
                                }
                        
                            } else
                            {
                                $msg =  '<div class="alert alert-danger alert-dismissable alert-premium">
                             <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                             <strong>Alert!</strong> File is not an image.
                             </div>';
                            }
                            
                        }else{
                            $errorCheckData = false;
                        }
                    }
                }
            }
        }
        
        if($errorCheckData){
            //Error
            if(!isset($msg)){
            $msg = '<div class="alert alert-success alert-dismissable alert-premium" >
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
            <strong>Alert!</strong> '.$lang['97'].'
            </div>'; 
            }
        }else{
            //Fine
            if(!isset($msg)){
            $msg = '<div class="alert alert-success alert-dismissable alert-premium" >
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
            <strong>Alert!</strong> Profile details was successfully updated!
            </div>'; 
            }
        }
      }
}

$userInfo = getUserInfo($username,$con);

if(isset($_SESSION['premiumClient']))
    $membership = 'Premium';
else
    $membership = 'Free';
    
$premiumUserInfo = getPremiumUserInfo($username,$con);

if($premiumUserInfo === false){
    $addInfo = false;
}else{
    $addInfo = true;
    extract($premiumUserInfo);
}

$countryCode = $country;
$userCountry = geoip_country_name_by_addr($gi,$userInfo['ip']);
$country = country_code_to_country($country);

if($userCountry == ''){
    if(isset($country)){
        $userCountry = $country;
    }else{
        $userCountry = 'Unknown';
    }
}

//Default Logo
$userDefaultLogo = '/theme/default/premium/img/user-default.png';

if($userInfo['picture'] == '' || strtolower($userInfo['picture']) == 'none' || $userInfo['picture'] == null)
    $userLogo = $userDefaultLogo;
else{
    if(!file_exists(ROOT_DIR.$userInfo['picture']))
        $userLogo = $userDefaultLogo;
    else
        $userLogo = '/'.$userInfo['picture'];
    
}

geoip_close($gi);
?>