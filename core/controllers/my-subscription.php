<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright � 2016 ProThemes.Biz
 *
 */
 
$p_title = 'My Subscription';

if(!isset($_SESSION['premiumClient']))
    die($lang['97']);

//Get Username
$username = $_SESSION['username'];
$premiumUserInfo = getPremiumUserInfo($username,$con);
$arrPDFLimit = decSerBase($premiumUserInfo['pdf_limit']);
$dbDate = $arrPDFLimit[0];
$dbCount = (int)$arrPDFLimit[1];
$todayDate = date('m/d/Y');
$totalLimit = (int)$_SESSION['premiumToken'][5];

//Check PDF Limit
if($totalLimit != '0'){
if($dbDate != '' || $dbCount != ''){
    if($dbDate == $todayDate){
        if($dbCount <= $totalLimit)
            $remCountMsg = ($totalLimit - $dbCount). ' PDF Reports';
        else
            $remCountMsg = '<b style="color: #d14233;">Today Limit Reached!</b>';
    }else{
        //Insert New Date
        newPDFLimit($con,$username);
        $remCountMsg = $totalLimit.' PDF Reports';
    }
}else{
    //No data found - Insert New Date
    newPDFLimit($con,$username);
    $remCountMsg = $totalLimit.' PDF Reports';
}
}

$subID = $_SESSION['premiumToken'][8];
$orderInfo = subInfo($subID,$con);
$cancelBtn = $controller.'/cancel';

if($orderInfo['billing_type'] == '0') {
    //One Time Fee Plans
    $expDate = 'Lifetime';
    $billType = 'One Time Fee';
    $renewBtn = '-';
    $recbilling_type = false;
}else{
    //Recurring Fee Plans
    $recbilling_type = true;
    $recCon = unserialize($orderInfo['rec_data']); 
    
    if($recCon[4] == 'paid')
        $orderInfo = subInfo($recCon[1],$con);

    $subDate = $orderInfo['date'];
    
    if($orderInfo['rec_type'] == 'rec1'){
        $billType = 'Monthly';
        $expDate = date('m/d/Y h:i:sA', getUnixTimestamp($subDate,'1M')); 
    }elseif ($orderInfo['rec_type'] == 'rec2'){
        $billType = 'Every 3 Months';
        $expDate = date('m/d/Y h:i:sA', getUnixTimestamp($subDate,'3M')); 
    }elseif ($orderInfo['rec_type'] == 'rec3'){
        $billType = 'Every 6 Months';
        $expDate = date('m/d/Y h:i:sA', getUnixTimestamp($subDate,'6M')); 
    }elseif ($orderInfo['rec_type'] == 'rec4'){
        $billType = 'Yearly';
        $expDate = date('m/d/Y h:i:sA', getUnixTimestamp($subDate,'1Y')); 
    }
    
    $query = mysqli_query($con, "SELECT * FROM premium_orders WHERE username='$username' AND status='rec_pending'");
    if(mysqli_num_rows($query) > 0){
        $renewInfo = mysqli_fetch_array($query);
        $renewBtn = '<a href="/invoice/'.$renewInfo['id'].'" class="btn btn-success">Renew</a>';
    }else{
        $renewBtn = '-';
    }
}

$planID = $_SESSION['premiumToken'][2];
$planName = $_SESSION['premiumToken'][3];
$allowPdf = filter_var($_SESSION['premiumToken'][4], FILTER_VALIDATE_BOOLEAN);
$pdfLimit = (int)$_SESSION['premiumToken'][5];
$brandPDF = filter_var($_SESSION['premiumToken'][6], FILTER_VALIDATE_BOOLEAN);

$premiumUserTools = unserialize($_SESSION['premiumToken'][7]);

if($allowPdf){
    if($pdfLimit == '0')
        $allowPdf = 'Unlimited';
    else
        $allowPdf = $pdfLimit.' Reports/per day';
        
}else{
    $allowPdf = 'Not Allowed';
}

if($brandPDF)
    $brandPDF = 'Allowed';
else
    $brandPDF = 'Not Allowed';  
    
$tools = array();
$result = mysqli_query($con,"SELECT * FROM seo_tools");
while ($row = mysqli_fetch_array($result)) {
    $toolId = $row['id'];
    
    if (in_array($toolId, $premiumUserTools))
        $tools[] = array($row['tool_name'],$row['tool_url'],$row['icon_name'],$row['tool_no']);
}

//Cancel Subscription
if(isset($_POST['cancel'])){
    $msg = raino_trim($_POST['can_msg']);
    if($recbilling_type){
        subCancel($username,$msg,$con);
    }else{
        $id = $_SESSION['premiumToken'][8];
        subCancelByID($id,$msg,$con);
    }
    session_destroy();
}
?>