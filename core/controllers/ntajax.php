<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/**
 * Author Balaji
 * Copyright � 2016 ProThemes.Biz
 * Date: 01/27/2016
 */

//AJAX ONLY 

if(isset($_POST['wpDetect'])){
   
    if (!isset($_POST['url']))
        die($lang['4']); //Malformed Request!
        
    $my_url = raino_trim($_POST['url']);
    $my_url = "http://".clean_with_www($my_url);
    if (filter_var($my_url, FILTER_VALIDATE_URL) === false) {
    $error = $lang['327'];
    }else {

    $regUserInput = $my_url;
    $my_urls = parse_url($my_url);
    $host = $my_urls['host'];
    $myHost = ucfirst($host);
	
	$doWP = new wpThemeDetect($my_url); 
		
	echo $doWP->getWordPressDetails();
    }
}

if(isset($_POST['skype'])){
   
    if (!isset($_POST['url']))
        die($lang['4']); //Malformed Request!
    
    if (!isset($_POST['type']))
        die($lang['4']); //Malformed Request!
            
    $my_url = raino_trim($_POST['url']);
    $type = raino_trim($_POST['type']);
  	
	$doSkype = new doskype($my_url,$type); 
		
	echo $doSkype->getSkypeDetails();
 
}

die();