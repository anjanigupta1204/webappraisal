<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));
define('PAYMODE','1');

/*
 * @author Balaji
 * @name: Rainbow PHP Framework
 * @copyright © 2016 ProThemes.Biz
 *
 */

$actionStop = false;
$nowDate = date('m/d/Y h:i:sA'); 
$username = $_SESSION['username'];
$paymentGateway = strtolower($route[1]);
$paymentAction = strtolower($route[2]);
$paymentData = '';
extract(orderSettings($con));

if ($paymentGateway == null && $paymentAction == null)
     $actionStop = true;

if(isset($route[3]))
    $eToken = $route[3];
    
if($paymentAction == 'process'){
    //Check eToken
    if($_SESSION['eToken'] == $eToken){
        //Login Check - Check user logged stats
        if(isset($_SESSION['userToken'])){
            //User Logged
            
            //Renew Token
            $eToken = randomPassword();
            $_SESSION['eToken'] = $eToken;
            
            //Check sLevel    
            if($_SESSION['sLevel'] == '3'){
                $orderID = $_SESSION['orderID'];
                $query = mysqli_query($con, "SELECT * FROM premium_orders WHERE id='".$orderID."'");
                $orderInfo = mysqli_fetch_array($query);
                extract($orderInfo);
                extract(getPremiumUserInfo($username,$con));
                $userInfo = getUserInfo($username,$con);
                $payerEmail = Trim($userInfo[3]);
                $amount_tax = removeFormatting($amount_tax);
            }else{
                $actionStop = true;
            }
        }else{
            $actionStop = true;
        }
    }else{ 
        $actionStop = true;
    }
}

if($actionStop)
    die($lang['97']);


#---Load Payment Gateway---

$paymentModule = PLG_DIR.'payments'.D_S.$paymentGateway.'.php';

if(file_exists($paymentModule)){
    require($paymentModule);
}else{
    die('Selected payment module not found!');
}

die();