<?php


defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Anjani
 * @name: Rainbow PHP Framework
 * @copyright � 2016 ProThemes.Biz
 *
 */

$p_title = 'Premium membership';

$orderSettings = orderSettings($con);
$currencyType = $orderSettings['currency_type'];
$currencySymbol = getCurrencySymbol($currencyType);
$currencySymbol = $currencySymbol[0];

$terms = unserialize($orderSettings['checkout_terms']); 
$checkOutTerms = filter_var($terms[0], FILTER_VALIDATE_BOOLEAN);
$checkOutPage = Trim($terms[1]);

if($pointOut == ''){
$plans = array();

$result = mysqli_query($con, 'SELECT * FROM premium_plans ORDER BY CAST(plan_id AS UNSIGNED) ASC');

while ($row = mysqli_fetch_array($result))
{
    $showPlan = filter_var($row['status'], FILTER_VALIDATE_BOOLEAN);
    $featured = filter_var($row['featured'], FILTER_VALIDATE_BOOLEAN);
    $hidden = filter_var($row['hidden'], FILTER_VALIDATE_BOOLEAN);
    
    if($showPlan) {
        if(!$hidden){
            $pricing_box = unserialize(base64_decode($row['pricing_box']));
            $plan_features = strEOL($pricing_box[4]);
            $plan_featuresArr = explode(PHP_EOL,$plan_features);
            $selCurType = getCurrencySymbol($pricing_box[1]);
            $plans[] = array($row['plan_name'],$row['url'],$featured,$row['title'],$pricing_box[0],$selCurType[0],$pricing_box[2],$pricing_box[3],$plan_featuresArr);
        }
    }
}
}else{
    $p_title = 'Premium membership';
    
    $paymentGateways = getPaymentGateways($con);
    array_multisort($paymentGateways, SORT_ASC);
    
    $query = mysqli_query($con, "SELECT * FROM premium_plans WHERE url='$pointOut'");
    if (mysqli_num_rows($query) > 0){
        $data = mysqli_fetch_array($query);
        $showPlan = filter_var($data['status'], FILTER_VALIDATE_BOOLEAN);  
    
        if(!$showPlan) 
            require(CON_DIR.'error.php');
        
        //Login Check   
        if(isset($_SESSION['userToken'])){
            //Old User
            
            $userInfo = getUserInfo($_SESSION['username'],$con);
            
            //Default Logo
            $userDefaultLogo = '/theme/default/premium/img/user-default.png';
            
            if($userInfo['picture'] == '' || strtolower($userInfo['picture']) == 'none' || $userInfo['picture'] == null)
                $userLogo = $userDefaultLogo;
            else{
                if(!file_exists(ROOT_DIR.$userInfo['picture']))
                    $userLogo = $userDefaultLogo;
                else
                    $userLogo = '/'.$userInfo['picture'];
                
            }
            $premiumUserInfo = getPremiumUserInfo($_SESSION['username'],$con);

            if($premiumUserInfo !== false)
                extract($premiumUserInfo);
    
            $eToken = randomPassword();
            $_SESSION['eToken'] = $eToken;
            $_SESSION['sLevel'] = 1;
            $_SESSION['planID'] = $data['id']; 
  
            $data['premium_tools'] = unserialize($data['premium_tools']);
           
            $tools = array();
            $result = mysqli_query($con,"SELECT * FROM seo_tools");
            while ($row = mysqli_fetch_array($result))
            {
                $toolId = $row['id'];
                if (in_array($toolId, $data['premium_tools'])){
                $tools[] = array($row['tool_name'],$row['tool_url'],$row['icon_name'],$row['tool_no']);
                }
            }
            
            $allowPDF = $paymentGatewayData = null;
            $planAmount = 0;
            foreach($paymentGateways as $paymentGateway){
                $isPaymentEnabled = filter_var($paymentGateway[5], FILTER_VALIDATE_BOOLEAN);
                if($isPaymentEnabled){
                    $paymentGatewayData.= '<br><br><input type="radio" class="payment_method" value="'.$paymentGateway[2].'" name="payment_method"> '.$paymentGateway[1];
                }
            }
            
            $allow_pdf = filter_var($data['allow_pdf'], FILTER_VALIDATE_BOOLEAN);
            $brand_pdf = filter_var($data['brand_pdf'], FILTER_VALIDATE_BOOLEAN);
            $pdfLimit = $data['projects'];
            
            if($allow_pdf)
                $allowPDF = 'Available';
            else
                $allowPDF = 'Not Available';
                
            if($brand_pdf)
                $brandPDF = 'Available';
            else
                $brandPDF = 'Not Available';
                
            if($data['payment_type'] == '0'){
                //One time Payment
                $billingType = 'One time Payment';
                $planAmount = calNewCurrencyAmount($currencyType,$data['one_time_fee'],$con);
            }else{
                //Recurring Payment
                $billingType = 'Recurring Payment';
                $recurringPlanAmount = unserialize($data['recurrent_fee']);
                $recCheckBoxData = '<select style="display: inline; width: 50%; margin-left: 6px;" class="form-control" id="rec_amount" name="rec_amount">';
                
                if($recurringPlanAmount[0][1])
                    $recCheckBoxData.= '<option id="rec1" value="'.calNewCurrencyAmount($currencyType,$recurringPlanAmount[0][0],$con).'">Monthly</option>'; //Monthly
                    
                if($recurringPlanAmount[1][1])
                    $recCheckBoxData.= '<option id="rec2" value="'.calNewCurrencyAmount($currencyType,$recurringPlanAmount[1][0],$con).'">Every 3 months</option>'; //Every 3 months
              
                if($recurringPlanAmount[2][1])
                    $recCheckBoxData.= '<option id="rec3" value="'.calNewCurrencyAmount($currencyType,$recurringPlanAmount[2][0],$con).'">Every 6 months</option>'; //Every 6 months
              
                if($recurringPlanAmount[3][1])
                    $recCheckBoxData.= '<option id="rec4" value="'.calNewCurrencyAmount($currencyType,$recurringPlanAmount[3][0],$con).'">Every year</option>'; //Every year
                    
                $recCheckBoxData.= '</select>';
            }
            
        }else{
            //New User & Not Logged User

            //Set Call Back URL
            $_SESSION['callBackURL'] = '/'.$controller.'/'.$pointOut;            
        }
        $controller = 'subscribe';
    }else{
        require(CON_DIR.'error.php');
    }  
}

?>