<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: Rainbow PHP Framework v1.0
 * @copyright � 2015 ProThemes.Biz
 *
 */

//AJAX ONLY 

//POST REQUEST Handler
if ($_SERVER['REQUEST_METHOD'] =='POST') {
    
    if(isset($_POST['scraper'])){
        $scraperType = strtolower(raino_trim($_POST['scraperType']));
        $articleLimit = raino_trim($_POST['articleLimit']);
        $keyword = raino_trim($_POST['keyword']);
        
        //Article Scraper - Examiner.Com
        if($scraperType == 'examiner'){
            $arrayContents = searchExaminer($keyword,$articleLimit);
            $count = 0;
            foreach($arrayContents as $arrayContent){
                $count++;
                echo '<textarea id="examinerText'.$count.'" spellcheck="false" class="form-control" rows="10">'.br2nl($arrayContent[0].'<br><br>'.$arrayContent[1]).'</textarea>
                        <div class="text-right">
                          <br />
                          <input type="hidden" value="'.$arrayContent[0].'" id="examinerFile'.$count.'" name="examinerFile'.$count.'" />
                          <a class="btn btn-danger" onclick="selectText(\'#examinerText'.$count.'\');">Select All</a>
                          <a class="btn btn-success" onclick="exportFile(\'#examinerText'.$count.'\',\'#examinerFile'.$count.'\');">Export</a>
                        </div>
                        <br />
                ';
            }
            die();
        }
        
        //Article Scraper - Articlesbase.Com
        if($scraperType == 'articlesbase'){
            $arrayContents = searchArticlesbase($keyword,$articleLimit);

            foreach($arrayContents as $arrayContent){
                $count++;
                echo '<textarea id="articlesbaseText'.$count.'" spellcheck="false" class="form-control" rows="10">'.br2nl($arrayContent[0].'<br><br>'.$arrayContent[1]).'</textarea>
                        <div class="text-right">
                          <br />
                          <input type="hidden" value="'.$arrayContent[0].'" id="articlesbaseFile'.$count.'" name="articlesbaseFile'.$count.'" />
                          <a class="btn btn-danger" onclick="selectText(\'#articlesbaseText'.$count.'\');">Select All</a>
                          <a class="btn btn-success" onclick="exportFile(\'#articlesbaseText'.$count.'\',\'#articlesbaseFile'.$count.'\');">Export</a>
                        </div>
                        <br />
                ';
            }
            die();
        }
        
        //Article Scraper - Ezinearticles.Com
        if($scraperType == 'ezinearticles'){
            $arrayContents = searchEzinearticles($keyword,$articleLimit);

            foreach($arrayContents as $arrayContent){
                $count++;
                echo '<textarea id="ezinearticlesText'.$count.'" spellcheck="false" class="form-control" rows="10">'.br2nl($arrayContent[0].'<br><br>'.$arrayContent[1]).'</textarea>
                        <div class="text-right">
                          <br />
                          <input type="hidden" value="'.$arrayContent[0].'" id="ezinearticlesFile'.$count.'" name="ezinearticlesFile'.$count.'" />
                          <a class="btn btn-danger" onclick="selectText(\'#ezinearticlesText'.$count.'\');">Select All</a>
                          <a class="btn btn-success" onclick="exportFile(\'#ezinearticlesText'.$count.'\',\'#ezinearticlesFile'.$count.'\');">Export</a>
                        </div>
                        <br />
                ';
            }
            die();
        }
        
    }
    
}

die();