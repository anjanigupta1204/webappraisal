<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright � 2016 ProThemes.Biz
 *
 */
 
$p_title = 'PDF Settings';

//Check PDF Allowed
$pdfAllowed = filter_var($_SESSION['premiumToken'][4], FILTER_VALIDATE_BOOLEAN);
$pdfCount = (int)$_SESSION['premiumToken'][5];
$isBranded = filter_var($_SESSION['premiumToken'][6], FILTER_VALIDATE_BOOLEAN);
$banClass = '';

if(!isset($_SESSION['premiumClient']))
    die($lang['97']);

//Get Username
$username = $_SESSION['username'];

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
    if($isBranded){
        
        $premiumUserInfo = getPremiumUserInfo($username,$con);
        $arrUserPDF = decSerBase($premiumUserInfo['pdf_data']);
        $headerFilePath = $arrUserPDF[1];
        $footerFilePath = $arrUserPDF[2];
        $pdf_Copyright = escapeTrim($con, $_POST['copyright']);
        $introduction_Code = escapeTrim($con, $_POST['introductionCode']);
        $footer_Code = escapeTrim($con, $_POST['footerCode']);
        
        if($_FILES["headerUpload"]["name"] != ''){
            
            $target_dir = ROOT_DIR."uploads/users/";
            $target_filename = basename($_FILES["headerUpload"]["name"]);
            $uploadSs = 1;
            $check = getimagesize($_FILES["headerUpload"]["tmp_name"]);
            
            // Check it is a image
            if ($check !== false) {
                // Check if file already exists
                $target_filename = unqFile($target_dir,$target_filename);
                $target_file = $target_dir . $target_filename;
                $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
                // Check file size
                if ($_FILES["headerUpload"]["size"] > 500000) {
                    $msg =  '<div class="alert alert-danger alert-dismissable alert-premium">
             <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
             <strong>Alert!</strong> Sorry, your file is too large.
             </div>';
                    $uploadSs = 0;
                } else {
                    // Allow certain file formats
                    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType !=
                        "jpeg" && $imageFileType != "gif") {
                        $msg =  '<div class="alert alert-danger alert-dismissable alert-premium">
             <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
             <strong>Alert!</strong> Sorry, only JPG, JPEG, PNG & GIF files are allowed.
             </div>';
                        $uploadSs = 0;
                    }
                }
        
                // Check if $uploadSs is set to 0 by an error
                if (!$uploadSs == 0) {
                    if (move_uploaded_file($_FILES["headerUpload"]["tmp_name"], $target_file)) {
                         //Uploaded
                         $headerFilePath = "/uploads/users/$target_filename";
                    } else {
                        $msg =  '<div class="alert alert-danger alert-dismissable alert-premium">
             <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
             <strong>Alert!</strong> Sorry, there was an error uploading your file.
             </div>';
                    }
                }
        
            } else {
                $msg =  '<div class="alert alert-danger alert-dismissable alert-premium">
             <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
             <strong>Alert!</strong> File is not an image.
             </div>';
            }
            }
            
            if($_FILES["footerUpload"]["name"] != ''){
             
                $target_dir = ROOT_DIR."uploads/users/";
                $target_filename = basename($_FILES["footerUpload"]["name"]);
                $uploadSs = 1;
                $check = getimagesize($_FILES["footerUpload"]["tmp_name"]);
                
                // Check it is a image
                if ($check !== false) {
                    // Check if file already exists
                    $target_filename = unqFile($target_dir,$target_filename);
                    $target_file = $target_dir . $target_filename;
                    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
                    // Check file size
                    if ($_FILES["footerUpload"]["size"] > 500000) {
                        $msg =  '<div class="alert alert-danger alert-dismissable alert-premium">
                 <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                 <strong>Alert!</strong> Sorry, your file is too large.
                 </div>';
                        $uploadSs = 0;
                    } else {
                        // Allow certain file formats
                        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType !=
                            "jpeg" && $imageFileType != "gif") {
                            $msg =  '<div class="alert alert-danger alert-dismissable alert-premium">
                 <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                 <strong>Alert!</strong> Sorry, only JPG, JPEG, PNG & GIF files are allowed.
                 </div>';
                            $uploadSs = 0;
                        }
                    }
            
                    // Check if $uploadSs is set to 0 by an error
                    if (!$uploadSs == 0) {
                        if (move_uploaded_file($_FILES["footerUpload"]["tmp_name"], $target_file)) {
                             //Uploaded
                             $footerFilePath = "/uploads/users/$target_filename";
                        } else {
                            $msg =  '<div class="alert alert-danger alert-dismissable alert-premium">
                 <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                 <strong>Alert!</strong> Sorry, there was an error uploading your file.
                 </div>';
                        }
                    }
            
                } else {
                    $msg =  '<div class="alert alert-danger alert-dismissable alert-premium">
                 <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                 <strong>Alert!</strong> File is not an image.
                 </div>';
                }
            }
            
            $pdfData = serBase(array($pdf_Copyright,$headerFilePath,$footerFilePath,$footer_Code,$introduction_Code));
        
            $query = "UPDATE premium_users SET pdf_data='$pdfData' WHERE username='$username'";
        
            if (!mysqli_query($con, $query)) {
                $msg = '<div class="alert alert-danger alert-dismissable alert-premium">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                        <b>Alert!</b> Something Went Wrong!
                        </div>';
            } else {
                $msg = '<div class="alert alert-success alert-dismissable alert-premium">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                        <b>Alert!</b> Settings saved successfully!
                        </div>
                        ';
            }
        }
}
    
//Default Header & Footer
$orderInfo = orderSettings($con);
$arrPDF = decSerBase($orderInfo['pdf_data']);
$defaultPdfCopyright = $arrPDF[0];
$defaultHeaderLogo = $arrPDF[1];
$defaultFooterLogo = $arrPDF[2];
$defaultIntroductionCode = $lang['PDF8'];
$defaultFooterCode = '<table style="width: 100%;  border: none; padding: 15px;">
            <tr>
                <td style="width: 33%; text-align: left;">
                    {(CopyRight Text)}
                </td>
                <td style="width: 34%; text-align: center">
                    Page {(CurrentPageNumber)}/{(TotalPageNumber)}
                </td>
                <td style="width: 33%; text-align: right">
                    {(FooterLogo)} 
                </td>
            </tr>
        </table>';

if($isBranded){
    $banClass = '';
    $premiumUserInfo = getPremiumUserInfo($username,$con);
    $arrUserPDF = decSerBase($premiumUserInfo['pdf_data']);
    $pdfCopyright = $arrUserPDF[0];
    $headerLogo = $arrUserPDF[1];
    $footerLogo = $arrUserPDF[2];
    $footerCode = $arrUserPDF[3];
    $introductionCode = $arrUserPDF[4];
}else{
    $banClass = 'disabled=""';
}

if($pdfCopyright == '')
    $pdfCopyright = $defaultPdfCopyright;

if($headerLogo == '')
    $headerLogo = $defaultHeaderLogo;
    
if($footerLogo == '')
    $footerLogo = $defaultFooterLogo;

if($footerCode == '')
    $footerCode = $defaultFooterCode;
    
if($introductionCode == '')
    $introductionCode = $defaultIntroductionCode;
?>