<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright © 2016 ProThemes.Biz
 *
 */
 
//AJAX ONLY 

//POST REQUEST Handler
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    
    //SoundCloud Downloader
    if(isset($_POST['soundCloud'])){
        if(isset($_POST['sitelink'])) {
            $siteLink = clean_with_www(raino_trim($_POST['sitelink']));
            $downloadLink = Trim(getSoundCloudDl("https://".$siteLink)); 

            if($downloadLink == '')
                die('Error');
            else
                die('<a class="btn btn-success" title="Download the track" href="'.$downloadLink.'">Download Link</a>');
        }
    }
    
    //SoundCloud Playlist Downloader
    if(isset($_POST['soundCloudPlay'])){
        if(isset($_POST['sitelink'])) {
            $siteLink = clean_with_www(raino_trim($_POST['sitelink']));
            $playlistArr = getSoundCloudPlayList("https://".$siteLink); 
            if($playlistArr !== false){
               $playListData = '<br><table class="table table-bordered"><thead><tr><th>#</th><th>Links</th><th class="text-center">Download</th></tr></thead><tbody>';
               $count = 1;
               foreach($playlistArr as $playlist){
                $playListData.= '<tr>
                <td align="center">'.$count.'</td>
                <td><a title="Track Link" href="'.$playlist[0].'">'.clean_with_www($playlist[0]).'</a></td>
                <td align="center"><a class="btn btn-success" title="Download the track" href="'.$playlist[1].'">Download Link</a></td>
                </tr>';
                $count++;
               }
               $playListData .= '</tbody></table>';
            }else{
                die('<div class="alert alert-danger text-center">
                    Error : Unable to download the playlist. 
                   </div> ');
            }
            die($playListData);
        }
    }
}

//GET REQUEST Handler


//AJAX END
die();
?>