<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: Rainbow PHP Framework
 * @copyright � 2016 ProThemes.Biz
 *
 */

//AJAX ONLY 

if(isset($_POST['spinData'])){

if (!isset($_POST['data']))
    die($lang['4']); //Malformed Request!

$userInput = stripslashes($_POST['data']);
$userInputLang = raino_trim($_POST['lang']);
$regUserInput = truncate($userInput,30,150);
$spin=new spin_my_data_new;
$spinned_data=$spin->spinMyData($userInput,$userInputLang);
//$spinned_data=$spin->randomSplit($spinned); 
echo $spinned_data;

}

die();
?>