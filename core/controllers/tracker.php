<?php

defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: Rainbow PHP Framework
 * @copyright � 2016 ProThemes.Biz
 *
 */

//AJAX ONLY 

//POST REQUEST Handler
if ($_SERVER['REQUEST_METHOD'] =='POST') {
    
    $siteLink = raino_trim($_POST['sitelink']);
    $seDomain = raino_trim($_POST['seDomain']);
    $mainDomain = raino_trim($_POST['mainDomain']);
    $indexData = $liveData = '';
    
    if($siteLink != ""){
        $siteLink = "http://".clean_with_www($siteLink);
        $mainDomain = "http://".clean_url($mainDomain);
        if (!filter_var($siteLink, FILTER_VALIDATE_URL) === false) {
        
            $my_url = parse_url($siteLink);
            $site = $my_url['host'];
            
            $mainDomainData = parse_url($mainDomain);
            $mainDomainHost = $mainDomainData['host'];
            $searchQuery = urlencode("site:$site $mainDomainHost");

            //--- SE Check Start---
            //Google
            if(str_contains($seDomain,'google',true)){
                
                $apiKey = 'AIzaSyCVAXiUzRYsML1Pv6RwSG1gunmMikTzQqY';
                $lang = 'en';
                $cx = '017909049407101904515:uifmlkwue1w';
                
                $googleDomain = 'www.'.$seDomain;

                $url = 'https://www.googleapis.com/customsearch/v1element?key='.$apiKey.'&rsz=filtered_cse&num=10&hl='.$lang.
                '&prettyPrint=false&source=gcsc&gss=.com&sig=3aa157001604e3bc243e85b7344d5d15&cx='.$cx.'&q='.$searchQuery.'&googlehost='.$googleDomain.'&oq='.$searchQuery.'&gs_l=partner.12...0.0.3.18708.0.0.0.0.0.0.0.0..0.0.gsnos%2Cn%3D13...0.0jj1..1ac..25.partner..0.0.0.&callback=google.search.Search.apiary9718';
                $data = curlGET($url);
                $data = explode('google.search.Search.apiary9718(',$data);
                $data = explode(');',$data[1]);
                $data = $data[0];
                $data = json_decode($data, true);
                //print_r($dataRes);
                $dataRes = Trim($data['cursor']['estimatedResultCount']);
                if ($dataRes == '')
                    $dataRes = 0;
                
                if($dataRes == 0){
                    //Not Indexed
                    $indexData = '<img src="'.$theme_path.'img/false.png" />';
                }else{
                    //Indexed
                    $indexData = '<img src="'.$theme_path.'img/true.png" />';
                }
            }
            
            //Yahoo
            if(str_contains($seDomain,'yahoo',true)){
            
                $link = "http://www.yahoo.com/search?q=".$searchQuery."&go=&qs=n&sk=&sc=8-5&form=QBLH";
                    
                $link = "http://www.bing.com/search?q=site%3A" . $searchQuery .
                    "&go=&qs=n&sk=&sc=8-5&form=QBLH";
                $source = curlGET($link);
                $s = explode('<span class="sb_count">', $source);
                $s = explode('</span>', $s[1]);
                $s = explode('results', $s[0]);
                $s = Trim($s[0]);
                $s = Trim(str_replace("Resultaten","",$s));
                $dataRes = str_replace(',','',$s);
                
                if ($dataRes == '')
                    $dataRes = 0;
                
                if($dataRes == 0){
                    //Not Indexed
                    $indexData = '<img src="'.$theme_path.'img/false.png" />';
                }else{
                    //Indexed
                    $indexData = '<img src="'.$theme_path.'img/true.png" />';
                }
            }
            
            //Bing
            if(str_contains($seDomain,'bing',true)){
                
                $link = "http://www.bing.com/search?q=site%3A" . $searchQuery .
                    "&go=&qs=n&sk=&sc=8-5&form=QBLH";
                $source = curlGET($link);
                $s = explode('<span class="sb_count">', $source);
                $s = explode('</span>', $s[1]);
                $s = explode('results', $s[0]);
                $s = Trim($s[0]);
                $s = Trim(str_replace("Resultaten","",$s));
                $dataRes = str_replace(',','',$s);
            
                if ($dataRes == '')
                    $dataRes = 0;
                
                if($dataRes == 0){
                    //Not Indexed
                    $indexData = '<img src="'.$theme_path.'img/false.png" />';
                }else{
                    //Indexed
                    $indexData = '<img src="'.$theme_path.'img/true.png" />';
                }
            }
            //--- SE Check End---
            
            //Check Site is Online
            $arrVal = get_headers($siteLink,1);
            if(isset($arrVal['4']))
                $httpCode = $arrVal['4'];
            elseif(isset($arrVal['3']))
                $httpCode = $arrVal['3'];
            elseif(isset($arrVal['2']))
                $httpCode = $arrVal['2'];
            elseif(isset($arrVal['1']))
                $httpCode = $arrVal['1'];
            else
                $httpCode = $arrVal['0'];
            
            if(str_contains($httpCode,'200',true)){
                $liveData = '<img src="'.$theme_path.'img/true.png" />';
            }else{
                $liveData = '<img src="'.$theme_path.'img/false.png" />';
            }
            
            die($indexData . ':|:' . $liveData);
            
        }
    }
}

die();