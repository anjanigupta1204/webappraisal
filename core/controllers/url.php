<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: Rainbow PHP Framework
 * @copyright � 2016 ProThemes.Biz
 *
 */

//AJAX ONLY 

$reqCount = raino_trim($_POST['count']);
$myUrl = raino_trim($_POST['domain']);

$urlArrs = externalUrlDB();
$myUrl = urlencode($myUrl);
$urlArr = $urlArrs[$reqCount];
$urlArr[1] = str_replace('{url}',$myUrl,$urlArr[1]);
$errors = array_filter($urlArr);

if (!empty($errors)) {

    if($reqCount == '0'){
        $url = getLocPOST($urlArr[0],$urlArr[1]);
        $url = str_replace('/show','',$url);
        $url = $urlArr[2].$url;
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die(urldecode($url));
    }elseif($reqCount == '1'){
        $url = getLocPOST($urlArr[0],$urlArr[1]);
        $url = str_replace('/van','',$url);
        $url = $urlArr[2].$url;
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);
    }elseif($reqCount == '2'){
        $url = getUrlPOST($urlArr[0],$urlArr[1]);
        $url = getCenterText('name="urlOrigin" value="','" class="shortenLink"',$url);
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);
    }elseif($reqCount == '3'){
        $url = getUrlPOST($urlArr[0],$urlArr[1]);
        $url = getCenterText('name="newurl" value="','" onfocus',$url);
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);
    }elseif($reqCount == '4'){
        $url = getUrlPOST($urlArr[0],$urlArr[1]);
        $url = getCenterText('id="copylink" size="40" value="','" class="',$url);
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);
    }elseif($reqCount == '5'){
        $url = getUrlPOST($urlArr[0],$urlArr[1]);
        $url = getCenterText('name="newurl" value="','" onfocus',$url);
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);
    }elseif($reqCount == '6'){
        $url = getUrlPOST($urlArr[0],$urlArr[1]);
        $url = getCenterText('getElementById("short").value=\'','\';',$url);
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);
    }elseif($reqCount == '7'){
        $url = getUrlPOST($urlArr[0],$urlArr[1]);
        $url = getCenterText('<td><a href="','" target=',$url);
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);
    }elseif($reqCount == '8'){
        $url = getLocPOST($urlArr[0],$urlArr[1]);
        $url = curlGET($url);
        $url = getCenterText('target="_blank">','</a>',$url);
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);
    }elseif($reqCount == '9'){
        $url = getLocPOST($urlArr[0],$urlArr[1]);
        $url = curlGET($url);
        $url = getCenterText('target="_blank">','</a>',$url);
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);
    }elseif($reqCount == '10'){
        $url = getUrlPOST($urlArr[0],$urlArr[1]);
        $url = getCenterText('"url_hash":"','"}',$url);
        $url = $urlArr[2] . '/' . $url;
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);
    }elseif($reqCount == '11'){
        $url = getUrlPOST($urlArr[0],$urlArr[1]);
        $url = getCenterText('"url": "','"',$url);
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);
    }elseif($reqCount == '12'){
        $code = getUrlRandomWord();
        $urlArr[1] = str_replace('{code}',$code,$urlArr[1]);
        $url = getUrlPOST($urlArr[0],$urlArr[1]);
        $url = getCenterText('value="','" dir="',$url);
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);
    }elseif($reqCount == '13'){
        $url = getLocPOST($urlArr[0],$urlArr[1]);
        $url = curlGET($url);
        $url = getCenterText('target="_blank">','</a>',$url);
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);
    }elseif($reqCount == '14'){
        $url = getLocPOST($urlArr[0],$urlArr[1]);
        $url = curlGET($url);
        $url = getCenterText('target="_blank">','</a>',$url);
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);
    }elseif($reqCount == '15'){
        $url = getLocPOST($urlArr[0],$urlArr[1]);
        $url = curlGET($url);
        $url = getCenterText('target="_blank">','</a>',$url);
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);
    }elseif($reqCount == '16'){
        $url = getUrlPOST($urlArr[0],$urlArr[1]);
        $url = getCenterText('id="res" value="','"',$url);
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);
    }elseif($reqCount == '17'){
        $url = getUrlPOST($urlArr[0],$urlArr[1]);
        $url = getCenterText('http://link5s.com/','"',$url);
        $url = $urlArr[2] . '/' . $url;
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);
    }elseif($reqCount == '18'){
        $url = getLocPOST($urlArr[0],$urlArr[1]);
        $url = curlGET($url);
        $url = getCenterText('target="_blank">','</a>',$url);
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);
    }elseif($reqCount == '19'){
        $url = getUrlPOST($urlArr[0],$urlArr[1]);
        $url = getCenterText('http:\/\/p.pw\/','"',$url);
        $url = $urlArr[2] . '/' . $url;
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);
    }elseif($reqCount == '20'){
        $url = getLocPOST($urlArr[0],$urlArr[1]);
        $url = curlGET($url);
        $url = getCenterText('target="_blank">','</a>',$url);
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);
    }elseif($reqCount == '21'){
        $url = getUrlPOST($urlArr[0],$urlArr[1]);
        $url = getCenterText('target="_blank" href="','"',$url);
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);
    }elseif($reqCount == '22'){
        $url = getLocPOST2($urlArr[0],$urlArr[1]);
        $url = curlGET($url);
        $url = getCenterText('target="_blank">','</a>',$url);
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);
    }elseif($reqCount == '23'){
        $url = getLocPOST2($urlArr[0],$urlArr[1]);
        $url = curlGET($url);
        $url = getCenterText('target="_blank">','</a>',$url);
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);
    }elseif($reqCount == '24'){
        $url = getLocPOST($urlArr[0],$urlArr[1]);
        $url = curlGET($url);
        $url = getCenterText('<div class="url-button">','</div>',$url);
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);
    }elseif($reqCount == '25'){
        $url = getUrlPOST($urlArr[0],$urlArr[1]);
        $url = getCenterText('<SPAN ID="copytext">','</SPAN>',$url);
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);
    }elseif($reqCount == '26'){
        $url = curlGET($urlArr[1]);
        $url = getCenterText('class="indent"><b>','</b>',$url);
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);  
    }elseif($reqCount == '27'){
        $url = getUrlPOST($urlArr[0],$urlArr[1]);
        $url = getCenterText('value="','"',$url);
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);
    }elseif($reqCount == '28'){
        $url = getUrlPOST($urlArr[0],$urlArr[1]);
        $url = getCenterText('class=\"first\"','class=\"second\"',$url);
        $url = getCenterText('href=\"','\"',$url);
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);
    }elseif($reqCount == '29'){
        $url = getUrlPOST($urlArr[0],$urlArr[1]);
        $url = getCenterText('value="','"',$url);
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);
    }elseif($reqCount == '30'){
        $ex = new adfshort();
        $res = $ex->shorten(array(urldecode($urlArr[1])), 'adfly.local');
        $urlData = $res['data'][0];
        $url = $urlData['short_url'];
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
            die('Error');
        else
            die($url);
    }
}else{
    
    //Error
    die('Error');
    
}

die();
?>