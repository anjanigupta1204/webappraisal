<?php

/*
 * @author Balaji
 * @name A to Z SEO Tools - PHP Script
 * @copyright � 2016 ProThemes.Biz
 *
 */

if(!function_exists('strEOLWin')){
   function strEOLWin($str){
       return str_replace(array('\r\n','\n','\r'),PHP_EOL,$str);
    }
}

if(!function_exists('br2nl')){
    function br2nl($str){
        return str_replace(array('<br>','<br />','<br/>','<BR>','<BR />','<BR/>'),PHP_EOL,$str);
    }
}

if(!function_exists('str_lreplace')){
    function str_lreplace($search, $replace, $subject) {
        $pos = strrpos($subject, $search);
    
        if($pos !== false)
        {
            $subject = substr_replace($subject, $replace, $pos, strlen($search));
        }
    
        return $subject;
    }
}

//Article Scraper - Examiner.Com

function extractExaminer($link,$removeLinks=true,$removeImages=true,$removeComHTML=false,$keepNewLinewithRemove=true,$removeScripts=false){
    
    $htmlData = curlGET($link);
    
    $headerData = getCenterText('<header class="article-header">','</header>',$htmlData);
    $articleTitle = strEOLWin(strip_tags(getCenterText('<h1 class="page-title">','</h1>',$headerData)));
    $bodyData = getCenterText('class="article-content">','<footer class="article-footer">',$htmlData);
    $bodyData = Trim(str_lreplace('</section>','',$bodyData));
    
    if(!$removeComHTML){
        if($removeLinks)
            $bodyData = preg_replace('#<a.*?>(.*?)</a>#i', '\1', $bodyData);
        
        if($removeImages)
            $bodyData = preg_replace('/<img[^>]+\>/i', '', $bodyData);
            
        if($removeScripts)
           $bodyData = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $bodyData);
           
        if($keepNewLinewithRemove){
            $bodyData = strip_tags($bodyData, '<br><br /><br/><BR><BR /><BR/>');
        }
    }else{
        $bodyData = strip_tags($bodyData);
    } 

    return array(Trim($articleTitle), Trim($bodyData));
}

function searchExaminer($searchKeyword,$count=10){
   
    $arrayContent = array();
    
    $searchData = curlGET('https://www.googleapis.com/customsearch/v1element?key=AIzaSyCVAXiUzRYsML1Pv6RwSG1gunmMikTzQqY&rsz=filtered_cse&num='.$count.'&hl=en&prettyPrint=false&source=gcsc&gss=.com&sig=8bdfc79787aa2b2b1ac464140255872c&cx=partner-pub-7479725245717969:9ze01gmnpyp&q='.$searchKeyword.'&safe=active&googlehost=www.google.com&callback=google.search.Search.apiary14533');
    $searchData = explode('google.search.Search.apiary14533(',$searchData);
    $searchData = explode(');',$searchData[1]);
    $searchData = $searchData[0];
    $searchData = json_decode($searchData, true);
    $dataRes = $searchData['results'];
    
    foreach($dataRes as $dataContent){
        //$title = strEOLWin(strip_tags($dataContent['title']));
        //$title = str_replace(array('| Examiner.com','| Examiner'),'',$title);
        $url = $dataContent['url'];

        if(str_contains($url, '/article/')){            
            $articleContent = extractExaminer($url);
            if($articleContent[1] != '')
                $arrayContent[] = $articleContent;
        }
    }
    
    return $arrayContent;
}


//Article Scraper - Articlesbase.Com

function searchArticlesbase($searchKeyword,$count=10){
   
    $arrayContent = array();
    
    $searchData = curlGET('https://www.googleapis.com/customsearch/v1element?key=AIzaSyCVAXiUzRYsML1Pv6RwSG1gunmMikTzQqY&rsz=filtered_cse&num='.$count.'&hl=en&prettyPrint=false&source=gcsc&gss=.com&sig=8bdfc79787aa2b2b1ac464140255872c&cx=partner-pub-5157679868954075:5812071684&q='.$searchKeyword.'&googlehost=www.google.com&callback=google.search.Search.apiary4670');
    $searchData = explode('google.search.Search.apiary4670(',$searchData);
    $searchData = explode(');',$searchData[1]);
    $searchData = $searchData[0];
    $searchData = json_decode($searchData, true);
    $dataRes = $searchData['results'];
    
    foreach($dataRes as $dataContent){
        $url = $dataContent['url'];
        $articleContent = extractArticlesbase($url);
        if($articleContent[1] != '')
            $arrayContent[] = $articleContent;
    }
    
    return $arrayContent;
}

function extractArticlesbase($link,$removeLinks=true,$removeImages=true,$removeComHTML=false,$keepNewLinewithRemove=true,$removeScripts=true){
    
    $htmlData = curlGET($link);
    
    $htmlData = getCenterText('<div class="post single"','<div class="row">',$htmlData);
    $htmlData = '<div class="post single"'.$htmlData;

    $headerData = getCenterText('<h1 class="post_title" itemprop="name">','</h1>',$htmlData);
    $articleTitle = strEOLWin(strip_tags($headerData));

    $bodyData = getCenterText('class="content_box">','<hr id="last" />',$htmlData.'<hr id="last" />');
    $bodyData = Trim($bodyData);
        
    if(!$removeComHTML){
        if($removeLinks)
            $bodyData = preg_replace('#<a.*?>(.*?)</a>#i', '\1', $bodyData);
        
        if($removeImages)
            $bodyData = preg_replace('/<img[^>]+\>/i', '', $bodyData);
        
        if($removeScripts)
           $bodyData = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $bodyData);
 
        if($keepNewLinewithRemove){
            $bodyData = strip_tags($bodyData, '<br><br /><br/><BR><BR /><BR/>');
        }
    }else{
        $bodyData = strip_tags($bodyData);
    } 
    
    return array(Trim($articleTitle), Trim($bodyData));
}


//Article Scraper - Ezinearticles.Com

function searchEzinearticles($searchKeyword,$count=10){
   
    $arrayContent = array();
    
    $searchData = curlGET('https://www.googleapis.com/customsearch/v1element?key=AIzaSyCVAXiUzRYsML1Pv6RwSG1gunmMikTzQqY&rsz=filtered_cse&num='.$count.'&hl=en&prettyPrint=false&source=gcsc&gss=.com&sig=8bdfc79787aa2b2b1ac464140255872c&cx=partner-pub-3754405753000444:8093409890&q='.$searchKeyword.'&googlehost=www.google.com&callback=google.search.Search.apiary6313');
    $searchData = explode('google.search.Search.apiary6313(',$searchData);
    $searchData = explode(');',$searchData[1]);
    $searchData = $searchData[0];
    $searchData = json_decode($searchData, true);
    $dataRes = $searchData['results'];
    
    foreach($dataRes as $dataContent){
        $url = $dataContent['url'];
        $url = urldecode($url);
        
        if(!str_contains($url, '?cat=')){
            $articleContent = extractEzinearticles($url);
            if($articleContent[1] != '')
                $arrayContent[] = $articleContent;
        }
    }
   
    return $arrayContent;
}

function extractEzinearticles($link,$removeLinks=true,$removeImages=true,$removeComHTML=false,$keepNewLinewithRemove=true,$removeScripts=true){
    
    $htmlData = curlGET($link);

    $headerData = getCenterText('<div id="article-title">','</div>',$htmlData);
    $articleTitle = strEOLWin(strip_tags(getCenterText('<h1>','</h1>',$headerData)));
    $bodyData = getCenterText('<div id="article-body">','<div id="article-resource">',$htmlData);
    $bodyData = Trim(str_lreplace('</div>','',$bodyData));
    
    if(!$removeComHTML){
        if($removeLinks)
            $bodyData = preg_replace('#<a.*?>(.*?)</a>#i', '\1', $bodyData);
        
        if($removeImages)
            $bodyData = preg_replace('/<img[^>]+\>/i', '', $bodyData);
            
        if($removeScripts)
           $bodyData = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $bodyData);
           
        if($keepNewLinewithRemove){
            $bodyData = strip_tags($bodyData, '<br><br /><br/><BR><BR /><BR/>');
        }
    }else{
        $bodyData = strip_tags($bodyData);
    } 

    return array(Trim($articleTitle), Trim($bodyData));
    
}
?>