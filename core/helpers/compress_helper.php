<?php

defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright � 2016 ProThemes.Biz
 *
 */

//CSS Minifier
function compressCSS($buffer) {     

    // Remove comments
    $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
    
    // Remove whitespace
    $buffer = str_replace(': ', ':', $buffer);
    
    $buffer = str_replace(' :', ':', $buffer);
    
    $buffer = str_replace(' ;', ';', $buffer);
    
    $buffer = str_replace('; ', ';', $buffer);
    
    $buffer = str_replace('{ ', '{', $buffer);
    
    $buffer = str_replace(' {', '{', $buffer);
    
    $buffer = str_replace('} ', '}', $buffer);
    
    $buffer = str_replace(' }', '}', $buffer);
    
    $buffer = str_replace(' ,', ',', $buffer);
    
    $buffer = str_replace(', ', ',', $buffer);
    
    $buffer = str_replace('  .', ' .', $buffer);
    
    $buffer = str_replace(array("\r\n", "\r", "\n", "\t"), '', $buffer);
    
    $buffer = str_replace(array('   ', '  '), ' ', $buffer);
    
    return $buffer;
}

//HTML Minifier
function compressHTML($buffer) {
    
    //Check HTML is valid
    if(trim($buffer) === "") 
        return $buffer;
    
    //CSS Minifier -> A to Z SEO Tools
    if(strpos($buffer, ' style=') !== false) {
        $buffer = preg_replace_callback('#<([^<]+?)\s+style=([\'"])(.*?)\2(?=[\/\s>])#s', function($matches) {
            return '<' . $matches[1] . ' style=' . $matches[2] . compressCSS($matches[3]) . $matches[2];
        }, $buffer);
    }
    
    if(strpos($buffer, 'style>') !== false) {
        $buffer = preg_replace_callback('#<style[^>]*>(.+?)</style>#s', function($matches) {
            return compressCSS($matches[0]);
        }, $buffer);
    }
    
    //Remove whitespace
    $replace = array(
        '/\>[^\S ]+/s'   => '>',
        '/[^\S ]+\</s'   => '<',
        '/([\t ])+/s'  => ' ',
        '/^([\t ])+/m' => '',
        '/([\t ])+$/m' => '',
        '~//[a-zA-Z0-9 ]+$~m' => '',
        '/[\r\n]+([\t ]?[\r\n]+)+/s'  => "\n",
        '/\>[\r\n\t ]+\</s'    => '><',
        '/}[\r\n\t ]+/s'  => '}',
        '/}[\r\n\t ]+,[\r\n\t ]+/s'  => '},',
        '/\)[\r\n\t ]?{[\r\n\t ]+/s'  => '){',
        '/,[\r\n\t ]?{[\r\n\t ]+/s'  => ',{',
        '/\),[\r\n\t ]+/s'  => '),',
        '~([\r\n\t ])?([a-zA-Z0-9]+)="([a-zA-Z0-9_/\\-]+)"([\r\n\t ])?~s' => '$1$2=$3$4',
    );
    $buffer = preg_replace(array_keys($replace), array_values($replace), $buffer);
    return $buffer;    
}

//Image Optimizer
function compressImage($sourcePath, $quality=90) {

    $info = getimagesize($sourcePath);

	if ($info['mime'] == 'image/jpeg'){
        $image = imagecreatefromjpeg($sourcePath);
        $quality = 90;
        imagejpeg($image, $sourcePath, $quality);
    } elseif ($info['mime'] == 'image/png'){
        $image = imagecreatefrompng($sourcePath);
        $quality = 9;
        imagealphablending($image, false);
        imagesavealpha($image, true);
        imagepng($image, $sourcePath, $quality);
	}else{
	   //Unknown Image Type
	}

	return array($sourcePath,$info['mime']);
}

//File Array Conversion
function conArrayFiles(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}

?>