<?php

/*
 * @author Balaji
 * @name A to Z SEO Tools - PHP Script
 * @copyright � 2016 ProThemes.Biz
 *
 */

function externalUrlDB(){
    $dataArr = array();
    
    //POST URL //POST DATA //Domain
    $dataArr[] = array('https://9m.no/create', 'url={url}','https://9m.no');
    $dataArr[] = array('http://lnk.bz/', 'url={url}&ujurl=Felvesz','http://lnk.bz');
    $dataArr[] = array('http://zzb.bz/panel/short/add_short', 'urlOrigin={url}&urlCustom=&urlPassword=&urlDescription=','http://zzb.bz');
    $dataArr[] = array('http://lin.io/', 'u_advanced=&u={url}&submit=+','http://lin.io');
    $dataArr[] = array('http://b54.in/', 'urla={url}&hc=check&keyword=','http://b54.in');
    $dataArr[] = array('http://flaturl.com/index.php', 'u_advanced=&u={url}&x=27&y=11','http://flaturl.com');
    $dataArr[] = array('http://boo.vg/', 'action=shorten&url={url}','http://boo.vg');
    $dataArr[] = array('http://num.to/', 'url={url}','http://num.to');
    $dataArr[] = array('http://infodetil.com/index.html', 'agreeTerms=1&longUrl={url}&submitted=1&submit=&customUrl=&shortUrlType=1&shortUrlPassword=&shortUrlExpiryDate=&shortUrlUses=0&additionalOptState=0','http://num.to');
    $dataArr[] = array('http://llinks.net/index.html', 'agreeTerms=1&longUrl={url}&submitted=1&submit=&customUrl=&shortUrlType=1&shortUrlPassword=&shortUrlExpiryDate=&shortUrlUses=0&additionalOptState=0','http://llinks.net');
    $dataArr[] = array('http://bit.do/mod_perl/url-shortener.pl', 'action=shorten&url={url}&url2=site2&url_hash=&url_stats_is_private=0&permasession=1463777607%7Cu5o41np5c6','http://bit.do');
    $dataArr[] = array('http://mcaf.ee/api/shorten', 'input_url={url}','http://mcaf.ee');    
    $dataArr[] = array('http://cutt.us/', 'txt_url={url}&txt_name={code}','http://cutt.us');
    $dataArr[] = array('http://linkk.in/index.html', 'agreeTerms=1&longUrl={url}&submitted=1&submit=&customUrl=&shortUrlType=1&shortUrlPassword=&shortUrlExpiryDate=&shortUrlUses=0&additionalOptState=0','http://linkk.in');
    $dataArr[] = array('http://fl-y.com/index.html', 'agreeTerms=1&longUrl={url}&submitted=1&submit=&customUrl=&shortUrlType=1&shortUrlPassword=&shortUrlExpiryDate=&shortUrlUses=0&additionalOptState=0','http://fl-y.com');
    $dataArr[] = array('http://ohi.im/index.html', 'agreeTerms=1&longUrl={url}&submitted=1&submit=&customUrl=&shortUrlType=1&shortUrlPassword=&shortUrlExpiryDate=&shortUrlUses=0&additionalOptState=0','http://ohi.im');    
    $dataArr[] = array('http://qrf.in/', 'url={url}&res=http%3A%2F%2Fqrf.in%2F','http://qrf.in');
    $dataArr[] = array('http://link5s.com/ajax/ajax.php', 'type=inter&url={url}','http://link5s.com');  
    $dataArr[] = array('http://1ab.ir/index.php', 'agreeTerms=1&longUrl={url}&submitted=1&submit=&customUrl=&shortUrlType=1&shortUrlPassword=&shortUrlExpiryDate=&shortUrlUses=0&additionalOptState=0','http://1ab.ir');
    $dataArr[] = array('http://p.pw/API/write/post', 'url={url}','http://p.pw');
    $dataArr[] = array('http://x11.pw/index.php', 'agreeTerms=1&longUrl={url}&submitted=1&submit=&customUrl=&shortUrlType=1&shortUrlPassword=&shortUrlExpiryDate=&shortUrlUses=0&additionalOptState=0','http://x11.pw');
    $dataArr[] = array('http://urlin.it/home.htm', 'hl=en&Url={url}&cmdOK=OK','http://urlin.it');
    $dataArr[] = array('http://v88.ca/index.php', 'agreeTerms=1&longUrl={url}&submitted=1&submit=&customUrl=&shortUrlType=1&shortUrlPassword=&shortUrlExpiryDate=&shortUrlUses=0&additionalOptState=0','http://v88.ca');
    $dataArr[] = array('http://urladda.com/index.php', 'longUrl={url}&submitted=1&customUrl=&shortUrlPassword=&shortUrlExpiryDate=&shortUrlUses=0&shortUrlType=0','http://urladda.com');  
    $dataArr[] = array('http://qr.net/', 'url={url}&submit=shorten&custom_shorturl=&password=&expirein=2592000','http://qr.net');
    $dataArr[] = array('http://rarme.com/', 'rnd=542865439&fullurl={url}&keyword=','http://rarme.com');   
    $dataArr[] = array('http://tinyurl.com', 'http://tinyurl.com/create.php?source=indexpage&url={url}&submit=Make+TinyURL!&alias=','http://tinyurl.com');         
    $dataArr[] = array('https://is.gd/create.php', 'url={url}&shorturl=&opt=0','https://is.gd');  
    $dataArr[] = array('http://u.to/', 'url={url}&a=add','http://u.to/');  
    $dataArr[] = array('https://v.gd/create.php', 'url={url}&shorturl=&opt=0','https://v.gd/');  
    $dataArr[] = array('adfly.local', '{url}','https://adf.ly/');  
    return $dataArr;

}

function getLocPOST($url,$post_data){
    $cookie=tempnam("/tmp","CURLCOOKIE");
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0');
    curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
	curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
	curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: application/x-www-form-urlencoded;","Accept: */*"));
	curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_REFERER, 'http://google.com');
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$post_data);
    $html=curl_exec($ch);
    curl_close($ch);
    preg_match("!\r\n(?:Location|URI): *(.*?) *\r\n!", $html, $matches);
    $url = $matches[1];
    return $url;
}

function getUrlPOST($url,$post_data){
    $cookie=tempnam("/tmp","CURLCOOKIE");
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0');
    curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
	curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
	curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: application/x-www-form-urlencoded;","Accept: */*"));
	curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_REFERER, 'http://google.com');
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$post_data);
    $html=curl_exec($ch);
    curl_close($ch);
    return $html;
}

function getLocPOST2($url,$post_data){
    $cookie=tempnam("/tmp","CURLCOOKIE");
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0');
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "Accept-Language: en-US,en;q=0.5",
    "Connection: keep-alive",
    "Content-Type: application/x-www-form-urlencoded"
    ));
    curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
	curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
	curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_REFERER, 'http://google.com');
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$post_data);
    $html=curl_exec($ch);
    curl_close($ch);
    preg_match("!\r\n(?:Location|URI): *(.*?) *\r\n!", $html, $matches);
    $url = $matches[1];
    return $url;
}

function getUrlPOST2($url,$post_data){
    $cookie=tempnam("/tmp","CURLCOOKIE");
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0');
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "Accept-Language: en-US,en;q=0.5",
    "Connection: keep-alive",
    "Content-Type: application/x-www-form-urlencoded"
    ));
    curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
	curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
	curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_REFERER, 'http://google.com');
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$post_data);
    $html=curl_exec($ch);
    curl_close($ch);
    return $html;
}

function getUrlRandomWord() {
    $len = rand(5,15);
    $word = array_merge(range('a', 'z'), range('A', 'Z'));
    shuffle($word);
    return substr(implode($word), 0, $len);
}

if(!function_exists('getCenterText')){
    function getCenterText($str1,$str2,$data){
        $data = explode($str1,$data);
        $data = explode($str2,$data[1]);
        return Trim($data[0]);
    }
}

?>