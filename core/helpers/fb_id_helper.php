<?php

/*
* @author Balaji
* @name A to Z SEO Tools
* @copyright � 2016 ProThemes.Biz
*
*/

if(!function_exists('getCenterText')){
    function getCenterText($str1,$str2,$data){
        $data = explode($str1,$data);
        $data = explode($str2,$data[1]);
        return Trim($data[0]);
    }
}

function getMyFBID($fbUrl){
    
    $fbId = $fbData = '';
    
    $fbUrl = Trim($fbUrl);
    
    $fbData = curlGET($fbUrl);
    
    if($fbData != ''){
        $fbId = getCenterText('"entity_id":"','"',$fbData);
        
        if($fbId == '')
            $fbId = 'Not a valid facebook url';
    } else
        $fbId = 'Error';
        
    return $fbId;
}

?>