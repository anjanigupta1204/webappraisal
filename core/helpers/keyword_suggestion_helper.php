<?php

/*
 * @author Balaji
 * @name A to Z SEO Tools - PHP Script
 * @copyright � 2016 ProThemes.Biz
 *
 */
 

function getGoogleSuggestion($userInput) {
    
    $googleUrl = 'http://suggestqueries.google.com/complete/search';
    $keywords = array();
    $json = getMyData($googleUrl.'?output=firefox&client=firefox&hl=en-US&q='.urlencode($userInput));
    
    if($json == '')
        return $keywords;
    
    $json = json_decode($json, true);
    $keywords = $json[1];

    return $keywords;
}

function getYoutubeSuggestion($userInput) {
    
    $youtubeUrl = 'http://suggestqueries.google.com/complete/search';
    $keywords = array();
    $json = getMyData($youtubeUrl.'?client=youtube&ds=yt&client=firefox&q='.urlencode($userInput));
    
    if($json == '')
        return $keywords;
    
    $json = json_decode($json, true);
    $keywords = $json[1];

    return $keywords;
}

function getBingSuggestion($userInput) {
    
    $bingUrl = 'http://api.bing.com/osjson.aspx';
    $keywords = array();
    $json = getMyData($bingUrl.'?query='.urlencode($userInput));
    
    if($json == '')
        return $keywords;
    
    $json = json_decode($json, true);
    $keywords = $json[1];

    return $keywords;
}

function getYahooSuggestion($userInput) {
    
    $yahooUrl = 'http://ff.search.yahoo.com/gossip';
    $keywords = array();
    $json = getMyData($yahooUrl.'?output=fxjson&command='.urlencode($userInput));
    
    if($json == '')
        return $keywords;
    
    $json = json_decode($json, true);
    $keywords = $json[1];

    return $keywords;
}

function getAmazonSuggestion($userInput) {
    
    $amazonUrl = 'http://completion.amazon.com/search/complete';
    $keywords = array();
    $json = getMyData($amazonUrl.'?search-alias=aps&client=amazon-search-ui&mkt=1&q='.urlencode($userInput));
    
    if($json == '')
        return $keywords;
    
    $json = json_decode($json, true);
    $keywords = $json[1];

    return $keywords;
}

function getWikipediaSuggestion($userInput) {
    
    $wikipediaUrl = 'http://en.wikipedia.org/w/api.php';
    $keywords = array();
    $json = getMyData($wikipediaUrl.'?action=opensearch&search='.urlencode($userInput));
    
    if($json == '')
        return $keywords;
    
    $json = json_decode($json, true);
    $keywords = $json[1];

    return $keywords;
}
?>