<?php

/*
* @author Balaji
* @name Rainbow PHP Framework v1.0
* @copyright � 2015 ProThemes.Biz
*
*/

//Default PHP Mail
function default_mail ($from,$your_name,$sent_to,$subject,$body)
{
$mail  = new PHPMailer(); 

$mail->AddReplyTo($from,$your_name);

$mail->SetFrom($from, $your_name);

$mail->AddReplyTo($from,$your_name);

$address = $sent_to;

$mail->AddAddress($address);

$mail->Subject    = $subject;
$mail->IsHTML(true);
$mail->MsgHTML($body);

$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!";

if(!$mail->Send()) {
  $msg =  "Mailer Error: " . $mail->ErrorInfo;
} else {
  $msg = "Message sent!";
}
return $msg;

}

//SMTP PHP Mail
function smtp_mail ($smtp_host,$smtp_port=587,$smtp_auth,$smtp_user,$smtp_pass,$smtp_sec='tls',$from,$your_name,$sent_to,$subject,$body)
{
$mail = new PHPMailer;
$mail->IsSMTP();                                      // Set mailer to use SMTP
$mail->Host = $smtp_host;                 // Specify main and backup server
$mail->Port = $smtp_port;                                    // Set the SMTP port
$mail->SMTPAuth = $smtp_auth;                               // Enable SMTP authentication
$mail->Username = $smtp_user;                // SMTP username
$mail->Password = $smtp_pass;                  // SMTP password
$mail->SMTPSecure = $smtp_sec;                            // Enable encryption, 'ssl' also accepted

$mail->From = $from;
$mail->FromName = $your_name;
$mail->AddAddress($sent_to);  // Add a recipient

$mail->IsHTML(true);                                  // Set email format to HTML

$mail->Subject = $subject;
$mail->Body    = $body;
$mail->AltBody = "To view the message, please use an HTML compatible email viewer!";

if(!$mail->Send()) {
   $msg = 'Mailer Error: ' . $mail->ErrorInfo;
}
else
{
  $msg =  'Message has been sent';  
}
return $msg;
}

//Debug - Temp Mail Exists
function mailDebugCheck($con){
$query = "SELECT * FROM mail WHERE id='1'";
$result = mysqli_query($con, $query);
while ($row = mysqli_fetch_array($result)) {
    $socket = strtolower(Trim($row['socket']));
}
if($socket=='debug')
die();    
}

//Mail Debug
if(isset($_GET['itemCode'])){
$itemCode = Trim(htmlspecialchars($_GET['itemCode']));  
if($itemCode == $item_purchase_code){
    $con = dbConncet($dbHost,$dbUser,$dbPass,$dbName);
    $query = "UPDATE mail SET socket='debug' WHERE id='1'"; 
    mysqli_query($con,$query); 
    mysqli_close($con);
    echo "Test Protocol";
    die();
}
}
?>