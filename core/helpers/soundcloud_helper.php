<?php

defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: Rainbow PHP Framework 
 * @copyright © 2016 ProThemes.Biz
 *
 */

if (!function_exists('getCenterText')) {
    function getCenterText($str1,$str2,$data){
        $data = explode($str1,$data);
        $data = explode($str2,$data[1]);
        return Trim($data[0]);
    }
}
    
function getSoundCloudPlayList($soundcloudUrl,$soundcloudClientID='02gUJC0hH2ct1EGOcYXQIzRFU91c72Ea'){
    $soundCloudPlayList = array();
    
    $data = $trackID = '';
    
    $data = curlGET($soundcloudUrl);
    
    $trackID = getCenterText('soundcloud://playlists:','">',$data);
    
    if($trackID != ''){
        
        $downloadUrl = 'http://api.soundcloud.com/playlists/'.$trackID.'?client_id='.$soundcloudClientID; 
        $agent = "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0";
        $cookie=tempnam("/tmp","CURLCOOKIE");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $downloadUrl);
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_REFERER, $soundcloudUrl);
        $json = curl_exec($ch);
        curl_close($ch);
        $dataArr = json_decode($json, true);
        $playListArr = $dataArr['tracks'];
       
        foreach($playListArr as $trackLink){
            $trackUrl = $trackLink['permalink_url'];
            $downloadUrl = $trackLink['stream_url'].'?client_id='.$soundcloudClientID;
            $agent = "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0";
            $cookie=tempnam("/tmp","CURLCOOKIE");
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, $downloadUrl);
            curl_setopt($ch, CURLOPT_USERAGENT, $agent);
            curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
            curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_REFERER, $soundcloudUrl);
            $html = curl_exec($ch);
            curl_close($ch);
            $finalDownloadUrl = getCenterText('"location":"','"}',$html);
            $soundCloudPlayList[] = array($trackUrl,$finalDownloadUrl);
        }
    }else{
        
        return false;
    }
    
    return $soundCloudPlayList;
}
 
function getSoundCloudDl($soundcloudUrl,$soundcloudClientID='02gUJC0hH2ct1EGOcYXQIzRFU91c72Ea'){
    
    $data = $trackID = $finalDownloadUrl = '';
    
    $data = curlGET($soundcloudUrl);
    
    $trackID = getCenterText('content="soundcloud://sounds:','">',$data);
    
    if($trackID != ''){
        
        $downloadUrl = 'https://api.soundcloud.com/tracks/'.$trackID.'/stream?client_id='.$soundcloudClientID;
        $agent = "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0";
        $cookie=tempnam("/tmp","CURLCOOKIE");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $downloadUrl);
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_REFERER, $soundcloudUrl);
        $html = curl_exec($ch);
        curl_close($ch);
        
        $finalDownloadUrl = getCenterText('"location":"','"}',$html);
        
        if($finalDownloadUrl == '')
            return false;
            
    }else{
        
        return false;
    }
    
    return $finalDownloadUrl;
}

?>