<?php

/*
* @author Balaji
* @name Rainbow PHP Framework
* @copyright � 2015 ProThemes.Biz
*
*/

function outQuestionBox($str){
    echo '<div class="questionBox" data-original-title="'.$str.'" data-toggle="tooltip" data-placement="top">
        <i class="fa fa-question-circle grayColor"></i>
    </div>';
}

function ordinalNum($num) {
    $num = (int)$num;
    // Special case "teenth"
    if ( ($num / 10) % 10 != 1 )
    {
        // Handle 1st, 2nd, 3rd
        switch( $num % 10 )
        {
            case 1: return $num . 'st';
            case 2: return $num . 'nd';
            case 3: return $num . 'rd'; 
        }
    }
    // Everything else is "nth"
    return number_format($num) . 'th';
}

function outHeadBox($title,array $sugMsg, $type){
    if($type == 1){
        $solveMsg = $sugMsg[0]; 
        $one = 'solveMsgBlue';
        $two = '';
        $three = '';
     }elseif($type == 2){
        $solveMsg = $sugMsg[1]; 
        $one = 'solveMsgBlue';
        $two = 'solveMsgBlue';
        $three = '';
     }elseif($type == 3){
        $solveMsg = $sugMsg[2]; 
        $one = 'solveMsgBlue';
        $two = 'solveMsgBlue';
        $three = 'solveMsgBlue';
     }else{
        $solveMsg = $sugMsg[3]; 
        $one = '';
        $two = '';
        $three = '';
     }
     
    echo '<div class="headBox clearfix">
            <h4 class="titleStr">'.$title.'</h4>
            <h4 class="solveMsg" data-original-title="'.$solveMsg.'" data-toggle="tooltip" data-placement="top">
                <i class="fa fa-gear fa-xs '.$one.'"></i>
                <i class="fa fa-gear fa-xs '.$two.'"></i>
                <i class="fa fa-gear fa-xs '.$three.'"></i>
            </h4>
         </div>';
}

?>