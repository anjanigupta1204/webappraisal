
//Article Scraper
function exportFile(data,filename){
    var fileNameToSaveAs = $(filename).val()+'.txt';
    var textToWrite = $(data).val();
    textToWrite = textToWrite.replace(/\n/g, "\r\n");
    var textFileAsBlob = new Blob([textToWrite], {type:'text/plain'});
    var downloadLink = document.createElement("a");
    downloadLink.download = fileNameToSaveAs;
    downloadLink.innerHTML = "My Link";
    window.URL = window.URL || window.webkitURL;
    downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
    downloadLink.onclick = destroyClickedElement;
    downloadLink.style.display = "none";
    document.body.appendChild(downloadLink);
    downloadLink.click();
}

function destroyClickedElement(event){
    document.body.removeChild(event.target);
}

function selectText(id) {
     $(id).select();
}

jQuery(document).ready(function(){
    jQuery("#checkButton").click(function()
    {
        var myKeyword = jQuery("#keyword").val();
        var selArticle= false;
        var selLimit = jQuery("#limit").val();
        var searchLimit = 0;
        var selArticleLimit = 0;   
        if ($('input[name=examiner]:checked').length > 0){
            selArticle = true;
            selArticleLimit = selArticleLimit + 1;
        }
        
        if ($('input[name=articlesbase]:checked').length > 0){
            selArticle = true;
            selArticleLimit = selArticleLimit + 1;
        }
                
        if ($('input[name=ezinearticles]:checked').length > 0){
            selArticle = true;
            selArticleLimit = selArticleLimit + 1;
        }
    
        if(myKeyword == ""){
            alert('Enter a Keyword!');
            return false;
        }
        
        if(!selArticle){
            alert('Select any article content site!');
            return false;
        }
        
        searchLimit  = Math.round(selLimit / selArticleLimit);

        if ($("#capCode").length > 0){
            var sCode= jQuery.trim($('input[name=scode]').val());
            if (sCode==null || sCode=="") {
            alert("Image verification field can't empty!");
            return false;
        } else {
        jQuery.post('../?route=ajax',{capthca:'1', scode:sCode},function(data){
        if (data == '1') {
            //Okay
            jQuery("#mainbox").fadeOut();
            jQuery("#resultBox").css({"display":"block"});
            jQuery("#resultBox").show();
            jQuery("#resultBox").fadeIn();
            jQuery(".percentimg").css({"display":"block"});
            jQuery(".percentimg").show();
            jQuery(".percentimg").fadeIn();
            
            setTimeout(function(){
            var pos = $('#title').offset();
            $('body,html').animate({ scrollTop: pos.top });
            }, 500);
            
            if ($('input[name=examiner]:checked').length > 0){
                jQuery.post('../?route=scraper',{scraper:'1', scode:'Null', scraperType:'examiner', keyword:myKeyword, articleLimit:searchLimit},function(data){
                    if(data != '')
                        jQuery("#results").append(data);
                    else
                         jQuery("#results").append('<div class="text-center"> <h4> No data found from Examiner </h4></div>'); 
                });
            }
            
            if ($('input[name=articlesbase]:checked').length > 0){
                jQuery.post('../?route=scraper',{scraper:'1', scode:'Null', scraperType:'articlesbase', keyword:myKeyword, articleLimit:searchLimit},function(data){
                    if(data != '')
                        jQuery("#results").append(data);
                    else
                        jQuery("#results").append('<div class="text-center"> <h4> No data found from Articlesbase </h4></div>'); 
                });
            }
                    
            if ($('input[name=ezinearticles]:checked').length > 0){
                jQuery.post('../?route=scraper',{scraper:'1', scode:'Null', scraperType:'ezinearticles', keyword:myKeyword, articleLimit:searchLimit},function(data){
                    if(data != '')
                        jQuery("#results").append(data);
                    else
                    jQuery("#results").append('<div class="text-center"> <h4> No data found from Ezinearticles </h4></div>');
                    
                });
            }
            
            jQuery.post('../?route=scraper',{out:'1'},function(data){
                    jQuery(".percentimg").fadeOut();
                    jQuery("#title").slideDown();
                    setTimeout(function(){
                    var pos = $('#title').offset();
                    $('body,html').animate({ scrollTop: pos.top });
                    }, 500);
            });
            }
            else {
                alert("Image verification code is wrong!");
                return false;
            }
        });   
        }
        }else{
        jQuery("#mainbox").fadeOut();
        jQuery("#resultBox").css({"display":"block"});
        jQuery("#resultBox").show();
        jQuery("#resultBox").fadeIn();
        jQuery(".percentimg").css({"display":"block"});
        jQuery(".percentimg").show();
        jQuery(".percentimg").fadeIn();
        
        setTimeout(function(){
        var pos = $('#title').offset();
        $('body,html').animate({ scrollTop: pos.top });
        }, 500);
        
        if ($('input[name=examiner]:checked').length > 0){
            jQuery.post('../?route=scraper',{scraper:'1', scode:'Null', scraperType:'examiner', keyword:myKeyword, articleLimit:searchLimit},function(data){
                if(data != '')
                    jQuery("#results").append(data);
                else
                     jQuery("#results").append('<div class="text-center"> <h4> No data found from Examiner </h4></div>'); 
            });
        }
        
        if ($('input[name=articlesbase]:checked').length > 0){
            jQuery.post('../?route=scraper',{scraper:'1', scode:'Null', scraperType:'articlesbase', keyword:myKeyword, articleLimit:searchLimit},function(data){
                if(data != '')
                    jQuery("#results").append(data);
                else
                    jQuery("#results").append('<div class="text-center"> <h4> No data found from Articlesbase </h4></div>'); 
            });
        }
                
        if ($('input[name=ezinearticles]:checked').length > 0){
            jQuery.post('../?route=scraper',{scraper:'1', scode:'Null', scraperType:'ezinearticles', keyword:myKeyword, articleLimit:searchLimit},function(data){
                if(data != '')
                    jQuery("#results").append(data);
                else
                jQuery("#results").append('<div class="text-center"> <h4> No data found from Ezinearticles </h4></div>');
                
            });
        }
        
        jQuery.post('../?route=scraper',{out:'1'},function(data){
                jQuery(".percentimg").fadeOut();
                jQuery("#title").slideDown();
                setTimeout(function(){
                var pos = $('#title').offset();
                $('body,html').animate({ scrollTop: pos.top });
                }, 500);
        });
        
        }
    });
});