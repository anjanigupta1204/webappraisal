
//Bulk Facebook ID Finder

var linksArr = new Array();
var nlinksArr = new Array();

function make(domainID,sqURL) { 
	if(domainID >= nlinksArr.length)
	{
		jQuery(".percentimg").fadeOut();
		return;
	}
    var c_link = nlinksArr[domainID];
    //AJAX Call
	jQuery.post('../?route=fbid',{facebook:'1',sitelink:c_link},function(data){
        jQuery("#status-"+domainID).html('<b style="color:#2c3e50">'+data+'</b>');
		window.setTimeout("make("+(domainID+1)+",'"+sqURL+"')", 500);
	});
}

function saveAsFile(str) {      
    var textToWrite = str;
    var textFileAsBlob = new Blob([textToWrite], {type:'text/plain'});
    var downloadLink = document.createElement("a");
    downloadLink.download = 'facebook-ids.txt';
    downloadLink.innerHTML = "My Link";
    window.URL = window.URL || window.webkitURL;
    downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
    downloadLink.onclick = destroyClickedElement;
    downloadLink.style.display = "none";
    document.body.appendChild(downloadLink);
    downloadLink.click();
}

function destroyClickedElement(event){
    document.body.removeChild(event.target);
}

jQuery(document).ready(function(){
    
    jQuery("#exportButton").click(function() {
    var tableDataLines = '';
    var tdDataLines = '';
    $("#resTable tr").each(function() {
    tdDataLines = '';
    var loop =0;
    var tableData = $(this).find('td');
    if (tableData.length > 0) {
        tableData.each(function() { 
            if(loop != 0){
                if(loop == 1){
                    tdDataLines = tdDataLines.concat('Facebook URL: ' + $(this).text() + '  \r\n'); 
                }else if(loop == 2){
                    tdDataLines = tdDataLines.concat('Facebook ID: ' + $(this).text() + '  \r\n \r\n'); 
                }
            }
            loop++;
        });
        tableDataLines = tableDataLines.concat('\r\n' + tdDataLines);
    }
    });
    saveAsFile(tableDataLines);    
    });
    
    jQuery("#checkButton").click(function()
    {
        var myUrl = "";
        var myURLs=jQuery("#linksBox").val();
        
        if(myURLs == ""){
            alert(msgDomain);
            return false;
        }
        
        linksArr = myURLs.split('\n');
        
        if ($("#capCode").length > 0){
        var sCode= jQuery.trim($('input[name=scode]').val());
        if (sCode==null || sCode=="") {
        alert("Image verification field can't empty!");
        return false;
        } else {
        jQuery.post('../?route=ajax',{capthca:'1', scode:sCode},function(data){
        if (data == '1') {
            //Okay
            jQuery("#mainbox").fadeOut();
            jQuery("#resultBox").css({"display":"block"});
            jQuery("#resultBox").show();
            jQuery("#resultBox").fadeIn();
            jQuery(".percentimg").css({"display":"block"});
            jQuery(".percentimg").show();
            jQuery(".percentimg").fadeIn();
            
            var nLoop = 0;  
            var listHTML = '<br><table id="resTable" class="table table-bordered"><thead><tr><th>#</th><th>'+msgTab1+'</th><th style="text-align: center !important;">'+msgTab2+'</th></tr></thead><tbody>';
            for(i=0; i < linksArr.length; i++)
    	    {
    	       myURL=jQuery.trim(linksArr[i]);
           	   if (myURL.indexOf("https://") == 0){myURL=myURL.substring(8);}
               if (myURL.indexOf("http://") == 0){myURL=myURL.substring(7);}
    	       if(myURL != ""){
    	        nlinksArr[nLoop] = myURL;
                var classTr = nLoop % 2 == 0?'even':'odd';
                listHTML+= '<tr class="'+classTr+'"><td align="center">'+(nLoop+1)+'</td><td id="link-'+nLoop+'"><a href="'+ "http://" + myURL +'" target="_blank">'+ myURL +'</a></td><td align="center" id="status-'+nLoop+'">&nbsp;</td></tr>';
                if(nLoop===19){
                break;
                }
                nLoop = nLoop +1;
               }
            }
            listHTML+= '</tbody></table>';
            jQuery("#results").html(listHTML);
            jQuery("#maxBox").slideDown();
            setTimeout(function(){
            var pos = $('#maxBox').offset();
            $('body,html').animate({ scrollTop: pos.top });
            }, 1500);
            window.setTimeout("make(0,'"+myURL+"')", 500);
        
            }
            else {
                alert("Image verification code is wrong!");
                return false;
            }
        });   
        }
        }else{
        jQuery("#mainbox").fadeOut();
        jQuery("#resultBox").css({"display":"block"});
        jQuery("#resultBox").show();
        jQuery("#resultBox").fadeIn();
        jQuery(".percentimg").css({"display":"block"});
        jQuery(".percentimg").show();
        jQuery(".percentimg").fadeIn();
        
        var nLoop = 0;  
        var listHTML = '<br><table id="resTable" class="table table-bordered"><thead><tr><th>#</th><th>'+msgTab1+'</th><th style="text-align: center !important;">'+msgTab2+'</th></tr></thead><tbody>';
        for(i=0; i < linksArr.length; i++)
	    {
	       myURL=jQuery.trim(linksArr[i]);
       	   if (myURL.indexOf("https://") == 0){myURL=myURL.substring(8);}
           if (myURL.indexOf("http://") == 0){myURL=myURL.substring(7);}
	       if(myURL != ""){
	        nlinksArr[nLoop] = myURL;
            var classTr = nLoop % 2 == 0?'even':'odd';
            listHTML+= '<tr class="'+classTr+'"><td align="center">'+(nLoop+1)+'</td><td id="link-'+nLoop+'"><a href="'+ "http://" + myURL +'" target="_blank">'+ myURL +'</a></td><td align="center" id="status-'+nLoop+'">&nbsp;</td></tr>';
            if(nLoop===19){
            break;
            }
            nLoop = nLoop +1;
           }
        }
        listHTML+= '</tbody></table>';
        jQuery("#results").html(listHTML);
        jQuery("#maxBox").slideDown();
        setTimeout(function(){
        var pos = $('#maxBox').offset();
        $('body,html').animate({ scrollTop: pos.top });
        }, 500);
        window.setTimeout("make(0,'"+myURL+"')", 500);
        }
    });
});