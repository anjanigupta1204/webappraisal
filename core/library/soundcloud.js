
//SoundCloud Downloader

var linksArr = new Array();
var nlinksArr = new Array();

function make(domainID,sqURL) { 
	if(domainID >= nlinksArr.length)
	{
		jQuery(".percentimg").fadeOut();
		return;
	}
    var c_link = nlinksArr[domainID];
    //AJAX Call
	jQuery.post('../?route=soundcloud-ajax',{soundCloud:'1',sitelink:c_link},function(data){
		if(data == 'Error')
		{
			jQuery("#status-"+domainID).html('<b style="color:red">Error</b>');
		}
		else
        {
			jQuery("#status-"+domainID).html(data);
		}
		window.setTimeout("make("+(domainID+1)+",'"+sqURL+"')", 100);
	});
}

jQuery(document).ready(function(){
    jQuery("#checkButton").click(function()
    {
        var myUrl = "";
        var myURLs=jQuery("#linksBox").val();
        
        if(myURLs == ""){
            alert(msgDomain);
            return false;
        }
        
        linksArr = myURLs.split('\n');
        
        if ($("#capCode").length > 0){
        var sCode= jQuery.trim($('input[name=scode]').val());
        if (sCode==null || sCode=="") {
        alert("Image verification field can't empty!");
        return false;
        } else {
        jQuery.post('../?route=ajax',{capthca:'1', scode:sCode},function(data){
        if (data == '1') {
            //Okay
            jQuery("#mainbox").fadeOut();
            jQuery("#resultBox").css({"display":"block"});
            jQuery("#resultBox").show();
            jQuery("#resultBox").fadeIn();
            jQuery(".percentimg").css({"display":"block"});
            jQuery(".percentimg").show();
            jQuery(".percentimg").fadeIn();
            
            var nLoop = 0;  
            var listHTML = '<br><table class="table table-bordered"><thead><tr><th>#</th><th>'+msgTab1+'</th><th class="text-center">'+msgTab2+'</th></tr></thead><tbody>';
            for(i=0; i < linksArr.length; i++)
    	    {
    	       myURL=jQuery.trim(linksArr[i]);
           	   if (myURL.indexOf("https://") == 0){myURL=myURL.substring(8);}
               if (myURL.indexOf("http://") == 0){myURL=myURL.substring(7);}
    	       if(myURL != ""){
    	        nlinksArr[nLoop] = myURL;
                var classTr = nLoop % 2 == 0?'even':'odd';
                listHTML+= '<tr class="'+classTr+'"><td align="center">'+(nLoop+1)+'</td><td id="link-'+nLoop+'"><a href="'+ "http://" + myURL +'" target="_blank">'+ myURL +'</a></td><td align="center" id="status-'+nLoop+'">&nbsp;</td></tr>';
                if(nLoop===19){
                break;
                }
                nLoop = nLoop +1;
               }
            }
            listHTML+= '</tbody></table>';
            jQuery("#results").html(listHTML);
            jQuery("#results").slideDown();
            setTimeout(function(){
            var pos = $('#tracks').offset();
            $('body,html').animate({ scrollTop: pos.top });
            }, 1500);
            window.setTimeout("make(0,'"+myURL+"')", 100);
            }
            else {
                alert("Image verification code is wrong!");
                return false;
            }
        });   
        }
        }else{
        jQuery("#mainbox").fadeOut();
        jQuery("#resultBox").css({"display":"block"});
        jQuery("#resultBox").show();
        jQuery("#resultBox").fadeIn();
        jQuery(".percentimg").css({"display":"block"});
        jQuery(".percentimg").show();
        jQuery(".percentimg").fadeIn();
        
        var nLoop = 0;  
        var listHTML = '<br><table class="table table-bordered"><thead><tr><th>#</th><th>'+msgTab1+'</th><th class="text-center">'+msgTab2+'</th></tr></thead><tbody>';
        for(i=0; i < linksArr.length; i++)
	    {
	       myURL=jQuery.trim(linksArr[i]);
       	   if (myURL.indexOf("https://") == 0){myURL=myURL.substring(8);}
           if (myURL.indexOf("http://") == 0){myURL=myURL.substring(7);}
	       if(myURL != ""){
	        nlinksArr[nLoop] = myURL;
            var classTr = nLoop % 2 == 0?'even':'odd';
            listHTML+= '<tr class="'+classTr+'"><td align="center">'+(nLoop+1)+'</td><td id="link-'+nLoop+'"><a href="'+ "http://" + myURL +'" target="_blank">'+ myURL +'</a></td><td align="center" id="status-'+nLoop+'">&nbsp;</td></tr>';
            if(nLoop===19){
            break;
            }
            nLoop = nLoop +1;
           }
        }
        listHTML+= '</tbody></table>';
        jQuery("#results").html(listHTML);
        jQuery("#results").slideDown();
        setTimeout(function(){
        var pos = $('#tracks').offset();
        $('body,html').animate({ scrollTop: pos.top });
        }, 1500);
        window.setTimeout("make(0,'"+myURL+"')", 100);
        }
    });
    
    jQuery("#checkButton1").click(function()
    {
        var myUrl=jQuery("#url").val();
        
        if(myUrl == ""){
            alert('Enter the Playlist URL');
            return false;
        }

        if ($("#capCode").length > 0){
        var sCode= jQuery.trim($('input[name=scode]').val());
        if (sCode==null || sCode=="") {
        alert("Image verification field can't empty!");
        return false;
        } else {
        jQuery.post('../?route=ajax',{capthca:'1', scode:sCode},function(data){
        if (data == '1') {
            //Okay
            jQuery("#mainbox").fadeOut();
            jQuery("#resultBox").css({"display":"block"});
            jQuery("#resultBox").show();
            jQuery("#resultBox").fadeIn();
            jQuery(".percentimg").css({"display":"block"});
            jQuery(".percentimg").show();
            jQuery(".percentimg").fadeIn();
            
            //AJAX Call
        	jQuery.post('../?route=soundcloud-ajax',{soundCloudPlay:'1',sitelink:myUrl},function(data){
        	    jQuery("#warnmsg").html("Note: If clicking on the download button doesn't start the downloading then Right Click and Save Link As to save the file.");
                jQuery("#results").html(data);
                jQuery("#results").slideDown();
                setTimeout(function(){
                var pos = $('#playlist').offset();
                $('body,html').animate({ scrollTop: pos.top });
                }, 1500);
                jQuery(".percentimg").fadeOut();
        	});
            }
            else {
                alert("Image verification code is wrong!");
                return false;
            }
        });   
        }
        }else{
        jQuery("#mainbox").fadeOut();
        jQuery("#resultBox").css({"display":"block"});
        jQuery("#resultBox").show();
        jQuery("#resultBox").fadeIn();
        jQuery(".percentimg").css({"display":"block"});
        jQuery(".percentimg").show();
        jQuery(".percentimg").fadeIn();

        //AJAX Call
    	jQuery.post('../?route=soundcloud-ajax',{soundCloudPlay:'1',sitelink:myUrl},function(data){
    	    jQuery("#warnmsg").html("Note: If clicking on the download button doesn't start the downloading then Right Click and Save Link As to save the file.");
            jQuery("#results").html(data);
            jQuery("#results").slideDown();
            setTimeout(function(){
            var pos = $('#playlist').offset();
            $('body,html').animate({ scrollTop: pos.top });
            }, 1500);
            jQuery(".percentimg").fadeOut();
    	});
        }
    });
});