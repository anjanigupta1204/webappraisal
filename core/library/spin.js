
//Article Spin

var tipActive = 0;
var currentbox = '';
var myData = '';

function nl2br (str, is_xhtml) {   
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';    
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
}

jQuery(document).ready(function(){

    $("#crumbs1 a").addClass('crumbsactive');
    jQuery("#closeTip").click(function()
    {
        $(".tooltipBox").css({"display":"none"});
        $(".tooltipBox").hide();
        tipActive = 0;
    });
    
    jQuery("#finishButton").click(function()
    {
        if(tipActive == 1){
            $(".tooltipBox").css({"display":"none"});
            $(".tooltipBox").hide();
            tipActive = 0;
        }
        $(".outboxA org").remove();
        $(".outboxA sug").remove();
        var dataRes = $(".outboxA").text();
        $("#step3").css({"display":"none"});
   	    $("#step3").hide();
       	$("#step4").css({"display":"block"});
       	$("#step4").show();
        $("#crumbs3 a").removeClass('crumbsactive');
        $("#crumbs4 a").addClass('crumbsactive');
        $(".outboxA").html('');
        $("textarea[name='outData']").val(dataRes);
        $("#palData").val(dataRes);
        $("#inputTextArea").val(myData);
        $("#textArea").val(dataRes);
    });
    
    $('.outboxA').on('click', '.tipsBox', function() {
        currentbox = $(this);
        var outerUID = $(this).closest('span').attr("id");
        var orgWord = $("#"+outerUID).find('org').text();
        $(".tooltipBody #orgWord").html('<span class="word">'+orgWord+'</span>');
        var sugWord = $("#"+outerUID).find('sug').text();
        var sugWordArr = sugWord.split('|');
        var sugWordData = '';
        
        for (i = 0; i < sugWordArr.length; ++i) 
            sugWordData = sugWordData + '<span class="word">'+sugWordArr[i]+'</span>';
            
        $(".tooltipBody #sugWord").html(sugWordData);
        if(tipActive == 1){
            $(".tooltipBox").css({"display":"none"});
            $(".tooltipBox").hide();
            tipActive = 0;
        }
  		var offset = $(this).offset();
		var topoff = parseInt(offset.top)+23;
        $(".tooltipBox").css({"display":"block"});
		$(".tooltipBox").css({"left" : offset.left + 'px', "top" : topoff + 'px'});
        tipActive = 1;
        $(".tooltipBox").show();
       	$(".word").click(function(){
			currentbox.html($(this).html());
		});
 	    $("#yourBtn").click(function(){
    		var yourWord = $("#yourWord").val();
			if(yourWord.length > 0)
                currentbox.html(yourWord);
                $("#yourWord").val('');
		});
    });

    jQuery("#checkButton").click(function()
    {
    myData=jQuery("#data").val();
    var myLang = jQuery("#lang").val();
	if(myData == '')
	{
		alert('Input field can\'t be empty');
		return;
	}
    
    if ($("#capCode").length > 0){
        var sCode= jQuery.trim($('input[name=scode]').val());
        if (sCode==null || sCode=="") {
        alert("Image verification field can't empty!");
        return false;
        } else {
        jQuery.post('../?route=ajax',{capthca:'1', scode:sCode},function(data){
        if (data == '1') {
            //Okay
           	jQuery("#step1").css({"display":"none"});
           	jQuery("#step1").hide();
           	jQuery("#step2").css({"display":"block"});
           	jQuery("#step2").show();
            $("#crumbs1 a").removeClass('crumbsactive');
            $("#crumbs2 a").addClass('crumbsactive');
            jQuery.post('../?route=spin-ajax',{spinData:'1', data:myData, lang:myLang},function(data){
                  data = nl2br(data);
                  $(".hideBox").html(data);
                  $(".hideBox span").each(function(index) {
                  var org = $(this).find('org').text();
                  var sug = $(this).find('sug').text();
                  var ip = index+1;
    		      $(this).attr("id", "tip_" + ip);
                  var arrayx = sug.split('|');
                  var colors = ["red", "blue", "green"]; 
                  var newWord = arrayx[Math.floor(arrayx.length * Math.random())];
                  var randColor = colors[Math.floor(colors.length * Math.random())];
                  $(this).find('new').html(newWord);
                  });
    
                var dataRes = $(".hideBox").html();
                $("#step2").css({"display":"none"});
               	$("#step2").hide();
               	$("#step3").css({"display":"block"});
               	$("#step3").show();
                $("#crumbs2 a").removeClass('crumbsactive');
                $("#crumbs3 a").addClass('crumbsactive');
                $(".hideBox").html('');
                $(".outboxA").html(dataRes);
            });
        
            } else {
                alert("Image verification code is wrong!");
                return false;
            }
        });   
        }
    }else{
       	jQuery("#step1").css({"display":"none"});
       	jQuery("#step1").hide();
       	jQuery("#step2").css({"display":"block"});
       	jQuery("#step2").show();
        $("#crumbs1 a").removeClass('crumbsactive');
        $("#crumbs2 a").addClass('crumbsactive');
        jQuery.post('../?route=spin-ajax',{spinData:'1', data:myData, lang:myLang},function(data){
              data = nl2br(data);
              $(".hideBox").html(data);
              $(".hideBox span").each(function(index) {
              var org = $(this).find('org').text();
              var sug = $(this).find('sug').text();
              var ip = index+1;
		      $(this).attr("id", "tip_" + ip);
              var arrayx = sug.split('|');
              var colors = ["red", "blue", "green"]; 
              var newWord = arrayx[Math.floor(arrayx.length * Math.random())];
              var randColor = colors[Math.floor(colors.length * Math.random())];
              $(this).find('new').html(newWord);
              });

            var dataRes = $(".hideBox").html();
            $("#step2").css({"display":"none"});
           	$("#step2").hide();
           	$("#step3").css({"display":"block"});
           	$("#step3").show();
            $("#crumbs2 a").removeClass('crumbsactive');
            $("#crumbs3 a").addClass('crumbsactive');
            $(".hideBox").html('');
            $(".outboxA").html(dataRes);
        });
        
        }
    });
});