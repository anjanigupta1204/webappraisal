
//URL Shortener

var myArr = new Array();

function getDomain(linkSt) {
	return linkSt.replace(/(http:\/\/[^\/]*)+([^$]*)/g, '$1');
}
function isValidUrl(url) {
    return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
}

function make(domainID,sqURL) { 
	if(domainID >= 31) {
		jQuery(".percentimg").fadeOut();
		return;
	}
    
	//AJAX Call
	jQuery.post('../?route=url',{domain:sqURL,count:domainID},function(data){
	    var checkData = data.toLowerCase();
		if(checkData == 'error') {
			jQuery("#short-"+domainID).html('<b style="color:red">Error</b>');
		} else {
			jQuery("#short-"+domainID).html('<b style="color:green">'+data+'</b>'); 
		}
		window.setTimeout("make("+(domainID+1)+",'"+sqURL+"')", 100);
	});
}

function saveAsFile(str) {      
    var textToWrite = str;
    var textFileAsBlob = new Blob([textToWrite], {type:'text/plain'});
    var downloadLink = document.createElement("a");
    downloadLink.download = 'short-urls.txt';
    downloadLink.innerHTML = "My Link";
    window.URL = window.URL || window.webkitURL;
    downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
    downloadLink.onclick = destroyClickedElement;
    downloadLink.style.display = "none";
    document.body.appendChild(downloadLink);
    downloadLink.click();
}

function destroyClickedElement(event){
    document.body.removeChild(event.target);
}

jQuery(document).ready(function(){
    
    jQuery("#exportButton").click(function() {
    var tableDataLines = '';
    var tdDataLines = '';
    $("#resTable tr").each(function() {
    tdDataLines = '';
    var loop =0;
    var tableData = $(this).find('td');
    if (tableData.length > 0) {
        tableData.each(function() { 
            if(loop == 2){
                if($(this).text() != 'Error')
                    tdDataLines = tdDataLines.concat($(this).text() + '\r\n'); 
            }
            loop++;
        });
        tableDataLines = tableDataLines.concat(tdDataLines);
    }
    });
    saveAsFile(tableDataLines);    
    });
    
    jQuery("#checkButton").click(function() {
    var myURL=jQuery("#myurl").val();
    myURL=jQuery.trim(myURL);
	if(!isValidUrl(myURL)) {
		alert(msgDomain);
		return;
	}
    if ($("#capCode").length > 0){
        var sCode= jQuery.trim($('input[name=scode]').val());
        if (sCode==null || sCode=="") {
        alert("Image verification field can't empty!");
        return false;
        } else {
        jQuery.post('../?route=ajax',{capthca:'1', scode:sCode},function(data){
        if (data == '1') {
            //Okay
            jQuery("#mainbox").fadeOut();
           	jQuery("#resultBox").css({"display":"block"});
           	jQuery("#resultBox").show();
           	jQuery("#resultBox").fadeIn();
           	jQuery(".percentimg").css({"display":"block"});
           	jQuery(".percentimg").show();
           	jQuery(".percentimg").fadeIn();
        	var listHTML = '<br><table id="resTable" class="table table-bordered table-striped"><thead><tr><th>#</th><th>Long Url</th><th>Short Url</th></tr></thead><tbody>';
        	for(i=0; i < 31; i++) {
        		var classTr = i % 2 == 0?'even':'odd';
        		listHTML+= '<tr class="'+classTr+'"><td>'+(i+1)+'</td><td id="link-'+i+'"><a href="'+myURL+'" target="_blank">'+myURL+'</a></td><td id="short-'+i+'">&nbsp;</td></tr>';
        	}
        	listHTML+= '</tbody></table>';
        	jQuery("#results").html(listHTML);
        	jQuery("#results").slideDown();
            setTimeout(function(){
        	var pos = $('#results').offset();
        	$('body,html').animate({ scrollTop: pos.top });
        	}, 500);
        	window.setTimeout("make(0,'"+myURL+"')", 100);
            } else {
                alert("Image verification code is wrong!");
                return false;
            }
        });   
        }
    }else{
    jQuery("#mainbox").fadeOut();
   	jQuery("#resultBox").css({"display":"block"});
   	jQuery("#resultBox").show();
   	jQuery("#resultBox").fadeIn();
   	jQuery(".percentimg").css({"display":"block"});
   	jQuery(".percentimg").show();
   	jQuery(".percentimg").fadeIn();
	var listHTML = '<br><table id="resTable" class="table table-bordered table-striped"><thead><tr><th>#</th><th>Long Url</th><th>Short Url</th></tr></thead><tbody>';
	for(i=0; i < 31; i++) {
		var classTr = i % 2 == 0?'even':'odd';
		listHTML+= '<tr class="'+classTr+'"><td>'+(i+1)+'</td><td id="link-'+i+'"><a href="'+myURL+'" target="_blank">'+myURL+'</a></td><td id="short-'+i+'">&nbsp;</td></tr>';
	}
	listHTML+= '</tbody></table>';
	jQuery("#results").html(listHTML);
	jQuery("#results").slideDown();
    setTimeout(function(){
	var pos = $('#results').offset();
	$('body,html').animate({ scrollTop: pos.top });
	}, 500);
	window.setTimeout("make(0,'"+myURL+"')", 100);
    }
    });
});