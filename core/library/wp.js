
//Wordpress Theme Detector

jQuery(document).ready(function(){
    jQuery("#checkButton").click(function()
    {
    var myURL=jQuery("#myurl").val();
    myURL=jQuery.trim(myURL);
	if (myURL.indexOf("https://") == 0){myURL=myURL.substring(8);}
    if (myURL.indexOf("http://") == 0){myURL=myURL.substring(7);}
    
	if(myURL == "") {
		alert(msgDomain);
		return;
	}

    if ($("#capCode").length > 0){
        var sCode= jQuery.trim($('input[name=scode]').val());
        if (sCode==null || sCode=="") {
        alert("Image verification field can't empty!");
        return false;
        } else {
        jQuery.post('../?route=ajax',{capthca:'1', scode:sCode},function(data){
        if (data == '1') {
            //Okay
            jQuery("#mainbox").fadeOut();
   	        jQuery("#resultBox").css({"display":"block"});
   	        jQuery("#resultBox").show();
   	        jQuery("#resultBox").fadeIn();
   	        jQuery(".percentimg").css({"display":"block"});
   	        jQuery(".percentimg").show();
   	        jQuery(".percentimg").fadeIn();
            jQuery.post('../?route=ntajax',{wpDetect:'1', url:myURL},function(outData){
            jQuery("#results").html(outData);
   	        jQuery(".percentimg").fadeOut();
   	        jQuery(".percentimg").css({"display":"none"});
   	        jQuery(".percentimg").hide();
            });  
            }else {
                alert("Image verification code is wrong!");
                return false;
            }
        });   
        }
    }else{
        jQuery("#mainbox").fadeOut();
       	jQuery("#resultBox").css({"display":"block"});
       	jQuery("#resultBox").show();
       	jQuery("#resultBox").fadeIn();
       	jQuery(".percentimg").css({"display":"block"});
       	jQuery(".percentimg").show();
       	jQuery(".percentimg").fadeIn();
        jQuery.post('../?route=ntajax',{wpDetect:'1', url:myURL},function(outData){
            jQuery("#results").html(outData);
   	        jQuery(".percentimg").fadeOut();
            jQuery(".percentimg").css({"display":"none"});
            jQuery(".percentimg").hide();
            }); 
            }
    });

});