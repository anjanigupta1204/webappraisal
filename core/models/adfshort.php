<?php
	/**
	 * http://code.google.com/p/gam-http/
	 */

	class Http_Exception extends Exception {
	    const NOT_MODIFIED = 304; 
	    const BAD_REQUEST = 400; 
	    const NOT_FOUND = 404; 
	    const NOT_ALOWED = 405; 
	    const CONFLICT = 409; 
	    const PRECONDITION_FAILED = 412; 
	    const INTERNAL_ERROR = 500; 
	}
	
	class Http_Multiple_Error {
	    private $_status = null;
	    private $_type = null;
	    private $_url = null;
	    private $_params = null;
	    
	    function __construct($status, $type, $url, $params) {
	        $this->_status = $status;
	        $this->_type = $type;
	        $this->_url = $url;
	        $this->_params = $params;
	    }
	    
	    function getStatus() {
	        return $this->_status;
	    }
	    
	    function getType() {
	        return $this->_type;
	    }
	    
	    function getUrl() {
	        return $this->_url;
	    }
	    
	    function getParams() {
	        return $this->_params;
	    }
	}
	
	class adfHttp {
	    private $_host = null;
	    private $_port = null;
	    private $_user = null;
	    private $_pass = null;
	    private $_protocol = null;
	    private $_timeout = 0;
	    private $_connectTimeout = 0;
	
	    const HTTP = 'http';
	    const HTTPS = 'https';
	    
	    private $_connMultiple = false;
	    
	    /**
	     * Factory of the class. Lazy connect
	     *
	     * @param string $host
	     * @param integer $port
	     * @param string $user
	     * @param string $pass
	     * @return Http
	     */
	    static public function connect($host, $port = 80, $protocol = self::HTTP) {
	        return new self($host, $port, $protocol, false);
	    }
	    
	    /**
	     *
	     * @return Http
	     */
	    static public function multiConnect() {
	        return new self(null, null, null, true);
	    }
	
	    private $_append = array();
	    
	    public function add($http) {
	        $this->_append[] = $http;
	        return $this;
	    }
	    
	    private $_silentMode = false;
	    
	    /**
	     *
	     * @param bool $mode
	     * @return Http
	     */
	    public function silentMode($mode=true) {
	        $this->_silentMode = $mode;
	        return $this;    
	    }
	    
	    protected function __construct($host, $port, $protocol, $connMultiple) {
	        $this->_connMultiple = $connMultiple;
	        
	        $this->_host = $host;
	        $this->_port = $port;
	        $this->_protocol = $protocol;
	    }
	    
	    public function setCredentials($user, $pass) {
	        $this->_user = $user;
	        $this->_pass = $pass;
	        return $this;
	    }
	    
	    public function setTimeout($timeout) {
	    	$this->_timeout = $timeout;
	    }
	    
		public function getTimeout() {
	    	return $this->_timeout;
	    }
	    
		public function setConnectTimeout($connectTimeout) {
	    	$this->_connectTimeout = $connectTimeout;
	    }
	    
		public function getConnectTimeout() {
	    	return $this->_connectTimeout;
	    }
	
	    const POST = 'POST';
	    const GET = 'GET';
	    const DELETE = 'DELETE';
	    const PUT = 'PUT';
	
	    private $_requests = array();
	    
	    /**
	     * @param string $url
	     * @param array $params
	     * @return Http
	     */
	    public function put($url, $params=array()) {
	        $this->_requests[] = array(self::PUT, $this->_url($url), $params);
	        return $this;
	    }
	    
	    /**
	     * @param string $url
	     * @param array $params
	     * @return Http
	     */
	    public function post($url, $params=array()) {
	        $this->_requests[] = array(self::POST, $this->_url($url), $params);
	        return $this;
	    }
	
	    /**
	     * @param string $url
	     * @param array $params
	     * @return Http
	     */
	    public function get($url, $params=array()) {
	        $this->_requests[] = array(self::GET, $this->_url($url), $params);
	        return $this;
	    }
	    
	    /**
	     * @param string $url
	     * @param array $params
	     * @return Http
	     */
	    public function delete($url, $params=array()) {
	        $this->_requests[] = array(self::DELETE, $this->_url($url), $params);
	        return $this;
	    }
	    
	    public function _getRequests() {
	        return $this->_requests;
	    }
	    
	    /**
	     * PUT request
	     *
	     * @param string $url
	     * @param array $params
	     * @return string
	     */
	    public function doPut($url, $params=array()) {
	        return $this->_exec(self::PUT, $this->_url($url), $params);
	    }
	    
	    /**
	     * POST request
	     *
	     * @param string $url
	     * @param array $params
	     * @return string
	     */
	    public function doPost($url, $params=array()) {
	        return $this->_exec(self::POST, $this->_url($url), $params);
	    }
	
	    /**
	     * GET Request
	     *
	     * @param string $url
	     * @param array $params
	     * @return string
	     */
	    public function doGet($url, $params=array()) {
	        return $this->_exec(self::GET, $this->_url($url), $params);
	    }
	    
	    /**
	     * DELETE Request
	     *
	     * @param string $url
	     * @param array $params
	     * @return string
	     */
	    public function doDelete($url, $params=array()) {
	        return $this->_exec(self::DELETE, $this->_url($url), $params);
	    }
	
	    private $_headers = array();
	    
	    /**
	     * setHeaders
	     *
	     * @param array $headers
	     * @return Http
	     */
	    public function setHeaders($headers) {
	        $this->_headers = $headers;
	        return $this;
	    }
	
	    /**
	     * Builds absolute url 
	     *
	     * @param unknown_type $url
	     * @return unknown
	     */
	    private function _url($url=null) {
	        return "{$this->_protocol}://{$this->_host}:{$this->_port}/{$url}";
	    }
	
	    const HTTP_OK = 200;
	    const HTTP_CREATED = 201;
	    const HTTP_ACEPTED = 202;
	
	    /**
	     * Performing the real request
	     *
	     * @param string $type
	     * @param string $url
	     * @param array $params
	     * @return string
	     */
	    private function _exec($type, $url, $params = array()) {
	        $headers = $this->_headers;
	        $s = curl_init();
	        
	        if (!is_null($this->_user)){
	           curl_setopt($s, CURLOPT_USERPWD, $this->_user.':'.$this->_pass);
	        }
	
	        switch ($type) {
	            case self::DELETE:
	                curl_setopt($s, CURLOPT_URL, $url . '?' . http_build_query($params));
	                curl_setopt($s, CURLOPT_CUSTOMREQUEST, self::DELETE);
	                break;
	            case self::PUT:
	                curl_setopt($s, CURLOPT_URL, $url);
	                curl_setopt($s, CURLOPT_CUSTOMREQUEST, self::PUT);
	                curl_setopt($s, CURLOPT_POSTFIELDS, $params);
	                break;
	            case self::POST:
	                curl_setopt($s, CURLOPT_URL, $url);
	                curl_setopt($s, CURLOPT_POST, true);
	                curl_setopt($s, CURLOPT_POSTFIELDS, $params);
	                break;
	            case self::GET:
	                curl_setopt($s, CURLOPT_URL, $url . '?' . http_build_query($params));
	                break;
	        }
	
	        curl_setopt($s, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($s, CURLOPT_HTTPHEADER, $headers);
	        $_out = curl_exec($s);
	        $status = curl_getinfo($s, CURLINFO_HTTP_CODE);
	        curl_close($s);
	        
            switch ($status) {
	            case self::HTTP_OK:
	            case self::HTTP_CREATED:
	            case self::HTTP_ACEPTED:
	                $out = $_out;
	                break;
	            default:
	                if (!$this->_silentMode) {
	                    throw new Http_Exception("http error: {$status}", $status);
	                }
	        }

	        return $out;
	    }
	    
	    public function run() {
	        if ($this->_connMultiple) {
	            return $this->_runMultiple();
	        } else {
	            return $this->_run();
	        }
	    }
	    
	    private function _runMultiple() {
	        $out= null;
	        if (count($this->_append) > 0) {
	            $arr = array();
	            foreach ($this->_append as $_append) {
	                $arr = array_merge($arr, $_append->_getRequests());
	            }
	            
	            $this->_requests = $arr;
	            $out = $this->_run();
	        }
	        return $out;
	    }
	    
	    private function _run() {
	        $headers = $this->_headers;
	        $curly = $result = array();
	
	        $mh = curl_multi_init();
	        
	        foreach ($this->_requests as $id => $reg) {
	            $curly[$id] = curl_init();
	            
	            $type = $reg[0];
	            $url = $reg[1];
	            $params = $reg[2];
	            
	            if (!is_null($this->_user)) {
	               curl_setopt($curly[$id], CURLOPT_USERPWD, $this->_user.':'.$this->_pass);
	            }
	            
	            switch ($type) {
	                case self::DELETE:
	                    curl_setopt($curly[$id], CURLOPT_URL, $url . '?' . http_build_query($params));
	                    curl_setopt($curly[$id], CURLOPT_CUSTOMREQUEST, self::DELETE);
	                    break;
	                case self::PUT:
	                    curl_setopt($curly[$id], CURLOPT_URL, $url);
	                    curl_setopt($curly[$id], CURLOPT_CUSTOMREQUEST, self::PUT);
	                    curl_setopt($curly[$id], CURLOPT_POSTFIELDS, $params);
	                    break;
	                case self::POST:
	                    curl_setopt($curly[$id], CURLOPT_URL, $url);
	                    curl_setopt($curly[$id], CURLOPT_POST, true);
	                    curl_setopt($curly[$id], CURLOPT_POSTFIELDS, $params);
	                    break;
	                case self::GET:
	                    curl_setopt($curly[$id], CURLOPT_URL, $url . '?' . http_build_query($params));
	                    break;
	            }
	            
	            curl_setopt($curly[$id], CURLOPT_TIMEOUT_MS, $this->_timeout);
	            curl_setopt($curly[$id], CURLOPT_CONNECTTIMEOUT_MS, $this->_connectTimeout);
	            
	            curl_setopt($curly[$id], CURLOPT_RETURNTRANSFER, true);
	            curl_setopt($curly[$id], CURLOPT_HTTPHEADER, $headers);
	            
	            curl_multi_add_handle($mh, $curly[$id]);
	        }
	    
	        $running = null;
	        
	        do {
	            curl_multi_exec($mh, $running);
	            usleep(25000);
	        } while ($running > 0);
	    
	        foreach ($curly as $id => $c) {
	            $status = curl_getinfo($c, CURLINFO_HTTP_CODE);
	            switch ($status) {
	                case self::HTTP_OK:
	                case self::HTTP_CREATED:
	                case self::HTTP_ACEPTED:
	                    $result[$id] = curl_multi_getcontent($c);
	                    break;
	                default:
	                    if (!$this->_silentMode) {
	                        $result[$id] = new Http_Multiple_Error($status, $type, $url, $params);
	                    }
	            }
	            curl_multi_remove_handle($mh, $c);
	        }
	
	        curl_multi_close($mh);
	        return $result;
	    }
	}
?>
<?php

	/**
	 * Describes two kinds of authentication. Acts as enum.
	 */
	class AuthType {
		const BASIC = 1;
		const HMAC = 2;
	}

	class adfshort {
		const BASE_HOST = 'api.adf.ly';
		// TODO: Replace following constant value with your secret key.
		const SECRET_KEY = '8cc9406f-3727-41f6-8bb0-64f930fbaf1b';
		// TODO: Replace following constant value with your public api key.
		const PUBLIC_KEY = '996fec4c4603d56f8f137bd14e9d4af2';
		// TODO: Replace following constant value with your user id.
		const USER_ID = 240838;
		const HMAC_ALGO = 'sha256';
		
		private $connection = null;
		
		public function __construct() {
			$this->connection = adfHttp::connect(self::BASE_HOST);
		}
		public function auth($username, $password) {
			return json_decode($this->connection->doPost('v1/auth',$this->getParams(array('username' => $username, 'password' => $password), null)), 1);
		}
		
		public function getGroups($page=1) {
			return json_decode($this->connection->doGet('v1/urlGroups',$this->getParams(array('_page' => $page), AuthType::HMAC)),1);
		}
        public function createGroup($name) {
            return json_decode($this->connection->doPost('v1/urlGroups',$this->getParams(array('name' => $name), AuthType::HMAC)),1);
        }
		
		public function expand(array $urls, array $hashes=array()) {
			$params = array();
			
			$i = 0;
			foreach ($urls as $url) {
				$params[sprintf('url[%d]', $i++)] = $url;
			}
			
			$i = 0;
			foreach ($hashes as $hash) {
				$params[sprintf('hash[%d]', $i++)] = $hash;
			}
			
			return json_decode($this->connection->doGet('v1/expand',$this->getParams($params)),1);
		}
		
		public function shorten(array $urls, $domain=false, $advertType=false, $groupId=false) {
			$params = array();
			if ($domain !== false) $params['domain'] = $domain;
			if ($advertType !== false) $params['advert_type'] = $advertType;
			if ($groupId !== false) $params['group_id'] = $groupId;
			
			$i = 0;
			foreach ($urls as $url) {
				$params[sprintf('url[%d]', $i++)] = $url;
			}
			
			return json_decode($this->connection->doPost('v1/shorten',$this->getParams($params)),1);
		}
		
		public function getUrls($page=1, $q=null) {
			$params = array('_page' => $page);
			
			if ($q) {
				$params['q'] = $q;
			}
			
			return json_decode($this->connection->doGet('v1/urls',$this->getParams($params, AuthType::HMAC)),1);
		}

        public function getReferrers($urlId=null) {
            $params = array();
            if ($urlId) $params['url_id'] = $urlId;

            return json_decode($this->connection->doGet('v1/referrers',$this->getParams($params, AuthType::HMAC)),1);
        }
        public function getCountries($urlId=null) {
            $params = array();
            if ($urlId) $params['url_id'] = $urlId;

            return json_decode($this->connection->doGet('v1/countries',$this->getParams($params, AuthType::HMAC)),1);
        }
        public function getAnnouncements($type=null) {
            $params = array();
            if (!empty($type) && in_array($type,array(1,2))) $params['type'] = $type;

            return json_decode($this->connection->doGet('v1/announcements',$this->getParams($params, AuthType::HMAC)),1);
        }


        public function getPublisherReferrals() {
            return json_decode($this->connection->doGet('v1/publisherReferralStats',$this->getParams(array(), AuthType::HMAC)),1);
        }
        public function getAdvertiserReferrals() {
            return json_decode($this->connection->doGet('v1/advertiserReferralStats',$this->getParams(array(), AuthType::HMAC)),1);
        }

        public function getWithdraw() {
            return json_decode($this->connection->doGet('v1/withdraw',$this->getParams(array(), AuthType::HMAC)));
        }
        public function getWithdrawalTransactions() {
            return json_decode($this->connection->doGet('v1/withdrawalTransactions',$this->getParams(array(), AuthType::HMAC)),1);
        }
        public function getPublisherStats($date = null, $urlId = 0){
            $params = array();
            if(!empty($date)) $params['date'] = $date;
            if(!empty($urlId)) $params['urlId'] = $urlId;

            return json_decode($this->connection->doGet('v1/publisherStats',$this->getParams($params, AuthType::HMAC)),1);
        }
        public function getProfile() {
            echo $this->connection->doGet('v1/profile',$this->getParams(array(), AuthType::HMAC));
            die;
        }
        public function getAdvertiserCampaigns( $fromDate = null, $toDate = null,$adType = null, $adFilter = null){
            if(!empty($fromDate)) $params['fromDate'] = $fromDate;
            if(!empty($toDate)) $params['toDate'] = $toDate;
            if(!empty($adType)) $params['adType'] = $adType;
            if(!empty($adFilter)) $params['adFilter'] = $adFilter;

            return json_decode($this->connection->doGet('v1/advertiserCampaigns',$this->getParams(array(), AuthType::HMAC)),1);
        }
        public function getAdvertiserGraph($date = null, $websiteId = 0,$adType = null, $adFilter = null){
            $params = array();
            if(!empty($date)) $params['date'] = $date;
            if(!empty($websiteId)) $params['websiteId'] = $websiteId;
            if(!empty($adType)) $params['adType'] = $adType;
            if(!empty($adFilter)) $params['adFilter'] = $adFilter;

            return json_decode($this->connection->doGet('v1/advertiserGraph',$this->getParams($params, AuthType::HMAC)),1);
        }
        public function getAdvertiserCampaignParts($campaignId, $fromDate = null, $toDate = null, $adType = null, $adFilter = null){
            $params = array('campaignId' => $campaignId);
            if(!empty($fromDate)) $params['fromDate'] = $fromDate;
            if(!empty($toDate)) $params['toDate'] = $toDate;
            if(!empty($adType)) $params['adType'] = $adType;
            if(!empty($adFilter)) $params['adFilter'] = $adFilter;

            return json_decode($this->connection->doGet('v1/advertiserCampaignParts',$this->getParams($params, AuthType::HMAC)),1);
        }

		public function updateUrl($id, $url=false, $advertType=false, $title=false, $groupId=false, $fbDescription=false, $fbImage=false) {
			$params = array();
			
			if ($url !== false) $params['url'] = $url;
			if ($advertType !== false) $params['advert_type'] = $advertType;
			if ($title !== false) $params['title'] = $title;
			if ($groupId !== false) $params['group_id'] = $groupId;
			if ($fbDescription !== false) $params['fb_description'] = $fbDescription;
			if ($fbImage !== false) $params['fb_image'] = $fbImage;
			
			return json_decode($this->connection->doPut('v1/urls/' . $id,$this->getParams($params, AuthType::HMAC)),1);
		}
		
		public function deleteUrl($id) {
			return json_decode($this->connection->doDelete('v1/urls/' . $id,$this->getParams(array(), AuthType::HMAC)),1);
		}

		/**
		 * Populates query parameters with required parameters. Such as
		 * _user_id, _api_key, etc.
		 * @param array $params
		 * @param integer $authType
		 */
		private function getParams(array $params=array(), $authType=AuthType::BASIC) {
			$params['_user_id'] = self::USER_ID;
			$params['_api_key'] = self::PUBLIC_KEY;

			if (AuthType::BASIC == $authType) {

			} else if (AuthType::HMAC == $authType) {
				// Get current unix timestamp (UTC time).
				$params['_timestamp'] = time();
				// And calculate hash.
				$params['_hash'] = $this->doHmac($params);
			}

			return $params;
		}

		private function doHmac(array $params) {
			// Built-in 'http_build_query' function which is used
			// to construct query string does not include parameters with null
			// values which is incorrect in our case.
			$params = array_map(function($x) { return is_null($x) ? '' : $x; }, $params);

			// Sort query parameters by names using byte ordering.
			// So 'param[10]' comes before 'param[2]'.
			if (ksort($params)) {
				// Url encode parameters. The encoding should be performed
				// per RFC 1738 (http://www.faqs.org/rfcs/rfc1738)
				// which implies that spaces are encoded as plus (+) signs.
				$queryStr = http_build_query($params);
				// Generate hash value based on encoded query string and
				// secret key.
				return hash_hmac(self::HMAC_ALGO, $queryStr, self::SECRET_KEY);
			} else {
				throw new RuntimeException('Could not ksort data array');
			}
		}
	}
?>