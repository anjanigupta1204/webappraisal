<?php 

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright © 2016 ProThemes.Biz
 *
 */
 
class spin_my_data_new
{

    function randomSplit($string)
    {
        $string = Trim($string);
        $res = -1;
        $finalData = "";
        $loopinput = $this->parse_br($string);
        for ($loop = 0; $loop < count($loopinput); $loop++)
        {
            for ($loopx = 0; $loopx < count($loopinput[$loop]); $loopx++)
            {
                if (!$loopinput[$loop][$loopx] == "" || "/n")
                {
                    $res++;
                    if (strstr($loopinput[$loop][$loopx], "|"))
                    {
                        $out = explode("|", $loopinput[$loop][$loopx]);
                        $output[$res] = $out[rand(0, count($out) - 1)];
                    } else
                    {
                        $output[$res] = $loopinput[$loop][$loopx];
                    }
                }
            }
        }
        for ($loop = 0; $loop < count($output); $loop++)
        {
            $finalData .= $output[$loop];
        }
        return $finalData;
    }

    function spinMyData($data, $lang)
    {
        $patern_code_1 = "/<[^<>]+>/us";
        $patern_code_2 = "/\[[^\[\]]+\]/i";
        $patern_code_3 = '/\$@.*?\$@/i';

        $data = Trim($data);
        preg_match_all($patern_code_1, $data, $found1, PREG_PATTERN_ORDER);
        preg_match_all($patern_code_2, $data, $found2, PREG_PATTERN_ORDER);
        preg_match_all($patern_code_3, $data, $found3, PREG_PATTERN_ORDER);
        $htmlcodes = $found1[0];
        $bbcodes = $found2[0];
        $vbcodes = $found3[0];
        $founds = array();
        $current_dir = dirname(__file__);
        $sel_lang = Trim($lang);

        $arr_data = array_merge($htmlcodes, $bbcodes, $vbcodes);
        foreach ($arr_data as $code)
        {
            $code_md5 = md5($code);
            $data = str_replace($code, '%%!%%' . $code_md5 . '%%!%%', $data);
        }

        $file = file($current_dir . '/spinner_db/' . $sel_lang . '_db.sdata');

        foreach ($file as $line)
        {
            //$data = mb_strtolower($data, 'UTF-8');
            $synonyms = explode('|', $line);
            foreach ($synonyms as $word)
            {
                $word = trim($word);
                if ($word != '')
                {
                    $word = str_replace('/', '\/', $word);
                    if (preg_match('/\b' . $word . '\b/i', $data))
                    {
                        $founds[$word.'-'.md5($word)] = str_replace(array("\n", "\r"), '', $line);
                        $data = preg_replace('/\b' . $word . '\b/i', md5($word), $data);
                    }
                }
            }

        }

        foreach ($arr_data as $code)
        {
            $code_md5 = md5($code);
            $data = str_replace('%%!%%' . $code_md5 . '%%!%%', $code, $data);
        }

        $array_count = count($founds);

        if ($array_count != 0)
        {
            foreach ($founds as $code => $value)
            {
                $code = explode('-', $code);
                $word = Trim($code[0]);
                $md5Hash = Trim($code[1]);
                $colors = array('#c0392b', '#2980b9', 'blue', '#d35400', 'red', '#19b698', '#27ae60', '#e74c3c', '#8e44ad');
                $color = $colors[array_rand($colors)];
                $data = str_replace($md5Hash, '<span><org class="hide">'.$word.'</org><sug class="hide">'.$value.'</sug><new style="color: ' . $color . ';" class="tipsBox"></new></span>', $data);
            }
        }

        return $data;
    }


    function parse_br($string)
    {
        @$string = explode("{", $string);
        for ($loop = 0; $loop < count($string); $loop++)
        {
            @$data[$loop] = explode("}", $string[$loop]);
        }
        return $data;
    }

}

?>