<?php

/**
 * Author Balaji
 * Name: Wordpress Theme Detector
 * Copyright � 2016 ProThemes.Biz
 * Date: 01/27/2016
 */

class wpThemeDetect {
    
    private $url;
    private $output = '<div align="center">';
	private $counter = 0;
	
	function __construct($url){
        $this->url =  $url;
        $this->timeout = 20;
    }
    
    function getDataCurl($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
		curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
        $response = curl_exec($ch);
        if (curl_error($ch)) {
            return '0';
        }
        return $response;
    }
	
	function getWordPressDetails() {
		$resp = $this->getDataCurl($this->url);
		$outputPlugins = $output = $version = '';
		$outputPlugins = $resp;
		
		if(preg_match('#name="generator" content="WordPress (\d.\d.\d)"#',$resp,$r))
		{
			$version = $r[1];
		}
		else
		{
			$tempResponse = $this->getDataCurl($this->url . "/readme.html");
			if(preg_match('#Version (.*)#',$tempResponse,$r))
			{
				$version = $r[1];
			}
		}
		
		if($version != "")
		{
			$output = "<br><div align='center'>
				<img src='/theme/default/img/wordpress.png' width='64' height='64'>
				<br>
				<span style='font-weight:bold; font-style:Arial; font-size: 20pt;'>Version: " . $version . "</span>
			</div>";
		}
		
		$output .= '<div align="center">';

		
		
		
		if(preg_match('#wp-content\/themes\/([^,;\/]*)#', $resp, $r))
		{
			$screenshotFile = $this->url . "/" . $r[0] . "/screenshot.png";
			$screenshotData = $this->getDataCurl($screenshotFile);
			if(strpos($screenshotData,"200 OK") === FALSE)
			{
				$screenshotFile = '/theme/default/img/no_wp_image.png';
			}
			$themeName = $r[1];
			$styleFile = $this->url . "/" . $r[0] . "/style.css";

				if($version == "")
				{
					$output .= "<br>";
				}
				$resp = $this->getDataCurl($styleFile);
					$output .= '<div class="panel1 panel-blue">
                    <div class="panel-heading">
                    <h3 class="panel-title">
                    <i class="fa fa-cog"></i> Theme Details for ' . $this->url . '
                    </h3>
                    </div>
                    <div class="panel-body">
                    <table class="table">
                    <tbody>';
				if(preg_match_all('#Theme Name:#',$resp,$r))
				{
					$resp1 = explode("/*",$resp);
					$resp1 = $resp1[1];
					$resp1 = explode("*/",$resp1);
					$resp1 = $resp1[0];
		

					
					$resp1 = str_replace("	"," ",$resp1);
					$resp1 = str_replace("	","",$resp1);
					$resp1 = str_replace("  "," ",$resp1);
					
					if(preg_match_all('#(\w.*):\s*(\w.*)#',$resp1,$r))
					{
						$theAuthorName = "";
						for($x = 0; $x<count($r[1]);$x++)
						{
							$r[2][$x] = str_replace("	","",$r[2][$x]);
							if(strpos(strtolower($r[1][$x]),"author") !== FALSE)
							{
								$tempVal = $r[1][$x];
								$tempVal = str_replace("-","",$tempVal);
								$tempVal = str_replace(" ","",$tempVal);
								$tempVal = str_replace(":","",$tempVal);
								$tempVal = str_replace("	","",$tempVal);
								$tempVal = strtolower($tempVal);
								if($tempVal == "author")
								{
									
									$theAuthorName = strtolower($r[2][$x]);
									break;
								}
								
							}
						}
						
						for($x = 0; $x<count($r[1]);$x++)
						{
														$r[1][$x] = str_replace("URI","Homepage",$r[1][$x]);
							$r[1][$x] = str_replace("URL","Homepage",$r[1][$x]);
							if(strpos(strtolower($r[1][$x]),"warning") !== FALSE || strpos(strtolower($r[1][$x]),"copyright notice") !== FALSE || strpos(strtolower($r[1][$x]),"license homepage") !== FALSE)
							{
								continue;
							}

							
							if(strpos($r[2][$x],"<a href") !== FALSE || (strpos($r[1][$x],"Homepage") !== FALSE  && strpos($r[2][$x],"<a href") === FALSE))
							{
								
								
								$files1 = scandir("recommends/");
								$theAuthorName = str_replace("-","",$theAuthorName);
								$theAuthorName = str_replace(" ","",$theAuthorName);
								
								$theAuthorName = strtolower($theAuthorName);
								$theAuthorName = str_replace(".","",$theAuthorName);
								$theAuthorName = str_replace("\n","",$theAuthorName);
								$theAuthorName = str_replace("\\n","",$theAuthorName);
								
								$theAuthorName = str_replace("\r\n","",$theAuthorName);
								$theAuthorName = str_replace("\r","",$theAuthorName);
								
								if(strpos($r[2][$x],"http://") === FALSE)
								{
									$r[2][$x] = str_replace("www.","http://www.",$r[2][$x]);
								}
								$theData = $r[2][$x];
								foreach ($files1 as $key => $value) {
										$tempValue = $value;
										$tempValue = str_replace("-","",$tempValue);
										$tempValue = str_replace(" ","",$tempValue);
										$tempValue = str_replace(".php","",$tempValue);
										$tempValue = strtolower($tempValue);
										
										if($theAuthorName == $tempValue)
										{
											$theData = "/recommends/" . $value;
										}
								
								}
								
								
								$output .= '<tr>
									<td><strong>' . $r[1][$x] . '</strong></td>
									<td><a href="' . $theData . '" target="_blank">' . $r[2][$x] . '</a></td>
								</tr>';
							}
							else
							{
									

		
		
							$output .= '
							<tr>
	<td><strong>' . $r[1][$x] . '</strong></td>
	<td>' . $r[2][$x] . '</td>
</tr>';
							}
						}
						
						$output .= '
						<tr>
	<td><strong>Theme Screenshot</strong></td>
	<td><img width="300" height="225" src="' . $screenshotFile . '"></td>
</tr>';
					}
				}
				else
				{

					$output .= "<tr><td>WordPress theme detected is called <strong>" . $themeName . "</strong>, but no further details can be determined. It is probably a customized theme</td></tr>";
					
					$files1 = scandir("recommends/");
					$theThemeName = str_replace("-","",$themeName);
					$theThemeName = str_replace(" ","",$theThemeName);
					$theThemeName = strtolower($theThemeName);
					
					foreach ($files1 as $key => $value) {
							$tempValue = $value;
							$tempValue = str_replace("-","",$tempValue);
							$tempValue = str_replace(" ","",$tempValue);
							$tempValue = str_replace(".php","",$tempValue);
							$tempValue = strtolower($tempValue);

							if($theThemeName == $tempValue)
							{
								$theData = "/recommends/" . $value;
								$output .= '<tr><td align=center><a href="' . $theData . '" class="btn btn-info fa fa-search" target="_blank"> Find more details about this theme</a></td></tr>';								break;
							}
					
					}
					
					
					
				}
				
				$output .= '</tbody>
</table>
</div>
</div><div class="panel1 panel-blue">

<div class="panel-heading"><h3 class="panel-title"><i class="fa fa-cog"></i> Detected WordPress Plugins</h3></div>

<table class="table">
<tbody>';


				
				if(preg_match_all('#wp-content\/plugins\/([^\/]+)#',$outputPlugins,$r))
				{
					
					$r = array_unique($r[1]);
					if(strpos($outputPlugins,"W3 Total Cache") !== FALSE)
					{
						array_push($r,"w3-total-cache");
					}
					$theCount = 0;
					
					foreach ($r as $key => $value) {
						$resp1 = $this->getDataCurl("https://wordpress.org/plugins/" . $value . "/");
						$theImage = '/theme/default/img/unknownwp.png';
						$theName = $value;
						$theDesc = "";
						$wpLink = "http://google.com/search?q=" . $value . "%20plugin";
						if(preg_match("#<meta property='og:title' content='([^']*)#",$resp1,$d1))
						{
							if($d1[1] != '')
							{
								if(preg_match('#background-image: url\(([^\)]*)#',$resp1,$d))
								{
									$theImage = "http:" . $d[1];
								}
								else
								{
								    $theImage = '/theme/default/img/wpplugin.png';
								}
								
								if(preg_match("#<meta property='og:title' content='([^']*)#",$resp1,$d))
								{
									$theName = $d[1];
								}
								
								if(preg_match("#<meta property='og:description' content='([^']*)#",$resp1,$d))
								{
									$theDesc = $d[1];
								}
								$wpLink = "http://wordpress.org/plugins/" . $value;
							}
						}
						$theCount++;
						
						$output .= '<tr><td width="55%"><div class="plugin-image"><img width="332" height="107" src="' . $theImage . '"><div class="plugin-name">' . $theName . '</div></div></td><td><span class="badge badge-u">' . $theCount . '</span><span class="plugin-title">&nbsp;' . $theName . '</span><br>		' . $theDesc . '	<p></p>
                        <a href="' . $wpLink . '" class="btn btn-info fa fa-search" target="_blank"> Click here for More Info</a></td>
                        </tr>';					
					}
				}
				else
				{
					$output .= '<tr><td>No plugins were detected</td></tr>';				
				}
				
				
				$output .= '</tbody>
</table>
</div></div>';
			return $output;
		}
		else if($resp == "0")
		{
			return "<div align=center class='plugin-name'>Error accessing website, are you typing something wrong?</div>";
		}
		else
		{
			return "<div align=center class='plugin-name'>This does not look like a WordPress Site, It maybe a highly customized theme.</div>";
		}
	}
	
}
?>