<?php

defined('PAYMODE') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: Rainbow PHP Framework
 * @copyright © 2016 ProThemes.Biz
 *
 */
 
//PayPal Payment Gateway
if($paymentGateway == 'paypal'){

$paymentGatewayData = getPaymentGateway($con,null,$paymentGateway);

//Gateway Settings
$paymentSettings = unserialize($paymentGatewayData[5]);

//PayPal ID
$paypalID = Trim($paymentSettings[0]);

//PayPal Class
$p = new paypal_class(); 

//Set notification email to Admin
$notification = false;
if($notification)
    $p->admin_mail 	= $notification;
    
//Sandbox Mode
$p->sandbox = filter_var($paymentSettings[1], FILTER_VALIDATE_BOOLEAN);

//Log IPN Results to File
$p->ipn_log = false;

//Log IPN Results to database
$dbLog = filter_var($paymentSettings[2], FILTER_VALIDATE_BOOLEAN);

//PayPal Header Logo
$paypalFilePath = $paymentSettings[3];

//Fraud Prevention
$amountMismatch = filter_var($paymentSettings[4], FILTER_VALIDATE_BOOLEAN);
$idMismatch = filter_var($paymentSettings[5], FILTER_VALIDATE_BOOLEAN);

//Other Settings
$p->sitename = $site_name;
$p->themepath = $theme_path;
$p->logo = $logo_path;

    switch($paymentAction){
    	case "process": // case process insert the form data in DB and process to the paypal
    		$scriptPath = 'http://'.$_SERVER['HTTP_HOST'].'/payments/'.$paymentGateway.'/';
            addtoOrderLog($orderID, 'User Selected PayPal Payment Module',$con);
    		$p->add_field('business', $paypalID); // Call the facilitator eaccount
    		$p->add_field('cmd', '_cart'); // cmd should be _cart for cart checkout
    		$p->add_field('upload', '1');
    		$p->add_field('cpp_header_image', 'http://'.$_SERVER['HTTP_HOST'].$paypalFilePath);
    		$p->add_field('return', $scriptPath.'success/'.$orderID); // return URL after the transaction got over
    		$p->add_field('cancel_return', $scriptPath.'cancel'); // cancel URL if the trasaction was cancelled during half of the transaction
    		$p->add_field('notify_url', $scriptPath.'ipn'); // Notify URL which received IPN (Instant Payment Notification)
    		$p->add_field('currency_code', $currency_type);
    		$p->add_field('invoice', $invoice_prefix);
    		$p->add_field('custom', $orderID);
    		$p->add_field('item_name_1', $plan_name);
    		$p->add_field('item_number_1', $plan_id);
    		$p->add_field('quantity_1', '1');
    		$p->add_field('amount_1', $amount_tax);
    		$p->add_field('first_name', $firstname);
    		$p->add_field('last_name', $lastname);
    		$p->add_field('address1', $address1);
            $p->add_field('address2', $address2);
    		$p->add_field('city', $city);
    		$p->add_field('state', $state);
    		$p->add_field('country', $country);
    		$p->add_field('zip', $postcode);
    		$p->add_field('email', $payerEmail);
    	//	$p->add_field('cpp_logo_image', $_POST["cpp_header_image"]);
    	//	$p->add_field('image_url', $_POST["cpp_header_image"]);
    		$p->add_field('cbt', 'Return to '.$site_name.' Site');
    		$p->submit_paypal_post(); // POST it to paypal
    	//	$p->dump_fields(); // Show the posted values for a reference, comment this line before app goes live
    	break;
    	
    	case "success": // success case to show the user payment got success
    		$p_title = 'Payment Done Successfully';
            $type = 'success';
            $paymentData .= '<script>
          function doPayment() {
            setTimeout(function () {
                window.location.replace("/invoice/'.$route[3].'/activate");
            }, 2000);
          }
        </script>';
        require_once(THEME_DIR.'payments.php');
    	break;
    	
    	case "cancel": // case cancel to show user the transaction was cancelled
            $type = 'cancel';
            $paymentData .= '<script>
          function doPayment() {
            setTimeout(function () {
                window.location.replace("/premium");
            }, 2000);
          }
        </script>';
            $p_title = 'Transaction Cancelled...';
            require_once(THEME_DIR.'payments.php');
    	break;
    	
    	case "ipn": // IPN case to receive payment information.
    		if (!isset($_POST["txn_id"]))
                die();
            $transaction_id  = escapeTrim($con, $_POST["txn_id"]);
    		$payment_status = strtolower(escapeTrim($con, $_POST["payment_status"]));
    		$paypalAmount = floatval(escapeTrim($con, strtolower($_POST["mc_gross"])));
            //$callbackPayPalID = escapeTrim($con, $_POST["receiver_email"]);
            $callbackPayPalID = escapeTrim($con, $_POST["business"]);
            $product_id = escapeTrim($con, $_POST["item_number1"]); 
    		$invoice = escapeTrim($con, $_POST["invoice"]);
            $orderID = escapeTrim($con, $_POST["custom"]);
    		$log_array = escapeTrim($con, print_r($_POST, true));
            $orderStatus = $order_status;

            //Log IPN Results to database
            if($dbLog){
                addtoOrderLog($orderID,$log_array,$con);
            }
            
            //Validate the IPN
    		if ($p->validate_ipn()){ 
    			if ($payment_status == 'completed') {
    			    $orderInfo = getOrderInfo($orderID,$con);
                    
                    if($orderInfo !== false){
                        //Order Found
                        $totalAmount = floatval(removeFormatting($orderInfo['amount_tax']));
                        $planID = $orderInfo['plan_id'];
                        $planInfo = getComPlanInfo($planID,$con);
                        $activation = filter_var($planInfo['activation'], FILTER_VALIDATE_BOOLEAN);
                        
                        //Fraud Prevention
                        if($amountMismatch){
                            if (abs(($totalAmount-$paypalAmount)/$paypalAmount) < 0.00001) 
                                $payment_status = 'completed'; //Amount Okay
                            else 
                                $payment_status = 'fraud'; //Amount Mismatch
                        }
                        if($idMismatch){
                            if($paypalID == $callbackPayPalID)
                                $payment_status = 'completed'; //ID Okay
                            else
                                $payment_status = 'fraud'; //ID Mismatch 
                        }
                        
                        //Order Activation
                        if($activation){
                            if($payment_status == 'completed')
                                $orderStatus = 'completed';
                        }else{
                             $orderStatus = 'pending'; //$orderInfo['status'];
                        }
                    }
                }
                mysqli_query($con, "UPDATE premium_orders SET transaction_id='$transaction_id', payment_status='$payment_status', status='$orderStatus' WHERE id='$orderID'");
                addtoOrderLog($orderID, 'PayPal Instant Payment Notification - Payment Recieved',$con);
                invoiceConfirmationMail($orderID,$con);
                orderConfirmationMail($orderID,$con);
    		}else{
                addtoOrderLog($orderID, 'PayPal Instant Payment Notification - Payment Failed',$con);
    		} 
            
    	break;
    }

}