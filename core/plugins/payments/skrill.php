<?php

defined('PAYMODE') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: Rainbow PHP Framework
 * @copyright © 2016 ProThemes.Biz
 *
 */
 
//Skrill Payment Gateway
if($paymentGateway == 'skrill') {

$paymentGatewayData = getPaymentGateway($con,null,$paymentGateway);

//Gateway Settings
$paymentSettings = unserialize($paymentGatewayData[5]);

//Skrill ID
$skrillID = Trim($paymentSettings[0]);

//Skrill Secret Word
$skrillSecret = Trim($paymentSettings[1]);

//Log IPN Results to database
$dbLog = filter_var($paymentSettings[2], FILTER_VALIDATE_BOOLEAN);

//Skrill Header Logo
$skrillFilePath = $paymentSettings[3];

//Fraud Prevention
$amountMismatch = filter_var($paymentSettings[4], FILTER_VALIDATE_BOOLEAN);
$idMismatch = filter_var($paymentSettings[5], FILTER_VALIDATE_BOOLEAN);

//Skrill Payment URL
$skrillPayUrl = 'https://www.skrill.com/app/payment.pl';

    switch($paymentAction){
    	case "process":
                $paymentData = '';
                $gatewayName = 'Skrill';
                $type = 'process';
                $scriptPath = 'http://'.$_SERVER['HTTP_HOST'].'/payments/'.$paymentGateway.'/';
                addtoOrderLog($orderID, 'User Selected Skrill Payment Module',$con);
                $paymentData.= '<form method="post" name="skrill_form" action="'.$skrillPayUrl.'">';
                $paymentData.= '<input type="hidden" name="pay_to_email" value="'.$skrillID.'"/>
                  <input type="hidden" name="status_url" value="'.$scriptPath.'verify"/>
                  <input type="hidden" name="language" value="EN"/>
                  <input type="hidden" name="amount" value="'.$amount_tax.'"/>
                  <input type="hidden" name="currency" value="'.$currency_type.'"/>
                  <input type="hidden" name="detail1_description" value="'.$plan_name.'"/>
                  <input type="hidden" name="merchant_fields" value="orderid, invoice">
                  <input type="hidden" name="invoice" value="'.$invoice_prefix.'"/>
                  <input type="hidden" name="orderid" value="'.$orderID.'"/>
                  <input type="hidden" name="return_url" value="'.$scriptPath.'success/'.$orderID.'"/>          
                  <input type="hidden" name="return_url_text" value="Return to '.$site_name.' Site"/>
                  <input type="hidden" name="return_url_target" value="1"/>
                  <input type="hidden" name="cancel_url" value="'.$scriptPath.'cancel"/>
                  <input type="hidden" name="cancel_url_target" value="1"/>
                  <input type="hidden" name="firstname" value="'.$firstname.'"/>
                  <input type="hidden" name="lastname" value="'.$lastname.'"/>
                  <input type="hidden" name="address" value="'.$address1.'"/>
                  <input type="hidden" name="address2" value="'.$address2.'"/>
                  <input type="hidden" name="phone_number" value="'.$telephone.'"/>
                  <input type="hidden" name="postal_code" value="'.$postcode.'"/>
                  <input type="hidden" name="city" value="'.$city.'"/>
                  <input type="hidden" name="state" value="'.$statestr.'"/>
                  <input type="hidden" name="country" value="'.iso2to3($country).'"/>
                  <input type="hidden" name="pay_from_email" value="'.$payerEmail.'"/>
                  <input type="hidden" name="logo_url" value="https://'.$_SERVER['HTTP_HOST'].$skrillFilePath.'"/> 
                </form>';
                $paymentData .= '<br/>If you are not automatically redirected to skrill within 5 seconds...<br/><br/>';
                $paymentData .= '<input class="btn btn-sm btn-warning" type="submit" value="Click Here"></form>';
                $paymentData .= '<script>
                  function doPayment() {
                    setTimeout(function () {
                        document.forms[\'skrill_form\'].submit();
                    }, 2000);
                  }
                </script>';
                $p_title = 'Processing Payment...';
                require_once(THEME_DIR.'payments.php');  
               
        break;
       	
        case "success": // success case to show the user payment got success
    		$p_title = 'Payment Done Successfully';
            $type = 'success';
            $paymentData .= '<script>
          function doPayment() {
            setTimeout(function () {
                window.location.replace("/invoice/'.$route[3].'/activate");
            }, 2000);
          }
        </script>';
        require_once(THEME_DIR.'payments.php');
    	break;
    	
    	case "cancel": // case cancel to show user the transaction was cancelled
            $type = 'cancel';
            $paymentData .= '<script>
          function doPayment() {
            setTimeout(function () {
                window.location.replace("/premium");
            }, 2000);
          }
        </script>';
            $p_title = 'Transaction Cancelled...';
            require_once(THEME_DIR.'payments.php');
    	break;
                
    	case "verify": // Verify received payment information.

           if (!isset($_POST["transaction_id"]))
                die();
           
            $transaction_id  = escapeTrim($con, $_POST["transaction_id"]);
            $statusCode = raino_trim($_POST["status"]);    
            /*
            Payment completed => Status Code: 2 
            Payment pending => Status Code: 0 
            Payment failed => Status Code: -2 
            Payment cancelled => Status Code: -1 
            Payment chargeback => Status Code: -3 
            */
            if($statusCode == '2')
                $payment_status = 'completed';
            elseif($statusCode == '-2')
                $payment_status = 'failed';
            elseif($statusCode == '-1')
                $payment_status = 'cancelled';
            elseif($statusCode == '-3')
                $payment_status = 'chargeback';
            else
                $payment_status = 'pending';
            
            $md5Sig = strtoupper(raino_trim($_POST["md5sig"]));
    		$skrillAmount = floatval(escapeTrim($con, $_POST["amount"]));
            $callbackSkrillID = escapeTrim($con, $_POST["pay_to_email"]);
            $orderID = escapeTrim($con, $_POST["orderid"]); 
    		$invoice = escapeTrim($con, $_POST["invoice"]);
    		$log_array = escapeTrim($con, print_r($_POST, true));
            $orderStatus = $order_status;
                        
            //Log IPN Results to database
            if($dbLog){
                addtoOrderLog($orderID,$log_array,$con);
            }
            
            // Validate the Skrill signature
            $signatureData = $_POST['merchant_id']
                .$_POST['transaction_id']
                .strtoupper(md5($skrillSecret))
                .$_POST['mb_amount']
                .$_POST['mb_currency']
                .$_POST['status'];
            
            if (strtoupper(md5($signatureData)) == $md5Sig){
                //Signature Valid   
   			      if ($payment_status == 'completed') {
    			    $orderInfo = getOrderInfo($orderID,$con);
                    
                    if($orderInfo !== false){
                        //Order Found
                        $totalAmount = floatval(removeFormatting($orderInfo['amount_tax']));
                        $planID = $orderInfo['plan_id'];
                        $planInfo = getComPlanInfo($planID,$con);
                        $activation = filter_var($planInfo['activation'], FILTER_VALIDATE_BOOLEAN);
                        
                        //Fraud Prevention
                        if($amountMismatch){
                            if (abs(($totalAmount-$skrillAmount)/$skrillAmount) < 0.00001) 
                                $payment_status = 'completed'; //Amount Okay
                            else 
                                $payment_status = 'fraud'; //Amount Mismatch
                        }
                        if($idMismatch){
                            if($skrillID == $callbackSkrillID)
                                $payment_status = 'completed'; //ID Okay
                            else
                                $payment_status = 'fraud'; //ID Mismatch 
                        }
                        
                        //Order Activation
                        if($activation){
                            if($payment_status == 'completed')
                                $orderStatus = 'completed';
                        }else{
                             $orderStatus = 'pending'; //$orderInfo['status'];
                        }
                    }
                }
                mysqli_query($con, "UPDATE premium_orders SET transaction_id='$transaction_id', payment_status='$payment_status', status='$orderStatus' WHERE id='$orderID'");
                addtoOrderLog($orderID, 'Skrill Successful Callback Recieved',$con);
                invoiceConfirmationMail($orderID,$con);
                orderConfirmationMail($orderID,$con);
            }else{
                addtoOrderLog($orderID, 'Skrill Signature Validation Failed',$con);
    		}
            
        break;
        }
}