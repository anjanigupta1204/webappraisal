<?php


/*
 * @author Balaji
 * @name A to Z SEO Tools - PHP Script
 * @copyright � 2015 ProThemes.Biz
 *
 */
  
// Disable Errors
error_reporting(1);

require_once('../functions.php');

function makeDocx($templateFile, $newFile, $row)
{
  if (!copy($templateFile, $newFile))  // make a duplicate so we dont overwrite the template
    return false; // could not duplicate template
  $zip = new ZipArchive();
  if ($zip->open($newFile, ZIPARCHIVE::CHECKCONS) !== TRUE)
    return false; // probably not a docx file
  $file = 'word/document.xml';
  $data = $zip->getFromName($file);
  foreach ($row as $key => $value)
    $data = str_replace($key, $value, $data);
  $zip->deleteName($file);
  $zip->addFromString($file, $data);
  $zip->close();
  return true;
}

$format = raino_trim($_POST['format']);
$data = stripslashes($_POST['textArea']);

if ($format == "txt")
{
header('Content-type: text/plain');
header('Content-Disposition: attachment; filename="solidSEOTools.txt"');
echo $data;
die();
}
elseif ($format == "doc")
{
header("Content-type: application/vnd.ms-word");
header("Content-Disposition: attachment;Filename=solidSEOTools.doc");

echo "<html>";
echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\">";
echo "<body>";
echo "$data";
echo "</body>";
echo "</html>";
die();
}
elseif ($format == "docx")
{
$replacements = array('[all Data]' => $data,'[space box]' => " ");
$newFile = "temp.docx";
$templateName = 'templateDocx.docx';
if (makeDocx($templateName, $newFile, $replacements))
{
  header('Content-type: application/msword');
  header('Content-Disposition: attachment; filename=solidSEOTools.docx');
  header('Accept-Ranges: bytes');
  header('Content-Length: '. filesize($newFile));
  echo file_get_contents($newFile);
  unlink($newFile);
}
}
elseif ($format == "pdf")
{
    
}
else
{
    echo 'Invalid Format Type!';
}
?>