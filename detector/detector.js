"use strict";

/*

Instructions

* detector_launch();
launches the adblock detector, and allows the user to close the modal window

* detector_launch(true);
launches the adblock detector, and does NOT allow the user to close the modal window

*/

//Start Ultimate AdBlock Detector
$(function(){
    if(typeof detector_active === "undefined") {
        detector_launch();
    }
});
function detector_launch(block_close) {
    //Settings
    var detector_path = 'detector/';
    var detector_icon_size = 100;

    //Strings
    var detector_title = 'AdBlock plugin detected';
    var detector_description = 'Hi! We have detected you are using an AdBlock plugin on your browser. Our website is not crowded with banners and they won\'t affect your browsing experience. Please disable AdBlock on this website.';
    var detector_close = 'Close';
    var detector_reload = 'Done, reload page!';
    var detector_instructions_title = 'Instructions on how to disable AdBlock in your browser';
    var detector_instructions_steps_1 = 'Look to the upper-right corner of your browser&apos;s window.';
    var detector_instructions_steps_2 = 'Search for a red icon and choose the plugin you have to read specific instructions:';

    //Modal
    var html = '';
    html += '<div id="detector" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="UltimateAdBlockDetector">';
        html += '<div class="modal-dialog" role="document">';
            html += '<div class="modal-content">';
                html += '<div class="modal-header">';
                    html += '<button type="button" class="close" data-dismiss="modal" aria-label="' + detector_close + '"><span aria-hidden="true">&times;</span></button>';
                    html += '<h4 class="modal-title">' + detector_title + '</h4>';
                html += '</div>';
                html += '<div class="modal-body">';
                    html += '<div id="detector_step_1">';
                        html += '<p>' + detector_description + '</p>';
                        html += '<p><img class="img-responsive center-block" src="' + detector_path + 'browser.png"></p>';
                        html += '<ol>';
                            html += '<li>' + detector_instructions_steps_1 + '</li>';
                            html += '<li>' + detector_instructions_steps_2 + '</li>';
                        html += '</ol>';
                        html += '<div class="row" id="detector_plugins">';
                            html += '<div class="col-xs-4"><p class="text-center"><a href="#" class="plugin" data-plugin="adblock"><img src="' + detector_path + 'icon.adblock.png" width="' + detector_icon_size + '" height="' + detector_icon_size + '" alt="Ad Block"></a></p></div>';
                            html += '<div class="col-xs-4"><p class="text-center"><a href="#" class="plugin" data-plugin="adblock_plus"><img src="' + detector_path + 'icon.adblock_plus.png" width="' + detector_icon_size + '" height="' + detector_icon_size + '" alt="Ad Block Plus"></a></p></div>';
                            html += '<div class="col-xs-4"><p class="text-center"><a href="#" class="plugin" data-plugin="ublock_origin"><img src="' + detector_path + 'icon.ublock_origin.png" width="' + detector_icon_size + '" height="' + detector_icon_size + '" alt="uBlock Origin"></a></p></div>';
                        html += '</div>';
                    html += '</div>';
                    html += '<div id="detector_step_2" class="hidden">';
                        html += '<img id="detector_img_instructions" class="center-block" src="" alt="Instructions">';
                    html += '</div>';
                html += '</div>';
                html += '<div class="modal-footer">';
                    html += '<button id="detector_close" type="button" class="btn btn-block btn-primary" data-dismiss="modal">' + detector_close + '</button>';
                    html += '<button id="detector_reload" type="button" class="btn btn-block btn-primary hidden">' + detector_reload + '</button>';
            html += '</div>'; //modal-content
        html += '</div>'; //modal-dialog
    html += '</div>'; //modal
    $('body').append(html);
    if(!block_close) {
        $('#detector .modal-header button.close').removeClass('hidden');
        $('#detector .modal-footer').removeClass('hidden');
    } else {
        $('#detector .modal-header button.close').addClass('hidden');
        $('#detector .modal-footer').addClass('hidden');
    }

    //Css
    var css = document.createElement('style');
    css.type = 'text/css';
    css.innerHTML = '#detector_plugins a.plugin img { width: ' + detector_icon_size + 'px; height: ' + detector_icon_size + 'px; padding: 10px; border-radius: 5px; border: 3px solid #fff; } #detector_plugins a.plugin:hover img { border-color: #ddd; }';
    document.body.appendChild(css);

    //Instructions to deactivate adblocker
    $('#detector_plugins a.plugin').on('click', function(e){
        e.preventDefault();
        $('#detector_img_instructions').attr('src', detector_path + 'preview.' + $(this).data('plugin') + '.png');
        $('#detector .modal-footer').removeClass('hidden');
        $('#detector_close').addClass('hidden');
        $('#detector_reload').removeClass('hidden');
        $('#detector_step_1').addClass('hidden');
        $('#detector_step_2').removeClass('hidden');
    });
    $('#detector_reload').on('click', function(e){
        e.preventDefault();
        window.location.reload(false);
    });

    //Trigger modal
    if(!block_close) {
        $('#detector').modal('show', {
            backdrop: true,
            keyboard: true
        });
    } else {
        $('#detector').modal('show', {
            backdrop: 'static',
            keyboard: false
        });
    }
}
