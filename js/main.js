	$(document).ready(function() {


	    $('.owl-carousel').owlCarousel({
	        loop: true,
	        margin: 0,
			nav: true,
			autoplay:true,
			autoplayTimeout:3000,
			autoplayHoverPause:true,
	        responsive: {
	            0: {
	                items: 1
	            },
	            600: {
	                items: 3
	            },
	            1000: {
	                items: 3
	            }
	        }
	    })


	    $('.scrollToTop').click(function() {
	        $("html, body").animate({ scrollTop: 0 }, "slow");
	        return false;
	    });

	    $('.section-scroll').click(function() {
	        $('html,body').animate({
	            scrollTop: $(".tool-section").offset().top -50 
	        }, 1000);
	        return true;
	    })


	    $(window).scroll(function() {
	        if ($(window).width() > 768) {
	            if ($(this).scrollTop() > 150) {
	                $('header').addClass("sticky");
	            } else {
	                $('header').removeClass("sticky");
	            }
	        }
	    });




	});