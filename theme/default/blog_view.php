<?php

defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: Rainbow PHP Framework v1.0
 * @copyright © 2015 ProThemes.Biz
 *
 */
?>

<link href="<?php echo $theme_path; ?>css/blog.css" rel="stylesheet" type="text/css" />

    <div class="container main-container">
        <div class="row">
            <div class="col-md-8 main-index">
            <div class="row">
              <div class="col-md-1">
                <div class="date_1">
                  <div class="date_up2">
                    <div class="center2 tbox"><?php echo $post_month; ?></div>
                  </div>
                  <div class="date_down2">
                    <div class="text_word"><?php echo $post_day; ?> </div>
                  </div>
                </div>
              </div>
              <div class="col-md-11 pad_left26">
              <div class="romantic_free">
                <h4><?php echo ucfirst($post_title); ?></h4>
              </div>
              
              <div class="text_size12 clock"> 
                <span class="color_text_in"> 
                    <i class="fa fa-clock-o color-grey" style="font-size:14px;"></i>
                    <b class="color_grap"> <?php echo $posted_date; ?></b> 
                </span> 
                
                <span class=" color_text_in">
                    <i class="fa fa-user color-grey" style="font-size:14px;"></i> by 
                    <b class="color_grap"><?php echo $posted_by; ?></b>
                </span>
                
                <span class="color_text_in"> 
                    <i class="fa fa-tag color-grey" style="font-size:14px;"></i>in 
                    <b class="color_grap"><a href="/blog/category/<?php echo str_replace(' ','-',$category); ?>"><?php echo ucfirst($category); ?></a></b> 
                </span>  
              </div>
              
              </div>
            </div>
            
            <hr /><br />
                
            <?php echo $post_content; ?>
                
            <br />
			<div align="center">
            <div class="xd_top_box">
                <?php echo $text_ads; ?>
            </div>
			</div>
            <br />
            <?php if($showComment){  ?>
            <div class="divider_h mr_top30"></div>
                <div class="row">
                    <div class="col-xs-12">
                    <?php if($post_allow_comment){ ?>
                    <div class="panel panel-default">
                      <div class="panel-heading leave">
                      <div class="posts_1">
                           <h5>leave<span> a comment</span></h5>
                            <span class="font_14 comment-here">Please post your comments here.</span>
                      </div>
                      </div>
                      <div class="panel-body">
                        <?php echo $discuss_id; ?>                
                      </div>
                  </div>
                    <?php } else { ?>
                    <br />
                    <div class="alert alert-error">
                    Comments are disabled for this post!
                    </div>
                    <br /><br />
                    <?php } ?>
                  </div>
               </div>
                            
                <div class="xd_top_box">
				<div align="center">
                    <?php echo $ads_720x90; ?>
                </div>
</div>
                <br />
             <?php } ?>
            </div>
            <?php 
            // Sidebar 
            require_once(THEME_DIR. "sidebar.php"); 
            ?>
        </div>
    </div>
    <br />