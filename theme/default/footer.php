<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));
/*
* @author Balaji
* @name: A to Z SEO Tools - PHP Script
* @Theme: Default Style
* @copyright � 2015 ProThemes.Biz
*
*/
?>
<footer class="footer">
	<!-- Widget Area -->
	<div class="b-widgets">
		<div class="container primary-footer"  data-aos="zoom-in">
			<div class="row">
				<!-- Links -->
				<div class="row-item col-sm-4">
					<div class="abt-block">
						<h3>ABOUT US</h3>
						<p>
							Our website lets you know alot of information about your seo insights and alow you to tweek your site to the best and optimal seo performace. You can your out free seo tools at anytime and anyware to improve your seo preformace on your website.
						</p>
					</div>
				</div>
				<!-- End Links -->
				<!-- Latest Tweets -->
				<div class="row-item col-sm-4 bl">
					<div class="footer-menu">
						<h3>NAVIGATION</h3>
						<div class="footer-nav">
							<!-- <ul>
								<li>
									<a href="#">About Us</a>
								</li>
								<li>
									<a href="#">Terms &amp; Conditions</a>
								</li>
								<li>
									<a href="#">Search Engine Optimization - What is SEO</a>
								</li>
								<li>
									<a href="#">What's Website Analysis ?</a>
								</li>
								<li>
									<a href="#">Get 25$ Payoneer Bonus and Payoneer MasterCard</a>
								</li>
							</ul> -->
							<ul class="b-list just-links m-dark">
								<?php
								foreach($footerLinks as $footerLink)
								echo $footerLink;
								?>
							</ul>
						</div>
					</div>
				</div>
				<!-- End Latest Tweets -->
				
				<!-- Contact Form -->
				<div class="row-item col-sm-4 bl">
					<div class="contact-form">
						<h3><?php echo $lang['244']; ?></h3>
						<!-- Success Message -->
						<div class="form-message"></div>
                        <div id="index_alert1"> 
                        <br /><br /><br /><br />
                        <div class="alertNew alertNew-block alertNew-success">
                        <button data-dismiss="alert" class="close" type="button">X</button>
	
                        <i class="fa fa-check green"></i>							
                        <b>Alert!!</b> <?php echo $lang['229']; ?>.
                        </div>
                        </div>  
						
						<!-- Form -->
						<div id="indexContact">  
						<form method="POST" action="#" class="b-form b-contact-form">
							<div class="form-group input-wrap m-full-width">
								<input type="text" placeholder="<?php echo $lang['245']; ?>" class="form-control" name="c_name"/>
							</div>
							<div class="form-group input-wrap m-full-width">
								<input type="email" placeholder="<?php echo $lang['246']; ?>" class="form-control" name="e_mail" />
							</div>
							<div class="form-group input-wrap m-full-width">
								<textarea placeholder="<?php echo $lang['238']; ?>" class="form-control" name="email_message"></textarea>
							</div>
							<div class="form-group input-wrap m-full-width text-right">
								
								<button class="btn btn-info send-btn" onclick="smallContactDoc()" type="button">
									<i class="fa fa-location-arrow" aria-hidden="true"></i> <?php echo $lang['247']; ?>
								</button>
								
							</div>
						</form>
						</div>
					</div>
					<!-- End Form -->
				</div>
				<!-- End Contact Form -->
			</div>
		</div>
		<div class="container secondary-footer"  >
			<div class="row">
				<div class="col-sm-9">
					<span class="copy-txt">Copyright &copy; 2017 Website-<a href="www.appraisal.com">appraisal.com</a>. All rights reserved</span>
				</div>
				<div class="col-sm-3">
					<ul class="f-social-links">
						<li>
							<a  href="<?php echo $face; ?>" target="_blank" rel="nofollow" data-toggle="tooltip" data-placement="top" title="Facebook Page">
								<i class="fa fa-facebook" aria-hidden="true"></i>
							</a>
						</li>
						<li>
							<a href="<?php echo $twit; ?>" target="_blank" rel="nofollow" data-toggle="tooltip" data-placement="top" title="Twitter Account">
								<i class="fa fa-twitter" aria-hidden="true"></i>
							</a>
						</li>
						<li>
							<a href="<?php echo $gplus; ?>" target="_blank" rel="nofollow" data-toggle="tooltip" data-placement="top" title="Google+ Profile">
								<i class="fa fa-google-plus" aria-hidden="true"></i>

							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<a href="javascript:void(0);" class="scrollToTop" ><i class="fa fa-angle-double-up" aria-hidden="true"></i></a>
	<!-- End Widget Area -->
	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	ga('create', '<?php echo $ga; ?>', 'auto');
	ga('send', 'pageview');
	</script>
</footer>
<!-- Latest compiled and minified Bootstrap JavaScript -->
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo $theme_path; ?>js/bootstrap.min.js" type="text/javascript"></script>
<!-- App -->
<script src="<?php echo $theme_path; ?>js/app/app.js" type="text/javascript"></script>
<script src="<?php echo $theme_path; ?>js/aos.js" type="text/javascript"></script>
<!-- Sign in -->
<div class="modal fade loginme" id="signin" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><?php echo $lang['263']; ?></h4>
			</div>
			<form method="POST" action="/?route=account&login" class="loginme-form">
				<div class="modal-body">
					<div class="alert alert-warning">
						<button type="button" class="close dismiss">&times;</button><span></span>
					</div>
					<?php if($enable_oauth){ ?>
					<div class="form-group connect-with">
						<div class="info"><?php echo $lang['267']; ?></div>
						<a href="/?route=facebook&login" class="connect facebook" title="<?php echo $lang['268']; ?>">Facebook</a>
						<a href="/?route=google&login" class="connect google" title="<?php echo $lang['269']; ?>">Google</a>
					</div>
					<?php } ?>
					<div class="info"><?php echo $lang['270']; ?></div>
					<div class="form-group">
						<label><?php echo $lang['271']; ?> <br />
							<input type="text" name="username" class="form-input"  style=" width: 96%;"/>
						</label>
					</div>
					<div class="form-group">
						<label><?php echo $lang['272']; ?> <br />
							<input type="password" name="password" class="form-input"  style=" width: 96%;" />
						</label>
					</div>
				</div>
				<div class="modal-footer"> <br />
					<button type="submit" class="btn btn-primary  pull-left"><?php echo $lang['263']; ?></button>
					<div class="pull-right align-right">
						<a style="color: #3C81DE;" href="/?route=account&forget"><?php echo $lang['273']; ?></a><br />
						<a style="color: #3C81DE;" href="/?route=account&resend"><?php echo $lang['274']; ?></a>
					</div>
				</div>
				<input type="hidden" name="signin" value="<?php echo md5($date.$ip); ?>" />
			</form>
		</div>
	</div>
</div>
<!-- Sign up -->
<div class="modal fade loginme" id="signup" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><?php echo $lang['264']; ?></h4>
			</div>
			<form action="/?route=account&register" method="POST" class="loginme-form">
				<div class="modal-body">
					<div class="alert alert-warning">
						<button type="button" class="close dismiss">&times;</button><span></span>
					</div>
					<?php if($enable_oauth){ ?>
					<div class="form-group connect-with">
						<div class="info"><?php echo $lang['267']; ?></div>
						<a href="/?route=facebook&login" class="connect facebook" title="<?php echo $lang['268']; ?>">Facebook</a>
						<a href="/?route=google&login" class="connect google" title="<?php echo $lang['269']; ?>">Google</a>
					</div>
					<?php } ?>
					<div class="info"><?php echo $lang['277']; ?></div>
					<div class="form-group">
						<label><?php echo $lang['271']; ?> <br />
							<input type="text" name="username" class="form-input" />
						</label>
					</div>
					<div class="form-group">
						<label><?php echo $lang['275']; ?> <br />
							<input type="text" name="email" class="form-input"  />
						</label>
					</div>
					<div class="form-group">
						<label><?php echo $lang['276']; ?> <br />
							<input type="text" name="full" class="form-input" />
						</label>
					</div>
					<div class="form-group">
						<label><?php echo $lang['272']; ?> <br />
							<input type="password" name="password" class="form-input" />
						</label>
					</div>
				</div>
				<div class="modal-footer"> <br />
					<button type="submit" class="btn btn-primary"><?php echo $lang['264']; ?></button>
				</div>
				<input type="hidden" name="signup" value="<?php echo md5($date.$ip); ?>" />
			</form>
		</div>
	</div>
</div>
</body>
</html>
<script type="application/ld+json"> {
"@context" : "https://schema.org",
"@type" : "Website",
"name":"Website Appraisal",
"url":"https://website-appraisal.com",
"aggregateRating":{
"@type":"AggregateRating",
"ratingValue":"4.2",
"reviewCount":"71"}
{
"@context": "http://schema.org",
"@type": "Organization",
"url": "https://www.website-appraisal.com",
"logo": "https://website-appraisal.com/uploads/7300_Untitled-24.png"
}
} </script>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-61659272-1', 'auto');
ga('send', 'pageview');
</script>
<script>
      AOS.init({
        easing: 'ease-in-out-sine'
      });
    </script>