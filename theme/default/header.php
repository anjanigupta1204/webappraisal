<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));
/*
* @author Balaji
* @name: A to Z SEO Tools - PHP Script
* @Theme: Default Style
* @copyright � 2016 ProThemes.Biz
*
*/
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta http-equiv="Content-Language" content="en" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" type="image/png" href="<?php echo $fav_path; ?>" />
        <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
        <!-- Meta Data-->
        <title><?php if(isset($p_title)) { echo $p_title.' | '. $site_name;}else { echo $title; } ?></title>
        
        <meta property="site_name" content="<?php echo $site_name; ?>"/>
        <meta name="description" content="<?php echo $des; ?>" />
        <meta name="keywords" content="<?php echo $keyword; ?>" />
        <meta name="author" content="website-appraisal." />
        <meta name="google-site-verification" content="c5yd4mzfL_kUgRnXCkD2SjIC4GGXQ4sbhn5PS9PH_G4" />
        <meta name="msvalidate.01" content="66D48D8EDBEC4EAD9DA3DD98E950ACFF" />
        <meta name="robots" content="index, follow">
        <meta name="wot-verification" content="d00205743742869afaf3"/>
        <meta name="google-site-verification" content="c5yd4mzfL_kUgRnXCkD2SjIC4GGXQ4sbhn5PS9PH_G4" />
        <meta name="yandex-verification" content="0a44789202b2ca77" />
        
        
        <!-- Open Graph -->
        <meta property="og:title" content="<?php if(isset($p_title)) { echo $p_title.' | '. $site_name;}else { echo $title; } ?>" />
        <meta property="og:site_name" content="<?php echo $site_name; ?>" />
        <meta property="og:type" content="website" />
        <meta property="og:description" content="<?php echo $des; ?>" />
        <meta property="og:image" content="<?php echo (substr($server_path,-1) == '/' ? substr($server_path,0,-1) : $server_path).$logo_path?>"/>
        <meta property="og:url" content="<?php echo (substr($server_path,-1) == '/' ? substr($server_path,0,-1) : $server_path).$_SERVER[REQUEST_URI]; ?>"/>
        <!-- Main style -->
        <link href="<?php echo $theme_path; ?>css/theme.css" rel="stylesheet" />
        
        <!-- Font-Awesome -->
        <link href="<?php echo $theme_path; ?>css/font-awesome.min.css" rel="stylesheet" />
        <!-- Owl Carousel style -->
        <link href="css/owl.carousel.css" rel="stylesheet" />
        <!-- Custom Theme style -->
        <link href="<?php echo $theme_path; ?>css/custom.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $theme_path; ?>css/reset.css" rel="stylesheet" type="text/css" />
        <!-- Page Theme style -->
        <link href="<?php echo $theme_path; ?>css/style.css" rel="stylesheet" />
        <link href="<?php echo $theme_path; ?>css/responsive.css" rel="stylesheet" />
        <!-- Animation CSS -->
        <link href="<?php echo $theme_path; ?>css/aos.css" rel="stylesheet" />
        
        <style>
        .navbar-nav > li.dropdown .dropdown-menu {
        box-shadow: 0 0 3px rgba(0, 0, 0, 0.09) !important;
        border-top: 0 !important;
        }
        .navbar-brand .icon {
        background: none !important;
        }
        .navbar-nav > li.dropdown .dropdown-menu a:hover {
        background: rgba(255, 255, 255, 0.02) !important;
        color: #989ea8;
        }
        .reviewIn{
        height: 50px !important;
        box-shadow: none !important;
        font-size: 15px !important;
        padding:8px 10px !important;
        color: #333 !important;
        }
        .reviewIn:focus {
        border-color: #51BAD6;
        box-shadow: 0 0 5px rgba(255, 255, 255, 0.4) !important;
        }
        
        .premiumBox h2{
        color: #e0e8eb;
        font-size: 22px;
        }
        .newH3{
        color: #989ea8;
        font-size: 20px;
        font-weight: 400;
        }
        .hisData {
        background: #fff none repeat scroll 0 0;
        border: 1px solid #e3e3e3;
        border-radius: 4px;
        margin-bottom: 20px;
        padding: 0 !important;
        }
        .hisData .titleBox {
        background: #f3f3f5 none repeat scroll 0 0;
        border-bottom: 1px solid #d7dbde;
        border-top-left-radius: 4px;
        border-top-right-radius: 4px;
        color: #2c3e50;
        font-size: 15px;
        font-weight: bold;
        padding: 15px 50px 15px 20px;
        position: relative;
        text-transform: uppercase;
        }
        .hisUl{
        list-style: outside none none;
        margin: 0 !important;
        padding: 0 !important;
        }
        .hisUl li{
        border-bottom: 1px solid #dcdcdc;
        list-style: outside none none;
        margin: 0 !important;
        padding: 0 !important;
        }
        .hisUl li a {
        color: #1f313f;
        display: block;
        padding: 10px 20px;
        position: relative;
        }
        .hisUl li a:hover {
        background: #fbfcfc none repeat scroll 0 0;
        color: #ec4060;
        }
        .favicon {
        margin-right: 8px;
        vertical-align: -2px;
        }
        </style>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <!-- jQuery 1.10.2 -->
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="js/owl.carousel.js"></script>
        <script src="detector/adsbygoogle.js"></script>
        <script src="detector/detector.js"></script>
        <script src="js/main.js"></script>
    </head>
    <body class="por">
        <header id="header" class="header-wrapper">
            <nav class="navbar navbar-default navbar-static-top" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#collapse-menu">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/">
                        <img alt="<?php echo $site_name; ?>" src="<?php echo $logo_path; ?>" class="atoz_seo_tools_logo lg-logo" />
                        
                        <img alt="<?php echo $site_name; ?>" src="<?php echo $theme_path; ?>/img/sm-logo.png" class="atoz_seo_tools_logo sm-logo" />
                        </a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="collapse-menu">
                        
                        <ul class="nav navbar-nav navbar-right login-wrap">
                            <!-- <li>
                                <a href="#"> <i class="fa fa-sign-in" aria-hidden="true"></i> Login </a>
                            </li>
                            <li>
                                <a href="#"> <i class="fa fa-user" aria-hidden="true"></i> Register </a>
                            </li> -->
                            <?php
                            
                            if($enable_reg){
                                if(!isset($_SESSION['userToken'])) {    
                                ?>
                                <li>
                                    <a href="#" data-target="#signin" data-toggle="modal">
                                        <i class="fa fa-sign-in" aria-hidden="true"></i><?php echo $lang['263']; ?> 
                                    </a>
                                </li>
                                <li>
                                    <a href="#" data-target="#signup" data-toggle="modal">
                                        <i class="fa fa-user" aria-hidden="true"></i><?php echo $lang['264']; ?> 
                                    </a>
                                </li>  
                                <?php } else { ?>
                                <li>
                                    <a href="/?logout" title="<?php echo $lang['266']; ?>">
                                        <?php echo $lang['266']; ?> 
                                    </a>
                                </li>  
                                <?php } } ?>
                        </ul>
                        <ul class="nav navbar-nav mr5px">
                            <li <?php if($controller == "main") echo 'class="active"'; ?>>
                                <a href="/"><?php echo $lang['1']; ?> </a>
                            </li>
                            <?php if(file_exists(CON_DIR."premium.php")){  if(!isset($_SESSION['premiumClient'])){ ?>
                                <li <?php if($controller == "premium") echo 'class="active"'; ?>> 
                                    <a href="/premium">Pricing</a>
                                </li>
                                <?php } else { ?>
                                <script>$(document).ready(function() {  $(".dropdown-toggle").dropdown(); });</script>    
                                <li class="dropdown">
                                    <a href="#" data-toggle="dropdown" class="dropdown-toggle">Account <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="/my-dashboard">Dashboard</a></li>
                                        <li><a href="/my-profile">Profile</a></li>
                                        <li><a href="/settings">PDF Settings</a></li>
                                        <li><a href="/my-subscription">Subscription</a></li>
                                        <li><a href="/my-invoice">My Invoices</a></li>
                                    </ul>
                                </li>
                                
                                <?php } } ?>
                          
                            <?php if($blog_status){ ?>
                                <li <?php if($controller == "blog") echo 'class="active"'; ?>> 
                                    <a href="/blog"><?php echo $lang['2']; ?></a>
                                </li>
                                <?php } ?>
                            
                                <?php
                                    foreach($headerLinks as $headerLink)
                                        echo $headerLink;
                                ?>
                          
                            <li <?php if($controller == "contact") echo 'class="active"'; ?>>
                                <a href="/contact"> Contact Us </a>
                            </li> 
                        </ul>
                        </div><!-- /.navbar-collapse -->
                        </div><!-- /.container -->
                        
                        </nav><!--/.navbar-->
                    </header>
                    <?php  if($controller == "main"){ ?>
                    <div class="masthead">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-offset-1 col-md-10 seobannerBig">
                                    <div class="premiumBox text-center">
                                        <h1 class="seobannerh1">Website appraisal &amp; Seo Tools &amp; Domain Appraisal</h1>
                                        <h2>Free seo tools and website value calculator, start using our site to analyze <strong> Seo, Valuate &amp; Optimize </strong> Unlimited websites.</h2>
                                        <form method="POST" action="/domain" onsubmit="return fixURL();">
                                            <div class="input-group reviewBox search-box">
                                                <div class="input-container">
                                                    <input type="text" tabindex="1" placeholder="Website URL to review" id="url" name="url" class="form-control reviewIn"/>
                                                </div>
                                                <!-- <div class="input-group-btn">
                                                    <button tabindex="2" type="submit" name="generate" class="btn btn-primary btn-lg">
                                                        <span class="ready">
                                                           
                                                        </span>
                                                    </button>
                                                </div> -->
                                            </div>
                                        
                                        <ul class="btn-list">
                                            <li>
                                                <button class="btn btn-default primary-btn hvr-rectangle-in" type="submit">
                                                    <?php echo isset($_SESSION['premiumToken']) ? 'Analyze' : 'Try it for Free'; ?>
                                                </button>
                                            </li>
                                            <li>
                                                <button class="btn btn-default secondary-btn hvr-rectangle-out" type="submit">View Features</button>
                                            </li>
                                        </ul>
                                        </form>
                                    </div>
                                </div>
                                
                                
                            </div>
                        </div>
                        <a href="javascript:void(0);" class="section-scroll">&nbsp;</a>
                    </div>
                    <?php } else { ?>
                    <div class="submasthead">
                        <div class="container">
                            <!--<div class="col-md-6 seobannerSmall">
                                <h1 class="sub_seobannerh1"><?php //echo $p_title; ?></h1>
                            </div>
                            
                            <div class="col-md-6">
                                <img class="visible-lg visible-md" alt="<?php //echo $lang['317']; ?>" src="<?php //echo $theme_path; ?>img/seobanner_mini.png" />
                            </div>-->
                            
                        </div>
                    </div>
                    <?php } if($maintenanceRes[0]){ ?>
                    <div class="alert alert-error text-center" style="margin: 35px 140px -10px 140px;">
                        <strong>Alert!</strong> &nbsp; Your website is currently set to be closed.
                    </div>
                    <?php } ?>
                    <script>
                    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
                    ga('create', 'UA-61659272-1', 'auto');
                    ga('send', 'pageview');
                    </script>