<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Invoice</title>
    <link href="<?php echo $theme_path; ?>premium/css/premium.css" rel="stylesheet" type="text/css" />
    <style>
    body {
    	background: #e3e6e7 url(<?php echo $theme_path; ?>premium/img/background.png) repeat;
    	color: #61686d;
    	font: 14px "Helvetica Neue", Helvetica, Arial, Verdana, sans-serif;
    	font-weight: lighter;
    	padding-bottom: 60px;
    }
    
    #pageInvoice {
    	background: #ffffff;
    	width: 878px;
    	margin: 0 auto;
    	margin-top: 80px;
    	display: block;
    	border: 1px solid #c4c7c7;
    	padding: 40px 40px 50px 40px;
    	position: relative;
    	z-index: 0;
    }
    
    .page-shadow {
    	background-image: url(<?php echo $theme_path; ?>premium/img/page-shadow.png);
    	width: 992px;
    	height: 60px;
    	margin: 0 auto;
    	margin-top: -1px;
    	z-index: 1;
    	position: relative;
    }
    strong{
        font-weight: bold;
    }
    </style>
</head>

<body>

    <div id="pageInvoice">
    	<div class="status <?php echo $invoiceClass; ?>">
    	<p><?php echo $invoiceStats; ?></p>
    	</div>
    		
    	<p class="recipient-address">
    	<strong>Invoiced To:</strong><br />
        <?php echo $userAddress; ?></p>
    	
    	<h1>INVOICE: <?php echo $orderInfo['invoice_prefix']; ?></h1>
    	<p class="terms"><strong>Date: <?php echo $invoiceDate; ?></strong><br/>
        Due Date: <?php echo $dueDate; ?></p>
    	
    	<img src="<?php echo $invoice_logo; ?>" alt="yourlogo" class="company-logo" />
    	<p class="company-address">
            <?php echo html_entity_decode(strEOL($company_add)); ?>
        
        <?php if($orderInfo['payment_status'] == 'pending'){ if($activateMsg == '') { ?>
        <form method="POST" action="#" style="margin-top: -20px; color: #a1a7ac; position: absolute; right: 40px; text-align: right; width: 200px;">
            <strong class="payMethod">Payment Method:</strong><br/>
            <select class="form-control select-inline" name="gateway">
                <?php echo $paymentGatewayData; ?>
            </select>
            <input type="hidden" name="paynow" value="1" />
            <button type="submit" class="btn btn-success btn-sm">Pay Now</button>
            <br /><br />
        </form>
        <?php } } ?>
	</p>
	
	<table id="table" class="tablesorter" cellspacing="0"> 
	<thead> 
    <tr> 
	    <th class="header">ID</th> 
	    <th class="header">Plan Name</th> 
	    <th class="header right" style="width:22%;">Amount</th> 
	    <th class="header right" style="width:18%;">Total</th> 
	</tr>
	</thead> 
	<tbody> 
	<tr class="odd"> 
	    <td>#<?php echo $orderInfo['plan_id']; ?></td> 
	    <td><?php echo $orderInfo['plan_name']; ?></td> 
	    <td class="right"><?php echo $currencySymbol[0].$amount; ?></td> 
	    <td class="right"><?php echo $currencySymbol[0].$amount; ?></td> 
	</tr> 
	</tbody> 
	</table> 
    <div id="invoiceTotals">
        <table class="totalTable" style="width:100%;" cellspacing="0">
            <tbody>
            <tr>
                <td>&nbsp;</td>
                <td style="width:22%;" class="text-right">Subtotal</td>
                <td style="width:18%;" class="text-right"><?php echo $currencySymbol[0].$amount; ?></td>
            </tr>
            <?php foreach($taxArrData as $taxCurData){ ?>
                <tr>
                    <td></td>
                    <td class="text-right"><?php echo $taxCurData[0]; ?></td>
                    <td class="text-right"><?php echo $currencySymbol[0].$taxCurData[1]; ?></td>
                </tr>
            <?php } ?>
            
            <tr class="invoiceTotal">
                <td></td>
                <td class="even text-right">Total</td>
                <td class="even text-right"><?php echo $currencySymbol[0].$totalAmount.' '.$orderInfo['currency_type']; ?></td>
            </tr>
            
            </tbody>
        </table>
    </div>
    
	<div class="total-due">
		<div class="total-heading"><p>Amount <?php echo $invoiceStats; ?></p></div>
		<div class="total-amount"><p><?php echo $currencySymbol[0].$totalAmount; ?></p></div>
	</div>
	
	<div class="invoiceClear"></div>
    <?php echo html_entity_decode(strEOL($invoice_footer)); ?>                 	
</div>
            
<div class="page-shadow"></div>

</body>
</html>