<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));
/*
 * @author Balaji
 * @name: A to Z SEO Tools - PHP Script
 * @Theme: Default Style
 * @copyright � 2016 ProThemes.Biz
 *
 */
?>

<link href="<?php echo $theme_path; ?>premium/css/premium.css" rel="stylesheet" type="text/css" />

    <div class="container main-container">
        <div class="row">
            <div class="col-md-8 main-index">
                  
                <?php if($activateMsg != '') { ?>
                <br />
                    <div class="alert alert-<?php echo $activateClass; ?> alert-dismissable alert-premium">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                    <?php echo $activateMsg; ?>
                    </div>
                <br />
                <?php } if($refreshMyUrl){ ?>
                    <meta http-equiv="refresh" content="10" />
                <?php } ?>
                                                
                <br />
                
                <div id="pageInvoice">
            	<div class="status <?php echo $invoiceClass; ?>">
           		<p><?php echo $invoiceStats; ?></p>
            	</div>
            		
            	<p class="recipient-address">
            	<strong>Invoiced To:</strong><br />
                <?php echo $userAddress; ?></p>
            	
            	<h1>INVOICE: <?php echo $orderInfo['invoice_prefix']; ?></h1>
            	<p class="terms"><strong>Date: <?php echo $invoiceDate; ?></strong><br/>
           	    Due Date: <?php echo $dueDate; ?></p>
            	
            	<img src="<?php echo $invoice_logo; ?>" alt="yourlogo" class="company-logo" />
            	<p class="company-address">
                        <?php echo html_entity_decode(strEOL($company_add)); ?>
                </p>
                
                <?php if($orderInfo['payment_status'] == 'pending'){ if($activateMsg == '') { ?>
                <form method="POST" action="#" style="margin-top: -20px; color: #a1a7ac; position: absolute; right: 40px; text-align: right; width: 200px;">
                    <strong class="payMethod">Payment Method:</strong><br/>
                    <select class="form-control select-inline" name="gateway">
                        <?php echo $paymentGatewayData; ?>
                    </select>
                    <input type="hidden" name="paynow" value="1" />
                    <button type="submit" class="btn btn-success btn-sm">Pay Now</button>
                    <br /><br />
                </form>
                <?php } } ?>

            	<table id="table" class="tablesorter" cellspacing="0"> 
            	<thead> 
                <tr> 
            	    <th class="header">ID</th> 
            	    <th class="header">Plan Name</th> 
            	    <th class="header right" style="width:22%;">Amount</th> 
            	    <th class="header right" style="width:18%;">Total</th> 
            	</tr>
            	</thead> 
            	<tbody> 
            	<tr class="odd"> 
            	    <td>#<?php echo $orderInfo['plan_id']; ?></td> 
            	    <td><?php echo $orderInfo['plan_name']; ?></td> 
            	    <td class="right"><?php echo $currencySymbol[0].$amount; ?></td> 
            	    <td class="right"><?php echo $currencySymbol[0].$amount; ?></td> 
            	</tr> 
            	</tbody> 
            	</table> 
                <div id="invoiceTotals">
                    <table class="totalTable" style="width:100%;" cellspacing="0">
                        <tbody>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="width:22%;" class="text-right">Subtotal</td>
                            <td style="width:18%;" class="text-right"><?php echo $currencySymbol[0].$amount; ?></td>
                        </tr>
                        <?php foreach($taxArrData as $taxCurData){ ?>
                            <tr>
                                <td></td>
                                <td class="text-right"><?php echo $taxCurData[0]; ?></td>
                                <td class="text-right"><?php echo $currencySymbol[0].$taxCurData[1]; ?></td>
                            </tr>
                        <?php } ?>
                        
                        <tr class="invoiceTotal">
                            <td></td>
                            <td class="even text-right">Total</td>
                            <td class="even text-right"><?php echo $currencySymbol[0].$totalAmount.' '.$orderInfo['currency_type']; ?></td>
                        </tr>
                        
                        </tbody>
                    </table>
                </div>
                
            	<div class="total-due">
            		<div class="total-heading"><p>Amount <?php echo $invoiceStats; ?></p></div>
            		<div class="total-amount"><p><?php echo $currencySymbol[0].$totalAmount; ?></p></div>
            	</div>
            	
            	<div class="invoiceClear"></div>
                <?php echo html_entity_decode(strEOL($invoice_footer)); ?>                 	
            </div>
                
            <br />

            </div>
            <?php 
            // Sidebar 
            require_once(THEME_DIR. "sidebar.php"); 
            ?>
        </div>
    </div>
    <br />