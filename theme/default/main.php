<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));
/*
* @author Balaji
* @name: A to Z SEO Tools - PHP Script
* @Theme: Default Style
* @copyright � 2015 ProThemes.Biz
*
*/
?>
<div class="horizontal-add">
		<img src="<?php echo $theme_path; ?>/img/add2.jpg" class="img-responsive center-block" />
	</div>
<div class="container  tool-section min_container">
	<div class="row">
		<div class="col-xs-12" id="seoTools">
			<div style="text-align: center;"  class="head-title" data-aos="fade-up">
				<h3 class="newH3">The tools you get with <strong> Website Appraisal</strong></h3>
				
				<p>
					Lorem ipsum dolor sit amet, consectetur olor adipiscing elit
				</p>
			</div>
			<div class="tab-section" data-aos="fade-up">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><i class="icon-seo"></i>Seo Analyzer</a></li>
					<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><i class="icon-seo-ana"></i>Seo Checker</a></li>
					<li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"><i class="icon-seo-opti">&nbsp;</i>
					Seo Optimization</a></li>
					<li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab"><i class="icon-seo-analysis">&nbsp;</i>
					Website Analysis</a></li>
					<li role="presentation"><a href="#value" aria-controls="value" role="tab" data-toggle="tab"><i class="icon-web-value">&nbsp;</i>
					Website Value</a></li>
					<li role="presentation"><a href="#security" aria-controls="security" role="tab" data-toggle="tab"><i class="icon-web-secure">&nbsp;</i>
					Web Security</a></li>
					<li role="presentation"><a href="#advance-seo" aria-controls="advance-seo" role="tab" data-toggle="tab"><i class="icon-ad-seo">&nbsp;</i>
					Advanced Seo</a></li>
				</ul>
				<!-- Tab panes -->
				<?php
					//print_r($tools);
				?>
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane fade in active" id="home" >
						<div class="row">
							<div class="col-xs-12 ">
								<div class="inner-tab-content" data-aos="zoom-in" >
									<div class="seotools">
										<?php
											foreach($tools as $tool){
												if($tool['5']==1){ //category 1
													echo "<div class='tool-listing'>
															<a class='seotoollink' data-placement='top' data-toggle='tooltip' data-original-title='".$tool[0]."' title='".$tool[0]."' href='".$baseURL.$tool[1]."'>
															<img alt='".$tool[0]."' src='".$theme_path.$tool[2]."' class='seotoolimg' />
															<div class='caption'>".$tool[0]."</div>
														</a>
													</div>";
												}
											}
										?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="profile" >
						<div class="row">
							<div class="col-xs-12 ">
								<div class="inner-tab-content">
								<div class="seotools">
										<?php
											foreach($tools as $tool){
												if($tool['5']==2){ //category 2
													echo "<div class='tool-listing'>
															<a class='seotoollink' data-placement='top' data-toggle='tooltip' data-original-title='".$tool[0]."' title='".$tool[0]."' href='".$baseURL.$tool[1]."'>
															<img alt='".$tool[0]."' src='".$theme_path.$tool[2]."' class='seotoolimg' />
															<div class='caption'>".$tool[0]."</div>
														</a>
													</div>";
												}
											}
										?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="messages" >
						<div class="row">
							<div class="col-xs-12 ">
								<div class="inner-tab-content">
								<div class="seotools">
									<?php
										foreach($tools as $tool){
											if($tool['5']==3){ //category 3
												echo "<div class='tool-listing'>
														<a class='seotoollink' data-placement='top' data-toggle='tooltip' data-original-title='".$tool[0]."' title='".$tool[0]."' href='".$baseURL.$tool[1]."'>
														<img alt='".$tool[0]."' src='".$theme_path.$tool[2]."' class='seotoolimg' />
														<div class='caption'>".$tool[0]."</div>
													</a>
												</div>";
											}
										}
									?>
								</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="settings">
						<div class="row">
							<div class="col-xs-12 ">
								<div class="inner-tab-content">
								<div class="seotools">
									<?php
										foreach($tools as $tool){
											if($tool['5']==4){ //category 4
												echo "<div class='tool-listing'>
														<a class='seotoollink' data-placement='top' data-toggle='tooltip' data-original-title='".$tool[0]."' title='".$tool[0]."' href='".$baseURL.$tool[1]."'>
														<img alt='".$tool[0]."' src='".$theme_path.$tool[2]."' class='seotoolimg' />
														<div class='caption'>".$tool[0]."</div>
													</a>
												</div>";
											}
										}
									?>
								</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="value" >
						<div class="row">
							<div class="col-xs-12 ">
								<div class="inner-tab-content">
								<div class="seotools">
									<?php
										foreach($tools as $tool){
											if($tool['5']==5){ //category 5
												echo "<div class='tool-listing'>
														<a class='seotoollink' data-placement='top' data-toggle='tooltip' data-original-title='".$tool[0]."' title='".$tool[0]."' href='".$baseURL.$tool[1]."'>
														<img alt='".$tool[0]."' src='".$theme_path.$tool[2]."' class='seotoolimg' />
														<div class='caption'>".$tool[0]."</div>
													</a>
												</div>";
											}
										}
									?>
								</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="security" >
						<div class="row">
							<div class="col-xs-12 ">
								<div class="inner-tab-content">
								<div class="seotools">
									<?php
										foreach($tools as $tool){
											if($tool['5']==6){ //category 6
												echo "<div class='tool-listing'>
														<a class='seotoollink' data-placement='top' data-toggle='tooltip' data-original-title='".$tool[0]."' title='".$tool[0]."' href='".$baseURL.$tool[1]."'>
														<img alt='".$tool[0]."' src='".$theme_path.$tool[2]."' class='seotoolimg' />
														<div class='caption'>".$tool[0]."</div>
													</a>
												</div>";
											}
										}
									?>
								</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="advance-seo" >
						<div class="row">
							<div class="col-xs-12 ">
								<div class="inner-tab-content">
								<div class="seotools">
								<?php
									foreach($tools as $tool){
										if($tool['5']==7){ //category 7
											echo "<div class='tool-listing'>
													<a class='seotoollink' data-placement='top' data-toggle='tooltip' data-original-title='".$tool[0]."' title='".$tool[0]."' href='".$baseURL.$tool[1]."'>
													<img alt='".$tool[0]."' src='".$theme_path.$tool[2]."' class='seotoolimg' />
													<div class='caption'>".$tool[0]."</div>
												</a>
											</div>";
										}
									}
								?>
								</div>
								</div>
							</div>
						</div>
					</div>
				</div>
					
			</div>
				
		</div>
	</div>
</div>
	<div class="vertical-add">
		<img src="<?php echo $theme_path; ?>/img/add1.png" class="img-responsive center-block" />
	</div>
	
	<section class="client-sec">
		<div class="container min_container">
			<div class="row">
				<div class="col-xs-12" >
					<div style="text-align: center;"  class="head-title"  data-aos="fade-up">
						<h3 class="newH3">Words From Our <strong> Clients</strong></h3>
						
						<p>
							Lorem ipsum dolor sit amet, consectetur 
						</p>
					</div>
					<div class="owl-carousel-wrap" data-aos="fade-up">
						<div class="owl-carousel owl-theme">
							<div class="item">
								<figure>
									<img src="<?php echo $theme_path; ?>img/new-images/team-01.jpg" alt="team-01">
								</figure>
								<p>
									Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making his the first true generator on the Internet. It uses a dictionary of combined with
								</p>
								<strong>
								John Dore
								</strong>
								<a href="#" class="cl-logo">
									<img src="<?php echo $theme_path; ?>img/new-images/client-logo-01.jpg" alt="client-logo-01">
								</a>
							</div>
							<div class="item">
								<figure>
									<img src="<?php echo $theme_path; ?>img/new-images/team-02.jpg" alt="team-02">
								</figure>
								<p>
									Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making his the first true generator on the Internet. It uses a dictionary of combined with
								</p>
								<strong>
								John Dore
								</strong>
								<a href="#" class="cl-logo">
									<img src="<?php echo $theme_path; ?>img/new-images/client-logo-02.jpg" alt="client-logo-02">
								</a>
							</div>
							<div class="item">
								<figure>
									<img src="<?php echo $theme_path; ?>img/new-images/team-03.jpg" alt="team-03">
								</figure>
								<p>
									Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making his the first true generator on the Internet. It uses a dictionary of combined with
								</p>
								<strong>
								John Dore
								</strong>
								<a href="#" class="cl-logo">
									<img src="<?php echo $theme_path; ?>img/new-images/client-logo-03.jpg" alt="client-logo-03">
								</a>
							</div>
							<div class="item">
								<figure>
									<img src="<?php echo $theme_path; ?>img/new-images/team-01.jpg" alt="team-01">
								</figure>
								<p>
									Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making his the first true generator on the Internet. It uses a dictionary of combined with
								</p>
								<strong>
								John Dore
								</strong>
								<a href="#" class="cl-logo">
									<img src="<?php echo $theme_path; ?>img/new-images/client-logo-01.jpg" alt="client-logo-01">
								</a>
							</div>
							<div class="item">
								<figure>
									<img src="<?php echo $theme_path; ?>img/new-images/team-02.jpg" alt="team-02">
								</figure>
								<p>
									Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making his the first true generator on the Internet. It uses a dictionary of combined with
								</p>
								<strong>
								John Dore
								</strong>
								<a href="#" class="cl-logo">
									<img src="<?php echo $theme_path; ?>img/new-images/client-logo-02.jpg" alt="client-logo-02">
								</a>
							</div>
							<div class="item">
								<figure>
									<img src="<?php echo $theme_path; ?>img/new-images/team-03.jpg" alt="team-03">
								</figure>
								<p>
									Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making his the first true generator on the Internet. It uses a dictionary of combined with
								</p>
								<strong>
								John Dore
								</strong>
								<a href="#" class="cl-logo">
									<img src="<?php echo $theme_path; ?>img/new-images/client-logo-03.jpg" alt="client-logo-03">
								</a>
							</div>
							<div class="item">
								<figure>
									<img src="<?php echo $theme_path; ?>img/new-images/team-01.jpg" alt="team-01">
								</figure>
								<p>
									Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making his the first true generator on the Internet. It uses a dictionary of combined with
								</p>
								<strong>
								John Dore
								</strong>
								<a href="#" class="cl-logo">
									<img src="<?php echo $theme_path; ?>img/new-images/client-logo-01.jpg" alt="client-logo-01">
								</a>
							</div>
							<div class="item">
								<figure>
									<img src="<?php echo $theme_path; ?>img/new-images/team-02.jpg" alt="team-02">
								</figure>
								<p>
									Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making his the first true generator on the Internet. It uses a dictionary of combined with
								</p>
								<strong>
								John Dore
								</strong>
								<a href="#" class="cl-logo">
									<img src="<?php echo $theme_path; ?>img/new-images/client-logo-02.jpg" alt="client-logo-02">
								</a>
							</div>
							<div class="item">
								<figure>
									<img src="<?php echo $theme_path; ?>img/new-images/team-03.jpg" alt="team-03">
								</figure>
								<p>
									Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making his the first true generator on the Internet. It uses a dictionary of combined with
								</p>
								<strong>
								John Dore
								</strong>
								<a href="#" class="cl-logo">
									<img src="<?php echo $theme_path; ?>img/new-images/client-logo-03.jpg" alt="client-logo-03">
								</a>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="reviews-sec">
		<div class="container min_container">
			<div class="row">
				<div class="col-sm-4  " data-aos="fade-down">
					<div class="inner-client-blk mb" >
						<div class="circle-box hovicon effect-1">
							<i class="icon-circle-1">&nbsp;</i>
						</div>
						<h4>What is SEO?</h4>
						<p>
							Short for search engine optimization. SEO is optimising a website to be more easily found by Search Engines.
						</p>
					</div>
				</div>
				<div class="col-sm-4 " data-aos="fade-up">
					<div class="inner-client-blk mb">
						<div class="circle-box hovicon effect-1">
							<i class="icon-circle-2">&nbsp;</i>
						</div>
						<h4>Detailed Seo Reviews</h4>
						<p>
							With our detailed website seo analyzer find and fix your SEO problems and Local SEO with clear instructions for each SEO
						</p>
					</div>
				</div>
				<div class="col-sm-4  " data-aos="fade-down">
					<div class="inner-client-blk mb">
						<div class="circle-box hovicon effect-1">
							<i class="icon-circle-3">&nbsp;</i>
						</div>
						<h4>Competitor Seo analysis</h4>
						<p>
							View SEO comparisons with your competitors. See how your SEO can improve against your competition.
						</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4" data-aos="fade-up">
					<div class="inner-client-blk">
						<div class="circle-box hovicon effect-1">
							<i class="icon-circle-4">&nbsp;</i>
						</div>
						<h4>Seo Analysis</h4>
						<p>
							Unlimited seo analysis and free on our dedicated servers.Downloadable reports make it easy to view and analyse your work progress.Downloadable seo report cards in .PDF extention.
						</p>
					</div>
				</div>
				<div class="col-sm-4" data-aos="fade-down">
					<div class="inner-client-blk">
						<div class="circle-box hovicon effect-1">
							<i class="icon-circle-5">&nbsp;</i>
						</div>
						<h4>Seo Tips and Tricks</h4>
						<p>
							Simple, easy and quick seo tips to get your website up and running in most of the search engines, including Google.com.
						</p>
					</div>
				</div>
				<div class="col-sm-4" data-aos="fade-up">
					<div class="inner-client-blk">
						<div class="circle-box hovicon effect-1">
							<i class="icon-circle-6">&nbsp;</i>
						</div>
						<h4>Check Your Seo Ranking</h4>
						<p>
							Check your website seo ranking and use our analyzer to get insights on your website weakest seo points and links.
						</p>
					</div>
				</div>
			</div>
			
		</div>

	</section>
