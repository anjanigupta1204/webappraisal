<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));
/*
 * @author Balaji
 * @name: A to Z SEO Tools - PHP Script
 * @Theme: Default Style
 * @copyright � 2016 ProThemes.Biz
 *
 */
?>
<link href="<?php echo $theme_path; ?>premium/css/premium.css" rel="stylesheet" type="text/css" />

<div class="container main-container">
    <div class="row">
        <div class="col-md-8 main-index">
                                                   
        <div class="premiumBoxDash">
        
        <h2 class="premiumTitle" style="text-align: left;">Hi <?php echo $premiumUserInfo['firstname']; ?>,</h2>
            <br />
            <h2 class="premiumTitle">Start Website Analysis</h2>
            
            <h3>From SEO to Digital Marketing, using our site to analyze and optimize unlimited websites.</h3>
           
            <form method="POST" action="/domain" onsubmit="return fixURL();">
            <div class="input-group dashBox">
                <div class="input-container">
                    <input type="url" tabindex="1" placeholder="Website URL to review" name="url" class="form-control dashIn"/>
                </div>
                
                <div class="input-group-btn">
                    <button tabindex="2" type="submit" name="generate" class="btn btn-info btn-lg">
                        <span class="ready">Analyze</span>
                    </button>
                </div>
            </div>
            </form>

            <hr class="small" />
        </div>
        
        <div id="seoTools">
            <div style="text-align: center;">
            <h3 class="newH3">Already analyzed your website? Want more? <br />
            Enjoy premium SEO tools.</h3><br />
            </div>
            
            <?php
            $count = 1;
            $smCount = 0;
            $oneTime  = 0;
            $tools_count =count($tools);
            $loop = 0; 
            foreach ($tools as $tool)
            {  
                $loop++;
                if ($count==1)
                {
                $smCount++;
                if($smCount == 4){
                    if($oneTime == 0){
                        $oneTime =1;
                        echo '<div class="text-center moreToolsBut"><button class="btn btn-info" id="browseTools">Browse More Tools</button></div>';
                    }
                    echo '<div class="row hideAll">';
                    $smCount--;
                }else{
                    echo '<div class="row">';
                }    

                } 
                if(!file_exists(THEME_DIR.$tool[2]))
                $tool[2] = "icons/no_image.png";   
                echo '   <div class="col-md-3">
                            <div class="thumbnail">
                                <a class="seotoollink" data-placement="top" data-toggle="tooltip" data-original-title="'.$tool[0].'" title="'.$tool[0].'" href="'.$baseURL.$tool[1].'"><img alt="'.$tool[0].'" src="'.$theme_path.$tool[2].'" class="seotoolimg" />
                                <div class="caption">
                                        '.$tool[0].'
                                </div></a>
                            </div>
                        </div>';
                        if ($loop == 20)
                        { ?>
                            <div class="xd_top_box">
                                <?php echo $ads_468x70; ?>
                            </div>
                       <?php }
                if ($tools_count==$loop)
                { 
               // if ($count==4)
                echo '</div><!-- /.row -->';
                $count = 0;
                }
                    if ($count==4)
                    {
                    $count = 0;
                    echo '</div><!-- /.row -->';
                    } 
                    $count++;   
                   
            } 
            ?>
      		</div>
        <br />

        </div>
        <?php 
        // Sidebar 
        require_once(THEME_DIR. "sidebar.php"); 
        ?>
    </div>
</div>
<br />