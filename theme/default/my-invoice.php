<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));
/*
 * @author Balaji
 * @name: A to Z SEO Tools - PHP Script
 * @Theme: Default Style
 * @copyright � 2016 ProThemes.Biz
 *
 */
?>
<link href="<?php echo $theme_path; ?>premium/css/premium.css" rel="stylesheet" type="text/css" />

<!-- DATA TABLES -->
<link href="<?php echo $theme_path; ?>css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    
<div class="container main-container">
    <div class="row">
        <div class="col-md-8 main-index">
                                            
     
        <h2 class="premiumTitle">My Invoices</h2>
           <br />
          <table id="invoiceTable" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Invoice #</th>
                <th>Invoice Date</th>
                <th>Due Date</th>
                <th>Detail</th>
                <th>Total</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach($invoices as $invoice){                               
                echo '<tr style="cursor: pointer;" onclick="openInvoice('.$invoice[6].');">
                <td>'.$invoice[0].'</td>
                <td>'.$invoice[1].'</td>
                <td>'.$invoice[2].'</td>
                <td>'.$invoice[3].'</td>
                <td>'.$invoice[4].'</td>
                <td>'.$invoice[5].'</td>
              </tr>';
            }
            ?>
            </tbody>
          </table>
            
        <br />

        </div>
        <?php 
        // Sidebar 
        require_once(THEME_DIR. "sidebar.php"); 
        ?>
    </div>
</div>
<br />
<!-- DATA TABES SCRIPT -->
<script src="<?php echo $theme_path; ?>js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $theme_path; ?>js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('#invoiceTable').dataTable({
        "aaSorting": [[ 0, "desc" ]]
    });
});

function openInvoice(id){
    //window.location.replace('/invoice/'+id);
    window.open('/invoice/'+id+'/view', '_blank');
}
</script>