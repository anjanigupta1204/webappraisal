<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));
/*
 * @author Balaji
 * @name: A to Z SEO Tools - PHP Script
 * @Theme: Default Style
 * @copyright � 2016 ProThemes.Biz
 *
 */
?>

<link href="<?php echo $theme_path; ?>premium/css/premium.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $theme_path; ?>premium/css/bootstrap-formhelpers.min.css" rel="stylesheet" type="text/css" />

<div class="container main-container">
    <div class="row">
        <div class="col-md-8 main-index">
        
        <h2 class="premiumTitle">My Profile</h2>     
                                       
        <br />
        <?php if(isset($msg)) {
            echo $msg.'<br>';
        }
        ?>
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#profile">Profile</a></li>
          <li><a data-toggle="tab" href="#info">Update Information</a></li>
          <li><a data-toggle="tab" href="#password">Change Password</a></li>
        </ul>
        
        <div class="tab-content">
          <br /><br />
          <div id="profile" class="tab-pane fade in active">
                <div class="row" style="margin: 5px;">
                
                <div class="col-md-4 thumbnail" style="padding: 20px;">
                    <div class="text-center">
                    <img width="180" height="180" alt="User Logo" src="<?php echo $userLogo; ?>" />                         
                    </div>
                </div>
                
                <div class="col-md-8">
                    <table class="table table-hover table-bordered">
                    <tbody>
                        <tr>
                            <td style="width: 200px;">Full Name</td>
                            <td><span><?php echo $userInfo['full_name']; ?></span></td>
                        </tr> 
                        <tr>
                            <td style="width: 200px;">Username</td>
                            <td><span><?php echo $username; ?></span></td>         
                        </tr> 
                        <tr>
                            <td style="width: 200px;">Email ID</td>
                            <td><span><?php echo $userInfo['email_id']; ?></span></td>         
                        </tr> 
                        <tr>
                            <td style="width: 200px;">Registered At</td>
                            <td><span><?php echo $userInfo['date']; ?></span></td>         
                        </tr> 
                        <tr>
                            <td style="width: 200px;">User Country</td>
                            <td><span><?php echo $userCountry; ?></span></td>         
                        </tr> 
                        <tr>
                            <td style="width: 200px;">Membership</td>
                            <td><span><?php echo $membership; ?></span></td>         
                        </tr> 
                    </tbody>
                    </table>
                </div>
                
                <?php if($addInfo) { ?>
                <div class="clear" style="margin-bottom: 10px;"></div>

                <div class="row" style="margin: 5px;">
                    <h4 style="font-weight: 500;">Personal Information:</h4>
                    <table class="table table-hover table-bordered">
                    <tbody>
                        <tr>
                            <td style="width: 30%;">First Name</td>
                            <td><span><?php echo $firstname; ?></span></td>
                        </tr> 
                        <tr>
                            <td style="width: 200px;">Last Name</td>
                            <td><span><?php echo $lastname; ?></span></td>         
                        </tr> 
                        <?php if($company != '') { ?>
                        <tr>
                            <td style="width: 200px;">Company</td>
                            <td><span><?php echo $company; ?></span></td>         
                        </tr> 
                        <?php } ?>
                        <tr>
                            <td style="width: 200px;">Address Line 1</td>
                            <td><span><?php echo $address1; ?></span></td>         
                        </tr> 
                        <?php if($address2 != '') { ?>
                        <tr>
                            <td style="width: 200px;">Address Line 2</td>
                            <td><span><?php echo $address2; ?></span></td>         
                        </tr> 
                        <?php } ?>
                        <tr>
                            <td style="width: 200px;">City</td>
                            <td><span><?php echo $city; ?></span></td>         
                        </tr>
                        <tr>
                            <td style="width: 200px;">State</td>
                            <td><span><?php echo $statestr; ?></span></td>         
                        </tr> 
                        <tr>
                            <td style="width: 200px;">Country</td>
                            <td><span><?php echo $country; ?></span></td>         
                        </tr>  
                        <tr>
                            <td style="width: 200px;">Post Code</td>
                            <td><span><?php echo $postcode; ?></span></td>         
                        </tr> 
                        <tr>
                            <td style="width: 200px;">Telephone</td>
                            <td><span><?php echo $telephone; ?></span></td>         
                        </tr> 
                    </tbody>
                    </table>
                </div>
                <?php } ?>
                
                </div>
          </div>
          <div id="info" class="tab-pane fade">
          
              <div class="row">
              
              <form onsubmit="return addressCheck()" name="userBox" method="POST" action="#" enctype="multipart/form-data"> 
              
              <div class="col-md-12">  
                
                <h4 style="margin-bottom: 15px; font-weight: 500;">General Information:</h4>
                
                <div class="form-group">
					Full Name <br />
					<input value="<?php echo $userInfo['full_name']; ?>" placeholder="Enter your full name" type="text" name="fullname" class="form-control"  style="width: 96%;"/>
				</div>	
                
                 <div class="form-group">
					Username <br />
					<input disabled="" value="<?php echo $username; ?>" placeholder="Enter your user name" type="text" name="username" class="form-control"  style="width: 96%;"/>
				</div>
                
                <div class="form-group">
			         Email ID <br />
					 <input disabled="" value="<?php echo $userInfo['email_id']; ?>" placeholder="Enter your email id" type="text" name="email" class="form-control"  style="width: 96%;"/>
				</div>
    
                <br />
                <h4 style="margin-bottom: 15px; font-weight: 500;">Avatar:</h4>
                

                <img class="userLogoBox" id="userLogoBox" width="180" height="180" alt="User Logo" src="<?php echo $userLogo; ?>" />   
                
                <br />
                Upload a new avatar: (JPEG 180x180px)
                <input type="file" name="logoUpload" id="logoUpload" class="btn btn-default" />
                
                <br /><br />
                <h4 style="margin-bottom: 15px; font-weight: 500;">Personal Information:</h4>
              </div>
              
               <div class="col-md-6">
                        
                     <div class="form-group">
    					First Name <br />
    					<input value="<?php echo $firstname; ?>" placeholder="Enter your first name" type="text" name="firstname" class="form-control"  style="width: 96%;"/>
    				</div>	
                    
                     <div class="form-group">
    					Last Name <br />
    					<input value="<?php echo $lastname; ?>" placeholder="Enter your last name" type="text" name="lastname" class="form-control"  style="width: 96%;"/>
    				</div>
                    
                    <div class="form-group">
    			         Company <br />
    					 <input value="<?php echo $company; ?>" placeholder="Enter your company name (optional)" type="text" name="company" class="form-control"  style="width: 96%;"/>
    				</div>  
                    
                    <div class="form-group">
    			          Telephone <br />
    					 <input value="<?php echo $telephone; ?>" placeholder="Enter your phone no." type="text" name="telephone" class="form-control bfh-phone" data-country="country" style="width: 96%;" />
    				</div>
                    
                    <div class="form-group">
    			         Country <br />
    					 <select id="country" name="country" class="form-control bfh-countries" data-country="<?php echo $countryCode != '' ? $countryCode : 'IN'; ?>" style="width: 96%;"></select>
    				</div>
                    
            </div><!-- /.col-md-6 -->
            
            <div class="col-md-6">
               
                <div class="form-group">
    		          Address 1 <br />
    				 <input value="<?php echo $address1; ?>" placeholder="Enter your home address" type="text" name="address1" class="form-control"  style="width: 96%;"/>
    			</div>    
                
                <div class="form-group">
    		         Address 2 <br />
    				 <input value="<?php echo $address2; ?>" placeholder="Address line 2 (optional)" type="text" name="address2" class="form-control"  style="width: 96%;"/>
    			</div> 
                
                <div class="form-group">
    		        City <br />
    	 	        <input value="<?php echo $city; ?>" placeholder="Enter your city" type="text" name="city" class="form-control"  style="width: 96%;"/>
    			</div>
                
                <div class="form-group">
    		         Post Code <br />
    				 <input value="<?php echo $postcode; ?>" placeholder="Enter your postal code" type="text" name="postcode" class="form-control"  style="width: 96%;"/>
    			</div>  
                
                <div class="form-group">
    		          Region / State <br />
                      <?php $state = ($state != '' ? 'data-state="'.$state.'"' : ''); ?>
    				 <select name="state" class="form-control bfh-states" data-country="country" <?php echo $state; ?> style="width: 96%;"></select>
    			</div>
                    
            </div><!-- /.col-md-6 -->
            
            <div class="col-md-12 text-center">  
                <br />
                <input type="submit" value="Submit" class="btn btn-success" />
                <input type="hidden" name="statestr" value="1" />
                <input type="hidden" name="user" value="1" />
                <br />
            </div>
            
            </form>
          </div>
      
          </div>
          <div id="password" class="tab-pane fade">
            
            <div class="row">
            <div class="col-md-12">    
             <form name="passwordBox" method="POST" action="#" onsubmit="return passCheck()"> 				
             <div class="form-group">
				Current Password <br />
				<input placeholder="Enter your current password" type="password" name="old_pass" class="form-control"  style="width: 96%;"/>
			</div>	
            
             <div class="form-group">
				New Password <br />
				<input placeholder="Enter your new password" type="password" name="new_pass" class="form-control"  style="width: 96%;"/>
			</div>
            
            <div class="form-group">
		         Retype Password <br />
				 <input placeholder="Retype your new password" type="password" name="retype_pass" class="form-control"  style="width: 96%;"/>
			</div>
            <br />
            <input type="submit" value="Change Password" class="btn btn-danger" />
            <input type="hidden" name="password" value="1" />
            </form>     
             </div>
            </div>
            
          </div>
        </div>
            
        <br />

        </div>
        <?php 
        // Sidebar 
        require_once(THEME_DIR. "sidebar.php"); 
        ?>
    </div>
</div>
<br />

<script src="<?php echo $theme_path; ?>premium/js/bootstrap-formhelpers.min.js" type="text/javascript"></script>
<script src="<?php echo $theme_path; ?>premium/js/profile.js" type="text/javascript"></script>