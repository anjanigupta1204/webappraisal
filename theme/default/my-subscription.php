<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));
/*
 * @author Balaji
 * @name: A to Z SEO Tools - PHP Script
 * @Theme: Default Style
 * @copyright � 2016 ProThemes.Biz
 *
 */
?>

<link href="<?php echo $theme_path; ?>premium/css/premium.css" rel="stylesheet" type="text/css" />

<div class="container main-container">
    <div class="row">
        <div class="col-md-8 main-index">
        
        <?php if($pointOut == 'cancel'){ if(isset($_POST['cancel'])){ ?>
        
        <h2 class="text-center premiumTitle">You've Canceled Your Subscription</h2> 
                                           
        <br />
        
        <p>
        <div class="canMsg">
        <b>Subscription Canceled!</b><br />
        All the products and related services will be inactive now!
        </div>
        </p>
       
        <meta http-equiv="refresh" content="5;url=/" />
        <br /><br />
        <?php } else { ?>
        <h2 class="premiumTitle">Cancel Subscription</h2> 
                                           
        <br />
        
        <p>Would you like to cancel this subscription? All the products and related services on this contract will be inactive. This operation cannot be undone.</p>
        
        <p> <input name="canSub" type="checkbox"/>
        <label> &nbsp; Yes, cancel this subscription.</label>
        </p>
        <br />
        <form method="POST" action="#" onsubmit="return doCanSub()">
            <label>Reason for Cancelling</label>
            <input class="form-control" type="text" name="can_msg" placeholder="(optional)" />
            <br />
            <input type="hidden" value="1" name="cancel" />
            <input class="btn btn-danger"  type="submit" value="Confirm Cancellation" />
            <a class="btn btn-success" href="<?php echo '/'.$controller; ?>">Go Back</a>
        </form>
        <br /><br />
        <script>
        function doCanSub(){
        if ($('input[name=canSub]:checked').length > 0){
            return true;
        }else{
            alert('Confirm cancellation checkbox!');
            return false;
        }
        }
        </script>
        <?php } } else { ?>
        
        <h2 class="premiumTitle">Subscription Details</h2> 
                                           
        <br />
        
        <table class="table table-hover table-bordered premiumTable">
        <tbody>
            <tr>
                <td style="width: 200px;">Subscribed Plan</td>
                <td><span><?php echo $planName; ?></span></td>
            </tr> 
            <tr>
                <td style="width: 200px;">Billing Type</td>
                <td><span><?php echo $billType; ?></span></td>         
            </tr> 
            <tr>
                <td style="width: 200px;">Expires On</td>
                <td><span><?php echo $expDate; ?></span></td>         
            </tr> 
            <tr>
                <td style="width: 200px;">Renew My Subscription</td>
                <td><span><?php echo $renewBtn; ?></span></td>         
            </tr> 
            <tr>
                <td style="width: 200px;">Cancel My Subscription</td>
                <td><span><a title="Cancelling my subscription" href="<?php echo $cancelBtn; ?>" class="text-center btn btn-danger">Cancel</a></span></td>         
            </tr> 
            <tr>
                <td style="width: 200px;">Downloadable PDF Reports</td>
                <td><span><?php echo $allowPdf; ?></span></td>         
            </tr> 
            <?php if($totalLimit != '0'){ ?>
            <tr>
                <td style="width: 200px;">Remaining PDF Reports(Today)</td>
                <td><span><?php echo $remCountMsg; ?></span></td>         
            </tr> 
            <?php } ?>
            <tr>
                <td style="width: 200px;">Premium Tools Access</td>
                <td><ul><?php foreach($tools as $tool)
                    echo '<li>'.$tool[0].'</li>';
                ?></ul></td>         
            </tr> 
        </tbody>
        </table>
        <?php } ?>          
        <br />

        </div>
        <?php 
        // Sidebar 
        require_once(THEME_DIR. "sidebar.php"); 
        ?>
    </div>
</div>
<br />