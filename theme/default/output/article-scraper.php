<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name A to Z SEO Tools - PHP Script
 * @copyright � 2015 ProThemes.Biz
 *
 */
?>
<style>
.percentbox {
    text-align: center;
    font-size: 18px;
}
.percentimg {
    text-align: center;
    display: none;
}
#resultBox{
    display:none;
}
</style>

<script src='<?php echo $baseURL; ?>core/library/article-scraper.js'></script>

  <div class="container main-container">
	<div class="row">
      	
          	<div class="col-md-8 main-index">
            
            <div class="xd_top_box">
             <?php echo $ads_720x90; ?>
            </div>
            
              	<h2 id="title"><?php echo $data['tool_name']; ?></h2>

               <?php if ($pointOut != 'output') { ?>
               <br />
               <div id="mainbox">
               <p>Enter a keyword
               </p>

               <input type="text" name="keyword" id="keyword" placeholder="Type your keyword here..." class="form-control"/>
               
               <br />
               
               <div class="sites text-center">
                   <input type="checkbox" name="examiner" value="1" /> Examiner.Com
                   <input type="checkbox" name="articlesbase" value="1" />  Articlesbase.Com
                   <input type="checkbox" name="ezinearticles" value="1" />  Ezinearticles.Com
               </div>
               
               <br />
               <div class="text-center">
               <label>Article Count:</label>
               <select style="width: 60px; display: inline-block !important;" class="form-control" id="limit" name="limit">
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                </select>
                </div>  
               <br />
               <?php
               if ($toolCap)
               {
               echo $captchaCode;  
               }
               ?>
               <div class="text-center">
               <a class="btn btn-info" id="checkButton">Scrape</a>
               </div>
               </div>
                
                <div id="resultBox">
                    <div class="percentimg">
                        <img src="<?php echo $theme_path; ?>img/load.gif" />
                        <br />
                        <?php echo $lang['146']; ?>...
                        <br /><br />
                    </div>
        
                    <div id="results"></div>
        
                    <div class="text-center">
                        <br /> &nbsp; <br />
                        <a class="btn btn-info" href="<?php echo $toolURL; ?>">Try New Keyword</a>
                        <br />
                    </div>
                </div>
                            
               <?php 
               }
               ?>
<br />

<div class="xd_top_box">
<?php echo $ads_720x90; ?>
</div>

<h2 id="sec1" class="about_tool"><?php echo $lang['11'].' '.$data['tool_name']; ?></h2>
<p>
<?php echo $data['about_tool']; ?>
</p> <br />
</div>              
            
<?php
// Sidebar
require_once(THEME_DIR."sidebar.php");
?>     		
        </div>
    </div> <br />