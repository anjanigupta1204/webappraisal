<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name A to Z SEO Tools - PHP Script
 * @copyright © 2016 ProThemes.Biz
 *
 */
?>
<link href="<?php echo $theme_path; ?>css/spin.css" rel="stylesheet" type="text/css" />
<script src='../core/library/spin.js'></script>
   
    <div class="tooltipBox">
	<span id="closeTip">X</span>
	<div class="tooltipTop"></div>
	<div class="tooltipBody">
    	<strong>Origanal Word: </strong> 
        <span id="orgWord"></span> 
        <br />
		<strong>Suggestions: </strong> 
        <span id="sugWord"></span>
        <br /><br />
       	<strong>Add your own word:</strong><br />
        <input type="text" id="yourWord" class="input" /> <span id="yourBtn" class="customButton">Use</span>
    	<hr />
        <strong>Click on the original word to restore.</strong><br />
        <strong>Click on any alternate words for replacement.</strong><br />
        <strong>Click close button to close it.</strong>
    </div>                        
</div>
                        
  <div class="container main-container">
	<div class="row">
      	
          	<div class="col-md-8 main-index">
            
            <div class="xd_top_box">
             <?php echo $ads_720x90; ?>
            </div>
            
              	<h2 id="title"><?php echo $data['tool_name']; ?></h2>

               <?php if ($pointOut != 'output') { ?>
               <div id="crumbs" class="text-center">
                 <ul>
                    <li id="crumbs1"><a href="#">Duplicate Article</a></li>
            		<li id="crumbs2"><a href="#">Processing</a></li>
            		<li id="crumbs3"><a href="#">Spin Suggestions</a></li>
            		<li id="crumbs4"><a href="#">Unique Article</a></li>
                 </ul>
               </div>
               <div id="step1">
                   <p class="text-center" style="margin-left: 20px;"><?php echo $lang['6']; ?>
                   </p>
                 
                   <textarea name="data" id="data" rows="3" style="height: 270px;" class="form-control"></textarea>
                   <br />
                   <label> Select Language: </label>
                    <select class="form-control" id="lang" name="lang">
                    <option value="en">English</option>
                    <option value="du">Dutch</option>
                    <option value="fr">French</option>
                    <option value="sp">Spanish</option>
                    <option value="ge">Germany</option>
                    <option value="tr">Turkish</option>
                    <option value="in">Indonesian</option>
                    </select> 
                   <?php
                   if ($toolCap)
                   {
                   echo $captchaCode;   
                   }
                   ?>
                   
                   <br />
                   
                   <div class="text-center"> 
                    <a class="btn btn-primary" style="cursor:pointer;" id="checkButton">Spin</a>
                   </div>
               </div>
               
               <div id="step2" style="display: none;">
                   <p class="text-center" style="margin-left: 20px;">Please Wait...
                   </p>
                 <div class="outBox hideBox processingBox">
                 </div>
                 <div class="loader">
                    <img alt="Processing" src="<?php echo $theme_path; ?>img/articleload.gif"/>
                    <br/><b>Processing...</b>
                 </div>
               </div>
               
               <div id="step3" style="display: none;">
                   <p class="text-center" style="margin-left: 20px;">Manage the spinned words as you want..
                   </p>
                   <div class="outBox outboxA">
                   </div>
                 
                 <div class="text-center"> 
                    <br /> 
                    <a class="btn btn-primary" style="cursor:pointer;" id="finishButton">Finish</a>
                 </div>
               </div>
               
                <div id="step4" style="display: none;">
                   <p class="text-center" style="margin-left: 20px;">Everything Done!
                   </p>
                <textarea name="outData" id="outData" rows="3" style="height: 270px;" class="form-control"></textarea>
    
                <div class="text-center-none">
                    <br /> 
                    <form method="POST" action="/generate"> 
                    <input type="hidden" value="<?php echo $data['tool_name']; ?>" name="toolName"/>
                    <input type="hidden" value="" id="inputTextArea" name="inputTextArea" />
                    <input type="hidden" value="" id="textArea" name="textArea" />
                    <div class="row">
                        <div class="col-md-6">
                                <label> Download as a file: </label>
                                <select class="form-control" name="format">
                                <option value="txt">TXT Format</option>
                                <option value="html">HTML Format</option>
                                </select> 
                        </div>
                        
                        <div class="col-md-4 text-center">
                            <label>&nbsp;</label>
                            <br /> 
                            <input class="btn btn-success" type="submit" value="Download" name="submit"/>
                        </div>
                        
                    </div>  
                    </form>
                    <br />
                    <br />
                    <form method="POST" action="../?route=plagiarism-checker">
                    <input type="hidden" value="1" id="doPal" name="doPal" />
                    <input type="hidden" value="" id="palData" name="palData" />
                    <input type="hidden" value="1" id="plagiarism-checker" name="plagiarism-checker" />
                    <input class="btn btn-danger" type="submit" value="Check for Plagiarism" />
                    <a class="btn btn-primary" href="<?php echo $toolURL; ?>">Try New Document</a>
                    </form>
                </div>
                
                </div>

                          
               <?php } ?>

<br />

<div class="xd_top_box">
<?php echo $ads_720x90; ?>
</div>

<h2 id="sec1" class="about_tool"><?php echo $lang['11'].' '.$data['tool_name']; ?></h2>
<p>
<?php echo $data['about_tool']; ?>
</p> <br />
</div>              
            
<?php
// Sidebar
require_once(THEME_DIR."sidebar.php");
?>     		
        </div>
    </div> <br />