﻿<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name A to Z SEO Tools - PHP Script
 * @copyright © 2016 ProThemes.Biz
 *
 */
?>

<style>
#outputBox {
    display: none;
}
.progress-bar-striped, .progress-striped .progress-bar {
    background-image: linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
    background-size: 40px 40px;
}
.percentimg {
    display: none;
    text-align: center;
}
.fileinput-button {
    overflow: hidden;
    position: relative;
}
.fileinput-button input {
    cursor: pointer;
    direction: ltr;
    font-size: 200px;
    margin: 0;
    opacity: 0;
    position: absolute;
    right: 0;
    top: 0;
}
@media screen {
.fileinput-button input {
    font-size: 100%;
    height: 100%;
}
}</style>

  <link rel="stylesheet" href="<?php echo $theme_path; ?>css/jquery.fileupload.css" />

  <link href="/langtool/css/style.css?v9" rel="stylesheet" type="text/css" />

  <link href="/langtool/js/fancybox/jquery.fancybox-1.3.4.css" rel="stylesheet" type="text/css" />

  <link href="/langtool/css/lib/dropkick/dropkick.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="/langtool/js/jquery-1.7.0.min.js"></script>

<script type="text/javascript" src="/langtool/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
  <script type="text/javascript">
  $(document).ready(function() {
    $("a.fancyboxImage").fancybox({
      'hideOnContentClick': true,
      'titlePosition': 'inside'
    });
  });
</script>


  <script type="text/javascript" src="/langtool/js/zebra_dialog.js"></script>
  <script type="text/javascript">
    function showDownloadOfficeThanks() {
        $.Zebra_Dialog('<b>Thanks for downloading LanguageTool!</b>' +
            '<ul>' +
            '<li>Note that if you have a 32-bit version of LibreOffice/OpenOffice, you will also need a 32-bit version of Java - ' +
            '  LanguageTool will not work otherwise.</li>' +
            '<li>Use <em>Tools &rarr; Extension Manager &rarr; Add&hellip;</em> in LibreOffice/OpenOffice to install this file</li>' +
            '  <li><strong>Restart LibreOffice/OpenOffice</strong> (including quickstarter) after installation of this extension</li>' +
            '  <li>If you are using LibreOffice and you want to check <strong>English</strong> or <strong>Russian</strong> texts:' +
            '  Use <em>Options &rarr; Language Settings &rarr; Writing Aids &rarr; Edit&hellip;</em> to disable LightProof and enable' +
            '  LanguageTool for English and/or Russian.</li>' +
            '  <li><strong>Having problems? Please see <a href="/issues">our checklist</a>.</strong></li>' +
            '  <li><a href="/contact/newsletter.php">Subscribe to our newsletter</a> to get information about new releases</li>' +
            '</ul>',
            {width: 600});
    }
    function showDownloadStandaloneThanks() {
        $.Zebra_Dialog('<b>Thanks for downloading LanguageTool!</b>' +
            '<p>Unzip the file and start languagetool.jar by double clicking it.</p>' +
            '<p><a href="/contact/newsletter.php">Subscribe to our newsletter</a> to get information about new releases.</p>',
            {width: 600});
    }
  </script>



  <script type="text/javascript" src="https://www.languagetool.org/online-check/tiny_mce/tiny_mce.js"></script>
  <script type="text/javascript" src="https://www.languagetool.org/online-check/tiny_mce/plugins/atd-tinymce/editor_plugin.js?v2014013"></script>
    <script type="text/javascript">

   // translation of language variant names:
   var lt_i18n = {
       'de': {
           'DE': 'Deutschland',
           'AT': 'Österreich',
           'CH': 'Schweiz'
       }
   };

   tinyMCE.init({
       mode : "specific_textareas",
       editor_selector : "SpecificEditor",
       plugins                     : "AtD,paste",
       //directionality              : 'auto',   // will display e.g. Persian in right-to-left -- doesn't work in MSIE

       //Keeps Paste Text feature active until user deselects the Paste as Text button
       paste_text_sticky : true,
       //select pasteAsPlainText on startup
       setup : function(ed) {
           ed.onInit.add(function(ed) {
               ed.pasteAsPlainText = true;
               if (document.checkform.lang.value) {
                   // needed so we don't get invalid lang + subLang combinations when
                   // user enters the page via back button:
                   fillSubLanguageSelect(document.checkform.lang.value.replace(/-..$/, ""));
               }
               doit();  // check immediately when entering the page
           });
           ed.onKeyDown.add(function(ed, e) {
               if (e.ctrlKey && e.keyCode == 13) {  // Ctrl+Return
                   doit(true);
                   tinymce.dom.Event.cancel(e);
               } else if (e.keyCode == 27) {   // Escape
                   // doesn't work in firefox, the re-init in turnOffFullScreenView()
                   // might clash with event handling:
                   if ($('form#checkform').hasClass('fullscreen')) {
                       setTimeout(turnOffFullScreenView, 100);  // use timeout to prevent problems on Firefox
                   }
               }
           });
           // remove any 'no errors found' message:
           ed.onKeyUp.add(function(ed, e) {
               if (!e.keyCode || e.keyCode != 17) {  // don't hide if user used Ctrl+Return
                   $('#feedbackMessage').html('');
               }
           });
           ed.onPaste.add(function(ed, e) {
               $('#feedbackMessage').html('');
           });
       },

       /* translations: */
       languagetool_i18n_no_errors :
          {
           // "No errors were found.":
           'ast': 'Nun s\'atoparon errores',
           'br': 'Fazi ebet.',
           'ca': 'No s\'ha trobat cap error',
           'de-DE': 'Keine Fehler gefunden.',
           'de-DE-x-simple-language': 'Keine möglichen Verstöße gegen Leichte Sprache gefunden.',
           'eo': 'Neniuj eraroj trovitaj.',
           'fr': 'Aucune erreur trouvée.',
           'nl': 'Geen aandachtspunten gevonden.',
           'pl': 'Nie znaleziono błędów',
           'ru': 'Ошибки не найдены.',
           'fake': 'last entry so all previous items can end with a comma'
          },
       languagetool_i18n_explain :
          {
           // "Explain..." - shown if there's an URL with a more detailed description:
           'ast': 'Más información…',
           'br': 'Muioc’h a ditouroù…',
           'ca': 'Més informació…',
           'de-DE': 'Mehr Informationen...',
           'de-DE-x-simple-language': 'Mehr Informationen...',
           'eo': 'Pliaj klarigoj…',
           'fr': 'Plus d’informations…',
           'nl': 'Uitleg...',
           'pl': 'Więcej informacji…',
           'ru': 'Подробнее…',
           'fake': 'last entry so all previous items can end with a comma'
          },

       languagetool_i18n_ignore_once :
          {
           // "Ignore this type of error" -- for non-spelling errors:
           'br': 'Na ober van ouzh fazi a seurt-se',
           'ca': 'Ignora aquesta classe d\'errors',
           'de-DE': 'Fehler dieser Art ignorieren',
           'de-DE-x-simple-language': 'Fehler dieser Art ignorieren',
           'eo': 'Ignori tiun tipon de eraro',
           'es': 'Ignorar este tipo de error',
           'fr': 'Ignorer ce type d’erreur',
           'nl': 'Negeer dergelijke fouten',
           'pt': 'Ignorar este tipo de erros',
           'sl': 'Prezri te vrste napako',
           'ru': 'Пропустить этот вид ошибок',
           'fake': 'last entry so all previous items can end with a comma'
          },
       languagetool_i18n_ignore_all :
       {
           // "Ignore error for this word" -- for spelling errors:
           'ca': 'Ignora l\'error d\'aquesta paraula',
           'de-DE': 'Fehlermeldung für dieses Wort ignorieren',
           'de-DE-x-simple-language': 'Fehlermeldung für dieses Wort ignorieren',
           'eo': 'Ignori eraron por tiu vorto',
           'es': 'Ignorar el error para esta palabra',
           'fr': 'Ignorer l’erreur pour ce mot',
           'nl': 'Negeer deze spelfout',
           'pt': 'Ignorar erros para esta palavra',
           'sl': 'Prezri napako za to besedo',
           'ru': 'Игнорировать эту ошибку',
           'fake': 'last entry so all previous items can end with a comma'
       },

       languagetool_i18n_rule_implementation :
          {
           // "Rule implementation":
           'ca': 'Implementació de la regla...',
           'de-DE': 'Technische Details...',
           'eo': 'Realigo de la regulo…',
           'fr': 'Implémentation de la règle…',
           'nl': 'De techniek erachter...',
           'pl': 'Dodatkowe informacje o regule…',
           'ru': 'Описание реализации правила...',
           'fake': 'last entry so all previous items can end with a comma'
          },

       languagetool_i18n_suggest_word :
          {
           // "Suggest word for dictionary...":
           // *** Also set languagetool_i18n_suggest_word_url below if you set this ***
           'ca': 'Suggereix una paraula per al diccionari...',
           'de-DE': 'Wort zur Aufnahme vorschlagen...',
           'eo': 'Sugesti vorton por la vortaro…',
           'fr': 'Suggerer un mot pour le dictionnaire…',
           'nl': 'Suggest word for dictionary...',
           'ru': 'Предложить слово для словаря...',
           'fake': 'last entry so all previous items can end with a comma'
          },
       languagetool_i18n_suggest_word_url :
          {
           // "Suggest word for dictionary...":
           'ca': 'http://community.languagetool.org/suggestion?word={word}&lang=ca',
           'de-DE': 'http://community.languagetool.org/suggestion?word={word}&lang=de',
           'nl': 'http://www.opentaal.org/suggesties?word={word}',
           'ru': 'http://myooo.ru/index.php?option=com_addtodictsuggest&Itemid=135&word={word}',
           'fake': 'last entry so all previous items can end with a comma'
          },

       languagetool_i18n_current_lang :    function() { return document.checkform.lang.value; },
       /* the URL of your proxy file: */
       languagetool_rpc_url                 : "/langtool/pro.php",
       /* edit this file to customize how LanguageTool shows errors: */
       languagetool_css_url                 : "/langtool/content.css",
       /* this stuff is a matter of preference: */
       theme                              : "advanced",
       theme_advanced_buttons1            : "",
       theme_advanced_buttons2            : "",
       theme_advanced_buttons3            : "",
       theme_advanced_toolbar_location    : "none",
       theme_advanced_toolbar_align       : "left",
       theme_advanced_statusbar_location  : "bottom",  // activated so we have a resize button
       theme_advanced_path                : false,     // don't display path in status bar
       theme_advanced_resizing            : true,
       theme_advanced_resizing_use_cookie : false,
       /* disable the gecko spellcheck since AtD provides one */
       gecko_spellcheck                   : false
   });

    function fullscreen_toggle() {
      if ($('form#checkform').hasClass('fullscreen')) {
        turnOffFullScreenView();
      } else {
        turnOnFullScreenView();
        if (_paq) { _paq.push(['trackEvent', 'Action', 'SwitchToFullscreen']); } // Piwik tracking
      }
      return false;
    }

   function turnOffFullScreenView() {
       // re-init the editor - this way we lose the error markers, but it's needed
       // to get proper position of the context menu:
       // source: http://stackoverflow.com/questions/4651676/how-do-i-remove-tinymce-and-then-re-add-it
       tinymce.EditorManager.execCommand('mceRemoveControl',true, 'checktext');
       tinymce.EditorManager.execCommand('mceAddControl', true, 'checktext');
       $('form#checkform').removeClass('fullscreen');
       $('body').removeClass('fullscreen');
       $('iframe#checktext_ifr').height(270);
       tinymce.execCommand('mceFocus', false, 'checktext');
   }

   function turnOnFullScreenView() {
       tinymce.EditorManager.execCommand('mceRemoveControl',true, 'checktext');
       tinymce.EditorManager.execCommand('mceAddControl', true, 'checktext');
       $('body').addClass('fullscreen');
       $('form#checkform').addClass('fullscreen');
       $('iframe#checktext_ifr').height( $(window).height() - $('#editor_controls').outerHeight() - $('#handle').outerHeight() );
       tinymce.execCommand('mceFocus', false, 'checktext');
   }

   function doit(doLog) {
       document.checkform._action_checkText.disabled = true;
       var langCode = document.checkform.lang.value;
       if (document.checkform.subLang && document.checkform.subLang.value) {
           langCode = langCode.replace(/-..$/, "")  // en-US -> en
               + "-" + document.checkform.subLang.value;
       }
       if (doLog) {
           if (_paq) { _paq.push(['trackEvent', 'Action', 'CheckText', langCode]); } // Piwik tracking
       }
       tinyMCE.activeEditor.execCommand('mceWritingImprovementTool', langCode);
   }

   $(function(){
    $(window).resize(function(){
      if ($('form#checkform').hasClass('fullscreen')) {
        $('iframe#checktext_ifr').height( $(window).height() - $('#editor_controls').outerHeight() );
      }
    });
   });

   </script>

    <script type="text/javascript" src="https://www.languagetool.org/css/lib/dropkick/jquery.dropkick.js"></script>
    <script type="text/javascript">
        function fillSubLanguageSelect(langCode) {
            //console.log("fillSubLanguageSelect " + langCode);
            var unmaintainedLanguages = {   // see https://languagetool.org/languages/ 
                "be": "Belarusian",
                "da": "Danish",
                "gl": "Galician",
                "is": "Icelandic",
                "ja": "Japanese",
                "lt": "Lithuanian",
                "ml": "Malayalam",
                //"pt": "Portuguese",
                "ro": "Romanian",
                "sv": "Swedish",
                "zh": "Chinese"
            };
            var halfMaintainedLanguages = { 
                "nl": "Dutch",
                "en": "English"
            };
            
            var languagesWithOwnPage = {   // see https://languagetool.org/languages/ 
                "br": "Ur bajenn vrezhonek hon eus ivez",
                "eo": "Ni ankaŭ havas hejmpaĝon en Esperanto",
                "fr": "Nous avons aussi une page en français",
                "de": "Informationen zu LanguageTool auf Deutsch",
                "it": "Abbiamo anche una pagina in italiano",
                "nl": "Meer over LanguageTool voor het Nederlands",
                "ca": "També tenim una pàgina en català",
                "ru": "Внимание: у нас есть страничка на русском языке!",
                "pt": "Também temos uma página em Português",
                "es": "También tenemos uma página en español",
                "uk": "Докладніше про українську в LanguageTool",
                "pl": "Odwiedź stronę programu LanguageTool po polsku",
                "zh": "Visit our Chinese page about LanguageTool"
            };
            
            // 'auto' doesn't work in MSIE, so we switch manually:
            if (langCode === 'fa') {
                tinymce.get('checktext').getBody().dir = "rtl";
            } else {
                tinymce.get('checktext').getBody().dir = "ltr";
            }
            
            var subLang = $('#subLang');
            subLang.find('option').remove();
            // For languages that have variants, offer those in a different select:
            // NOTE: keep in sync with checkform.php!
            var langToSubLang = {
                'en': [
                    {code: 'US', name: 'American'},
                    {code: 'GB', name: 'British'},
                    {code: 'ZA', name: 'South Africa'},
                    {code: 'CA', name: 'Canada'},
                    {code: 'AU', name: 'Australia'},
                    {code: 'NZ', name: 'New Zealand'}
                    ],
                'de': [
                    {code: 'DE', name: 'Germany'},
                    {code: 'AT', name: 'Austria'},
                    {code: 'CH', name: 'Switzerland'}],
                'pt': [{code: 'PT', name: 'Portugal'}, {code: 'BR', name: 'Brazil'}],
                'ca': [{code: 'ES', name: 'General'}, {code: 'ES-Valencia', name: 'Valencian'}]
            };
            if (langToSubLang[langCode]) {
                var subLangs = langToSubLang[langCode];
                var langCodeWithCountry = "en-us";
                var langCountry = langCodeWithCountry.replace(/^.*-/, "").toUpperCase();
                var urlLang;
                if (window.location.href) {
                    urlLang = window.location.href.replace(/.*\/(..)\/.*/, "$1");
                    if (urlLang.length != 2) {
                      urlLang = null;
                    }
                }
                var i18n = lt_i18n[urlLang];
                //var i18n = lt_i18n['en'];
                //var i18n = lt_i18n[langCode];
                subLangs.forEach(function(entry) {
                    var displayName = entry.name;
                    if (i18n && i18n[entry.code]) {
                      displayName = i18n[entry.code];
                    }
                    if (entry == langCountry) {
                      subLang.append($("<option selected/>").val(entry.code).text(displayName));
                    } else {
                      subLang.append($("<option />").val(entry.code).text(displayName));
                    }
                });
                $('#subLangDropDown').show();
                subLang.dropkick('refresh');
            } else {
                $('#subLangDropDown').hide();
            }
        }
        $(function(){
            var languageToText = [];
languageToText['ast'] = 'Apega testu equí. o revisa toes les pallabres de esti testu pa ver dalgún de los problemis que LanguageTool ye pa deteutar. ¿Afáyeste con los correutores gramaticales? Has date cuenta de que entá nun son perfeutos.';
languageToText['en'] = 'Paste your own text here and click the \'Check Text\' button. Click the colored phrases for details on potential errors. or use this text too see an few of of the problems that LanguageTool can detecd. What do you thinks of grammar checkers? Please not that they are not perfect.';
languageToText['br'] = 'Lakait amañ ho testenn vrezhonek da vezañ gwiriet. Pe implijit an frazenn-mañ gant meur a fazioù yezhadurel.';
languageToText['ca'] = 'Introduïu açí el vostre text. o feu servir aquest texts com a a exemple per a alguns errades que LanguageTool hi pot detectat.';
languageToText['zh'] = '将文本粘贴在此，或者检测以下文本：我和她去看了二部电影。';
languageToText['eo'] = 'Alglui vian kontrolendan tekston ĉi tie... Aŭ nur kontrolu tiun ekzemplon. Ĉu vi vi rimarkis, ke estas gramatikaj eraro en tiu frazo? Rimarku, ke Lingvoilo ankaux atentigas pri literumaj erraroj kiel ĉi-tiu.';
languageToText['fa'] = 'لطفا متن خود را اینجا قرار دهید . یا بررسی کنید که این متن را‌ برای دیدن بعضی بعضی از اشکال هایی که ابزار زبان توانسته تشخیس هدد. درباره ی نرم افزارهای بررسی کننده های گرامر چه فکر می کنید؟ لطفا در نظر داشته باشید که آن‌ها بی نقص نمی باشند.‎';
languageToText['fr'] = 'Copiez votre texte ici ou vérifiez cet exemple contenant plusieurs erreur que LanguageTool doit doit pouvoir detecter.';
languageToText['de'] = 'Fügen Sie hier Ihren Text ein. Klicken Sie nach der Prüfung auf die farbig unterlegten Textstellen. oder nutzen Sie diesen Text als Beispiel für ein Paar Fehler , die LanguageTool erkennen kann: Ihm wurde Angst und bange, als er davon hörte. ( Eine Rechtschreibprüfun findet findet übrigens auch statt.';
languageToText['de-DE-x-simple-language'] = 'Fügen Sie hier Ihren Text ein oder benutzen Sie diesen Text als Beispiel. Dieser Text wurde nur zum Testen geschrieben. Die Donaudampfschifffahrt darf da nicht fehlen. Und die Nutzung des Genitivs auch nicht.';
languageToText['it'] = 'Inserite qui lo vostro testo... oppure controlate direttamente questo ed avrete un assaggio di quali errori possono essere identificati con LanguageTool.';
languageToText['pl'] = 'Wpisz tekst lub użyj istniejącego przykładu. To jest przykładowy tekst który pokazuje, jak jak działa LanguageTool. LanguageTool ma jusz korektor psowni, który wyrurznia bledy na czewrono.';
languageToText['pt'] = 'Cola o teu próprio texto aqui... ou verifica este texto, afim de ver alguns dos dos problemas que o LanguageTool consegue detectar. Isto tal vez permita corrigir os teus erros à última da hora.';
languageToText['ru'] = 'Вставьте ваш текст сюда .. или проверьте этот текстт.';
languageToText['be'] = 'Паспрабуйце напісаць нейкі тэкст з памылкамі, а LanguageTool паспрабуе паказаць нейкия найбольш распаусюджаныя памылки.';
languageToText['da'] = 'Indsæt din egen tekst her , eller brug denne tekst til at se nogle af de fejl LanguageTool fanger. vær opmærksom på at den langtfra er er perfect, men skal være en hjælp til at få standartfejl frem i lyset.';
languageToText['es'] = 'Escriba un texto aquí. LanguageTool le ayudará a afrentar algunas dificultades propias de la escritura. Se a hecho un esfuerzo para detectar errores tipográficos, ortograficos y incluso gramaticales. También algunos errores de estilo, a grosso modo.';
languageToText['gl'] = 'Esta vai a ser unha mostra de de exemplo para amosar  o funcionamento de LanguageTool.';
languageToText['is'] = 'Þetta er dæmi um texta sem á að sína farm á hvernig LanguageTool virkar. Það er þó hérmeð gert ljóst að forritið framkvæmir ekki hefðbundna ritvilluleit.';
languageToText['km'] = 'ឃ្លា​នេះ​បង្ហាញ​ពី​ពី​កំហុស​វេយ្យាករណ៍ ដើម្បី​បញ្ជាក់​ពី​ប្រសិទ្ធភាព​របស់​កម្មវិធី LanguageTool សំរាប់​ភាសាខ្មែរ។';
languageToText['nl'] = 'Een ieder kan fouten maken, tikvouten bij voorbeeld.';
languageToText['sk'] = 'Toto je ukážkový vstup, na predvedenie funkčnosti LanguageTool. Pamätajte si si, že neobsahuje "kontrolu" preklepo.';
languageToText['sl'] = 'Tukaj vnesite svoje besedilo... Pa poglejmo primer besedila s nekaj napakami ki jih lahko razpozna orodje LanguageTool; ko opazite napake, jih lahko enostavno popiravite. ( Obenem se izvrši tudi preverjanje črkovanja črkovanja.';
languageToText['ta'] = 'இந்த பெட்டியில் உங்கள் உரையை ஒட்டி சரிவர சோதிக்கிறதா என பாருங்கள். \'லேங்குவேஜ் டூல்\' சில இலக்கணப் பிழைகளைச் சரியாக கண்டுபிடிக்கும். பல பிழைகளைப் பிடிக்க தடுமாறலாம்.';
languageToText['tl'] = 'Ang LanguageTool ay maganda gamit sa araw-araw. Ang talatang ito ay nagpapakita ng ng kakayahan ng LanguageTool at hinahalimbawa kung paano ito gamitin. Litaw rin sa talatang ito na may mga bagaybagay na hindii pa kayang itama nng LanguageTool.';
languageToText['uk'] = 'УВАГА! Внизу наведено приклад тексту з помилками, які допоможе виправити LanguageTool. Будь-ласка, вставте тутт ваш текст, або перевірте цей текст на предмет помилок. Знайти всі помилки для LanguageTool є не по силах з багатьох причин але дещо він вам все таки підкаже. Порівняно з засобами перевірки орфографії LanguageTool також змайде граматичні та стильові проблеми. LanguageTool — ваш самий кращий помічник.';
            $('#lang').dropkick({
                change: function (value, label) {
                    value = value.replace(/-..$/, "");  // en-US -> en
                    fillSubLanguageSelect(value);
                    var newText = languageToText[value];  // 'languageToText' comes from default_texts.php
                    if (newText) {
                        tinyMCE.activeEditor.setContent(newText);
                        tinyMCE.get('checktext').focus();
                        doit();
                    } else {
                                                if (value !== "auto") {
                            tinyMCE.activeEditor.setContent("Add your text here");
                        }
                                                tinyMCE.get('checktext').focus();
                        tinyMCE.activeEditor.selection.select(tinyMCE.activeEditor.getBody(), true);
                    }
                    $('#feedbackMessage').html('');
                }
            });
            $('#subLang').dropkick({
                // TODO: this messes up the text - it gets called more than once after the main
                // language has been changed:
                /*change: function (value, label) {
                    //doit();
                }*/
            });
        });
    </script>

    <script type="text/javascript">
        function resize_buttons(){
            var max_height = 0;
            $('.button_container .title').each(function(){
                $(this).height('auto');
                if ($(this).height() > max_height) {
                    max_height = $(this).height();
                }
            });
            $('.button_container .title').height(max_height);
        }
        $(function(){
            $(window).resize(function(){
                resize_buttons();
            });
            resize_buttons();
        });
    </script>

  <div class="container main-container">
	<div class="row">
      	
          	<div class="col-md-8 main-index">
            
            <div class="xd_top_box">
             <?php echo $ads_720x90; ?>
            </div>
            
              	<h2 id="title"><?php echo $data['tool_name']; ?></h2>

                <?php if ($pointOut != 'output') { ?>
                
                 <div id="mainbox">
  
            
                <div>
                    <p>Enter your text:</p>
                    <div class="text-center">(or)</div>
                    <p>Upload a document: (Supported Format: .doc, .docx, .txt)</p>
                    <span class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Select file</span>
                    <!-- The file input field used as target for the file upload widget -->
                    <input id="fileupload" type="file" name="files[]" multiple>
                    </span>
                    <br />
                    <br />
                    
                    <!-- The global progress bar -->
                    <div id="progress" class="progress">
                    <div class="progress-bar progress-bar-success progress-bar-striped"></div>
                    </div>
                </div>
            
         <center>
            <div id="stage" class="start">
        <div class="inner">
          <div id="editor">
            <div class="inner">
      

        <script>
            if (navigator.userAgent.indexOf("Edge/") !== -1 || navigator.userAgent.indexOf("Trident/") !== -1) {
                document.write('<div class="warning">Note: MSIE and Edge browsers currently don\'t work with this page. Please download the stand-alone version of LanguageTool or use <a href="https://www.mozilla.org/en-US/firefox/new/">Firefox</a> instead</div>');
            }
        </script>

        <form id="checkform" name="checkform" action="#" method="post">
            <div id="handle"><div id="feedbackMessage"></div></div>
            <div class="window">
                <div class="fullscreen-toggle">
                                <a href="#" title="toggle fullscreen mode" onClick="fullscreen_toggle();return false;"></a>
                </div>
                <p id="checktextpara" style="margin: 0">
                                <textarea class="SpecificEditor" id="checktext" name="text" style="width: 100%" rows="40">Paste your own text here and click the 'Check Text' button. Click the colored phrases for details on potential errors. or use this text too see an few of of the problems that LanguageTool can detecd. What do you thinks of grammar checkers? Please not that they are not perfect.</textarea>
                </p>
                <div id="editor_controls">
                    <!--
                    <div class="message">
                                    </div>
                    -->
                    <div id="feedbackErrorMessage"></div>
                    <div class="dropdown">
                        <select class="dropkick" name="lang" id="lang">
                            <option value="auto" >Auto-detect</option>
        <option value="ast" >Asturian</option>
        <option value="be" >Belarusian</option>
        <option value="br" >Breton</option>
        <option value="ca" >Catalan</option>
        <option value="zh" >Chinese</option>
        <option value="da" >Danish</option>
        <option value="nl" >Dutch</option>
        <option value="en-US"  selected='selected'>English</option>
        <option value="eo" >Esperanto</option>
        <option value="fr" >French</option>
        <option value="gl" >Galician</option>
        <option value="de-DE" >German</option>
        <option value="el" >Greek</option>
        <option value="is" >Icelandic</option>
        <option value="it" >Italian</option>
        <option value="ja" >Japanese</option>
        <option value="km" >Khmer</option>
        <option value="lt" >Lithuanian</option>
        <option value="ml" >Malayalam</option>
        <option value="fa" >Persian</option>
        <option value="pl" >Polish</option>
        <option value="pt" >Portuguese</option>
        <option value="ro" >Romanian</option>
        <option value="ru" >Russian</option>
        <option value="sk" >Slovak</option>
        <option value="sl" >Slovenian</option>
        <option value="es" >Spanish</option>
        <option value="sv" >Swedish</option>
        <option value="ta" >Tamil</option>
        <option value="tl" >Tagalog</option>
        <option value="uk" >Ukrainian</option>
                        </select>
                                        <div id="subLangDropDown" style="display: block;float:left;margin-left:6px">
                            <!-- NOTE: keep this in sync with header.php and the if() above: -->
                            <select class="dropkick" name="subLang" id="subLang">
                                    <option>US</option>
                                    <option>GB</option>
                                    <option>AU</option>
                                    <option>CA</option>
                                    <option>NZ</option>
                                    <option>ZA</option>
                            </select>
                        </div>
                    </div>
                    <div class="submit">
                        <input type="submit" name="_action_checkText" value="Check Grammar" 
                               onClick="doit(true);return false;" title="Check Grammar - you can also use Ctrl+Return">
                    </div>
                    <div style="clear:both;"></div>
                </div>
            </div>
        </form>
    <br /><br />

    </div>
  </div>
</div>
</div> </center>
                        
            </div>


<!-- Piwik -->
<script type="text/javascript">
var _paq = _paq || [];
_paq.push(['trackPageView']);
_paq.push(['enableLinkTracking']);
(function() {
  var u=(("https:" == document.location.protocol) ? "https" : "http") + "://openthesaurus.stats.mysnip-hosting.de/";
  _paq.push(['setTrackerUrl', u+'piwik.php']);
  _paq.push(['setSiteId', 2]);
  var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript';
  g.defer=true; g.async=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
})();
</script>
<noscript><p><img src="http://openthesaurus.stats.mysnip-hosting.de/piwik.php?idsite=2" style="border:0;" alt="" /></p></noscript>
<!-- End Piwik Code -->
                          
               <?php } ?>

<br />

<div class="xd_top_box">
<?php echo $ads_720x90; ?>
</div>

<h2 id="sec1" class="about_tool"><?php echo $lang['11'].' '.$data['tool_name']; ?></h2>
<p>
<?php echo $data['about_tool']; ?>
</p> <br />
</div>              
            
<?php
// Sidebar
require_once(THEME_DIR."sidebar.php");
?>     		
        </div>
    </div> <br />
    
<script src="<?php echo $theme_path; ?>js/vendor/jquery.ui.widget.js"></script>
<script src="<?php echo $theme_path; ?>js/jquery.iframe-transport.js"></script>
<script src="<?php echo $theme_path; ?>js/jquery.fileupload.js"></script>

<script>
/*jslint unparam: true */
/*global window, $ */
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = 'core/upload/';
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                // Completed 
                var file_name = file.name;
                jQuery.post('core/upload/process.php',{fileName:file_name},function(data){
                $("#checktext_ifr").contents().find("body").html('<p>'+data+'</p>');
                });
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
</script>