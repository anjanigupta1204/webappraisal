<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name A to Z SEO Tools - PHP Script
 * @copyright � 2016 ProThemes.Biz
 *
 */
?>
<style>
.fileinput-button {
    overflow: hidden;
    position: relative;
}

.fileinput-button input {
    cursor: pointer;
    direction: ltr;
    font-size: 200px;
    margin: 0;
    opacity: 0;
    position: absolute;
    right: 0;
    top: 0;
}

@media screen {
.fileinput-button input {
    font-size: 100%;
    height: 100%;
}
</style>

<script src='../core/library/dup.js'></script>

  <div class="container main-container">
	<div class="row">
      	
          	<div class="col-md-8 main-index">
            
            <div class="xd_top_box">
             <?php echo $ads_720x90; ?>
            </div>
            
              	<h2 id="title"><?php echo $data['tool_name']; ?></h2>

               <?php if ($pointOut != 'output') {
               ?>
               <br />
               
            <div id="menu"></div>
            <div id="bodypadding" style="margin-top:0px !important;">

                    <p class="text-center">Enter your text here <br />
                    (or) <br />
                     <span class="btn btn-danger fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span>Load a file</span>
                        <!-- The file input field used as target for the file upload widget -->
                        <input id="file" onchange="setTimeout('loadfile(\'file\',\'input_output\')',100);" type="file">
                     </span>
                     
                    </p>
                  
                    <textarea class="form-control" id="input_output" style="width: 100%; height: 336px;" rows="4" wrap="off">Enter your text here, select options and click the "Remove Duplicate Lines" button from above.
                    
Example:
Click the "Remove Duplicate Lines" button and watch these duplicate 
lines become one.
Click the "Remove Duplicate Lines" button and watch these duplicate 
lines become one.
Click the "Remove Duplicate Lines" button and watch these duplicate 
lines become one.
                    </textarea>

                    <div style="padding:4px 0px 7px 0px;">
                        <input class="btn btn-info" value="Remove Duplicate Lines" onclick="rduplin();" type="button" />&nbsp;
                        <input id="case_sen" type="checkbox">Case sensitive.
                        <input id="rel" type="checkbox">Remove empty lines.
                        <input id="dis_rem" onclick="if(this.checked==true)document.getElementById('removed_div').style.display='block'; if(this.checked==false)document.getElementById('removed_div').style.display='none'; resizepage();" type="checkbox">Display removed.
                        <input class="btn btn-info" title="Select All Text" value="S" onclick="SelectAll('input_output')" type="button">
                        <input class="btn btn-info" title="Clear All Text" value="C" onclick="cleartext();" type="button">
                        <span id="removed"></span>
                    </div>

                    <div id="removed_div" style="display:none; padding-top:10px;">
                        <textarea id="removed_output" rows="4" style="width:100%;" wrap="off">Removed Line Box - Removed lines will display here.
                        Removed lines will be prefixed with their duplicate info* for reference and analysis. *e.g. (Line 10 was a duplicate of line 9.)</textarea>
                    </div>
                    
                    <div style="padding:7px 0px 0px 0px;" class="row">
                       
                        <div class="col-md-2">
                            <label>Type: </label> <br />
                            <input name="sfrmt" id="unix" type="radio">Unix &nbsp;
                            <input checked="checked" name="sfrmt" id="dos" type="radio">Dos
                        </div>

                        <div class="col-md-2">
                            <label>File Name: </label> <br />
                            <input id="saveas" value="output.txt" style="width:90px;" type="text" />
                        </div>
                        
                        <div class="col-md-2">
                            <br />
                           <input class="btn btn-success" value="Save As" onclick="savefile('saveas','input_output');" type="button">
                        </div>

                    </div>

                </div> 
                          
               <?php 
               } ?>
<br />

<div class="xd_top_box">
<?php echo $ads_720x90; ?>
</div>

<h2 id="sec1" class="about_tool"><?php echo $lang['11'].' '.$data['tool_name']; ?></h2>
<p>
<?php echo $data['about_tool']; ?>
</p> <br />
</div>              
            
<?php
// Sidebar
require_once(THEME_DIR."sidebar.php");
?>     		
        </div>
    </div> <br />