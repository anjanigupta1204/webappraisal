<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name A to Z SEO Tools - PHP Script
 * @copyright � 2016 ProThemes.Biz
 *
 */
if(isset($msg))
    $pointOut = 'error';
?>

  <div class="container main-container">
	<div class="row">
      	
          	<div class="col-md-8 main-index">
            
            <div class="xd_top_box">
             <?php echo $ads_720x90; ?>
            </div>
            
              	<h2 id="title"><?php echo $data['tool_name']; ?></h2>

               <?php if ($pointOut != 'output') {
                if(isset($msg))
                    echo '<br>'.$msg.'<br>'; 
               ?>
               <br />
               <p><?php echo $lang['23']; ?>
               </p>
               <form enctype="multipart/form-data" method="POST" action="<?php echo $toolOutputURL;?>"> 
               <input type="text" name="url" value="" class="form-control"/> <br />  
                <center>(or)</center>
                
                <div class="form-group">											
                	<label class="form-label" for="logoID">Select image to upload:</label><br />		   
                    <input type="file" name="logoUpload" id="logoUpload" class="form-control" style="height: auto;" />
                    <input type="hidden" name="logoID" id="logoID" value="1" />
                </div><center>
               <br />
               <?php
               if ($toolCap)
               {
               echo $captchaCode;  
               }
               ?>
               <div class="text-center">
               <input class="btn btn-info" type="submit" value="<?php echo $lang['8']; ?>" name="submit"/>
               </div>
               </form>     
                          
               <?php 
               } else { 
               //Output Block
               if(isset($error)) {
                
                echo '<br/><br/><div class="alert alert-error">
                <strong>Alert!</strong> '.$error.'
                </div><br/><br/>
                <div class="text-center"><a class="btn btn-info" href="'.$toolURL.'">'.$lang['12'].'</a>
                </div><br/>';
                
               } else {
               ?>
               
               <br />

                <div class="box box-primary">
                                <div class="box-header">
                                    <center><h4>Result</h4></center>
                                </div><!-- /.box-header -->
                                <div class="box-body">

                                    <table class="table table-bordered">
                                    <thead>
                                            <tr style="background:#367FA9; color: #fff">
                                                <th style="text-align:center">Search Engine</th>
                                                <th style="text-align:center">Details</th>
                                                <th style="text-align:center">Action</th>
                                            </tr>
                                         </thead>
                                        <tbody><tr>
                                            <td class="text-center">
                                            <img src="<?php echo $theme_path; ?>img/logo_google.png" alt="Google" />
                                            </td>
                                            <td> Images that are similar with your provided image according to google </td>
                                            <td><a class="btn btn-primary" href="<?php echo $google_file_path; ?>" target="_blank" rel="nofollow" title="Check on Google">Check Images</a></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                            <img src="<?php echo $theme_path; ?>img/logo_bing.png" alt="Bing" />
                                            </td>
                                            <td> Images that are similar with your provided image according to Bing </td>
                                            <td><a class="btn btn-primary" href="<?php echo $bing_file_path; ?>" target="_blank" rel="nofollow" title="Check on Bing">Check Images</a></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                            <img src="<?php echo $theme_path; ?>img/logo_yandex.jpg" alt="Yandex" />
                                            </td>
                                            <td> Images that are similar with your provided image according to Yandex </td>
                                            <td><a class="btn btn-primary" href="<?php echo $yandex_file_path; ?>" target="_blank" rel="nofollow" title="Check on Yandex">Check Images</a></td>
                                        </tr>
                                  
                                    </tbody></table>

                                </div><!-- /.box-body -->

                            </div>
    <div class="text-center">
    <br /> &nbsp; <br />
    <a class="btn btn-info" href="<?php echo $toolURL; ?>"><?php echo $lang['27']; ?></a>
    <br />
    </div>

<?php } } ?>

<br />

<div class="xd_top_box">
<?php echo $ads_720x90; ?>
</div>

<h2 id="sec1" class="about_tool"><?php echo $lang['11'].' '.$data['tool_name']; ?></h2>
<p>
<?php echo $data['about_tool']; ?>
</p> <br />
</div>              
            
<?php
// Sidebar
require_once(THEME_DIR."sidebar.php");
?>     		
        </div>
    </div> <br />