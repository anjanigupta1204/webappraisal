<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name A to Z SEO Tools - PHP Script
 * @copyright � 2015 ProThemes.Biz
 *
 */
?>

<style>
.bulkcheck .icheckbox_minimal{
    margin-left: 12px;
}
table {
    table-layout: fixed; width: 100%;
}
td {
  word-wrap: break-word;
}
</style>
<script>
function fixKeySug(){
    var myKeyword= jQuery.trim($('input[name=keyword]').val());
    if (myKeyword==null || myKeyword=="") {
        alert('Enter a keyword!');
        return false;
    }else{
        return true;
    }
}
function processLoadBar() {
    var myKeyword= jQuery.trim($('input[name=keyword]').val());
    if (myKeyword==null || myKeyword=="") {
        //alert('Enter a keyword!');
    }else{
        jQuery("#percentimg").css({"display":"block"});
       	jQuery("#mainBox").fadeOut();
    }
}
</script>
  <div class="container main-container">
	<div class="row">
      	
          	<div class="col-md-8 main-index">
            
            <div class="xd_top_box">
             <?php echo $ads_720x90; ?>
            </div>
            
              	<h2 id="title"><?php echo $data['tool_name']; ?></h2>

                <?php if ($pointOut != 'output') { ?>
               <br /><div id="mainBox">
               <p>Enter the keyword you want suggestions for</p>
               <form method="POST" action="<?php echo $toolOutputURL;?>" onsubmit="return fixKeySug();"> 
               <input type="text" name="keyword" id="keyword" placeholder="<?php echo $lang['328']; ?>" class="form-control" />
               
               <br />
               <br />
               <div class="bulkcheck text-center">
                   <input type="checkbox" name="google" value="1" /> Google
                   <input type="checkbox" name="bing" value="1" />  Bing
                   <input type="checkbox" name="yahoo" value="1" />  Yahoo
                   <input type="checkbox" name="youtube" value="1" />  Youtube
                   <input type="checkbox" name="amazon" value="1" />  Amazon
                   <input type="checkbox" name="wikipedia" value="1" />  Wikipedia
               </div>               
               <br />
               <?php
               if ($toolCap) {
               echo $captchaCode;   
               }
               ?>
               <div class="text-center">
               <input onclick="processLoadBar();" class="btn btn-info" type="submit" value="<?php echo $lang['8']; ?>" name="submit"/>
               </div>
               </form> </div>
              
              <br /><br />         
               <div id="percentimg" class="text-center" style="display:none;">
                    <img src="<?php echo $theme_path; ?>img/load.gif" />
                    <br /><br />
                    <?php echo $lang['146']; ?>...
                    <br /><br />
               </div>  
               <br />   
                
               <?php 
               } else { 
               //Output Block
               if(isset($error)) {
                
                echo '<br/><br/><div class="alert alert-error">
                <strong>Alert!</strong> '.$error.'
                </div><br/><br/>
                <div class="text-center"><a class="btn btn-info" href="'.$toolURL.'">'.$lang['12'].'</a>
                </div><br/>';
                
               } else {
               ?>
            <br />
            <br />
			<table class="table table-hover table-bordered table-striped" style="margin-bottom: 30px;">
				<thead>
					<tr>
                        <th style="width: 6%;">#</th>
                        <th style="text-align: center;"><?php echo $lang['329']; ?></th>
				    </tr>
				</thead>
				<tbody>
					<?php
                        $textKeywords = null;
                        $loop = 1;
						foreach($allKeywords as $allKeyword) {
					?>
					<tr>
						<td><?php echo $loop; ?></td>
						<td style="text-align: center;"><?php echo $allKeyword; ?></td>
					</tr>
					<?php
                        $textKeywords .= $allKeyword.PHP_EOL;
                        $loop++;
						}
					?>
				</tbody>
			</table>   
            <textarea hidden="" id="dlKeywords"><?php echo $textKeywords; ?></textarea>
    <div class="text-center">
    <br /> &nbsp; <br />
    <a onclick="saveAsFile()" class="btn btn-success" title="Save as Text File">Export</a>
    <a class="btn btn-info" href="<?php echo $toolURL; ?>">Try New Keyword</a>
    <br />
    </div>

<?php } } ?>

<br />

<div class="xd_top_box">
<?php echo $ads_720x90; ?>
</div>

<h2 id="sec1" class="about_tool"><?php echo $lang['11'].' '.$data['tool_name']; ?></h2>
<p>
<?php echo $data['about_tool']; ?>
</p> <br />
</div>              
            
<?php
// Sidebar
require_once(THEME_DIR."sidebar.php");
?>     		
        </div>
    </div> <br />
<script>
function saveAsFile() {      
    var textToWrite = document.getElementById("dlKeywords").value;
    var textFileAsBlob = new Blob([textToWrite], {type:'text/plain'});
    var fileNameToSaveAs = "keywords.txt";
    var downloadLink = document.createElement("a");
    downloadLink.download = fileNameToSaveAs;
    downloadLink.innerHTML = "My Link";
    window.URL = window.URL || window.webkitURL;
    downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
    downloadLink.onclick = destroyClickedElement;
    downloadLink.style.display = "none";
    document.body.appendChild(downloadLink);
    downloadLink.click();
}

function destroyClickedElement(event){
    document.body.removeChild(event.target);
}
</script>