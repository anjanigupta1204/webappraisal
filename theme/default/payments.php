<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name A to Z SEO Tools - PHP Script
 * @copyright © 2016 ProThemes.Biz
 *
 */
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta charset="utf-8" />
	<title><?php echo $p_title; ?></title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" />
</head>
<body onLoad="doPayment();">
<div class="container" style="margin-top: 100px;">
<br />
<hr />
<br />
    <div style="text-align: center;">
        <img class="atoz_seo_tools_logo" src="<?php echo $logo_path; ?>" />
        <?php if($type == 'cancel'){ ?>
            <h4 style="color: #c43b2d;">Transaction Cancelled</h4>
            <br/><img src="<?php echo $theme_path; ?>premium/img/loader.gif" alt="Loader" /><br/>
            <?php echo $paymentData; ?>  
        <?php }else if($type == 'process'){ ?>
            <h5>Please wait, your order is being processed and you will be redirected to the <?php echo $gatewayName; ?> website.</h5>
            <br/><img src="<?php echo $theme_path; ?>premium/img/loader.gif" alt="Loader" /><br/>
            <?php echo $paymentData; ?>
        <?php }else if($type == 'success'){ ?>
            <h4 style="color: #27ae60;">Payment Transaction Done Successfully</h4>
            <br/><img src="<?php echo $theme_path; ?>premium/img/loader.gif" alt="Loader" /><br/>
            <?php echo $paymentData; ?> 
        <?php } ?>
    </div>
<br />
<hr />
<br />
</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>