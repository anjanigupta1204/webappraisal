<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));
/*
 * @author Balaji
 * @name: A to Z SEO Tools - PHP Script
 * @Theme: Default Style
 * @copyright � 2016 ProThemes.Biz
 *
 */
$planLimit = 0;
$allPlanLimit = 1;
?>

<style>
#secPriceBox{
    display: none;
}
</style>

<link href="<?php echo $theme_path; ?>premium/css/premium.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $theme_path; ?>premium/css/pricing.css" rel="stylesheet" type="text/css" />

    <div class="container main-container">
        <div class="row">
            <div class="col-md-8 main-index">

                <h4 class="gray16 text-center">
                From individuals and marketing teams to agencies and enterprise clients, <br/>we have a plan for everybody.
                </h4>
                
                <div id="mainPriceBox" class="rp-format-2 rp-hex-hover">
                        <?php foreach($plans as $plan){ ?>
						<div class="col-sm-4 col-xs-6">
							<div class="rp-item">
                            <?php if($plan[2]){ ?>
                                <span class="rp-ribbon rp-ribbon-right">Best Plan</span>
                            <?php } ?>
								<div class="rp-plan-top">
									<h3 class="rp-plan-name"><?php echo $plan[0]; ?></h3>
									<p class="rp-plan-description"><?php echo $plan[4]; ?></p>
									<div class="rp-hex">
										<div class="rp-plan-cost">
											<span class="rp-currency"><?php echo $plan[5]; ?></span>
											<span class="rp-cost"><?php echo $plan[6]; ?></span>
											<span class="rp-duration"><?php echo $plan[7]; ?></span>
										</div>
									</div>
								</div>
								<div class="rp-plan-details">
									<ul id="plan_data"> 
                                    <?php foreach($plan[8] as $plan_feature){
                                        if(Trim($plan_feature) != '')
                                            echo '<li><p>'.html_entity_decode(Trim($plan_feature)).'</p></li>';
                                    } ?>                                      
									</ul>
								</div><!-- /.plan-details -->
								<div class="rp-plan-bottom">
									<a href="<?php echo '/'.$controller.'/'.$plan[1]; ?>" class="btn rp-btn rp-pricing-btn">Subscribe</a>
								</div><!-- /.plan-bottom -->
							</div><!-- /.item -->
						</div>
                        <?php $planLimit++; if($planLimit == 3) break; } ?>
					</div>
                    
                    <div class="browseBtnBox">
                        <a onclick="browseBtn();" href="#" class="browseBtn">Browse all Plans</a>
                    </div>
                    
                    <div id="secPriceBox" class="rp-format-2 rp-hex-hover">
                        <?php foreach($plans as $plan){ ?>
						<div class="">
							<div class="row rp-item rp-hor">
								<div class="col-md-9">
									<h3 class="rp-hor-name"><?php echo $allPlanLimit; ?>) <?php echo $plan[0]; ?></h3>
									<p class="rp-hor-description"><?php echo $plan[4]; ?></p>
                                    <?php $lOne = 0; $lOneData = $lTwoData = ''; $lTwo = 1;
                                    
                                    foreach($plan[8] as $plan_feature){
                                        if(Trim($plan_feature) != ''){
                                            $plan_feature = html_entity_decode(Trim($plan_feature));
                                            if($lOne == 0){
                                                if(str_contains($plan_feature,'<s>'))
                                                    $lOneData .= '<li class="wrong">'.str_replace(array('<s>','</s>'),'',$plan_feature).'</li>';
                                                else
                                                    $lOneData .= '<li class="right">'.$plan_feature.'</li>';
                                            }
                                            if($lTwo == 0){
                                                if(str_contains($plan_feature,'<s>'))
                                                    $lTwoData .= '<li class="wrong">'.$plan_feature.'</li>';
                                                else
                                                    $lTwoData .= '<li class="right">'.$plan_feature.'</li>';
                                            }
                                            if($lOne == 0){
                                                $lOne = 1;
                                                $lTwo = 0;
                                            }else{
                                                $lTwo = 1;
                                                $lOne = 0;
                                            }
                                        }  
                                    } ?>  
                                    <div class="row">
                                        <div class="col-md-6">
								        <div class="rp-hor-details">
    									<ul>
                                            <?php echo $lOneData; ?>
    									</ul>
    								</div><!-- /.plan-details -->
                                        </div>
                                        
                                        <div class="col-md-6">
                                        <div class="rp-hor-details">
    									<ul>
                                             <?php echo $lTwoData; ?>                                         
    									</ul>
    								</div><!-- /.plan-details -->
                                        </div>
                                           </div>

								</div><!-- /.plan-top -->
                                    <div class="col-md-3">
  						            <div class="rp-hex rp-hex-hor">
										<div class="rp-plan-cost">
											<span class="rp-currency"><?php echo $plan[5]; ?></span>
											<span class="rp-cost"><?php echo $plan[6]; ?></span>
											<span class="rp-duration"><?php echo $plan[7]; ?></span>
										</div><!-- /.rp-plan-cost -->
									</div><!-- /.rp-hex -->

								<div class="rp-hex-hor-bottom">
                                <br />
								    <a href="<?php echo '/'.$controller.'/'.$plan[1]; ?>" class="btn rp-btn rp-pricing-btn">Subscribe</a>
								</div><!-- /.plan-bottom -->
                                </div>
							</div><!-- /.item -->
						</div>
                        <?php $allPlanLimit++; } ?>
					</div>
                    
                    <br /><br />
            </div>
            <?php 
            // Sidebar 
            require_once(THEME_DIR. "sidebar.php"); 
            ?>
        </div>
    </div>
    <br />
    
    <script>
    function browseBtn(){
        jQuery(".browseBtnBox").fadeOut();
        jQuery("#mainPriceBox").fadeOut();
        jQuery("#secPriceBox").fadeIn();
    }
    </script>