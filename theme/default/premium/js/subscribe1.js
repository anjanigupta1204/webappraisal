
var pos = '#step1';
var totalAmount = 0;
var billType = $('input[name=billVal]').val();
var eToken = $('input[name=eToken]').val();
var planID = $('input[name=planID]').val();
var amWithTax = 0;
var recType = 0;

function doAnimate(pos){
    var timeToDelay = 1000;
    $('body,html').animate({ scrollTop: $(pos).offset().top }, timeToDelay);
}

function validatePhone(txtPhone) {
    var filter = /^((\+[0-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
    if (filter.test(txtPhone)) {
        return true;
    }else {
        return false;
    }
}

function addressCheck(){
    var errorCheck = 1;
    var firstname = $.trim($('input[name=firstname]').val());
    var lastname = $.trim($('input[name=lastname]').val());
    var telephone = $.trim($('input[name=telephone]').val());
    var country = $.trim($('select[name=country]').val());
    var address1 = $.trim($('input[name=address1]').val());
    var address2 = $.trim($('input[name=address2]').val());
    var city = $.trim($('input[name=city]').val());
    var postcode = $.trim($('input[name=postcode]').val());
    var state = $.trim($('select[name=state]').val());  

    if(firstname == ''){
        $('input[name=firstname]').parent().addClass('has-error');
        errorCheck = 0;
    }
    if(lastname == ''){
        $('input[name=lastname]').parent().addClass('has-error');
        errorCheck = 0;
    }
    if(!validatePhone(telephone)){
        $('input[name=telephone]').parent().addClass('has-error');
        errorCheck = 0;
    }
    if(country == ''){
        $('select[name=country]').parent().addClass('has-error');
        errorCheck = 0;
    }
    if(address1 == ''){
        $('input[name=address1]').parent().addClass('has-error');
        errorCheck = 0;
    }
    if(city == ''){
        $('input[name=city]').parent().addClass('has-error');
        errorCheck = 0;
    }
    if(postcode == ''){
        $('input[name=postcode]').parent().addClass('has-error');
        errorCheck = 0;
    }
    if(state == ''){
        $('select[name=state]').parent().addClass('has-error');
        errorCheck = 0;
    }
    
    if(address2 != ''){
        address2 = address2 + "<br>";
    }

    if(errorCheck == 1){
        $('input[name=firstname]').parent().removeClass('has-error');
        $('input[name=lastname]').parent().removeClass('has-error');
        $('select[name=country]').parent().removeClass('has-error');
        $('input[name=telephone]').parent().removeClass('has-error');
        $('input[name=address1]').parent().removeClass('has-error');
        $('input[name=city]').parent().removeClass('has-error');
        $('input[name=postcode]').parent().removeClass('has-error');
        $('select[name=state]').parent().removeClass('has-error');
        $('#invoiceAddress').html(firstname + " " + lastname + "<br>" + address1 + "<br>" + address2 + city + " - " + postcode + "<br>" + $("select[name=state] option:selected").text() + "<br>" + BFHCountriesList[country]);        
        return true;
    } else{
        return false; 
    }
                     
}


jQuery(document).ready(function(){
    doAnimate('#step1');
    
    jQuery(document).on('change','#rec_amount',function(){
        totalAmount = $('select[name=rec_amount]').val().trim();
        $('#planAm').html(currencySymbol + totalAmount);
    });
    
    jQuery(document).on('change','input[name=firstname]',function(){
        $('input[name=firstname]').parent().removeClass('has-error');
    });
    
    jQuery(document).on('change','input[name=lastname]',function(){
        $('input[name=lastname]').parent().removeClass('has-error');
    });
    
    jQuery(document).on('change','input[name=telephone]',function(){
        $('input[name=telephone]').parent().removeClass('has-error');
    });
    
    jQuery(document).on('change','select[name=country]',function(){
        $('select[name=country]').parent().removeClass('has-error');
    });
    
    jQuery(document).on('change','input[name=address1]',function(){
        $('input[name=address1]').parent().removeClass('has-error');
    });
    
    jQuery(document).on('change','input[name=city]',function(){
        $('input[name=city]').parent().removeClass('has-error');
    });
    
    jQuery(document).on('change','input[name=postcode]',function(){
        $('input[name=postcode]').parent().removeClass('has-error');
    });
    
    jQuery(document).on('change','select[name=state]',function(){
        $('select[name=state]').parent().removeClass('has-error');
    });
    
    jQuery("#conBtn").click(function()
    {
        var activeStep= $.trim($('input[name=stage]').val());
        if(activeStep == '1'){
            $("#step1").hide(); 
            $("#step2").fadeIn();
            $("#step2").removeClass('hide');
            doAnimate('#step2');
            $(".nextPrev").show();
            $('input[name=stage]').val('2');
            $('.premium-2-inner').addClass('tagactive');
        }
    });
    
    jQuery("#nextBtn").click(function()
    {
        var activeStep= $.trim($('input[name=stage]').val());
        if (activeStep == '2'){
            if(addressCheck()){
                $("#step2").hide(); 
                $("#step3").fadeIn();
                $("#step3").removeClass('hide');
                doAnimate('#step3');
                $('input[name=stage]').val('3');
                $('.premium-3-inner').addClass('tagactive'); 
                if(billType == '1'){
                    totalAmount = $('select[name=rec_amount]').val().trim();
                }else{
                    totalAmount = $('input[name=one_amount]').val().trim();
                }
                $('#planAm').html(currencySymbol + totalAmount);
            }else{
                doAnimate('#step2');
            }
        } else if (activeStep == '3'){
            if(billType == '1'){
                $('#bill_Val').html($("#rec_amount :selected").text());
            }else{
                $('#bill_Val').html(oneTimeStr);
            }
            if ($("#agree").length > 0){
                if($("#agree").prop("checked") == false){
                    alert('Agree terms and conditions to continue!');
                    doAnimate('#step3');
                    return false;
                }
            }
            if($(".payment_method:checked").val()){
                $.ajax({
                url: "../?route=premium-ajax&calTax&amount="+totalAmount+"&currencyType="+currencyType,
                timeout: 30000,
                error: function(){
                    //do something
                    alert('Unable get the Tax Data');
                    doAnimate('#step3');
                },
                success: function(data){
                    //do something
                    $('#ajaxTotal').html(data);
                    $('#bill_Price').html(currencySymbol + totalAmount);
                    $("#step3").hide(); 
                    $("#step4").fadeIn();
                    $("#step4").removeClass('hide');
                    doAnimate('#step4');
                    $('input[name=stage]').val('4');
                    $('.premium-4-inner').addClass('tagactive'); 
                    amWithTax = $.trim($('input[name=amWithTax]').val());
                    recType = $('select[name=rec_amount]').children(":selected").attr("id");
                }
                });
            }else{
                alert(errStr);
                doAnimate('#step3');
            }
        } else if (activeStep == '4'){

            //Address Information
            var firstname = $.trim($('input[name=firstname]').val());
            var lastname = $.trim($('input[name=lastname]').val());
            var telephone = $.trim($('input[name=telephone]').val());
            var country = $.trim($('select[name=country]').val());
            var company = $.trim($('input[name=company]').val());
            var address1 = $.trim($('input[name=address1]').val());
            var address2 = $.trim($('input[name=address2]').val());
            var city = $.trim($('input[name=city]').val());
            var postcode = $.trim($('input[name=postcode]').val());
            var state = $.trim($('select[name=state]').val());  
            var stateStr = $.trim($('select[name=state] option:selected').text());
            jQuery.post('../?route=premium-ajax',{order:'1', etoken:eToken, firstname:firstname, lastname:lastname, company:company, telephone:telephone, address1:address1, address2:address2, city:city, state:state, postcode:postcode, country:country, statestr:stateStr},function(data){
                myArr = data.split(':::');
                eToken = myArr[1].trim();
                var paymentVal = $(".payment_method:checked").val();
                if(myArr[0].trim() == '1'){
                   //Create Order ID
                   jQuery.post('../?route=premium-ajax',{order:'1', etoken:eToken, paymentval:paymentVal, totalamount:totalAmount, WithTax:amWithTax, billtype:billType, recType:recType, planid:planID, currencyType:currencyType},function(data){
                   myArr = data.split(':::');
                   eToken = myArr[1].trim();
                   if(myArr[0].trim() == '1'){
                        //Process Payment
                        window.location.replace('../payments/'+myArr[2].trim()+'/process/'+eToken);
                   }else{
                    alert('Failed-2');
                   }
                   });
                }else{
                    alert('Failed-1');
                }
            });
        }
    });
    
    jQuery("#prevBtn").click(function()
    {
        var activeStep= $.trim($('input[name=stage]').val());
        if (activeStep == '2'){
            $("#step2").hide(); 
            $("#step1").show();
            doAnimate('#step1');
            $(".nextPrev").hide();
            $('input[name=stage]').val('1');
            $('.premium-2-inner').removeClass('tagactive');
        }else if(activeStep == '3'){
            $("#step3").hide(); 
            $("#step2").show();
            doAnimate('#step2');
            $('input[name=stage]').val('2');
            $('.premium-3-inner').removeClass('tagactive');
        }else if(activeStep == '4'){
            $("#step4").hide(); 
            $("#step3").show();
            doAnimate('#step3');
            $('input[name=stage]').val('3');
            $('.premium-4-inner').removeClass('tagactive');
        }
    });
});