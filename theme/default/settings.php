<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));
/*
 * @author Balaji
 * @name: A to Z SEO Tools - PHP Script
 * @Theme: Default Style
 * @copyright � 2016 ProThemes.Biz
 *
 */
?>

<link href="<?php echo $theme_path; ?>premium/css/premium.css" rel="stylesheet" type="text/css" />

<div class="container main-container">
    <div class="row">
        <div class="col-md-8 main-index">
        
        <h2 class="premiumTitle">Customize PDF report:</h2>     
                                       
        <br />
        <?php if(isset($msg)) {
            echo $msg.'<br>';
        }
        ?>
        
          <div id="pdf" >
          
            <div class="row">
              
           <form name="PDFBox" method="POST" action="#" enctype="multipart/form-data"> 
            <?php if(!$isBranded) { ?>
            <div class="alert alert-danger alert-dismissable alert-premium" style="margin: 5px;">
                 <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                 <strong>Alert!</strong> Your subscribed plan not allowed customizable PDF Report
            </div> <br />
            <?php } ?>
            
            <div class="col-md-6">
               <h4 style="margin-bottom: 15px; font-weight: 500;">Header Logo:</h4>
                <img class="headerLogoBox" id="headerLogoBox" alt="Header Logo" src="<?php echo $headerLogo; ?>" />   
                <br />
                Recommended Size: (JPEG 150x75px)
                <input <?php echo $banClass; ?> type="file" name="headerUpload" id="headerUpload" class="btn btn-default" />
                <br /><br />
            </div><!-- /.col-md-6 -->
            
            <div class="col-md-6">
                <h4 style="margin-bottom: 15px; font-weight: 500;">Footer Logo:</h4>
                <img class="footerLogoBox" id="footerLogoBox" alt="Footer Logo" src="<?php echo $footerLogo; ?>" />   
                <br />
                Recommended Size: (JPEG 100x50px)
                <input <?php echo $banClass; ?> type="file" name="footerUpload" id="footerUpload" class="btn btn-default" />
                <br /><br />  
            </div><!-- /.col-md-6 -->
            
            <div class="col-md-12">  
                <div class="form-group">
                    <h4 style="margin-bottom: 15px; font-weight: 500;">Copyright Text:</h4>
    				<input <?php echo $banClass; ?> value="<?php echo $pdfCopyright; ?>" placeholder="Enter your copyright text" type="text" name="copyright" class="form-control"  style="width: 96%;"/>
    			</div>	
                <br />
                
               <h4 style="margin-bottom: 15px; font-weight: 500;">Introduction Message: <small>(HTML/CSS Supported)</small></h4>
               <textarea name="introductionCode" <?php echo $banClass; ?> rows="7" class="form-control"><?php echo stripslashes(strEOL($introductionCode)); ?></textarea>
              
               <br />
               
               <h4 style="margin-bottom: 15px; font-weight: 500;">Footer PDF Code: <small>(HTML/CSS Supported)</small></h4>
               <textarea name="footerCode" <?php echo $banClass; ?> rows="8" class="form-control"><?php echo stripslashes(strEOL($footerCode)); ?></textarea>
               <br /><h4 style="margin-bottom: 15px; font-weight: 500;">Replacement Code Reference:</h4>
            </div>
            
            <div class="col-md-6">
                    <div class="well alert-warning">
                    <b>{(CopyRight Text)}</b> - Returns your Copyright text<br /><br />
                    <b>{(InputSite)}</b> - Returns your current domain name<br /><br />
                    <b>{(CurrentPageNumber)}</b> - Returns current page number<br /><br />
                    <b>{(TotalPageNumber)}</b> - Returns total page numbers<br /><br />
                    <b>{(HeaderLogo)}</b> - Returns your header logo
                    </div>
                <br /><br />  
               
            </div><!-- /.col-md-6 -->
            
            <div class="col-md-6">
                    <div class="well alert-warning">
                    <b>{(FooterLogo)}</b> - Returns your footer logo<br /><br />
                    <b>{(Date)}</b> - Returns today date<br /><br />
                    <b>{(Time)}</b> - Returns current time<br /><br />
                    <b>{(DateTime)}</b> - Returns today date and current time<br /><br />
                    </div>
                    <br /><br />  
            </div><!-- /.col-md-6 -->
              
            <div class="col-md-12 text-center">  
                <br />
                <input type="submit" value="Submit" class="btn btn-success" />
                <input type="hidden" name="statestr" value="1" />
                <input type="hidden" name="user" value="1" />
                <br />
            </div>
            
            </form>
          </div>
      
          </div>
        <br />

        </div>
        <?php 
        // Sidebar 
        require_once(THEME_DIR. "sidebar.php"); 
        ?>
    </div>
</div>
<br />

<script>
function readURL(input,idBox){
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(idBox).attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#headerUpload").change(function(){
    readURL(this,'#headerLogoBox');
});
$("#footerUpload").change(function(){
    readURL(this,'#footerLogoBox');
});
</script>