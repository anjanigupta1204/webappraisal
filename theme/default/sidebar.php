﻿<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));
/*
 * @author Balaji
 * @name: A to Z SEO Tools - PHP Script
 * @Theme: Default Style
 * @copyright © 2015 ProThemes.Biz
 *
 */
?>

<div class="col-md-4" id="rightCol">
    <?php if(!isset($_SESSION['premiumClient'])) { ?>          	
    <div class="well"> 
                
        <div class="sideXd">
        <?php echo $ads_250x300; ?>
	        </div>
                
        <div class="sideXd">
         <?php echo $ads_250x125; ?>
        </div>
        
    </div>
    <?php } else { ?>  
     
    <div class="hisData">
        <div class="titleBox">Lastest Analyzed Websites</div>
        <ul class="hisUl">
            <?php foreach(getUserRecentSites($con) as $domain){ ?>
                <li><a href="/domain/<?php echo $domain; ?>"><img class="favicon" alt="img" src="https://s2.googleusercontent.com/s2/favicons?domain_url=<?php echo $domain; ?>" /><?php echo ucfirst($domain); ?></a></li>
            <?php } ?>
        </ul>
    </div>
    
    <?php } ?>
</div> 