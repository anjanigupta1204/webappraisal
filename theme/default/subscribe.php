<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools - PHP Script
 * @Theme: Default Style
 * @copyright � 2016 ProThemes.Biz
 *
 */
?>

<link href="<?php echo $theme_path; ?>premium/css/premium.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $theme_path; ?>premium/css/bootstrap-formhelpers.min.css" rel="stylesheet" type="text/css" />

<script>
var currencySymbol = '<?php echo $currencySymbol; ?>';
var currencyType = '<?php echo $currencyType; ?>';
var oneTimeStr = 'One Time Fee';
var errStr = 'Select Payment Method';
</script>

<div class="container main-container">
    <div class="row">
        <div class="col-md-8 main-index">
            <br />
            <?php if(!isset($_SESSION['premiumClient'])){ ?>                                
    		<div class="progress-meter-container step1">
             <div class="progress-meter"><div class="meter"></div></div> 
             <div class="premium-bg-1"></div>
             <div class="premium-1"><h5 class="sclass">ACCOUNT</h5></div><div class="premium-1-inner tagactive"><i class="fa fa-user activeiciion"></i></div>
             <div class="premium-bg-2"></div>
             <div class="premium-2"><h5 class="sclass dff">BILLING</h5></div><div class="premium-2-inner"><i class="fa fa-building-o notactiveicon"></i></div>
    	     <div class="premium-bg-3"></div>
    	     <div class="premium-3"><h5 class="sclass dff2">CHECKOUT</h5></div><div class="premium-3-inner"><i class="fa fa-credit-card notactiveicon abdioo"></i></div>
    	     <div class="premium-bg-4"></div>
    	     <div class="premium-4"><h5 class="sclass dff3">CONFIRMATION</h5></div><div class="premium-4-inner"><i class="fa fa-check notactiveicon abdioo"></i></div>
            </div>
	
	
	    <!-- page row -->
	    <div class="row">

         <!-- Step 1 -->   
         <div id="step1">

	      <h4 class="h4dd"> ACCOUNT INFORMATION</h4>
          
	      <p class="pdd">Here, type your login information to subscribe into new premium membership</p>

		  <h4 class="h4information"><span>1</span> Account Information :</h4>
          
 		 <?php if(!isset($_SESSION['userToken'])){ ?>
          <?php if($enable_oauth){ ?>
			<div class="premiumLogin text-center">
				<p style="margin-left: 15px;">Sign in / Sign up using social network</p>
				<a href="/?route=facebook&login" class="connect facebook" title="<?php echo $lang['268']; ?>">Facebook</a>
	        	<a href="/?route=google&login" class="connect google" title="<?php echo $lang['269']; ?>">Google</a>  			        

            </div>
        <?php } ?>
         
        <div class="col-md-6">
        
			<form action="/?route=account&register" method="POST" class="loginme-form">
			<div class="premium-body">
	        <h5 class="h5dd"> New Customer</h5>

				<div class="text-center"><?php echo $lang['277']; ?></div>
                <br />
				<div class="form-group">
					<?php echo $lang['271']; ?> <br />
						<input type="text" name="username" class="form-control" />
				</div>	
								<div class="form-group">
					<?php echo $lang['275']; ?> <br />
						<input type="text" name="email" class="form-control" />
				</div>
								<div class="form-group">
					<?php echo $lang['276']; ?> <br />
						<input type="text" name="full" class="form-control" />
				</div>
								<div class="form-group">
					<?php echo $lang['272']; ?> <br />
						<input type="password" name="password" class="form-control" />
				</div>
				</div>
			<div class="premium-footer"> <br />
            	<div class="pull-right align-right">
				<button type="submit" class="btn btn-primary"><?php echo $lang['264']; ?></button>	
                	</div>
			</div>
			 <input type="hidden" name="signup" value="<?php echo md5($date.$ip); ?>" />
			</form>
            
	    </div><!-- /.col-md-6 -->
                
        <div class="col-md-6">
        
        <form method="POST" action="/?route=account&login" class="loginme-form">
			<div class="premium-body">
	        <h5 class="h5dd"> Returning Customer</h5>
          
				<div class="text-center"><?php echo $lang['270']; ?></div>
                <br />
				<div class="form-group">
					<?php echo $lang['271']; ?> <br />
					<input type="text" name="username" class="form-control"  style="width: 96%;"/>
				</div>	
				<div class="form-group">
					<?php echo $lang['272']; ?> <br />
					<input type="password" name="password" class="form-control"  style="width: 96%;" />
				</div>
			</div>
			<div class="premium-footer"> <br />
                    <div class="pull-left align-left"><a style="color: #3C81DE;" href="/?route=account&forget"><?php echo $lang['273']; ?></a><br />
					<a style="color: #3C81DE;" href="/?route=account&resend"><?php echo $lang['274']; ?></a></div>

				<div class="pull-right align-right">
                    <button type="submit" class="btn btn-primary  pull-left"><?php echo $lang['263']; ?></button>
				</div>
			</div>
			 <input type="hidden" name="signin" value="<?php echo md5($date.$ip); ?>" />
			</form> 
            
	    </div><!-- /.col-md-6 -->
        <?php } else {?>
        
         <div class="lockscreen-wrapper">
     
          <div class="lockscreen-item">

            <div class="lockscreen-image text-center">
              <img style="width: 120px; height: 120px;" alt="User Image" src="<?php echo $userLogo; ?>" />
            </div>

             <div class="form-group">
				 Username <br />
				<input value="<?php echo $userInfo['username']; ?>" disabled="" type="text" name="username" class="form-control"  style="width: 96%;"/>
			 </div>
             
             <div class="form-group">
				Email <br />
				<input value="<?php echo $userInfo['email_id']; ?>" disabled="" type="text" name="email" class="form-control"  style="width: 96%;"/>
			 </div>
            <div class="lockscreen-image text-center">
                <a href="#" id="conBtn" class="text-center btn btn-primary"><i class="fa fa-arrow-right"></i> CONTINUE</a>
                <br /><br />
                <a class="text-center" href="&logout" title="Logout">Or sign in as a different user</a>
			</div>        
          </div>
        </div>
   
        <?php } ?>
        </div>
        
        <!-- Step 2 -->
        <div id="step2" class="hide">
        <h4 class="h4dd"> BILLING DETAILS </h4>
          
        <p class="pdd">Here, type your full personal information used for billing/invoice.</p>

        <h4 class="h4information"><span>2</span> Your Personal Details :</h4>
        
        <form> 
        <div class="col-md-6">

 				
                 <div class="form-group">
					First Name <br />
					<input value="<?php echo $firstname; ?>" placeholder="Enter your first name" type="text" name="firstname" class="form-control"  style="width: 96%;"/>
				</div>	
                
                 <div class="form-group">
					Last Name <br />
					<input value="<?php echo $lastname; ?>" placeholder="Enter your last name" type="text" name="lastname" class="form-control"  style="width: 96%;"/>
				</div>
                
                <div class="form-group">
			         Company <br />
					 <input value="<?php echo $company; ?>" placeholder="Enter your company name (optional)" type="text" name="company" class="form-control"  style="width: 96%;"/>
				</div>  
                
                <div class="form-group">
			          Telephone <br />
					 <input value="<?php echo $telephone; ?>" placeholder="Enter your phone no." type="text" name="telephone" class="form-control bfh-phone" data-country="country" style="width: 96%;" />
				</div>
                
                <div class="form-group">
			         Country <br />
					 <select id="country" name="country" class="form-control bfh-countries" data-country="<?php echo $country != '' ? $country : 'IN'; ?>" style="width: 96%;"></select>
				</div>
                
        </div><!-- /.col-md-6 -->
        
        <div class="col-md-6">
           
            <div class="form-group">
		          Address 1 <br />
				 <input value="<?php echo $address1; ?>" placeholder="Enter your home address" type="text" name="address1" class="form-control"  style="width: 96%;"/>
			</div>    
            
            <div class="form-group">
		         Address 2 <br />
				 <input value="<?php echo $address2; ?>" placeholder="Address line 2 (optional)" type="text" name="address2" class="form-control"  style="width: 96%;"/>
			</div> 
            
            <div class="form-group">
		        City <br />
	 	        <input value="<?php echo $city; ?>" placeholder="Enter your city" type="text" name="city" class="form-control"  style="width: 96%;"/>
			</div>
            
            <div class="form-group">
		         Post Code <br />
				 <input value="<?php echo $postcode; ?>" placeholder="Enter your postal code" type="text" name="postcode" class="form-control"  style="width: 96%;"/>
			</div>  
            
            <div class="form-group">
		          Region / State <br />
                  <?php $state = ($state != '' ? 'data-state="'.$state.'"' : ''); ?>
				 <select name="state" class="form-control bfh-states" data-country="country" <?php echo $state; ?> style="width: 96%;"></select>
			</div>
                
        </div><!-- /.col-md-6 -->
        </form>
        </div>
        
        <!-- Step 3 -->
        <div id="step3" class="hide">
        <h4 class="h4dd"> CHECKOUT DETAILS </h4>
          
        <p class="pdd">Here, select your payment type and also you will find information about this plan</p>

        <h4 class="h4information"><span>3</span> Checkout Details :</h4>
        
        <table class="table table-bordered table-striped" align="center" style="width: 94%;">
            <thead>
                <th>Plan Name</th>
                <th>Premium SEO Tools</th>
                <th>PDF Reports</th>
                <th>Branded PDF</th>
            </thead>
                    <tbody>
                    <tr>
                        <td><?php echo $data['plan_name']; ?></td>
                        <td><ul>
                        <?php 
                        foreach($tools as $myTool)
                            echo '<li>'.$myTool[0].'</li>';
                        ?>
                        </ul></td>
                        <td><?php echo $allowPDF; ?></td>
                        <td><?php echo $brandPDF; ?></td>
                    </tr>
                    </tbody>
         </table>

        <table class="table table-bordered" align="center" style="width: 94%;">
            <thead>
                <th>Billing Type</th>
                <th>Amount</th>
            </thead>
                    <tbody>
                    <tr>
                        <td><?php echo $billingType . $recCheckBoxData; ?></td>
                        <td id="planAm"><?php echo $planAmount; ?></td>
                    </tr>
                    </tbody>
         </table>
         
         <div id="paymentGateway" style=" margin: 0 auto; width: 94%;">
            <h4 class="bold">Payment Type:</h4>
            Please select the preferred payment method to use on this order:
                <?php echo $paymentGatewayData; ?>
         </div>
         
         <?php if($checkOutTerms){ ?>
            
            <div id="checkOut" style=" margin: 0 auto; width: 94%;">
                <br />
                <input type="checkbox" id="agree" name="agree" />
                I have read and agree to the <a target="_blank" href="<?php echo $checkOutPage; ?>">Terms and Conditions</a>.
            </div>
            
         <?php } ?>

        </div>
        
        <!-- Step 4 -->
        <div id="step4" class="hide">
        <h4 class="h4dd"> CONFIRM ORDER </h4>
          
        <p class="pdd">Final confirmation before making into payment</p>

        <h4 class="h4information"><span>4</span> Plan CONFIRMATION :</h4>
        
        <table class="table table-bordered table-striped" align="center" style="width: 94%;">
            <thead>
                <th>Plan ID</th>
                <th>Plan Name</th>
                <th>Billing Type </th>
                <th>Price</th>
            </thead>
                    <tbody>
                    <tr>
                        <td>#<?php echo $data['id']; ?></td>
                        <td><?php echo $data['plan_name']; ?></td>
                        <td id="bill_Val"></td>
                        <td id="bill_Price"></td>
                    </tr>
                    </tbody>
         </table>
         
         <div class="text-center" style="width: 94%;">
             <div class="totalLeft">
             <div class="bill6 gray16">
             Billing Address
             </div>
             <div id="invoiceAddress"></div>
             </div>
             <div class="totalRight" id="ajaxTotal">
             </div>
         </div>
        
        </div>
        
        <input type="hidden" name="stage" value="1" />
        <input type="hidden" name="eToken" value="<?php echo $eToken; ?>" />
        <input type="hidden" name="one_amount" value="<?php echo $planAmount; ?>" />
        <input type="hidden" name="billVal" value="<?php echo $data['payment_type']; ?>" />
        <input type="hidden" name="planID" value="<?php echo $data['id']; ?>" />       
        </div><!-- /.row -->
	  
	  
	  	<div class="nextPrev row botton-row">
		     <div class="leftfloat"> <a href="#" id="prevBtn" class="action-button shadow animate green"><i class="fa fa-arrow-left iprev"></i>PREV</a></div>
             <div class="rightfloat"><a href="#" id="nextBtn" class="action-button shadow animate blue"><i class="fa fa-arrow-right"></i> NEXT</a></div>			 
		</div><!-- /.botton-row -->
        
        <?php } else {  ?>
        
        <div class="text-center">
        <div class="alert alert-error">
            <h3>You have an active subscription!</h3>
        </div>
            <p>If you've already bought a subscription and you can't make new subscription without cancelling it.</p>
            
            <a class="text-center btn btn-success" href="/my-dashboard" title="Dashboard">My Dashboard</a>
            <a class="text-center btn btn-danger" href="/my-subscription/cancel" title="Cancelling my subscription">Cancel my subscription</a>
            <a class="text-center btn btn-warning" href="&logout" title="Logout">Sign in as a different user</a>
        </div>
        <?php } ?>
                <br /><br /><br /><br />
    </div>
    <?php 
    // Sidebar 
    require_once(THEME_DIR. "sidebar.php"); 
    ?>
</div>
</div>
<br />
<script src="<?php echo $theme_path; ?>premium/js/bootstrap-formhelpers.min.js" type="text/javascript"></script>
<script src="<?php echo $theme_path; ?>premium/js/subscribe1.js" type="text/javascript"></script>