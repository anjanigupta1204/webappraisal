<?php

defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: Rainbow PHP Framework v1.0
 * @copyright © 2015 ProThemes.Biz
 *
 */
 
?>
<link href="<?php echo $theme_path; ?>css/blog.css" rel="stylesheet" type="text/css" />
        
<div class="container main-container">
      <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8 main-index">
            
       <div class="col-md-12">
        
        <?php
        $loop = 0;
        $first_time =1;

        //we loop through each records
        while($row = mysqli_fetch_array($rsd)) {
            $post_title = $row['post_title'];
            $posted_by = $row['posted_by'];
            $posted_date = $row['date'];
            $category = $row['category'];
            $meta_title = $row['meta_title'];
            $meta_des = $row['meta_des'];
            $tags = $row['meta_tag'];
            $post_url = $row['post_url'];
            $feature_image = Trim($row['featured_image']);
            $page_content = htmlspecialchars_decode($row['post_content']);
            
            $posted_date_raw=date_create($posted_date);
            $post_month = date_format($posted_date_raw,"M");
            $post_day = date_format($posted_date_raw,"j");
    
            if ($feature_image == "") {
            $myimage =  $no_image_path;
            } else {
            $myimage =  $feature_image;
            }

        ?>
        
    <div class="row mr_top30">
        <div class="col-md-12">
          <div>
            <div class="row">
              <div class="col-md-10">
              <div class="title_bar">
                  <a href="/blog/<?php echo $post_url; ?>">
                  <h3 style="font-size:30px;"><?php echo ucfirst($post_title); ?></h3>
                  </a>
              </div>
              
              <div class="text_size12"> 
                  <span class="color_text_in"> 
                    <i class="fa fa-tag color-grey" style="font-size:14px;"></i>in 
                    <b class="color_grap"><a href="/blog/category/<?php echo str_replace(' ','-',$category); ?>"><?php echo ucfirst($category); ?></a></b> 
                  </span> 
                  <span class="color_text_in">
                    <i class="fa fa-user color-grey" style="font-size:14px;"></i> by <b class="color_grap"> <?php echo ucfirst($posted_by); ?></b>
                  </span> 
              </div>
              </div>
                
              <div class="col-md-2 pad_right">
                <div class="date_1 pull-right">
                  <div class="date_up2">
                    <p class="center2 tbox"><?php echo $post_month; ?></p>
                  </div>
                  <div class="date_down2">
                    <p class="text_word"><?php echo $post_day; ?></p>
                  </div>
                </div>
              </div>
            </div>
              <hr style="margin:20px 0;" />
            <div class="row">
              <div class="col-md-4">
                <div class="thumbnail blog-img"> 
                <a href="/blog/<?php echo $post_url; ?>"><img src="/core/library/imagethumb.php?w=180&=180&src=<?php echo $myimage; ?>" alt="<?php echo $post_title; ?>" /></a> 
                </div>
               </div>
               
                <div class="col-md-8 pad_left">
                    
                    <p style="text-align: justify;" class="font_14 version">
                    <?php echo Trim(truncate(strip_tags($page_content), "90", $post_length)); ?>
                    <br /><a style="color: #fff;" href="/blog/<?php echo $post_url; ?>" class="btn btn-info">Read More</a> </p>                    
                </div> 
                
            </div>
          </div>
        </div>
      </div>

      <div class="divider_h mr_top30"></div>
      
      <?php } ?>    
      
      <div class="row mr_top30 mr_bottom1">
        <div class="col-md-5"> </div>
        <div class="col-md-5">
            <br /> <?php echo $pg->process(); ?>
        </div>
      </div>
       
          </div>   		
        </div>
                    
        <?php
        // Sidebar
        require_once(THEME_DIR."sidebar.php");
        ?>  
    </div> 
</div> 
<br />