var comma = angular.module('comma',[]);
comma.controller("MainComma", function($scope){
	
	$scope.settings = {
		newLine : 1,
		delimiter : ',',
		tag_open : '',
		tag_close : '',
		docsv_string : '',
		undocsv_string : '',
		explode : "\n",
		showSettings : false,
		quotes: 'none',
		result_break : 0,
		interval_wrap_open : '',
		interval_wrap_close : '',
		duplicate : false
	};

	$scope.clr = function(settings) {
		settings.docsv_string = '';
		settings.undocsv_string = '';
	}
	$scope.addCommas = function(settings) {
		
		var str = settings.docsv_string;
		var data = str.split(settings.explode);
		var tmp = '';
		var nl = '';
		var quotes = '';
		var arrayTmp = new Array();

		var pre='';

		// are we addning new lines to the string?
		
		if (settings.delimiter == 'Spaces') {
			delimiter = ' ';
		} else if (settings.delimiter == 'New Line') {
			delimiter = "\n";
		} else if (settings.delimiter == '|') { 
			delimiter = "|";
			pre="\\";
		} else if (settings.delimiter == '+') { 
			delimiter = "+";
			pre="\\";
		} else {
			delimiter = settings.delimiter;
		}

		if (!settings.newLine) {
			nl = "\r\n";
		}

		if (settings.quotes == 'double') {
			quotes = '"';
		} else if (settings.quotes == 'single') {
			quotes = '\'';
		}

		// Remove Duplicates
		if (settings.duplicate) {
			index = 0;
			for(i=0; i < data.length; i++) {
				dup = 0;
				for (k=0; k < arrayTmp.length+1; k++) {
					console.log( data[i] + ' : ' + arrayTmp[k] );
					if ( data[i] == arrayTmp[k] ) {
						dup = true;
					}
				}
				if (!dup) {
					arrayTmp[arrayTmp.length] = data[i];
				}
			}
			data = arrayTmp;
		}

		re = new RegExp(pre+settings.delimiter+'+$', 'g');

		if (settings.result_break >= 1) {
			start = true;
			for(i=0; i < data.length; i++) {

				if ( start == true) {
					start_wrap = settings.interval_wrap_open;
					start = false;
				} else {
					start_wrap = '';
				}

				tmp = tmp + start_wrap + settings.tag_open + quotes + data[i] + quotes + settings.tag_close + delimiter + nl;

				if ( (i+1) % settings.result_break == 0) {
					start = true;
					tmp = tmp.replace(re,'');
					tmp = tmp + settings.interval_wrap_close
					tmp = tmp + "\r\n";
				}
			}

			if ( !start ) {
				tmp = tmp.replace(re,'');
				tmp = tmp + settings.interval_wrap_close;
			}
		} else {
			for(i=0; i < data.length; i++) {
				tmp = tmp + settings.tag_open + quotes + data[i] + quotes + settings.tag_close + delimiter + nl;
			}
		}

		settings.undocsv_string = tmp.replace(re,'');
	}
	$scope.removeCommas = function(settings) {
		var pre='';
		if (settings.delimiter == 'Spaces') {
			delimiter = ' ';
		} else if (settings.delimiter == 'New Line') {
			delimiter = "\n";
		} else if (settings.delimiter == '|') { 
			delimiter = "|";
			pre="\\";
		} else if (settings.delimiter == '+') { 
			delimiter = "+";
			pre="\\";
		} else {
			delimiter = settings.delimiter;
		}
		
		quotes ='';
		if (settings.quotes == 'double') {
			quotes = '"';
		} else if (settings.quotes == 'single') {
			quotes = '\'';
		}

		var str = settings.undocsv_string;
		
		// Undoing add commas. // Replace all the delimiteres with the explode settings
		var re = new RegExp(pre+delimiter, "g");
		str = str.replace(re, settings.explode);

		var re = new RegExp(quotes, "g");
		str = str.replace(re, '');

		var re = new RegExp(settings.tag_open, "g");
		str = str.replace(re, '');

		var re = new RegExp(settings.tag_close, "g");
		str = str.replace(re, '');
		//var data = str.split(settings.delimiter);

		settings.docsv_string = str;
	}
	$scope.toggleSettings = function(settings) {
		if (settings.showSettings)
			settings.showSettings = false;
		else
			settings.showSettings = true;
	}
});