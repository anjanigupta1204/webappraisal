<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name A to Z SEO Tools - PHP Script
 * @copyright � 2015 ProThemes.Biz
 *
 */
?>
<style>
#outTable td{
    font-weight: bold;
    color: #2c3e50;
}
</style>
  <div class="container main-container">
	<div class="row">
      	
          	<div class="col-md-8 main-index">
            
            <div class="xd_top_box">
             <?php echo $ads_720x90; ?>
            </div>
            
              	<h2 id="title"><?php echo $data['tool_name']; ?></h2>
                
                <br />
                
                <?php if ($pointOut != 'output') { ?>
      
               <form method="POST" action="<?php echo $toolOutputURL;?>" onsubmit="return doCheck();"> 
			
            	<div class="form-group">
					<label>Daily page impressions</label>
					<input placeholder="Type your daily page impressions" type="text" name="impressions" class="form-control" />
				</div>	
                
               	<div class="form-group">
					<label>Page CTR (in percentage %)</label>
					<input placeholder="Type your CTR in %" type="text" name="ctr" class="form-control" />
				</div>
                
   	            <div class="form-group">
					<label>Cost per click ($)</label>
					<input placeholder="Type your cost per click" type="text" name="cpr" class="form-control" />
				</div>		
                
               <br />
               <?php
               if ($toolCap)
               {
               echo $captchaCode;   
               }
               ?>
               <div class="text-center">
                   <input class="btn btn-info" type="submit" value="Calculate Earning" name="submit"/>
               </div>
               </form>     
                          
               <?php 
               } else { 
               //Output Block
               if(isset($error)) {
                
                echo '<br/><br/><div class="alert alert-error">
                <strong>Alert!</strong> '.$error.'
                </div><br/><br/>
                <div class="text-center"><a class="btn btn-info" href="'.$toolURL.'">'.$lang['12'].'</a>
                </div><br/>';
                
               } else {
               ?>
        <h3 id="outBox">Earning Result</h3>
        <br />
              <table class="table table-bordered table-striped">
                <tbody>
                    <tr><td style="width: 50%;">Daily page impressions</td><td><?php echo $impressions; ?></td></tr>
                    <tr><td style="width: 50%;">Page CTR</td><td><?php echo $ctr; ?>%</td></tr>
                    <tr><td style="width: 50%;">Cost per click</td><td>$<?php echo $cpr; ?></td></tr>
             </tbody></table>
             
             <div class="row">
             
             <div class="col-md-6">
              <table id="outTable" class="table table-bordered table-striped">
                <tbody>
                    <tr><td style="width: 50%;">Daily Earnings</td><td>$<?php echo $dailyEarnings; ?></td></tr>
                    <tr><td style="width: 50%;">Monthhly Earnings</td><td>$<?php echo $monthlyEarnings; ?></td></tr>
                    <tr><td style="width: 50%;">Yearly Earnings</td><td>$<?php echo $yearlyEarnings; ?></td></tr>
             </tbody></table>
             </div>
             
             <div class="col-md-6">
              <table id="outTable" class="table table-bordered table-striped">
                <tbody>
                    <tr><td style="width: 50%;">Daily Clicks</td><td><?php echo $dailyClicks; ?></td></tr>
                    <tr><td style="width: 50%;">Monthly Clicks</td><td><?php echo $monthlyClicks; ?></td></tr>
                    <tr><td style="width: 50%;">Yearly Clicks</td><td><?php echo $yearlyClicks; ?></td></tr>
             </tbody></table>
             </div>
             
             </div>

    <div class="text-center">
    <br /> &nbsp; <br />
    <a class="btn btn-info" href="<?php echo $toolURL; ?>">Try New Values</a>
    <br />
    </div>

<?php } } ?>

<br />

<div class="xd_top_box">
<?php echo $ads_720x90; ?>
</div>

<h2 id="sec1" class="about_tool"><?php echo $lang['11'].' '.$data['tool_name']; ?></h2>
<p>
<?php echo $data['about_tool']; ?>
</p> <br />
</div>              
            
<?php
// Sidebar
require_once(THEME_DIR."sidebar.php");
?>     		
        </div>
    </div> <br />
    
<script>
<?php if ($pointOut == 'output') { ?>
jQuery(document).ready(function(){
    setTimeout(function(){
	var pos = $('#outBox').offset();
	$('body,html').animate({ scrollTop: pos.top });
	}, 100);
});
<?php } ?>
function doCheck(){
    var impressions = jQuery.trim($('input[name=impressions]').val());
    var cpr = jQuery.trim($('input[name=cpr]').val());
    var ctr = jQuery.trim($('input[name=ctr]').val());
    if (ctr == null || ctr== "" || cpr == null || cpr == "" || impressions == null || impressions == "") {
        alert("All fields must be filled out!");
        return false;
    }
    return true;
}
</script>