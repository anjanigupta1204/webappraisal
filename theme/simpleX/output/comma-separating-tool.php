<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name A to Z SEO Tools - PHP Script
 * @copyright � 2015 ProThemes.Biz
 *
 */
$noBootstrap = true;
?>
<link rel="stylesheet" type="text/css" href="<?php echo $theme_path; ?>css/comma.css" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js" ></script>
<script type="text/javascript" src="<?php echo $theme_path; ?>js/bootstrap.min1.js"></script>
<script type="text/javascript" src="<?php echo $theme_path; ?>js/jquery-linedtextarea.js"></script>
<script type="text/javascript" src="<?php echo $theme_path; ?>js/comma.js" ></script>

  <div class="container main-container">
	<div class="row">
      	
          	<div class="col-md-8 main-index">
            
            <div class="xd_top_box">
             <?php echo $ads_720x90; ?>
            </div>
            
              	<h2 id="title"><?php echo $data['tool_name']; ?></h2>

               <?php if ($pointOut != 'output') { ?>
                
  	<section ng-app='comma' ng-controller='MainComma' class="delimwrap">
		<div class="col-xs-5 col-hack">
			<div class="workspace vcenter">
				<h2 class="text-primary">Column Data Here...</h2>
				<textarea class="codearea scrollbars" ng-model="settings.docsv_string">{{settings.docsv_string}}</textarea>
				<div class="copy">Select</div>
			</div>
		</div>
		<div class="col-xs-2 col-hack-sm">
			<div class="push col-xs-12">
				<div class="col-xs-12">
					<div class="btn-group">
					<button type="button" class="btn btn-default dropdown-toggle dropdown-delim" data-toggle="dropdown">
					  {{settings.delimiter}}
					  <span class="caret"></span>
					</button>
					<ul class="dropdown-menu">
						<li><a ng-click="settings.delimiter=','">,</a></li>
						<li><a ng-click="settings.delimiter=';'">;</a></li>
						<li><a ng-click="settings.delimiter='|'">|</a></li>
						<li><a ng-click="settings.delimiter='Spaces'">Spaces</a></li>
						<li><a ng-click="settings.delimiter='New Line'">New Line</a></li>
					 </ul>
					</div>
				</div>
				<div class=" col-xs-12">
					<a ng-click='addCommas(settings)' class="btn btn-primary btn-convert"><span class="glyphicon glyphicon-chevron-right"></span></a>
				</div>
				<div class=" col-xs-12">
					<a ng-click='removeCommas(settings)' class="btn btn-primary btn-convert"><span class="glyphicon glyphicon-chevron-left"></span></a>
				</div>
				<div class="col-xs-12">
					<a ng-click='clr(settings)' class="btn btn-danger btn-clear"><span class="glyphicon glyphicon-remove"></span></a>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="col-xs-5 col-hack">
			<div class="workspace vcenter">
				<h2 class="text-primary">Delimited Data Here...</h2>
				<textarea name='undo_csv' class="codearea scrollbars" ng-model="settings.undocsv_string">{{settings.undocsv_string}}</textarea>
				<div class="copy">Select</div>
			</div>
		</div>
		<div class="clearfix"></div>

		<div class="col-xs-12">

			<div class="settings" ng-click="showSettings" ng-class="{open : settings.showSettings == true}">
				<div class="settings_btn">
					<a href="" id="settings" type="button"  ng-click="toggleSettings(settings)" >
						<span class="glyphicon glyphicon-cog"></span> Converter Settings
					</a>
				</div>
				<form>
					<fieldset>
					<div class="col-md-6 dg">
						<div class="form-group">
							<div class="col-sm-2 col-md-4 lbl">
								<label>Tidy Up</label><br/>
								<p>Remove the new lines from output?</p> 
							</div>
							<div class="col-xs-12 col-sm-10 col-md-8">
								<div class="options">
									<div class="row">
										<div class="btn-group" data-toggle="buttons-radio">
											<div class="col-xs-12 col-sm-6 col-md-3">
												<button type="button" class="btn btn-primary block active" ng-click="settings.newLine=1">Yes</button>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-3">
												<button type="button" class="btn btn-primary block"  ng-click="settings.newLine=0">No</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="form-group">
							<div class="col-sm-2 col-md-4 lbl">
								<label>Attack the clones</label><br/>
								<p>Remove the duplicates from the result set</p> 
							</div>
							<div class="col-xs-12 col-sm-10 col-md-8">
								<div class="options">
									<div class="row">
										<div class="btn-group" data-toggle="buttons-radio">
											<div class="col-xs-12 col-sm-6 col-md-3">
												<button type="button" class="btn btn-primary block" ng-click="settings.duplicate=1">Yes</button>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-3">
												<button type="button" class="btn btn-primary block active"  ng-click="settings.duplicate=0">No</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="form-group">
							<div class="col-sm-2 col-md-4 lbl">
								<label>Explode</label><br/>
								<p>Explode your records using this</p> 
							</div>
							<div class="col-xs-12 col-sm-10 col-md-8">
								<div class="options">
									<div class="row">
										<div class="btn-group" data-toggle="buttons-radio">
											<div class="col-xs-12 col-sm-6 col-lg-3">
												<button type="button" class="btn btn-primary block active" ng-click="settings.explode='\n'">New Lines</button>
											</div>
											<div class="col-xs-12 col-sm-6 col-lg-3">
												<button type="button" class="btn btn-primary block"  ng-click="settings.explode=' '">Spaces</button>
											</div>
											<div class="col-xs-12 col-sm-6 col-lg-3">
												<button type="button" class="btn btn-primary block"  ng-click="settings.explode=','">Commas</button>
											</div>
											<div class="col-xs-12 col-sm-6 col-lg-3">
												<button type="button" class="btn btn-primary block"  ng-click="settings.explode=';'">Semicolons</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="form-group">
							<div class="col-sm-2 col-md-4 lbl">
								<label>Quotes</label><br/>
								<p>Add quotes to each record</p> 
							</div>
							<div class="col-xs-12 col-sm-10 col-md-8">
								<div class="options">
									<div class="row">
										<div class="btn-group" data-toggle="buttons-radio">
											<div class="col-xs-12 col-sm-4">
												<button type="button" class="btn btn-primary active block"  ng-click="settings.quotes='none'">No</button>
											</div>
											<div class="col-xs-12 col-sm-4">
												<button type="button" class="btn btn-primary block"  ng-click="settings.quotes='double'">Double</button>
											</div>
											<div class="col-xs-12 col-sm-4">
												<button type="button" class="btn btn-primary block"  ng-click="settings.quotes='single'">Single</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="col-md-6 dg">
						<div class="form-group">
							<div class="col-sm-2 col-md-4 lbl">
								<label>Delimiter</label><br/>
								<p>Character used between records</p> 
							</div>
							<div class="col-xs-12 col-sm-10 col-md-8">
								<div class="options">
									<input class="form-control input-sm" type="text" id="delimiter" value="," name="delimiter" ng-model="settings.delimiter">
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="form-group">
							<div class="col-sm-2 col-md-4 lbl">
								<label>Tags </label>
								<p>Use Tags to wrap your records. EX :  &lt;strong&gt; <br /></p> 							
							</div>
							<div class="col-xs-12 col-sm-10 col-md-8">
								<div class="options">
									<div class="row">
										<div class="col-md-6">
											<label>Open Tag</label>
											<input class="form-control input-sm" type="text" id="delimiter" value="" name="tag_open" ng-model="settings.tag_open"  placeholder="<li>"> 
										</div>
										<div class="col-md-6">
											<label>Close Tag</label>
											<input class="form-control input-sm" type="text" id="delimiter" value="" name="tag_close" ng-model="settings.tag_close"  placeholder="</li>">
										</div>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="form-group">
							<div class="col-sm-2 col-md-4 lbl">
								<label>Interval</label>
								<p>Add a new line after x amount </p>
							</div>
							<div class="col-xs-12 col-sm-10 col-md-8">
								<div class="options">
									<input class="form-control input-sm" type="text" id="result_break" value="" name="result_break" ng-model="settings.result_break">
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="form-group">
							<div class="col-sm-2 col-md-4 lbl">
								<label>Interval Wrap </label>
								<p>Wrap your intervals with tags</p>
							</div>
							<div class="col-xs-12 col-sm-10 col-md-8">
								<div class="options">
									<div class="row">
										<div class="col-md-6">
											<label>Open Tag</label>
											<input class="form-control input-sm" type="text" id="interval_wrap_open" value="" name="interval_wrap_open" ng-model="settings.interval_wrap_open" placeholder="<ul>"> 
										</div>
										<div class="col-md-6">
											<label>Close Tag</label>
											<input class="form-control input-sm" type="text" id="interval_wrap_close" value="" name="interval_wrap_close" ng-model="settings.interval_wrap_close"  placeholder="</ul>">
										</div>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					</fieldset>
				</form>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-xs-12">
		<div class="clearfix"></div>
	</section>


                <?php  } ?>

<br />
<div class="xd_top_box">
<?php echo $ads_720x90; ?>
</div>

<h2 id="sec1" class="about_tool"><?php echo $lang['11'].' '.$data['tool_name']; ?></h2>
<p>
<?php echo $data['about_tool']; ?>
</p> <br />
</div>              
            
<?php
// Sidebar
require_once(THEME_DIR."sidebar.php");
?>     		
        </div>
    </div> <br />
<script type="text/javascript">
$(function() {

	$(".copy").on('click', function() {
		$(this).parent().find('.codearea').select();
	});
	$(".codearea").linedtextarea();
	$('a[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top
				}, 1000);
				return false;
			}
		}
	});
	$(window).on('resize', function() {
		width = $('.workspace').outerWidth();
		$('.codearea').css('width', (width - $('.codelines').outerWidth() - 10 ));
	});
});
</script>