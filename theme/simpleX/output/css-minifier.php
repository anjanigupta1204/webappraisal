<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name A to Z SEO Tools - PHP Script
 * @copyright � 2016 ProThemes.Biz
 *
 */
?>
<style>
.inputCenter{
    text-align: center;
    margin-top: 20px !important;
}
.progress-striped {
    background-image: linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
    background-size: 40px 40px;
}
.progress-bar.active, .progress.active {
    animation: 2s linear 0s normal none infinite running progress-bar-stripes;
}
.input-group{
    margin-top: 12px !important;
}
.btnSave{
    margin-top: 10px;
}
.cssBtn{
    border: 1px solid #c3e8f4;
    border-radius: 5px;
    box-shadow: 0 0 0 4px #f0fbff;
    padding: 7px 12px !important;
}
.cssFormMain{
    display: table;
    margin: 0 auto;
    width: 60%;
}
.input-group-btn:last-child > .btn, .input-group-btn:last-child > .btn-group {
    margin-left: 5px !important;
}
.table tbody>tr>td {
    vertical-align: middle;
}
</style>
<script>
var loadCount = 1;
jQuery(document).ready(function(){
    jQuery("#addCSSFile").click(function() {
        $("#cssFormData").append('<input id="inputCSS" type="file" name="cssUpload[]" class="filestyle" />');
        loadCount++;
        $("input[type=file]").filestyle({
            iconName: "fa fa-file-text",
            buttonName: "btn-info cssBtn",
            placeholder: "No file selected."
        });
        if(loadCount == 10){
            $("#addCSSFile").addClass('disabled');
        }
    });
});
</script>
  <div class="container main-container">
	<div class="row">
      	
          	<div class="col-md-8 main-index" id="cssMainBox">
            
            <div class="xd_top_box">
             <?php echo $ads_720x90; ?>
            </div>
            
              	<h2 id="title"><?php echo $data['tool_name']; ?></h2>

               <?php if ($pointOut != 'output') { ?>
               <br />
               
               <ul class="nav nav-tabs">
                  <li class="<?php echo $tab1; ?>"><a href="<?php echo $toolURL; ?>">Copy & Paste CSS Code</a></li>
                  <li class="<?php echo $tab2; ?>"><a href="<?php echo $toolURL; ?>/upload">Upload CSS Files</a></li>
                </ul>
                
               <div class="tab-content">
               
               <div id="cssBox" class="tab-pane <?php echo $tab1; ?>">  
               
                   <br />
                   <p>Enter your CSS code to compress:
                   </p>
                   
                   <form method="POST" action="<?php echo $toolOutputURL;?>" onsubmit="return fixData();"> 
                   <textarea placeholder="Enter your CSS codes here..." name="data" id="data" rows="3" style="height: 270px;" class="form-control"></textarea>
                   <br />
                   <?php
                   if ($toolCap)
                   {
                   echo $captchaCode;  
                   }
                   ?>
                   <div class="text-center">
                   <input class="btn btn-info" type="submit" value="<?php echo $lang['8']; ?>" name="submit"/>
                   </div>
                   </form>     
               </div>  
               
               <div id="cssUploadBox" class="tab-pane <?php echo $tab2; ?>">
                <div class="inputCenter"> 
                   <br />
                   <p>Add up to 10 multiple CSS files <small>(Size Limit: 2MB per file)</small>
                   </p>
    
                   <form enctype="multipart/form-data" method="post" action="<?php echo $toolOutputURL;?>">
                       <div class="cssFormMain">
                       <input data-iconName="fa fa-file-text" data-buttonName="btn-info cssBtn" data-placeholder="No file selected." id="inputCSS" type="file" name="cssUpload[]" class="filestyle" />
                      
                       <div id="cssFormData"></div>
                       </div>
                       <div class="btnSave">
                           <a class="btn btn-danger" id="addCSSFile">Add Another CSS File</a>   
                           <input class="btn btn-success" type="submit" value="Compress Files" name="submit"/>
                       </div>
                   </form>
                   <br />  
                </div>
               </div>
 
               <br />  
               
               </div>           
               <?php 
               } else { 
               //Output Block
               if(isset($error)) {
                
                echo '<br/><br/><div class="alert alert-error">
                <strong>Alert!</strong> '.$error.'
                </div><br/><br/>
                <div class="text-center"><a class="btn btn-info" href="'.$toolURL.'">'.$lang['12'].'</a>
                </div><br/>';
                
               } else {
               ?>
               <br />
               <?php 
               if ($_FILES['cssUpload']) {
                   $count = 1;
               ?>
               <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>File Name</th>
                            <th>Compression Ratio</th>
                            <th>Download</th>
                        </tr>
                    </thead>
                    <tbody>
               <?php
                   foreach($comArr as $data){
                     echo '<tr><td>'.$count.'.</td>';
                     if($data[5]){
                     echo '<td>'.$data[3].'<textarea id="dlID'.$count.'" style="display:none;" class="form-control">'.$data[1].'</textarea>';
                     echo '</td><td>  <small>Original File</small><div class="progress" style="margin-bottom: 5px;">
                        <div class="progress-bar progress-bar-danger progress-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
                          '.size_as_kb($data[0]).' Kb
                      </div>
                      </div>
                    
                      <small>Compressed File</small>
                       <div class="progress" style="margin-bottom: 5px;">
                        <div class="progress-bar progress-bar-success progress-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:'.$data[4].'%">
                          '.size_as_kb($data[2]).' Kb
                      </div>
                      </div></td><td><a onclick="saveAsFile(\'dlID'.$count.'\',\''.$data[3].'\')" class="btn btn-primary" title="Save as CSS File">Download</a>';
                     }else{
                        echo '<td colspan="3" style="text-align: center; color:#d14233;"><br><b>'.$data[1].'</b><br>&nbsp;';
                      }
                      echo '</td></tr>';
                     $count++;
                   }
               ?>
                    </tbody>
               </table>
              
               <br />
               
               <div class="text-center">
                <a class="btn btn-info" href="<?php echo $toolURL; ?>/upload">Upload New CSS Files</a>
                <br />
                </div>
               <?php
               } else { 
               ?>
              
               <p>Optimized and Compressed CSS Code:</p>
               <textarea id="optimizedCSS" onclick="this.focus();this.select()" readonly="readonly" rows="3" style="height: 270px;" class="form-control"><?php echo $comCss; ?></textarea>
              
               <br />
               
               	<div class="row">
          	    <div class="col-md-6">
               
                   <small>Original File</small><div class="progress" style="margin-bottom: 5px;">
                    <div class="progress-bar progress-bar-danger progress-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
                      <?php echo size_as_kb($fileCSSSize); ?> Kb
                  </div>
                  </div>
                
                  <small>Compressed File</small>
                   <div class="progress" style="margin-bottom: 5px;">
                    <div class="progress-bar progress-bar-success progress-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percentage; ?>%">
                      <?php echo size_as_kb($comCSSSize); ?> Kb
                  </div>
                  </div>
              
                </div>
                <div class="col-md-6">
                
                <div class="text-center">
                 &nbsp; <br />
                <a onclick="saveAsFile('optimizedCSS','output.css')" class="btn btn-success">Download</a><br /> &nbsp; <br />
                <a class="btn btn-info" href="<?php echo $toolURL; ?>">Try New CSS Document</a>
                <br />
                </div>
                
                </div>
                </div>
                

                      

                
               <?php } ?>
 
<?php } } ?>

<br />

<div class="xd_top_box">
<?php echo $ads_720x90; ?>
</div>

<h2 id="sec1" class="about_tool"><?php echo $lang['11'].' '.$data['tool_name']; ?></h2>
<p>
<?php echo $data['about_tool']; ?>
</p> <br />
</div>              
            
<?php
// Sidebar
require_once(THEME_DIR."sidebar.php");
?>     		
        </div>
    </div> <br />
<script src="<?php echo $theme_path; ?>js/bootstrap-filestyle.min.js"></script>
<script>
jQuery(document).ready(function(){
    $(":file").filestyle();
    setTimeout(function(){
        var pos = $('#cssMainBox').offset();
        $('body,html').animate({ scrollTop: pos.top });
    }, 500);
});
function saveAsFile(boxId,fileNameToSaveAs) {      
    var textToWrite = document.getElementById(boxId).value;
    var textFileAsBlob = new Blob([textToWrite], {type:'text/plain'});
    var downloadLink = document.createElement("a");
    downloadLink.download = fileNameToSaveAs;
    downloadLink.innerHTML = "My Link";
    window.URL = window.URL || window.webkitURL;
    downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
    downloadLink.onclick = destroyClickedElement;
    downloadLink.style.display = "none";
    document.body.appendChild(downloadLink);
    downloadLink.click();
}

function destroyClickedElement(event){
    document.body.removeChild(event.target);
}
</script>