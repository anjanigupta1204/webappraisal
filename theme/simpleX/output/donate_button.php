<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name A to Z SEO Tools - PHP Script
 * @copyright © 2015 ProThemes.Biz
 *
 */
?>
<style>
.form-control {
    box-shadow: none;
}
#result{
    display: none;
}
</style>
<!-- Bootstrap Color Picker CSS -->
<link href="/theme/default/css/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet"/>
<script src="/theme/default/js/plugins/colorpicker/bootstrap-colorpicker.min.js" type="text/javascript"></script>
<script> 
  $(function() { $('.my-colorpicker1').colorpicker(); }); 

function livePre(){
  var btnColor = $("#backColor").val();
  var textColor = $("#textColor").val();
  var plusColor = $("#plusColor").val();
  var btnText = $("#btnText").val();
  var donateType = jQuery.trim($('select[name=position]').val());
  var bottomType = '1';
  var yourPaypal = $("#paypalEmail").val();
  
  if(donateType == '1')
    donateType = 'left';
  else if(donateType == '2')
    donateType = 'right';
  else if(donateType == '3'){
    donateType = 'bottom';
    bottomType = '1';
  } else if(donateType == '4'){
    donateType = 'bottom';
    bottomType = '3';
  } else if(donateType == '5'){
    donateType = 'bottom';
    bottomType = '2';
  }
    window.open("../core/library/donatejs/iframe.html?demo=1&yourPaypal="+yourPaypal+"&bottomType="+bottomType+"&donateType="+donateType+"&btnColor="+btnColor+"&textColor="+textColor+"&plusColor="+plusColor+"&btnText="+btnText+"", "_blank", "toolbar=no,status=yes,scrollbars=yes,location=yes,menubar=no,directories=yes,width=1024,height=768");
  return true;
}

function selectElementText(el, win) {
    win = win || window;
    var doc = win.document, sel, range;
    if (win.getSelection && doc.createRange) {
        sel = win.getSelection();
        range = doc.createRange();
        range.selectNodeContents(el);
        sel.removeAllRanges();
        sel.addRange(range);
    } else if (doc.body.createTextRange) {
        range = doc.body.createTextRange();
        range.moveToElementText(el);
        range.select();
    }
}


function donateGenCode(){
  var btnColor = $("#backColor").val();
  var textColor = $("#textColor").val();
  var plusColor = $("#plusColor").val();
  var btnText = $.trim($("#btnText").val());
  if(btnText == ''){
    alert('Enter your button text!');
    return false;
  }
  btnText =  btnText.replace(/ /g, "");
  btnText = btnText.split('').join(' ');
  var donateType = $.trim($('select[name=position]').val());
  var bottomType = '1';
  var yourPaypal = $("#paypalEmail").val();
  if(yourPaypal == ''){
    alert('Enter your PayPal ID!');
    return false;
  }
  var dataBox = '';
  var pathData = '<?php echo 'http://'.$_SERVER['HTTP_HOST'].'/core/library/donatejs/'; ?>';
  if(donateType == '1')
    donateType = 'left';
  else if(donateType == '2')
    donateType = 'right';
  else if(donateType == '3'){
    donateType = 'bottom';
    bottomType = '1';
  } else if(donateType == '4'){
    donateType = 'bottom';
    bottomType = '3';
  } else if(donateType == '5'){
    donateType = 'bottom';
    bottomType = '2';
  }
  
  dataBox = "&lt;script&gt; var yourPaypal = '"+yourPaypal+"'; var textColor = '"+textColor+"'; var btnColor = '"+btnColor+"'; var plusColor = '"+plusColor+"'; var btnText = '"+btnText+"'; var donateType = '"+donateType+"'; var bottomType = '"+bottomType+"'; \r\n";
  if(donateType == 'left'){
    dataBox = dataBox + "document.write('&lt;script src=\""+pathData+"screen.js?url='+encodeURIComponent(document.location.href)+'\"&gt;&lt;/scr'+'ipt&gt;')&lt;/script&gt;";
  }else if(donateType == 'right'){
    dataBox = dataBox + "document.write('&lt;script src=\""+pathData+"screen.js?url='+encodeURIComponent(document.location.href)+'\"&gt;&lt;/scr'+'ipt&gt;')&lt;/script&gt;";
  }else{
    dataBox = dataBox + "document.write('&lt;script src=\""+pathData+"bottom.js?url='+encodeURIComponent(document.location.href)+'\"&gt;&lt;/scr'+'ipt&gt;')&lt;/script&gt;";
  }
  jQuery("#result").fadeIn();
  jQuery("#resultData").html(dataBox);
  return true;
}

function selPre(){
    selectElementText(document.getElementById("resultData"));
}

</script>

  <div class="container main-container">
	<div class="row">
      	
          	<div class="col-md-8 main-index">
            
            <div class="xd_top_box">
             <?php echo $ads_720x90; ?>
            </div>
            
              	<h2 id="title"><?php echo $data['tool_name']; ?></h2>

               <?php if ($pointOut != 'output') { ?>
               <br />
               
               <div class="row">
                <div class="col-md-6">
                   <label>Background Button Color</label>
                   <div class="input-group my-colorpicker1 colorpicker-element">                                            
                      <input type="text" id="backColor" name="backColor" class="form-control" value="#000000" />
                      <div class="input-group-addon">
                        <i style="background-color: #000000;"></i>
                      </div>
                   </div>
                   
                   <br />
                   
                   <label>Text Color</label>
                   <div class="input-group my-colorpicker1 colorpicker-element">                                            
                      <input type="text" id="textColor" name="textColor" class="form-control" value="#ffffff" />
                      <div class="input-group-addon">
                        <i style="background-color: #ffffff;"></i>
                      </div>
                   </div>
                   
                   <br />
                   
                   <label>Position</label>
                   <select id="position" name="position" class="form-control">                            
                        <option selected="" value="1">Left</option>
                        <option value="2">Right</option>
                        <option value="3">Footer Left</option>
                        <option value="4">Footer Right</option>
                        <option value="5">Footer Middle</option>
                   </select>
                   
                </div>
                
                <div class="col-md-6">
                
                <label>Plus Button Background Color</label>
               <div class="input-group my-colorpicker1 colorpicker-element">                                            
                  <input type="text" id="plusColor" name="plusColor" class="form-control" value="#3CB9D8" />
                  <div class="input-group-addon">
                    <i style="background-color: #3CB9D8;"></i>
                  </div>
               </div>
               
               <br />
               
               <label>Button Text</label>                                      
               <input maxlength="7" type="text" id="btnText" name="btnText" class="form-control" value="DONATE" />
              
               <br />
               
               <label>PayPal Email</label>                                      
               <input type="text" id="paypalEmail" name="paypalEmail" class="form-control" placeholder="eg. user@example.com" />
                
                </div>
                
               </div>
               <br />  <br />
               <div class="text-center">
               <a class="btn btn-success" onclick="livePre();">Live Preview</a>
               <a class="btn btn-info" onclick="donateGenCode();">Generate</a>
               </div>
               
               <div id="result"><br />  <br /> <h4 class="text-center">Add a donate button to your website</h4>
                <pre onclick="selPre();" class="well" id="resultData"></pre>
                <br />
                <div class="alert alert-warning">
                    <strong>Note:</strong> &nbsp; Paste the above code before closing your body tag and also <a target="_blank" href="https://jquery.com/download/">Jquery</a> library is required to execute the code.
                </div>
                
               </div>      
                          
               <?php 
               } else { 
               //Output Block
               if(isset($error)) {
                
                echo '<br/><br/><div class="alert alert-error">
                <strong>Alert!</strong> '.$error.'
                </div><br/><br/>
                <div class="text-center"><a class="btn btn-info" href="'.$toolURL.'">'.$lang['12'].'</a>
                </div><br/>';
                
               } else {
               ?>
<br />

                                    <br />
   
    
    <div class="text-center">
    <br /> &nbsp; <br />
    <a class="btn btn-info" href="<?php echo $toolURL; ?>"><?php echo $lang['27']; ?></a>
    <br />
    </div>

<?php } } ?>

<br />

<div class="xd_top_box">
<?php echo $ads_720x90; ?>
</div>

<h2 id="sec1" class="about_tool"><?php echo $lang['11'].' '.$data['tool_name']; ?></h2>
<p>
<?php echo $data['about_tool']; ?>
</p> <br />
</div>              
            
<?php
// Sidebar
require_once(THEME_DIR."sidebar.php");
?>     		
        </div>
    </div> <br />