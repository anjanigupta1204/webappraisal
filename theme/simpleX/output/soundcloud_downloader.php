<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name A to Z SEO Tools - PHP Script
 * @copyright © 2015 ProThemes.Biz
 *
 */
if($pointOut == 'playlist')
    $tab2 = 'active';
else
    $tab1 = 'active';
?>

<style>
.percentbox {
    text-align: center;
    font-size: 18px;
}
.percentimg {
    text-align: center;
    display: none;
}
#resultBox{
    display:none;
}
</style>
<script>
var msgDomain = "<?php echo $lang['330']; ?>";
var msgTab1 = "<?php echo $lang['46']; ?>";
var msgTab2 = "Download";
var msgTab3 = "<?php echo $lang['144']; ?>";
</script>
<script src='../core/library/soundcloud.js'></script>

  <div class="container main-container">
	<div class="row">
      	
          	<div class="col-md-8 main-index">
            
            <div class="xd_top_box">
             <?php echo $ads_720x90; ?>
            </div>
            
            <h2 id="title"><?php echo $data['tool_name']; ?></h2>
            <br />
            <ul class="nav nav-tabs">
              <li class="<?php echo $tab1; ?>"><a href="<?php echo $toolURL; ?>">Tracks</a></li>
              <li class="<?php echo $tab2; ?>"><a href="<?php echo $toolURL; ?>/playlist">Playlist</a></li>
            </ul>
            
            <div class="tab-content">
              <div id="tracks" class="tab-pane <?php echo $tab1; ?>">
              <?php if($pointOut != 'playlist'){ ?>
                <div id="mainbox">
                   <?php if ($pointOut != 'output') { ?>
                   <br />
                   <p><?php echo $lang['320']; ?>
                   </p>
                   <textarea placeholder="An Example URL : https://soundcloud.com/newpartyarmy/tell-me-the-truth" class="form-control" name="linksBox" id="linksBox" rows="3" style="height: 270px;"></textarea>
                   <br />
                   <?php
                   if ($toolCap)
                   {
                   echo $captchaCode;  
                   }
                   ?>
                   <div class="text-center">
                   <a class="btn btn-info" style="cursor:pointer;" id="checkButton">Download</a>
                   </div>     
                   </div>           
                   <?php 
                   } 
                   ?>
                <div id="resultBox">
                <div class="percentimg">
                <br /><br />
                <img src="<?php echo $theme_path; ?>img/load.gif" />
                <br />
                <?php echo $lang['146']; ?>...
                <br />
                </div>
                <br />
                <div class="alert alert-warning text-center">
                    Note: If clicking on the download button doesn't start the downloading then Right Click and Save Link As to save the file.
                </div>
                <div id="results"></div>
    
                <div class="text-center">
                <br /> &nbsp; <br />
                <a class="btn btn-info" href="<?php echo $toolURL; ?>">Download Another Tracks</a>
                <br />
                </div>
                </div>
                <?php } ?>
              </div>
              <div id="playlist" class="tab-pane <?php echo $tab2; ?>">
              <?php if($pointOut == 'playlist'){ ?>
                <div id="mainbox">
                   <?php if ($pointOut != 'output') { ?>
                   <br />
                   <p>Enter the Playlist URL:
                   </p>
                   <input placeholder="eg. https://soundcloud.com/bhupinder-heer/sets/kishore-kumar-songs" type="text" name="url" id="url" value="" class="form-control"/>
                   <br />
                   <?php
                   if ($toolCap)
                   {
                   echo $captchaCode;  
                   }
                   ?>
                   <div class="text-center">
                   <a class="btn btn-info" style="cursor:pointer;" id="checkButton1">Download</a>
                   </div>
                   <br /> 
                   <div class="alert alert-warning text-center">
                    Note : It may take more than 30 seconds if the playlist is big. 
                   </div>    
                   </div>           
                   <?php 
                   } 
                   ?>
                <div id="resultBox">
                <div class="percentimg">
                <br /><br />
                <img src="<?php echo $theme_path; ?>img/load.gif" />
                <br />
                <?php echo $lang['146']; ?>...
                <br />
                </div>
                <br />
                <div class="alert alert-warning text-center" id="warnmsg">
                    Note : It may take more than 30 seconds if the playlist is big.
                </div>
                <div id="results"></div>
    
                <div class="text-center">
                <br /> &nbsp; <br />
                <a class="btn btn-info" href="<?php echo $toolURL; ?>/playlist">Download Another Playlist</a>
                <br />
                </div>
                </div>
              <?php } ?>
              </div>
              </div>          
            <br />
<div class="xd_top_box">
<?php echo $ads_720x90; ?>
</div>

<h2 id="sec1" class="about_tool"><?php echo $lang['11'].' '.$data['tool_name']; ?></h2>
<p>
<?php echo $data['about_tool']; ?>
</p> <br />
</div>              
            
<?php
// Sidebar
require_once(THEME_DIR."sidebar.php");
?>     		
        </div>
    </div> <br />